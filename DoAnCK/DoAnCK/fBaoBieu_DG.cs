﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;

namespace DoAnCK
{
    public partial class fBaoBieu_DG : Form
    {
        public fBaoBieu_DG(BaoBieu_DG rp)
        {
            InitializeComponent();
            crystalReportViewer1.ReportSource = rp;
            crystalReportViewer1.RefreshReport();
        }
    }
}
