﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using DTO;
using BLL;

namespace DoAnCK
{
	public partial class fRegistration : Form
	{
        BLL_TaiKhoan tk = new BLL_TaiKhoan();

		public fRegistration()
		{
			InitializeComponent();
			RandomCaptcha();
			StylesForm();
		}

		void RandomCaptcha()
		{
			Random rd = new Random();
			lbCaptchaCode.Text = rd.Next(0, 10000).ToString();
		}

		void StylesForm()
		{
			//Load icon từ Resources
			this.Icon = DoAnCK.Properties.Resources.registrationicon;

			//disable nút đăng nhập lúc khởi chạy
			btnSubmit.Enabled = false;

			////giới hạn ký tự cho Textbox
			txtName.MaxLength = 40;
			txtUsername.MaxLength = 20;
			txtPassword.PasswordChar = '*';
			txtPassword.MaxLength = 32;
			txtRePassword.PasswordChar = '*';
			txtRePassword.MaxLength = 32;
		}

		private bool isVailName = false;
		private bool isVailUser = false;
		private bool isVailPas = false;
		private bool isVailRePas = false;
		void isVailAllTextBox()
		{
			//Đặt khoảng cách giữa icon đến textbox
			epName.SetIconPadding(txtName, 3);
			epUser.SetIconPadding(txtUsername, 3);
			epPass.SetIconPadding(txtPassword, 3);
			epRePass.SetIconPadding(txtRePassword, 3);

			if (isVailUser == true && isVailPas == true && isVailRePas == true && isVailName == true)
			{
				btnSubmit.Enabled = true;
			}
			else
			{
				btnSubmit.Enabled = false;
			}
		}

		//Placeholder textbox
		private void txtCaptcha_Enter(object sender, EventArgs e)
		{

			if (txtCaptcha.Text == "Nhập mã xác thực")
			{
				txtCaptcha.Text = "";
				txtCaptcha.ForeColor = Color.Black;
			}
		}
		private void txtCaptcha_Leave(object sender, EventArgs e)
		{
			if (txtCaptcha.Text == "")
			{
				txtCaptcha.Text = "Nhập mã xác thực";
				txtCaptcha.ForeColor = Color.Silver;
			}
		}

		private void txtUsername_TextChanged(object sender, EventArgs e)
		{
			if (string.IsNullOrEmpty(txtUsername.Text))
			{
				epUser.SetError(txtUsername, "Tài khoản không được để trống");
				isVailUser = false;
			}
			else if (txtUsername.Text.Contains(" "))
			{
				epUser.SetError(txtUsername, "Tài khoản không được chứa dấu khoảng trắng");
				isVailUser = false;
			}
			else
			{
				epUser.SetError(txtUsername, string.Empty);
				epUser.Clear();
				isVailUser = true;
			}
			isVailAllTextBox();
		}

		private void txtPass_TextChanged(object sender, EventArgs e)
		{
			if (string.IsNullOrEmpty(txtPassword.Text))
			{
				epPass.SetError(txtPassword, "Mật khẩu không được để trống");
				isVailPas = false;
			}
			else if (txtPassword.Text.Contains(" "))
			{
				epPass.SetError(txtPassword, "Mật khẩu không được chứa dấu khoảng trắng");
				isVailPas = false;
			}
			else
			{
				epPass.SetError(txtUsername, string.Empty);
				epPass.Clear();
				isVailPas = true;
			}
			isVailAllTextBox();
		}

		private void txtRePassword_TextChanged(object sender, EventArgs e)
		{
			if (string.IsNullOrEmpty(txtRePassword.Text))
			{
				epRePass.SetError(txtRePassword, "Mật khẩu không được để trống");
				isVailRePas = false;
			}
			else if (txtRePassword.Text.Contains(" "))
			{
				epRePass.SetError(txtRePassword, "Mật khẩu không được chứa dấu khoảng trắng");
				isVailRePas = false;
			}
			else if (txtRePassword.Text != txtPassword.Text)
			{
				epRePass.SetError(txtRePassword, "Mật khẩu phải trùng nhau");
				isVailRePas = false;
			}
			else
			{
				epRePass.SetError(txtRePassword, string.Empty);
				epRePass.Clear();
				isVailRePas = true;
			}
			isVailAllTextBox();
		}

		private void txtName_TextChanged(object sender, EventArgs e)
		{
			if (string.IsNullOrWhiteSpace(txtName.Text))
			{
				epName.SetError(txtName, "Họ và Tên không được để trống");
				isVailName = false;
			}
			else
			{
				epName.SetError(txtName, string.Empty);
				epName.Clear();
				isVailName = true;
			}
			isVailAllTextBox();
		}

		private void btnSubmit_Click(object sender, EventArgs e)
		{
			if (txtCaptcha.Text != lbCaptchaCode.Text)
			{
				MessageBox.Show("Sai mã xác thực", "Lỗi", MessageBoxButtons.OK, MessageBoxIcon.Error);
				RandomCaptcha();
				txtCaptcha.Text = null;
				txtCaptcha.Focus();
			}
			else
			{
                string id = txtUsername.Text;
                string pass = txtPassword.Text;
                string hoten = txtName.Text;
                if (tk.KiemTraTK(id))
                    MessageBox.Show("Tài khoản đã tồn tại", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                else
                {
                    if (MessageBox.Show("Bạn thực sự muốn đăng ký với thông tin trên???", "Xác nhận đăng ký", MessageBoxButtons.OKCancel, MessageBoxIcon.Question) == DialogResult.OK)
                    {
                        tk.LuuTK(hoten, id, pass);
                        this.Close();
                    }
                }
			}
		}

		//Reset nội dung đã nhập
		private void btnReset_Click(object sender, EventArgs e)
		{
			txtName.Text = txtUsername.Text = txtPassword.Text = txtRePassword.Text = txtCaptcha.Text = null;
		}


		//Nhấn ESC để tắt Form
		protected override bool ProcessCmdKey(ref Message msg, Keys keyData)
		{
			if (keyData == Keys.Escape)
			{
				this.Close();
				return true;
			}
			return base.ProcessCmdKey(ref msg, keyData);
		}
	}
}
