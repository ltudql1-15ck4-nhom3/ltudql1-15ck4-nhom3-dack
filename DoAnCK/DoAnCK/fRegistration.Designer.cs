﻿namespace DoAnCK
{
	partial class fRegistration
	{
		/// <summary>
		/// Required designer variable.
		/// </summary>
		private System.ComponentModel.IContainer components = null;

		/// <summary>
		/// Clean up any resources being used.
		/// </summary>
		/// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
		protected override void Dispose(bool disposing)
		{
			if (disposing && (components != null))
			{
				components.Dispose();
			}
			base.Dispose(disposing);
		}

		#region Windows Form Designer generated code

		/// <summary>
		/// Required method for Designer support - do not modify
		/// the contents of this method with the code editor.
		/// </summary>
		private void InitializeComponent()
		{
			this.components = new System.ComponentModel.Container();
			this.lbName = new System.Windows.Forms.Label();
			this.lbPass = new System.Windows.Forms.Label();
			this.lbRePass = new System.Windows.Forms.Label();
			this.lbCaptcha = new System.Windows.Forms.Label();
			this.panel1 = new System.Windows.Forms.Panel();
			this.btnSubmit = new System.Windows.Forms.Button();
			this.lbCaptchaCode = new System.Windows.Forms.Label();
			this.txtCaptcha = new System.Windows.Forms.TextBox();
			this.txtRePassword = new System.Windows.Forms.TextBox();
			this.txtPassword = new System.Windows.Forms.TextBox();
			this.txtUsername = new System.Windows.Forms.TextBox();
			this.txtName = new System.Windows.Forms.TextBox();
			this.lbUsername = new System.Windows.Forms.Label();
			this.epUser = new System.Windows.Forms.ErrorProvider(this.components);
			this.epPass = new System.Windows.Forms.ErrorProvider(this.components);
			this.epRePass = new System.Windows.Forms.ErrorProvider(this.components);
			this.epName = new System.Windows.Forms.ErrorProvider(this.components);
			this.btnReset = new System.Windows.Forms.Button();
			this.panel1.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.epUser)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.epPass)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.epRePass)).BeginInit();
			((System.ComponentModel.ISupportInitialize)(this.epName)).BeginInit();
			this.SuspendLayout();
			// 
			// lbName
			// 
			this.lbName.AutoSize = true;
			this.lbName.Location = new System.Drawing.Point(44, 13);
			this.lbName.Name = "lbName";
			this.lbName.Size = new System.Drawing.Size(61, 13);
			this.lbName.TabIndex = 0;
			this.lbName.Text = "Họ và Tên:";
			// 
			// lbPass
			// 
			this.lbPass.AutoSize = true;
			this.lbPass.Location = new System.Drawing.Point(50, 80);
			this.lbPass.Name = "lbPass";
			this.lbPass.Size = new System.Drawing.Size(55, 13);
			this.lbPass.TabIndex = 1;
			this.lbPass.Text = "Mật khẩu:";
			// 
			// lbRePass
			// 
			this.lbRePass.AutoSize = true;
			this.lbRePass.Location = new System.Drawing.Point(9, 111);
			this.lbRePass.Name = "lbRePass";
			this.lbRePass.Size = new System.Drawing.Size(96, 13);
			this.lbRePass.TabIndex = 2;
			this.lbRePass.Text = "Nhập lại mật khẩu:";
			// 
			// lbCaptcha
			// 
			this.lbCaptcha.AutoSize = true;
			this.lbCaptcha.Location = new System.Drawing.Point(52, 141);
			this.lbCaptcha.Name = "lbCaptcha";
			this.lbCaptcha.Size = new System.Drawing.Size(53, 13);
			this.lbCaptcha.TabIndex = 3;
			this.lbCaptcha.Text = "Xác thực:";
			// 
			// panel1
			// 
			this.panel1.Controls.Add(this.btnReset);
			this.panel1.Controls.Add(this.btnSubmit);
			this.panel1.Controls.Add(this.lbCaptchaCode);
			this.panel1.Controls.Add(this.txtCaptcha);
			this.panel1.Controls.Add(this.txtRePassword);
			this.panel1.Controls.Add(this.txtPassword);
			this.panel1.Controls.Add(this.txtUsername);
			this.panel1.Controls.Add(this.txtName);
			this.panel1.Controls.Add(this.lbUsername);
			this.panel1.Controls.Add(this.lbName);
			this.panel1.Controls.Add(this.lbCaptcha);
			this.panel1.Controls.Add(this.lbPass);
			this.panel1.Controls.Add(this.lbRePass);
			this.panel1.Location = new System.Drawing.Point(12, 12);
			this.panel1.Name = "panel1";
			this.panel1.Size = new System.Drawing.Size(316, 237);
			this.panel1.TabIndex = 4;
			// 
			// btnSubmit
			// 
			this.btnSubmit.Location = new System.Drawing.Point(210, 190);
			this.btnSubmit.Name = "btnSubmit";
			this.btnSubmit.Size = new System.Drawing.Size(75, 23);
			this.btnSubmit.TabIndex = 11;
			this.btnSubmit.Text = "Xác nhận";
			this.btnSubmit.UseVisualStyleBackColor = true;
			this.btnSubmit.Click += new System.EventHandler(this.btnSubmit_Click);
			// 
			// lbCaptchaCode
			// 
			this.lbCaptchaCode.AutoSize = true;
			this.lbCaptchaCode.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(163)));
			this.lbCaptchaCode.ForeColor = System.Drawing.Color.Red;
			this.lbCaptchaCode.Location = new System.Drawing.Point(234, 138);
			this.lbCaptchaCode.Name = "lbCaptchaCode";
			this.lbCaptchaCode.Size = new System.Drawing.Size(51, 20);
			this.lbCaptchaCode.TabIndex = 10;
			this.lbCaptchaCode.Text = "Code";
			// 
			// txtCaptcha
			// 
			this.txtCaptcha.ForeColor = System.Drawing.Color.Silver;
			this.txtCaptcha.Location = new System.Drawing.Point(111, 138);
			this.txtCaptcha.Name = "txtCaptcha";
			this.txtCaptcha.Size = new System.Drawing.Size(102, 20);
			this.txtCaptcha.TabIndex = 9;
			this.txtCaptcha.Text = "Nhập mã xác thực";
			this.txtCaptcha.Enter += new System.EventHandler(this.txtCaptcha_Enter);
			this.txtCaptcha.Leave += new System.EventHandler(this.txtCaptcha_Leave);
			// 
			// txtRePassword
			// 
			this.txtRePassword.Location = new System.Drawing.Point(111, 108);
			this.txtRePassword.Name = "txtRePassword";
			this.txtRePassword.Size = new System.Drawing.Size(174, 20);
			this.txtRePassword.TabIndex = 8;
			this.txtRePassword.TextChanged += new System.EventHandler(this.txtRePassword_TextChanged);
			// 
			// txtPassword
			// 
			this.txtPassword.Location = new System.Drawing.Point(111, 77);
			this.txtPassword.Name = "txtPassword";
			this.txtPassword.Size = new System.Drawing.Size(174, 20);
			this.txtPassword.TabIndex = 7;
			this.txtPassword.TextChanged += new System.EventHandler(this.txtPass_TextChanged);
			// 
			// txtUsername
			// 
			this.txtUsername.Location = new System.Drawing.Point(111, 45);
			this.txtUsername.Name = "txtUsername";
			this.txtUsername.Size = new System.Drawing.Size(174, 20);
			this.txtUsername.TabIndex = 6;
			this.txtUsername.TextChanged += new System.EventHandler(this.txtUsername_TextChanged);
			// 
			// txtName
			// 
			this.txtName.Location = new System.Drawing.Point(111, 10);
			this.txtName.Name = "txtName";
			this.txtName.Size = new System.Drawing.Size(174, 20);
			this.txtName.TabIndex = 5;
			this.txtName.TextChanged += new System.EventHandler(this.txtName_TextChanged);
			// 
			// lbUsername
			// 
			this.lbUsername.AutoSize = true;
			this.lbUsername.Location = new System.Drawing.Point(29, 48);
			this.lbUsername.Name = "lbUsername";
			this.lbUsername.Size = new System.Drawing.Size(76, 13);
			this.lbUsername.TabIndex = 4;
			this.lbUsername.Text = "Tên tài khoản:";
			// 
			// epUser
			// 
			this.epUser.ContainerControl = this;
			// 
			// epPass
			// 
			this.epPass.ContainerControl = this;
			// 
			// epRePass
			// 
			this.epRePass.ContainerControl = this;
			// 
			// epName
			// 
			this.epName.ContainerControl = this;
			// 
			// btnReset
			// 
			this.btnReset.Location = new System.Drawing.Point(129, 190);
			this.btnReset.Name = "btnReset";
			this.btnReset.Size = new System.Drawing.Size(75, 23);
			this.btnReset.TabIndex = 12;
			this.btnReset.Text = "Nhập lại";
			this.btnReset.UseVisualStyleBackColor = true;
			this.btnReset.Click += new System.EventHandler(this.btnReset_Click);
			// 
			// fRegistration
			// 
			this.AcceptButton = this.btnSubmit;
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.ClientSize = new System.Drawing.Size(342, 261);
			this.Controls.Add(this.panel1);
			this.Name = "fRegistration";
			this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
			this.Text = "Đăng ký";
			this.panel1.ResumeLayout(false);
			this.panel1.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.epUser)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.epPass)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.epRePass)).EndInit();
			((System.ComponentModel.ISupportInitialize)(this.epName)).EndInit();
			this.ResumeLayout(false);

		}

		#endregion

		private System.Windows.Forms.Label lbName;
		private System.Windows.Forms.Label lbPass;
		private System.Windows.Forms.Label lbRePass;
		private System.Windows.Forms.Label lbCaptcha;
		private System.Windows.Forms.Panel panel1;
		private System.Windows.Forms.Label lbUsername;
		private System.Windows.Forms.TextBox txtCaptcha;
		private System.Windows.Forms.TextBox txtRePassword;
		private System.Windows.Forms.TextBox txtPassword;
		private System.Windows.Forms.TextBox txtUsername;
		private System.Windows.Forms.TextBox txtName;
		private System.Windows.Forms.Label lbCaptchaCode;
		private System.Windows.Forms.Button btnSubmit;
		private System.Windows.Forms.ErrorProvider epUser;
		private System.Windows.Forms.ErrorProvider epPass;
		private System.Windows.Forms.ErrorProvider epRePass;
		private System.Windows.Forms.ErrorProvider epName;
		private System.Windows.Forms.Button btnReset;
	}
}