﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using BLL;

namespace DoAnCK
{
    public partial class fRequest : Form
    {

        public fRequest()
        {
            InitializeComponent();
        }

        private void btnGuiYeuCau_Click(object sender, EventArgs e)
        {
            if (txtTenTL.Text.Length == 0)
            {
                MessageBox.Show("Chưa nhập tên tài liệu yêu cầu", "Information", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            else
            {
                BLL_TaiLieu tl = new BLL_TaiLieu();
                tl.themYeuCauTL(txtTenTL.Text, txtLinhVuc.Text);
                MessageBox.Show("Đã gửi yêu cầu", "Information", MessageBoxButtons.OK, MessageBoxIcon.Information);
                this.Close();
            }
        }

        private void btnHuy_Click(object sender, EventArgs e)
        {
            this.Close();
        }

    }
}
