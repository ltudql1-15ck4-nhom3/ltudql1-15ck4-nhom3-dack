﻿namespace DoAnCK
{
    partial class fMain
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
			System.ComponentModel.ComponentResourceManager resources = new System.ComponentModel.ComponentResourceManager(typeof(fMain));
			this.panel1 = new System.Windows.Forms.Panel();
			this.btnTaiLieu = new System.Windows.Forms.Button();
			this.btnTrangChu = new System.Windows.Forms.Button();
			this.btnPhieuTra = new System.Windows.Forms.Button();
			this.btnPhieuMuon = new System.Windows.Forms.Button();
			this.btnDocGia = new System.Windows.Forms.Button();
			this.panel3 = new System.Windows.Forms.Panel();
			this.label4 = new System.Windows.Forms.Label();
			this.panel4 = new System.Windows.Forms.Panel();
			this.btnDangXuat = new System.Windows.Forms.Button();
			this.panelTitle = new System.Windows.Forms.Panel();
			this.lblTitle = new System.Windows.Forms.Label();
			this.lblMinimize = new System.Windows.Forms.Label();
			this.lblClose = new System.Windows.Forms.Label();
			this.btnThoat = new System.Windows.Forms.Button();
			this.panelFooter = new System.Windows.Forms.Panel();
			this.pnlTool = new System.Windows.Forms.Panel();
			this.btnXoa = new System.Windows.Forms.Button();
			this.btnXemChiTiet = new System.Windows.Forms.Button();
			this.pnlTrangChu = new System.Windows.Forms.Panel();
			this.groupBox27 = new System.Windows.Forms.GroupBox();
			this.btnTaiKhoan = new System.Windows.Forms.Button();
			this.pictureBox2 = new System.Windows.Forms.PictureBox();
			this.groupBox26 = new System.Windows.Forms.GroupBox();
			this.btnBaoBieu = new System.Windows.Forms.Button();
			this.groupBox25 = new System.Windows.Forms.GroupBox();
			this.btnTimTL = new System.Windows.Forms.Button();
			this.groupBox24 = new System.Windows.Forms.GroupBox();
			this.btnLapPT = new System.Windows.Forms.Button();
			this.btnTimPT = new System.Windows.Forms.Button();
			this.groupBox23 = new System.Windows.Forms.GroupBox();
			this.btnLapPM = new System.Windows.Forms.Button();
			this.btnTimPM = new System.Windows.Forms.Button();
			this.groupBox22 = new System.Windows.Forms.GroupBox();
			this.btnDangKy = new System.Windows.Forms.Button();
			this.btnTimDG = new System.Windows.Forms.Button();
			this.pnlDangKyDG = new System.Windows.Forms.Panel();
			this.label8 = new System.Windows.Forms.Label();
			this.txtMscb = new System.Windows.Forms.TextBox();
			this.label7 = new System.Windows.Forms.Label();
			this.txtMssv = new System.Windows.Forms.TextBox();
			this.btnHuy = new System.Windows.Forms.Button();
			this.button1 = new System.Windows.Forms.Button();
			this.groupBox2 = new System.Windows.Forms.GroupBox();
			this.label11 = new System.Windows.Forms.Label();
			this.label10 = new System.Windows.Forms.Label();
			this.cboThang = new System.Windows.Forms.ComboBox();
			this.cboNgay = new System.Windows.Forms.ComboBox();
			this.label9 = new System.Windows.Forms.Label();
			this.label5 = new System.Windows.Forms.Label();
			this.label6 = new System.Windows.Forms.Label();
			this.txtDiaChi = new System.Windows.Forms.TextBox();
			this.label12 = new System.Windows.Forms.Label();
			this.txtSdt = new System.Windows.Forms.TextBox();
			this.label13 = new System.Windows.Forms.Label();
			this.txtNamSinh = new System.Windows.Forms.TextBox();
			this.label14 = new System.Windows.Forms.Label();
			this.txtEmail = new System.Windows.Forms.TextBox();
			this.label15 = new System.Windows.Forms.Label();
			this.txtHoTen = new System.Windows.Forms.TextBox();
			this.txtCmnd = new System.Windows.Forms.TextBox();
			this.groupBox1 = new System.Windows.Forms.GroupBox();
			this.rdoSinhVien = new System.Windows.Forms.RadioButton();
			this.rdoKhach = new System.Windows.Forms.RadioButton();
			this.rdoGiangVien = new System.Windows.Forms.RadioButton();
			this.pnlTimKiemDG = new System.Windows.Forms.Panel();
			this.btnDGPhat = new System.Windows.Forms.Button();
			this.btnTKDG_DangKyDG = new System.Windows.Forms.Button();
			this.btnLapPhieuCanhCao = new System.Windows.Forms.Button();
			this.btnTKDG_LapPT = new System.Windows.Forms.Button();
			this.btnTKDG_LapPM = new System.Windows.Forms.Button();
			this.groupBox3 = new System.Windows.Forms.GroupBox();
			this.dgvDsdg = new System.Windows.Forms.DataGridView();
			this.btnTKDG_Xemtatca = new System.Windows.Forms.Button();
			this.groupBox4 = new System.Windows.Forms.GroupBox();
			this.rdoMaso = new System.Windows.Forms.RadioButton();
			this.rdoTenDG = new System.Windows.Forms.RadioButton();
			this.rdoMaDG = new System.Windows.Forms.RadioButton();
			this.btnTKDG_Timkiem = new System.Windows.Forms.Button();
			this.txtTKDG_Maso = new System.Windows.Forms.TextBox();
			this.txtTKDG_TenDG = new System.Windows.Forms.TextBox();
			this.txtMaDG = new System.Windows.Forms.TextBox();
			this.pnlLapPM = new System.Windows.Forms.Panel();
			this.groupBox15 = new System.Windows.Forms.GroupBox();
			this.dgvTaiLieuMuon = new System.Windows.Forms.DataGridView();
			this.btnLPM_LapPhieu = new System.Windows.Forms.Button();
			this.groupBox5 = new System.Windows.Forms.GroupBox();
			this.ltbDocGia = new System.Windows.Forms.ListBox();
			this.pnlTimKiemPM = new System.Windows.Forms.Panel();
			this.btnTKPM_XemPMDT = new System.Windows.Forms.Button();
			this.btnTKPM_LapPT = new System.Windows.Forms.Button();
			this.groupBox6 = new System.Windows.Forms.GroupBox();
			this.dgvDSPM = new System.Windows.Forms.DataGridView();
			this.btnTKPM_LapPM = new System.Windows.Forms.Button();
			this.btnTKPM_XemPMQH = new System.Windows.Forms.Button();
			this.btnTKPM_Xemtatca = new System.Windows.Forms.Button();
			this.groupBox7 = new System.Windows.Forms.GroupBox();
			this.rdoTKPM_MaDG = new System.Windows.Forms.RadioButton();
			this.rdoTKPM_MaPM = new System.Windows.Forms.RadioButton();
			this.btnTKPM_Timkiem = new System.Windows.Forms.Button();
			this.txtTKPM_MaDG = new System.Windows.Forms.TextBox();
			this.txtTKPM_MaPM = new System.Windows.Forms.TextBox();
			this.pnlLapPT = new System.Windows.Forms.Panel();
			this.groupBox17 = new System.Windows.Forms.GroupBox();
			this.dgvTaiLieuDaMuon = new System.Windows.Forms.DataGridView();
			this.btnLPT_LapPhieu = new System.Windows.Forms.Button();
			this.groupBox8 = new System.Windows.Forms.GroupBox();
			this.ltbPhieuMuon = new System.Windows.Forms.ListBox();
			this.pnlTimKiemPT = new System.Windows.Forms.Panel();
			this.btnTKPT_LapPT = new System.Windows.Forms.Button();
			this.groupBox9 = new System.Windows.Forms.GroupBox();
			this.dgvDspt = new System.Windows.Forms.DataGridView();
			this.btnTKPT_Xemtatca = new System.Windows.Forms.Button();
			this.groupBox10 = new System.Windows.Forms.GroupBox();
			this.rdoTKPT_MaDG = new System.Windows.Forms.RadioButton();
			this.rdoTKPT_MaPM = new System.Windows.Forms.RadioButton();
			this.rdoTKPT_MaPT = new System.Windows.Forms.RadioButton();
			this.txtTKPT_MaPT = new System.Windows.Forms.TextBox();
			this.btnTKPT_TimKiem = new System.Windows.Forms.Button();
			this.txtTKPT_MaDG = new System.Windows.Forms.TextBox();
			this.txtTKPT_MaPM = new System.Windows.Forms.TextBox();
			this.pnlTimKiemTL = new System.Windows.Forms.Panel();
			this.btnYeuCauThemTL = new System.Windows.Forms.Button();
			this.btnTKTL_LapPM = new System.Windows.Forms.Button();
			this.groupBox11 = new System.Windows.Forms.GroupBox();
			this.dgvDstl = new System.Windows.Forms.DataGridView();
			this.btnTKTL_Xemtatca = new System.Windows.Forms.Button();
			this.groupBox12 = new System.Windows.Forms.GroupBox();
			this.pnlLoaiTimKiem = new System.Windows.Forms.Panel();
			this.cboLinhVuc = new System.Windows.Forms.ComboBox();
			this.rdoLinhVuc = new System.Windows.Forms.RadioButton();
			this.rdoLoaiTL = new System.Windows.Forms.RadioButton();
			this.rdoTKTL_TenTL = new System.Windows.Forms.RadioButton();
			this.rdoTKTL_MaTL = new System.Windows.Forms.RadioButton();
			this.btnLoaiTimKiem = new System.Windows.Forms.Button();
			this.btnTKTL_TimKiem = new System.Windows.Forms.Button();
			this.txtTKTL_TenTL = new System.Windows.Forms.TextBox();
			this.txtTKTL_MaTL = new System.Windows.Forms.TextBox();
			this.pnlChinhSua = new System.Windows.Forms.Panel();
			this.btnChinhSua = new System.Windows.Forms.Button();
			this.pnlEdit = new System.Windows.Forms.Panel();
			this.btnLuu = new System.Windows.Forms.Button();
			this.btnHuyChinhSua = new System.Windows.Forms.Button();
			this.pnlChiTietDG = new System.Windows.Forms.Panel();
			this.groupBox13 = new System.Windows.Forms.GroupBox();
			this.dgvChiTietDG = new System.Windows.Forms.DataGridView();
			this.pnlChiTietTL = new System.Windows.Forms.Panel();
			this.groupBox14 = new System.Windows.Forms.GroupBox();
			this.dgvChiTietTL = new System.Windows.Forms.DataGridView();
			this.pnlChiTietPM = new System.Windows.Forms.Panel();
			this.groupBox16 = new System.Windows.Forms.GroupBox();
			this.dgvChiTietPM = new System.Windows.Forms.DataGridView();
			this.pnlChiTietPT = new System.Windows.Forms.Panel();
			this.groupBox18 = new System.Windows.Forms.GroupBox();
			this.dgvChiTietPT = new System.Windows.Forms.DataGridView();
			this.pnlBaoBieu = new System.Windows.Forms.Panel();
			this.groupBox19 = new System.Windows.Forms.GroupBox();
			this.cboDSTL_Nam = new System.Windows.Forms.ComboBox();
			this.cboDSDG_Nam = new System.Windows.Forms.ComboBox();
			this.cboDSTL_TenDG = new System.Windows.Forms.ComboBox();
			this.cboDSTL_Thang = new System.Windows.Forms.ComboBox();
			this.cboDSDG_Thang = new System.Windows.Forms.ComboBox();
			this.btnLapBaoBieu = new System.Windows.Forms.Button();
			this.rdoDSTLMaDG = new System.Windows.Forms.RadioButton();
			this.rdoDSTLThang = new System.Windows.Forms.RadioButton();
			this.rdoDSDGThang = new System.Windows.Forms.RadioButton();
			this.pnlTaiKhoan = new System.Windows.Forms.Panel();
			this.pnlAdmin = new System.Windows.Forms.Panel();
			this.pnlThemTL = new System.Windows.Forms.Panel();
			this.groupBox21 = new System.Windows.Forms.GroupBox();
			this.btnThemTL = new System.Windows.Forms.Button();
			this.cboThemLinhVuc = new System.Windows.Forms.ComboBox();
			this.cboLoaiTL = new System.Windows.Forms.ComboBox();
			this.cboMaNguonTL = new System.Windows.Forms.ComboBox();
			this.cboTenTL = new System.Windows.Forms.ComboBox();
			this.label25 = new System.Windows.Forms.Label();
			this.label24 = new System.Windows.Forms.Label();
			this.label23 = new System.Windows.Forms.Label();
			this.label22 = new System.Windows.Forms.Label();
			this.dgvThemTL = new System.Windows.Forms.DataGridView();
			this.gbxThemLDG = new System.Windows.Forms.GroupBox();
			this.label17 = new System.Windows.Forms.Label();
			this.txtThemLDG_SoNgayToiDa = new System.Windows.Forms.TextBox();
			this.label16 = new System.Windows.Forms.Label();
			this.txtThemLDG_PhiThuongNien = new System.Windows.Forms.TextBox();
			this.label3 = new System.Windows.Forms.Label();
			this.txtThemLDG_SoSachToiDa = new System.Windows.Forms.TextBox();
			this.label2 = new System.Windows.Forms.Label();
			this.txtThemLDG_LoaiDG = new System.Windows.Forms.TextBox();
			this.btnAdmin_ThemLoaiDG = new System.Windows.Forms.Button();
			this.gbxDoiQD = new System.Windows.Forms.GroupBox();
			this.cboDoiQD_LoaiDG = new System.Windows.Forms.ComboBox();
			this.label18 = new System.Windows.Forms.Label();
			this.txtDoiQD_SoNgayToiDa = new System.Windows.Forms.TextBox();
			this.label19 = new System.Windows.Forms.Label();
			this.txtDoiQD_PhiThuongNien = new System.Windows.Forms.TextBox();
			this.label20 = new System.Windows.Forms.Label();
			this.txtDoiQD_SoSachToiDa = new System.Windows.Forms.TextBox();
			this.label21 = new System.Windows.Forms.Label();
			this.btnAdmin_ThayDoiQD = new System.Windows.Forms.Button();
			this.btnAdmin_ThemTL = new System.Windows.Forms.Button();
			this.btnAdmin_TaiKhoan = new System.Windows.Forms.Button();
			this.btnAdmin = new System.Windows.Forms.Button();
			this.gbxMatKhau = new System.Windows.Forms.GroupBox();
			this.label1 = new System.Windows.Forms.Label();
			this.lblMKMoi = new System.Windows.Forms.Label();
			this.btnMK_Doi = new System.Windows.Forms.Button();
			this.txtXacNhanMK = new System.Windows.Forms.TextBox();
			this.txtMKMoi = new System.Windows.Forms.TextBox();
			this.gbxThongTin = new System.Windows.Forms.GroupBox();
			this.lblXacNhanHoTen = new System.Windows.Forms.Label();
			this.lblHoTenMoi = new System.Windows.Forms.Label();
			this.txtXacNhanHoTen = new System.Windows.Forms.TextBox();
			this.txtHoTenMoi = new System.Windows.Forms.TextBox();
			this.btnHoTen_Doi = new System.Windows.Forms.Button();
			this.btnDoiMK = new System.Windows.Forms.Button();
			this.btnDoiHoTen = new System.Windows.Forms.Button();
			this.groupBox20 = new System.Windows.Forms.GroupBox();
			this.lblTenTK = new System.Windows.Forms.Label();
			this.lblHoTen = new System.Windows.Forms.Label();
			this.pictureBox1 = new System.Windows.Forms.PictureBox();
			this.cboTKTL_LoaiTL = new System.Windows.Forms.ComboBox();
			this.panel1.SuspendLayout();
			this.panel3.SuspendLayout();
			this.panel4.SuspendLayout();
			this.panelTitle.SuspendLayout();
			this.panelFooter.SuspendLayout();
			this.pnlTool.SuspendLayout();
			this.pnlTrangChu.SuspendLayout();
			this.groupBox27.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).BeginInit();
			this.groupBox26.SuspendLayout();
			this.groupBox25.SuspendLayout();
			this.groupBox24.SuspendLayout();
			this.groupBox23.SuspendLayout();
			this.groupBox22.SuspendLayout();
			this.pnlDangKyDG.SuspendLayout();
			this.groupBox2.SuspendLayout();
			this.groupBox1.SuspendLayout();
			this.pnlTimKiemDG.SuspendLayout();
			this.groupBox3.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.dgvDsdg)).BeginInit();
			this.groupBox4.SuspendLayout();
			this.pnlLapPM.SuspendLayout();
			this.groupBox15.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.dgvTaiLieuMuon)).BeginInit();
			this.groupBox5.SuspendLayout();
			this.pnlTimKiemPM.SuspendLayout();
			this.groupBox6.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.dgvDSPM)).BeginInit();
			this.groupBox7.SuspendLayout();
			this.pnlLapPT.SuspendLayout();
			this.groupBox17.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.dgvTaiLieuDaMuon)).BeginInit();
			this.groupBox8.SuspendLayout();
			this.pnlTimKiemPT.SuspendLayout();
			this.groupBox9.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.dgvDspt)).BeginInit();
			this.groupBox10.SuspendLayout();
			this.pnlTimKiemTL.SuspendLayout();
			this.groupBox11.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.dgvDstl)).BeginInit();
			this.groupBox12.SuspendLayout();
			this.pnlLoaiTimKiem.SuspendLayout();
			this.pnlChinhSua.SuspendLayout();
			this.pnlEdit.SuspendLayout();
			this.pnlChiTietDG.SuspendLayout();
			this.groupBox13.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.dgvChiTietDG)).BeginInit();
			this.pnlChiTietTL.SuspendLayout();
			this.groupBox14.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.dgvChiTietTL)).BeginInit();
			this.pnlChiTietPM.SuspendLayout();
			this.groupBox16.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.dgvChiTietPM)).BeginInit();
			this.pnlChiTietPT.SuspendLayout();
			this.groupBox18.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.dgvChiTietPT)).BeginInit();
			this.pnlBaoBieu.SuspendLayout();
			this.groupBox19.SuspendLayout();
			this.pnlTaiKhoan.SuspendLayout();
			this.pnlAdmin.SuspendLayout();
			this.pnlThemTL.SuspendLayout();
			this.groupBox21.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.dgvThemTL)).BeginInit();
			this.gbxThemLDG.SuspendLayout();
			this.gbxDoiQD.SuspendLayout();
			this.gbxMatKhau.SuspendLayout();
			this.gbxThongTin.SuspendLayout();
			this.groupBox20.SuspendLayout();
			((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
			this.SuspendLayout();
			// 
			// panel1
			// 
			this.panel1.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
			this.panel1.Controls.Add(this.btnTaiLieu);
			this.panel1.Controls.Add(this.btnTrangChu);
			this.panel1.Controls.Add(this.btnPhieuTra);
			this.panel1.Controls.Add(this.btnPhieuMuon);
			this.panel1.Controls.Add(this.btnDocGia);
			this.panel1.Controls.Add(this.panel3);
			this.panel1.Location = new System.Drawing.Point(0, 0);
			this.panel1.Name = "panel1";
			this.panel1.Size = new System.Drawing.Size(164, 456);
			this.panel1.TabIndex = 0;
			// 
			// btnTaiLieu
			// 
			this.btnTaiLieu.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
			this.btnTaiLieu.FlatAppearance.BorderSize = 0;
			this.btnTaiLieu.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
			this.btnTaiLieu.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.btnTaiLieu.ForeColor = System.Drawing.Color.White;
			this.btnTaiLieu.Image = global::DoAnCK.Properties.Resources.books_stack_of_three;
			this.btnTaiLieu.Location = new System.Drawing.Point(0, 347);
			this.btnTaiLieu.Name = "btnTaiLieu";
			this.btnTaiLieu.Size = new System.Drawing.Size(164, 67);
			this.btnTaiLieu.TabIndex = 6;
			this.btnTaiLieu.Text = "Tài liệu";
			this.btnTaiLieu.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
			this.btnTaiLieu.UseVisualStyleBackColor = false;
			this.btnTaiLieu.Click += new System.EventHandler(this.btnTaiLieu_Click);
			// 
			// btnTrangChu
			// 
			this.btnTrangChu.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
			this.btnTrangChu.FlatAppearance.BorderSize = 0;
			this.btnTrangChu.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
			this.btnTrangChu.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.btnTrangChu.ForeColor = System.Drawing.Color.White;
			this.btnTrangChu.Image = ((System.Drawing.Image)(resources.GetObject("btnTrangChu.Image")));
			this.btnTrangChu.Location = new System.Drawing.Point(0, 55);
			this.btnTrangChu.Name = "btnTrangChu";
			this.btnTrangChu.Size = new System.Drawing.Size(164, 67);
			this.btnTrangChu.TabIndex = 4;
			this.btnTrangChu.Text = "Trang chủ";
			this.btnTrangChu.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
			this.btnTrangChu.UseVisualStyleBackColor = false;
			this.btnTrangChu.Click += new System.EventHandler(this.btnTrangChu_Click);
			// 
			// btnPhieuTra
			// 
			this.btnPhieuTra.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
			this.btnPhieuTra.FlatAppearance.BorderSize = 0;
			this.btnPhieuTra.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
			this.btnPhieuTra.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.btnPhieuTra.ForeColor = System.Drawing.Color.White;
			this.btnPhieuTra.Image = ((System.Drawing.Image)(resources.GetObject("btnPhieuTra.Image")));
			this.btnPhieuTra.Location = new System.Drawing.Point(0, 274);
			this.btnPhieuTra.Name = "btnPhieuTra";
			this.btnPhieuTra.Size = new System.Drawing.Size(164, 67);
			this.btnPhieuTra.TabIndex = 5;
			this.btnPhieuTra.Text = "Phiếu trả";
			this.btnPhieuTra.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
			this.btnPhieuTra.UseVisualStyleBackColor = false;
			this.btnPhieuTra.Click += new System.EventHandler(this.btnPhieuTra_Click);
			// 
			// btnPhieuMuon
			// 
			this.btnPhieuMuon.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
			this.btnPhieuMuon.FlatAppearance.BorderSize = 0;
			this.btnPhieuMuon.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
			this.btnPhieuMuon.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.btnPhieuMuon.ForeColor = System.Drawing.Color.White;
			this.btnPhieuMuon.Image = ((System.Drawing.Image)(resources.GetObject("btnPhieuMuon.Image")));
			this.btnPhieuMuon.Location = new System.Drawing.Point(0, 201);
			this.btnPhieuMuon.Name = "btnPhieuMuon";
			this.btnPhieuMuon.Size = new System.Drawing.Size(164, 67);
			this.btnPhieuMuon.TabIndex = 4;
			this.btnPhieuMuon.Text = "Phiếu mượn";
			this.btnPhieuMuon.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
			this.btnPhieuMuon.UseVisualStyleBackColor = false;
			this.btnPhieuMuon.Click += new System.EventHandler(this.btnPhieuMuon_Click);
			// 
			// btnDocGia
			// 
			this.btnDocGia.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(0)))), ((int)(((byte)(64)))), ((int)(((byte)(64)))));
			this.btnDocGia.FlatAppearance.BorderSize = 0;
			this.btnDocGia.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
			this.btnDocGia.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.btnDocGia.ForeColor = System.Drawing.Color.White;
			this.btnDocGia.Image = global::DoAnCK.Properties.Resources.man_user;
			this.btnDocGia.Location = new System.Drawing.Point(0, 128);
			this.btnDocGia.Name = "btnDocGia";
			this.btnDocGia.Size = new System.Drawing.Size(164, 67);
			this.btnDocGia.TabIndex = 3;
			this.btnDocGia.Text = "Độc giả";
			this.btnDocGia.TextAlign = System.Drawing.ContentAlignment.BottomCenter;
			this.btnDocGia.UseVisualStyleBackColor = false;
			this.btnDocGia.Click += new System.EventHandler(this.btnDocGia_Click);
			// 
			// panel3
			// 
			this.panel3.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(255)))), ((int)(((byte)(192)))));
			this.panel3.Controls.Add(this.label4);
			this.panel3.Dock = System.Windows.Forms.DockStyle.Top;
			this.panel3.Location = new System.Drawing.Point(0, 0);
			this.panel3.Name = "panel3";
			this.panel3.Size = new System.Drawing.Size(164, 52);
			this.panel3.TabIndex = 0;
			// 
			// label4
			// 
			this.label4.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.label4.Location = new System.Drawing.Point(33, 5);
			this.label4.Name = "label4";
			this.label4.Size = new System.Drawing.Size(96, 47);
			this.label4.TabIndex = 0;
			this.label4.Text = "QUẢN LÝ THƯ VIỆN";
			// 
			// panel4
			// 
			this.panel4.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(0)))), ((int)(((byte)(64)))));
			this.panel4.Controls.Add(this.btnDangXuat);
			this.panel4.Location = new System.Drawing.Point(0, 454);
			this.panel4.Name = "panel4";
			this.panel4.Size = new System.Drawing.Size(164, 54);
			this.panel4.TabIndex = 2;
			// 
			// btnDangXuat
			// 
			this.btnDangXuat.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(128)))));
			this.btnDangXuat.FlatAppearance.BorderColor = System.Drawing.Color.Black;
			this.btnDangXuat.FlatAppearance.BorderSize = 2;
			this.btnDangXuat.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
			this.btnDangXuat.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.btnDangXuat.ForeColor = System.Drawing.Color.Black;
			this.btnDangXuat.Location = new System.Drawing.Point(37, 11);
			this.btnDangXuat.Name = "btnDangXuat";
			this.btnDangXuat.Size = new System.Drawing.Size(89, 31);
			this.btnDangXuat.TabIndex = 21;
			this.btnDangXuat.Text = "Đăng xuất";
			this.btnDangXuat.UseVisualStyleBackColor = false;
			this.btnDangXuat.Click += new System.EventHandler(this.btnDangXuat_Click);
			// 
			// panelTitle
			// 
			this.panelTitle.BackColor = System.Drawing.Color.White;
			this.panelTitle.Controls.Add(this.lblTitle);
			this.panelTitle.Controls.Add(this.lblMinimize);
			this.panelTitle.Controls.Add(this.lblClose);
			this.panelTitle.Location = new System.Drawing.Point(164, 0);
			this.panelTitle.Name = "panelTitle";
			this.panelTitle.Size = new System.Drawing.Size(694, 52);
			this.panelTitle.TabIndex = 1;
			this.panelTitle.MouseMove += new System.Windows.Forms.MouseEventHandler(this.pnlTitle_MouseMove_1);
			// 
			// lblTitle
			// 
			this.lblTitle.AutoSize = true;
			this.lblTitle.Font = new System.Drawing.Font("Microsoft Sans Serif", 15.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.lblTitle.ForeColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(0)))), ((int)(((byte)(0)))));
			this.lblTitle.Location = new System.Drawing.Point(279, 12);
			this.lblTitle.Name = "lblTitle";
			this.lblTitle.Size = new System.Drawing.Size(109, 25);
			this.lblTitle.TabIndex = 2;
			this.lblTitle.Text = "Trang chủ";
			// 
			// lblMinimize
			// 
			this.lblMinimize.AutoSize = true;
			this.lblMinimize.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.lblMinimize.Location = new System.Drawing.Point(651, 0);
			this.lblMinimize.Name = "lblMinimize";
			this.lblMinimize.Size = new System.Drawing.Size(13, 17);
			this.lblMinimize.TabIndex = 1;
			this.lblMinimize.Text = "-";
			this.lblMinimize.Click += new System.EventHandler(this.lblMinimize_Click);
			// 
			// lblClose
			// 
			this.lblClose.AutoSize = true;
			this.lblClose.Font = new System.Drawing.Font("Microsoft Sans Serif", 10F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.lblClose.Location = new System.Drawing.Point(674, 0);
			this.lblClose.Name = "lblClose";
			this.lblClose.Size = new System.Drawing.Size(17, 17);
			this.lblClose.TabIndex = 0;
			this.lblClose.Text = "X";
			this.lblClose.Click += new System.EventHandler(this.lblClose_Click);
			// 
			// btnThoat
			// 
			this.btnThoat.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(128)))));
			this.btnThoat.FlatAppearance.BorderColor = System.Drawing.Color.Black;
			this.btnThoat.FlatAppearance.BorderSize = 2;
			this.btnThoat.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
			this.btnThoat.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.btnThoat.ForeColor = System.Drawing.Color.Black;
			this.btnThoat.Location = new System.Drawing.Point(604, 8);
			this.btnThoat.Name = "btnThoat";
			this.btnThoat.Size = new System.Drawing.Size(78, 34);
			this.btnThoat.TabIndex = 15;
			this.btnThoat.Text = "Thoát";
			this.btnThoat.UseVisualStyleBackColor = false;
			this.btnThoat.Click += new System.EventHandler(this.btnThoat_Click);
			// 
			// panelFooter
			// 
			this.panelFooter.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(0)))), ((int)(((byte)(64)))));
			this.panelFooter.Controls.Add(this.pnlTool);
			this.panelFooter.Controls.Add(this.btnThoat);
			this.panelFooter.Location = new System.Drawing.Point(164, 454);
			this.panelFooter.Name = "panelFooter";
			this.panelFooter.Size = new System.Drawing.Size(694, 54);
			this.panelFooter.TabIndex = 18;
			// 
			// pnlTool
			// 
			this.pnlTool.Controls.Add(this.btnXoa);
			this.pnlTool.Controls.Add(this.btnXemChiTiet);
			this.pnlTool.Dock = System.Windows.Forms.DockStyle.Left;
			this.pnlTool.Location = new System.Drawing.Point(0, 0);
			this.pnlTool.Name = "pnlTool";
			this.pnlTool.Size = new System.Drawing.Size(359, 54);
			this.pnlTool.TabIndex = 16;
			this.pnlTool.Visible = false;
			// 
			// btnXoa
			// 
			this.btnXoa.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(128)))));
			this.btnXoa.FlatAppearance.BorderColor = System.Drawing.Color.Black;
			this.btnXoa.FlatAppearance.BorderSize = 2;
			this.btnXoa.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
			this.btnXoa.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.btnXoa.ForeColor = System.Drawing.Color.Black;
			this.btnXoa.Location = new System.Drawing.Point(239, 8);
			this.btnXoa.Name = "btnXoa";
			this.btnXoa.Size = new System.Drawing.Size(78, 34);
			this.btnXoa.TabIndex = 18;
			this.btnXoa.Text = "Xóa";
			this.btnXoa.UseVisualStyleBackColor = false;
			this.btnXoa.Click += new System.EventHandler(this.btnXoa_Click);
			// 
			// btnXemChiTiet
			// 
			this.btnXemChiTiet.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(128)))));
			this.btnXemChiTiet.FlatAppearance.BorderColor = System.Drawing.Color.Black;
			this.btnXemChiTiet.FlatAppearance.BorderSize = 2;
			this.btnXemChiTiet.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
			this.btnXemChiTiet.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.btnXemChiTiet.ForeColor = System.Drawing.Color.Black;
			this.btnXemChiTiet.Location = new System.Drawing.Point(12, 8);
			this.btnXemChiTiet.Name = "btnXemChiTiet";
			this.btnXemChiTiet.Size = new System.Drawing.Size(158, 34);
			this.btnXemChiTiet.TabIndex = 17;
			this.btnXemChiTiet.Text = "Xem chi tiết";
			this.btnXemChiTiet.UseVisualStyleBackColor = false;
			this.btnXemChiTiet.Click += new System.EventHandler(this.btnXemChiTiet_Click);
			// 
			// pnlTrangChu
			// 
			this.pnlTrangChu.BackColor = System.Drawing.Color.Silver;
			this.pnlTrangChu.Controls.Add(this.groupBox27);
			this.pnlTrangChu.Controls.Add(this.pictureBox2);
			this.pnlTrangChu.Controls.Add(this.groupBox26);
			this.pnlTrangChu.Controls.Add(this.groupBox25);
			this.pnlTrangChu.Controls.Add(this.groupBox24);
			this.pnlTrangChu.Controls.Add(this.groupBox23);
			this.pnlTrangChu.Controls.Add(this.groupBox22);
			this.pnlTrangChu.Location = new System.Drawing.Point(164, 53);
			this.pnlTrangChu.Name = "pnlTrangChu";
			this.pnlTrangChu.Size = new System.Drawing.Size(694, 401);
			this.pnlTrangChu.TabIndex = 19;
			// 
			// groupBox27
			// 
			this.groupBox27.Controls.Add(this.btnTaiKhoan);
			this.groupBox27.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.groupBox27.Location = new System.Drawing.Point(237, 118);
			this.groupBox27.Name = "groupBox27";
			this.groupBox27.Size = new System.Drawing.Size(217, 120);
			this.groupBox27.TabIndex = 33;
			this.groupBox27.TabStop = false;
			this.groupBox27.Text = "Tài khoản";
			// 
			// btnTaiKhoan
			// 
			this.btnTaiKhoan.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
			this.btnTaiKhoan.FlatAppearance.BorderSize = 2;
			this.btnTaiKhoan.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
			this.btnTaiKhoan.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.btnTaiKhoan.ImageAlign = System.Drawing.ContentAlignment.MiddleLeft;
			this.btnTaiKhoan.Location = new System.Drawing.Point(18, 24);
			this.btnTaiKhoan.Name = "btnTaiKhoan";
			this.btnTaiKhoan.Size = new System.Drawing.Size(182, 81);
			this.btnTaiKhoan.TabIndex = 26;
			this.btnTaiKhoan.Text = "Quản lý tài khoản";
			this.btnTaiKhoan.UseVisualStyleBackColor = false;
			this.btnTaiKhoan.Click += new System.EventHandler(this.btnTaiKhoan_Click);
			// 
			// pictureBox2
			// 
			this.pictureBox2.Image = global::DoAnCK.Properties.Resources.home_2;
			this.pictureBox2.Location = new System.Drawing.Point(269, 17);
			this.pictureBox2.Name = "pictureBox2";
			this.pictureBox2.Size = new System.Drawing.Size(155, 92);
			this.pictureBox2.SizeMode = System.Windows.Forms.PictureBoxSizeMode.Zoom;
			this.pictureBox2.TabIndex = 32;
			this.pictureBox2.TabStop = false;
			// 
			// groupBox26
			// 
			this.groupBox26.Controls.Add(this.btnBaoBieu);
			this.groupBox26.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.groupBox26.Location = new System.Drawing.Point(488, 125);
			this.groupBox26.Name = "groupBox26";
			this.groupBox26.Size = new System.Drawing.Size(182, 113);
			this.groupBox26.TabIndex = 31;
			this.groupBox26.TabStop = false;
			this.groupBox26.Text = "Báo biểu";
			// 
			// btnBaoBieu
			// 
			this.btnBaoBieu.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
			this.btnBaoBieu.FlatAppearance.BorderSize = 2;
			this.btnBaoBieu.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
			this.btnBaoBieu.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.btnBaoBieu.Location = new System.Drawing.Point(26, 25);
			this.btnBaoBieu.Name = "btnBaoBieu";
			this.btnBaoBieu.Size = new System.Drawing.Size(133, 73);
			this.btnBaoBieu.TabIndex = 25;
			this.btnBaoBieu.Text = "Lập báo biểu";
			this.btnBaoBieu.UseVisualStyleBackColor = false;
			this.btnBaoBieu.Click += new System.EventHandler(this.btnBaoBieu_Click);
			// 
			// groupBox25
			// 
			this.groupBox25.Controls.Add(this.btnTimTL);
			this.groupBox25.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.groupBox25.Location = new System.Drawing.Point(488, 8);
			this.groupBox25.Name = "groupBox25";
			this.groupBox25.Size = new System.Drawing.Size(182, 113);
			this.groupBox25.TabIndex = 30;
			this.groupBox25.TabStop = false;
			this.groupBox25.Text = "Tài liệu";
			// 
			// btnTimTL
			// 
			this.btnTimTL.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
			this.btnTimTL.FlatAppearance.BorderSize = 2;
			this.btnTimTL.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
			this.btnTimTL.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.btnTimTL.Location = new System.Drawing.Point(26, 26);
			this.btnTimTL.Name = "btnTimTL";
			this.btnTimTL.Size = new System.Drawing.Size(133, 73);
			this.btnTimTL.TabIndex = 20;
			this.btnTimTL.Text = "Tìm kiếm tài liệu";
			this.btnTimTL.UseVisualStyleBackColor = false;
			this.btnTimTL.Click += new System.EventHandler(this.btnTimTL_Click);
			// 
			// groupBox24
			// 
			this.groupBox24.Controls.Add(this.btnLapPT);
			this.groupBox24.Controls.Add(this.btnTimPT);
			this.groupBox24.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.groupBox24.Location = new System.Drawing.Point(369, 255);
			this.groupBox24.Name = "groupBox24";
			this.groupBox24.Size = new System.Drawing.Size(308, 131);
			this.groupBox24.TabIndex = 29;
			this.groupBox24.TabStop = false;
			this.groupBox24.Text = "Phiếu trả";
			// 
			// btnLapPT
			// 
			this.btnLapPT.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
			this.btnLapPT.FlatAppearance.BorderSize = 2;
			this.btnLapPT.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
			this.btnLapPT.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.btnLapPT.Location = new System.Drawing.Point(23, 30);
			this.btnLapPT.Name = "btnLapPT";
			this.btnLapPT.Size = new System.Drawing.Size(268, 34);
			this.btnLapPT.TabIndex = 22;
			this.btnLapPT.Text = "Lập phiếu trả";
			this.btnLapPT.UseVisualStyleBackColor = false;
			this.btnLapPT.Click += new System.EventHandler(this.btnLapPT_Click);
			// 
			// btnTimPT
			// 
			this.btnTimPT.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
			this.btnTimPT.FlatAppearance.BorderSize = 2;
			this.btnTimPT.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
			this.btnTimPT.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.btnTimPT.Location = new System.Drawing.Point(23, 78);
			this.btnTimPT.Name = "btnTimPT";
			this.btnTimPT.Size = new System.Drawing.Size(268, 34);
			this.btnTimPT.TabIndex = 24;
			this.btnTimPT.Text = "Tìm kiếm phiếu trả";
			this.btnTimPT.UseVisualStyleBackColor = false;
			this.btnTimPT.Click += new System.EventHandler(this.btnTimPT_Click);
			// 
			// groupBox23
			// 
			this.groupBox23.Controls.Add(this.btnLapPM);
			this.groupBox23.Controls.Add(this.btnTimPM);
			this.groupBox23.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.groupBox23.Location = new System.Drawing.Point(12, 255);
			this.groupBox23.Name = "groupBox23";
			this.groupBox23.Size = new System.Drawing.Size(308, 131);
			this.groupBox23.TabIndex = 28;
			this.groupBox23.TabStop = false;
			this.groupBox23.Text = "Phiếu mượn";
			// 
			// btnLapPM
			// 
			this.btnLapPM.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
			this.btnLapPM.FlatAppearance.BorderSize = 2;
			this.btnLapPM.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
			this.btnLapPM.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.btnLapPM.Location = new System.Drawing.Point(21, 30);
			this.btnLapPM.Name = "btnLapPM";
			this.btnLapPM.Size = new System.Drawing.Size(268, 34);
			this.btnLapPM.TabIndex = 21;
			this.btnLapPM.Text = "Lập phiếu mượn";
			this.btnLapPM.UseVisualStyleBackColor = false;
			this.btnLapPM.Click += new System.EventHandler(this.btnLapPM_Click);
			// 
			// btnTimPM
			// 
			this.btnTimPM.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
			this.btnTimPM.FlatAppearance.BorderSize = 2;
			this.btnTimPM.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
			this.btnTimPM.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.btnTimPM.Location = new System.Drawing.Point(21, 76);
			this.btnTimPM.Name = "btnTimPM";
			this.btnTimPM.Size = new System.Drawing.Size(268, 34);
			this.btnTimPM.TabIndex = 23;
			this.btnTimPM.Text = "Tìm kiếm phiếu mượn";
			this.btnTimPM.UseVisualStyleBackColor = false;
			this.btnTimPM.Click += new System.EventHandler(this.btnTimPM_Click);
			// 
			// groupBox22
			// 
			this.groupBox22.Controls.Add(this.btnDangKy);
			this.groupBox22.Controls.Add(this.btnTimDG);
			this.groupBox22.Font = new System.Drawing.Font("Microsoft Sans Serif", 12F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.groupBox22.Location = new System.Drawing.Point(12, 8);
			this.groupBox22.Name = "groupBox22";
			this.groupBox22.Size = new System.Drawing.Size(182, 230);
			this.groupBox22.TabIndex = 27;
			this.groupBox22.TabStop = false;
			this.groupBox22.Text = "Độc giả";
			// 
			// btnDangKy
			// 
			this.btnDangKy.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
			this.btnDangKy.FlatAppearance.BorderSize = 2;
			this.btnDangKy.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
			this.btnDangKy.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.btnDangKy.Location = new System.Drawing.Point(23, 31);
			this.btnDangKy.Name = "btnDangKy";
			this.btnDangKy.Size = new System.Drawing.Size(133, 81);
			this.btnDangKy.TabIndex = 18;
			this.btnDangKy.Text = "Đăng ký độc giả";
			this.btnDangKy.UseVisualStyleBackColor = false;
			this.btnDangKy.Click += new System.EventHandler(this.btnDangKy_Click);
			// 
			// btnTimDG
			// 
			this.btnTimDG.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(128)))), ((int)(((byte)(255)))), ((int)(((byte)(255)))));
			this.btnTimDG.FlatAppearance.BorderSize = 2;
			this.btnTimDG.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
			this.btnTimDG.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.btnTimDG.ForeColor = System.Drawing.SystemColors.ControlText;
			this.btnTimDG.Location = new System.Drawing.Point(24, 135);
			this.btnTimDG.Name = "btnTimDG";
			this.btnTimDG.Size = new System.Drawing.Size(133, 81);
			this.btnTimDG.TabIndex = 19;
			this.btnTimDG.Text = "Tìm kiếm độc giả";
			this.btnTimDG.UseVisualStyleBackColor = false;
			this.btnTimDG.Click += new System.EventHandler(this.btnTimDG_Click);
			// 
			// pnlDangKyDG
			// 
			this.pnlDangKyDG.BackColor = System.Drawing.Color.Silver;
			this.pnlDangKyDG.Controls.Add(this.label8);
			this.pnlDangKyDG.Controls.Add(this.txtMscb);
			this.pnlDangKyDG.Controls.Add(this.label7);
			this.pnlDangKyDG.Controls.Add(this.txtMssv);
			this.pnlDangKyDG.Controls.Add(this.btnHuy);
			this.pnlDangKyDG.Controls.Add(this.button1);
			this.pnlDangKyDG.Controls.Add(this.groupBox2);
			this.pnlDangKyDG.Controls.Add(this.groupBox1);
			this.pnlDangKyDG.Location = new System.Drawing.Point(167, 55);
			this.pnlDangKyDG.Name = "pnlDangKyDG";
			this.pnlDangKyDG.Size = new System.Drawing.Size(691, 395);
			this.pnlDangKyDG.TabIndex = 25;
			this.pnlDangKyDG.Visible = false;
			// 
			// label8
			// 
			this.label8.AutoSize = true;
			this.label8.Location = new System.Drawing.Point(422, 223);
			this.label8.Name = "label8";
			this.label8.Size = new System.Drawing.Size(41, 13);
			this.label8.TabIndex = 29;
			this.label8.Text = "MSGV:";
			// 
			// txtMscb
			// 
			this.txtMscb.Enabled = false;
			this.txtMscb.Location = new System.Drawing.Point(425, 239);
			this.txtMscb.Name = "txtMscb";
			this.txtMscb.Size = new System.Drawing.Size(133, 20);
			this.txtMscb.TabIndex = 28;
			// 
			// label7
			// 
			this.label7.AutoSize = true;
			this.label7.Location = new System.Drawing.Point(422, 184);
			this.label7.Name = "label7";
			this.label7.Size = new System.Drawing.Size(40, 13);
			this.label7.TabIndex = 22;
			this.label7.Text = "MSSV:";
			// 
			// txtMssv
			// 
			this.txtMssv.Enabled = false;
			this.txtMssv.Location = new System.Drawing.Point(425, 200);
			this.txtMssv.Name = "txtMssv";
			this.txtMssv.Size = new System.Drawing.Size(133, 20);
			this.txtMssv.TabIndex = 27;
			// 
			// btnHuy
			// 
			this.btnHuy.Location = new System.Drawing.Point(493, 299);
			this.btnHuy.Name = "btnHuy";
			this.btnHuy.Size = new System.Drawing.Size(75, 23);
			this.btnHuy.TabIndex = 26;
			this.btnHuy.Text = "Hủy bỏ";
			this.btnHuy.UseVisualStyleBackColor = true;
			// 
			// button1
			// 
			this.button1.Location = new System.Drawing.Point(412, 299);
			this.button1.Name = "button1";
			this.button1.Size = new System.Drawing.Size(75, 23);
			this.button1.TabIndex = 25;
			this.button1.Text = "Đăng ký";
			this.button1.UseVisualStyleBackColor = true;
			this.button1.Click += new System.EventHandler(this.button1_Click);
			// 
			// groupBox2
			// 
			this.groupBox2.Controls.Add(this.label11);
			this.groupBox2.Controls.Add(this.label10);
			this.groupBox2.Controls.Add(this.cboThang);
			this.groupBox2.Controls.Add(this.cboNgay);
			this.groupBox2.Controls.Add(this.label9);
			this.groupBox2.Controls.Add(this.label5);
			this.groupBox2.Controls.Add(this.label6);
			this.groupBox2.Controls.Add(this.txtDiaChi);
			this.groupBox2.Controls.Add(this.label12);
			this.groupBox2.Controls.Add(this.txtSdt);
			this.groupBox2.Controls.Add(this.label13);
			this.groupBox2.Controls.Add(this.txtNamSinh);
			this.groupBox2.Controls.Add(this.label14);
			this.groupBox2.Controls.Add(this.txtEmail);
			this.groupBox2.Controls.Add(this.label15);
			this.groupBox2.Controls.Add(this.txtHoTen);
			this.groupBox2.Controls.Add(this.txtCmnd);
			this.groupBox2.Location = new System.Drawing.Point(24, 30);
			this.groupBox2.Name = "groupBox2";
			this.groupBox2.Size = new System.Drawing.Size(332, 307);
			this.groupBox2.TabIndex = 24;
			this.groupBox2.TabStop = false;
			this.groupBox2.Text = "Thông tin cá nhân";
			// 
			// label11
			// 
			this.label11.AutoSize = true;
			this.label11.Location = new System.Drawing.Point(185, 112);
			this.label11.Name = "label11";
			this.label11.Size = new System.Drawing.Size(32, 13);
			this.label11.TabIndex = 16;
			this.label11.Text = "Năm:";
			// 
			// label10
			// 
			this.label10.AutoSize = true;
			this.label10.Location = new System.Drawing.Point(93, 112);
			this.label10.Name = "label10";
			this.label10.Size = new System.Drawing.Size(41, 13);
			this.label10.TabIndex = 15;
			this.label10.Text = "Tháng:";
			// 
			// cboThang
			// 
			this.cboThang.FormattingEnabled = true;
			this.cboThang.Items.AddRange(new object[] {
            "1",
            "2",
            "3",
            "4",
            "5",
            "6",
            "7",
            "8",
            "9",
            "10",
            "11",
            "12"});
			this.cboThang.Location = new System.Drawing.Point(136, 108);
			this.cboThang.Name = "cboThang";
			this.cboThang.Size = new System.Drawing.Size(43, 21);
			this.cboThang.TabIndex = 14;
			this.cboThang.SelectedIndexChanged += new System.EventHandler(this.cboThang_SelectedIndexChanged);
			// 
			// cboNgay
			// 
			this.cboNgay.FormattingEnabled = true;
			this.cboNgay.Location = new System.Drawing.Point(42, 109);
			this.cboNgay.Name = "cboNgay";
			this.cboNgay.Size = new System.Drawing.Size(45, 21);
			this.cboNgay.TabIndex = 13;
			// 
			// label9
			// 
			this.label9.AutoSize = true;
			this.label9.Location = new System.Drawing.Point(6, 112);
			this.label9.Name = "label9";
			this.label9.Size = new System.Drawing.Size(35, 13);
			this.label9.TabIndex = 12;
			this.label9.Text = "Ngày:";
			// 
			// label5
			// 
			this.label5.AutoSize = true;
			this.label5.Location = new System.Drawing.Point(6, 47);
			this.label5.Name = "label5";
			this.label5.Size = new System.Drawing.Size(57, 13);
			this.label5.TabIndex = 0;
			this.label5.Text = "Họ và tên:";
			// 
			// label6
			// 
			this.label6.AutoSize = true;
			this.label6.Location = new System.Drawing.Point(7, 256);
			this.label6.Name = "label6";
			this.label6.Size = new System.Drawing.Size(42, 13);
			this.label6.TabIndex = 1;
			this.label6.Text = "CMND:";
			// 
			// txtDiaChi
			// 
			this.txtDiaChi.Location = new System.Drawing.Point(9, 195);
			this.txtDiaChi.Name = "txtDiaChi";
			this.txtDiaChi.Size = new System.Drawing.Size(281, 20);
			this.txtDiaChi.TabIndex = 11;
			// 
			// label12
			// 
			this.label12.AutoSize = true;
			this.label12.Location = new System.Drawing.Point(6, 86);
			this.label12.Name = "label12";
			this.label12.Size = new System.Drawing.Size(57, 13);
			this.label12.TabIndex = 2;
			this.label12.Text = "Ngày sinh:";
			// 
			// txtSdt
			// 
			this.txtSdt.Location = new System.Drawing.Point(9, 156);
			this.txtSdt.Name = "txtSdt";
			this.txtSdt.Size = new System.Drawing.Size(133, 20);
			this.txtSdt.TabIndex = 10;
			// 
			// label13
			// 
			this.label13.AutoSize = true;
			this.label13.Location = new System.Drawing.Point(6, 140);
			this.label13.Name = "label13";
			this.label13.Size = new System.Drawing.Size(73, 13);
			this.label13.TabIndex = 3;
			this.label13.Text = "Số điện thoại:";
			// 
			// txtNamSinh
			// 
			this.txtNamSinh.Location = new System.Drawing.Point(223, 108);
			this.txtNamSinh.Name = "txtNamSinh";
			this.txtNamSinh.Size = new System.Drawing.Size(67, 20);
			this.txtNamSinh.TabIndex = 9;
			// 
			// label14
			// 
			this.label14.AutoSize = true;
			this.label14.Location = new System.Drawing.Point(6, 218);
			this.label14.Name = "label14";
			this.label14.Size = new System.Drawing.Size(71, 13);
			this.label14.TabIndex = 4;
			this.label14.Text = "Địa chỉ Email:";
			// 
			// txtEmail
			// 
			this.txtEmail.Location = new System.Drawing.Point(9, 234);
			this.txtEmail.Name = "txtEmail";
			this.txtEmail.Size = new System.Drawing.Size(133, 20);
			this.txtEmail.TabIndex = 8;
			// 
			// label15
			// 
			this.label15.AutoSize = true;
			this.label15.Location = new System.Drawing.Point(6, 179);
			this.label15.Name = "label15";
			this.label15.Size = new System.Drawing.Size(43, 13);
			this.label15.TabIndex = 5;
			this.label15.Text = "Địa chỉ:";
			// 
			// txtHoTen
			// 
			this.txtHoTen.Location = new System.Drawing.Point(9, 63);
			this.txtHoTen.Name = "txtHoTen";
			this.txtHoTen.Size = new System.Drawing.Size(281, 20);
			this.txtHoTen.TabIndex = 7;
			// 
			// txtCmnd
			// 
			this.txtCmnd.Location = new System.Drawing.Point(10, 272);
			this.txtCmnd.Name = "txtCmnd";
			this.txtCmnd.Size = new System.Drawing.Size(133, 20);
			this.txtCmnd.TabIndex = 6;
			// 
			// groupBox1
			// 
			this.groupBox1.Controls.Add(this.rdoSinhVien);
			this.groupBox1.Controls.Add(this.rdoKhach);
			this.groupBox1.Controls.Add(this.rdoGiangVien);
			this.groupBox1.Location = new System.Drawing.Point(425, 30);
			this.groupBox1.Name = "groupBox1";
			this.groupBox1.Size = new System.Drawing.Size(135, 90);
			this.groupBox1.TabIndex = 23;
			this.groupBox1.TabStop = false;
			this.groupBox1.Text = "Loại độc giả";
			// 
			// rdoSinhVien
			// 
			this.rdoSinhVien.AutoSize = true;
			this.rdoSinhVien.Location = new System.Drawing.Point(6, 19);
			this.rdoSinhVien.Name = "rdoSinhVien";
			this.rdoSinhVien.Size = new System.Drawing.Size(69, 17);
			this.rdoSinhVien.TabIndex = 12;
			this.rdoSinhVien.TabStop = true;
			this.rdoSinhVien.Text = "Sinh viên";
			this.rdoSinhVien.UseVisualStyleBackColor = true;
			this.rdoSinhVien.CheckedChanged += new System.EventHandler(this.rdoSinhVien_CheckedChanged);
			// 
			// rdoKhach
			// 
			this.rdoKhach.AutoSize = true;
			this.rdoKhach.Checked = true;
			this.rdoKhach.Location = new System.Drawing.Point(6, 65);
			this.rdoKhach.Name = "rdoKhach";
			this.rdoKhach.Size = new System.Drawing.Size(96, 17);
			this.rdoKhach.TabIndex = 14;
			this.rdoKhach.TabStop = true;
			this.rdoKhach.Text = "Khách vãng lai";
			this.rdoKhach.UseVisualStyleBackColor = true;
			this.rdoKhach.CheckedChanged += new System.EventHandler(this.rdoKhach_CheckedChanged);
			// 
			// rdoGiangVien
			// 
			this.rdoGiangVien.AutoSize = true;
			this.rdoGiangVien.Location = new System.Drawing.Point(6, 42);
			this.rdoGiangVien.Name = "rdoGiangVien";
			this.rdoGiangVien.Size = new System.Drawing.Size(76, 17);
			this.rdoGiangVien.TabIndex = 13;
			this.rdoGiangVien.TabStop = true;
			this.rdoGiangVien.Text = "Giảng viên";
			this.rdoGiangVien.UseVisualStyleBackColor = true;
			this.rdoGiangVien.CheckedChanged += new System.EventHandler(this.rdoGiangVien_CheckedChanged);
			// 
			// pnlTimKiemDG
			// 
			this.pnlTimKiemDG.BackColor = System.Drawing.Color.Silver;
			this.pnlTimKiemDG.Controls.Add(this.btnDGPhat);
			this.pnlTimKiemDG.Controls.Add(this.btnTKDG_DangKyDG);
			this.pnlTimKiemDG.Controls.Add(this.btnLapPhieuCanhCao);
			this.pnlTimKiemDG.Controls.Add(this.btnTKDG_LapPT);
			this.pnlTimKiemDG.Controls.Add(this.btnTKDG_LapPM);
			this.pnlTimKiemDG.Controls.Add(this.groupBox3);
			this.pnlTimKiemDG.Controls.Add(this.btnTKDG_Xemtatca);
			this.pnlTimKiemDG.Controls.Add(this.groupBox4);
			this.pnlTimKiemDG.Location = new System.Drawing.Point(167, 52);
			this.pnlTimKiemDG.Name = "pnlTimKiemDG";
			this.pnlTimKiemDG.Size = new System.Drawing.Size(691, 401);
			this.pnlTimKiemDG.TabIndex = 30;
			this.pnlTimKiemDG.Visible = false;
			// 
			// btnDGPhat
			// 
			this.btnDGPhat.Location = new System.Drawing.Point(401, 131);
			this.btnDGPhat.Name = "btnDGPhat";
			this.btnDGPhat.Size = new System.Drawing.Size(248, 32);
			this.btnDGPhat.TabIndex = 18;
			this.btnDGPhat.Text = "Xem độc giả bị phạt";
			this.btnDGPhat.UseVisualStyleBackColor = true;
			this.btnDGPhat.Click += new System.EventHandler(this.btnDGPhat_Click);
			// 
			// btnTKDG_DangKyDG
			// 
			this.btnTKDG_DangKyDG.Location = new System.Drawing.Point(401, 19);
			this.btnTKDG_DangKyDG.Name = "btnTKDG_DangKyDG";
			this.btnTKDG_DangKyDG.Size = new System.Drawing.Size(248, 32);
			this.btnTKDG_DangKyDG.TabIndex = 17;
			this.btnTKDG_DangKyDG.Text = "Đăng ký độc giả";
			this.btnTKDG_DangKyDG.UseVisualStyleBackColor = true;
			this.btnTKDG_DangKyDG.Click += new System.EventHandler(this.btnTKDG_DangKyDG_Click);
			// 
			// btnLapPhieuCanhCao
			// 
			this.btnLapPhieuCanhCao.Location = new System.Drawing.Point(401, 94);
			this.btnLapPhieuCanhCao.Name = "btnLapPhieuCanhCao";
			this.btnLapPhieuCanhCao.Size = new System.Drawing.Size(248, 32);
			this.btnLapPhieuCanhCao.TabIndex = 16;
			this.btnLapPhieuCanhCao.Text = "Lập phiếu cảnh cáo";
			this.btnLapPhieuCanhCao.UseVisualStyleBackColor = true;
			this.btnLapPhieuCanhCao.Click += new System.EventHandler(this.btnLapPhieuCanhCao_Click);
			// 
			// btnTKDG_LapPT
			// 
			this.btnTKDG_LapPT.Location = new System.Drawing.Point(528, 57);
			this.btnTKDG_LapPT.Name = "btnTKDG_LapPT";
			this.btnTKDG_LapPT.Size = new System.Drawing.Size(121, 31);
			this.btnTKDG_LapPT.TabIndex = 15;
			this.btnTKDG_LapPT.Text = "Lập phiếu trả";
			this.btnTKDG_LapPT.UseVisualStyleBackColor = true;
			this.btnTKDG_LapPT.Click += new System.EventHandler(this.btnTKDG_LapPT_Click);
			// 
			// btnTKDG_LapPM
			// 
			this.btnTKDG_LapPM.Location = new System.Drawing.Point(401, 57);
			this.btnTKDG_LapPM.Name = "btnTKDG_LapPM";
			this.btnTKDG_LapPM.Size = new System.Drawing.Size(121, 31);
			this.btnTKDG_LapPM.TabIndex = 14;
			this.btnTKDG_LapPM.Text = "Lập phiếu mượn";
			this.btnTKDG_LapPM.UseVisualStyleBackColor = true;
			this.btnTKDG_LapPM.Click += new System.EventHandler(this.btnTKDG_LapPM_Click);
			// 
			// groupBox3
			// 
			this.groupBox3.Controls.Add(this.dgvDsdg);
			this.groupBox3.Location = new System.Drawing.Point(19, 204);
			this.groupBox3.Name = "groupBox3";
			this.groupBox3.Size = new System.Drawing.Size(642, 188);
			this.groupBox3.TabIndex = 13;
			this.groupBox3.TabStop = false;
			this.groupBox3.Text = "Danh sách độc giả";
			// 
			// dgvDsdg
			// 
			this.dgvDsdg.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
			this.dgvDsdg.Location = new System.Drawing.Point(7, 20);
			this.dgvDsdg.Name = "dgvDsdg";
			this.dgvDsdg.ReadOnly = true;
			this.dgvDsdg.Size = new System.Drawing.Size(629, 162);
			this.dgvDsdg.TabIndex = 0;
			// 
			// btnTKDG_Xemtatca
			// 
			this.btnTKDG_Xemtatca.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.btnTKDG_Xemtatca.Location = new System.Drawing.Point(401, 169);
			this.btnTKDG_Xemtatca.Name = "btnTKDG_Xemtatca";
			this.btnTKDG_Xemtatca.Size = new System.Drawing.Size(248, 32);
			this.btnTKDG_Xemtatca.TabIndex = 12;
			this.btnTKDG_Xemtatca.Text = "Xem tất cả";
			this.btnTKDG_Xemtatca.UseVisualStyleBackColor = true;
			this.btnTKDG_Xemtatca.Click += new System.EventHandler(this.btnTKDG_Xemtatca_Click);
			// 
			// groupBox4
			// 
			this.groupBox4.Controls.Add(this.rdoMaso);
			this.groupBox4.Controls.Add(this.rdoTenDG);
			this.groupBox4.Controls.Add(this.rdoMaDG);
			this.groupBox4.Controls.Add(this.btnTKDG_Timkiem);
			this.groupBox4.Controls.Add(this.txtTKDG_Maso);
			this.groupBox4.Controls.Add(this.txtTKDG_TenDG);
			this.groupBox4.Controls.Add(this.txtMaDG);
			this.groupBox4.Location = new System.Drawing.Point(0, 0);
			this.groupBox4.Name = "groupBox4";
			this.groupBox4.Size = new System.Drawing.Size(331, 168);
			this.groupBox4.TabIndex = 11;
			this.groupBox4.TabStop = false;
			this.groupBox4.Text = "Thông tin độc giả";
			// 
			// rdoMaso
			// 
			this.rdoMaso.AutoSize = true;
			this.rdoMaso.Location = new System.Drawing.Point(39, 91);
			this.rdoMaso.Name = "rdoMaso";
			this.rdoMaso.Size = new System.Drawing.Size(128, 17);
			this.rdoMaso.TabIndex = 20;
			this.rdoMaso.Text = "CMND/MSSV/MSGV";
			this.rdoMaso.UseVisualStyleBackColor = true;
			this.rdoMaso.CheckedChanged += new System.EventHandler(this.rdoMaso_CheckedChanged);
			// 
			// rdoTenDG
			// 
			this.rdoTenDG.AutoSize = true;
			this.rdoTenDG.Location = new System.Drawing.Point(39, 66);
			this.rdoTenDG.Name = "rdoTenDG";
			this.rdoTenDG.Size = new System.Drawing.Size(83, 17);
			this.rdoTenDG.TabIndex = 19;
			this.rdoTenDG.Text = "Tên độc giả";
			this.rdoTenDG.UseVisualStyleBackColor = true;
			this.rdoTenDG.CheckedChanged += new System.EventHandler(this.rdoCmnd_CheckedChanged);
			// 
			// rdoMaDG
			// 
			this.rdoMaDG.AutoSize = true;
			this.rdoMaDG.Checked = true;
			this.rdoMaDG.Location = new System.Drawing.Point(39, 40);
			this.rdoMaDG.Name = "rdoMaDG";
			this.rdoMaDG.Size = new System.Drawing.Size(79, 17);
			this.rdoMaDG.TabIndex = 18;
			this.rdoMaDG.TabStop = true;
			this.rdoMaDG.Text = "Mã độc giả";
			this.rdoMaDG.UseVisualStyleBackColor = true;
			this.rdoMaDG.CheckedChanged += new System.EventHandler(this.rdoMaDG_CheckedChanged);
			// 
			// btnTKDG_Timkiem
			// 
			this.btnTKDG_Timkiem.Location = new System.Drawing.Point(29, 130);
			this.btnTKDG_Timkiem.Name = "btnTKDG_Timkiem";
			this.btnTKDG_Timkiem.Size = new System.Drawing.Size(281, 23);
			this.btnTKDG_Timkiem.TabIndex = 6;
			this.btnTKDG_Timkiem.Text = "Tìm kiếm";
			this.btnTKDG_Timkiem.UseVisualStyleBackColor = true;
			this.btnTKDG_Timkiem.Click += new System.EventHandler(this.btnTKDG_Timkiem_Click);
			// 
			// txtTKDG_Maso
			// 
			this.txtTKDG_Maso.Enabled = false;
			this.txtTKDG_Maso.Location = new System.Drawing.Point(174, 90);
			this.txtTKDG_Maso.Name = "txtTKDG_Maso";
			this.txtTKDG_Maso.Size = new System.Drawing.Size(136, 20);
			this.txtTKDG_Maso.TabIndex = 5;
			// 
			// txtTKDG_TenDG
			// 
			this.txtTKDG_TenDG.Enabled = false;
			this.txtTKDG_TenDG.Location = new System.Drawing.Point(174, 64);
			this.txtTKDG_TenDG.Name = "txtTKDG_TenDG";
			this.txtTKDG_TenDG.Size = new System.Drawing.Size(136, 20);
			this.txtTKDG_TenDG.TabIndex = 4;
			// 
			// txtMaDG
			// 
			this.txtMaDG.Location = new System.Drawing.Point(174, 37);
			this.txtMaDG.Name = "txtMaDG";
			this.txtMaDG.Size = new System.Drawing.Size(136, 20);
			this.txtMaDG.TabIndex = 3;
			// 
			// pnlLapPM
			// 
			this.pnlLapPM.BackColor = System.Drawing.Color.Silver;
			this.pnlLapPM.Controls.Add(this.groupBox15);
			this.pnlLapPM.Controls.Add(this.btnLPM_LapPhieu);
			this.pnlLapPM.Controls.Add(this.groupBox5);
			this.pnlLapPM.Location = new System.Drawing.Point(164, 55);
			this.pnlLapPM.Name = "pnlLapPM";
			this.pnlLapPM.Size = new System.Drawing.Size(694, 398);
			this.pnlLapPM.TabIndex = 25;
			this.pnlLapPM.Visible = false;
			this.pnlLapPM.VisibleChanged += new System.EventHandler(this.pnlLapPM_VisibleChanged);
			// 
			// groupBox15
			// 
			this.groupBox15.Controls.Add(this.dgvTaiLieuMuon);
			this.groupBox15.Location = new System.Drawing.Point(6, 196);
			this.groupBox15.Name = "groupBox15";
			this.groupBox15.Size = new System.Drawing.Size(677, 198);
			this.groupBox15.TabIndex = 24;
			this.groupBox15.TabStop = false;
			this.groupBox15.Text = "Tài liệu";
			// 
			// dgvTaiLieuMuon
			// 
			this.dgvTaiLieuMuon.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
			this.dgvTaiLieuMuon.Location = new System.Drawing.Point(7, 16);
			this.dgvTaiLieuMuon.Name = "dgvTaiLieuMuon";
			this.dgvTaiLieuMuon.Size = new System.Drawing.Size(664, 179);
			this.dgvTaiLieuMuon.TabIndex = 0;
			// 
			// btnLPM_LapPhieu
			// 
			this.btnLPM_LapPhieu.Location = new System.Drawing.Point(536, 69);
			this.btnLPM_LapPhieu.Name = "btnLPM_LapPhieu";
			this.btnLPM_LapPhieu.Size = new System.Drawing.Size(108, 55);
			this.btnLPM_LapPhieu.TabIndex = 22;
			this.btnLPM_LapPhieu.Text = "Lập phiếu";
			this.btnLPM_LapPhieu.UseVisualStyleBackColor = true;
			this.btnLPM_LapPhieu.Click += new System.EventHandler(this.btnLPM_LapPhieu_Click);
			// 
			// groupBox5
			// 
			this.groupBox5.Controls.Add(this.ltbDocGia);
			this.groupBox5.Location = new System.Drawing.Point(6, 6);
			this.groupBox5.Name = "groupBox5";
			this.groupBox5.Size = new System.Drawing.Size(480, 175);
			this.groupBox5.TabIndex = 21;
			this.groupBox5.TabStop = false;
			this.groupBox5.Text = "Độc giả";
			// 
			// ltbDocGia
			// 
			this.ltbDocGia.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.ltbDocGia.FormattingEnabled = true;
			this.ltbDocGia.ItemHeight = 16;
			this.ltbDocGia.Location = new System.Drawing.Point(8, 19);
			this.ltbDocGia.Name = "ltbDocGia";
			this.ltbDocGia.Size = new System.Drawing.Size(464, 148);
			this.ltbDocGia.TabIndex = 0;
			this.ltbDocGia.SelectedIndexChanged += new System.EventHandler(this.ltbDocGia_SelectedIndexChanged);
			// 
			// pnlTimKiemPM
			// 
			this.pnlTimKiemPM.BackColor = System.Drawing.Color.Silver;
			this.pnlTimKiemPM.Controls.Add(this.btnTKPM_XemPMDT);
			this.pnlTimKiemPM.Controls.Add(this.btnTKPM_LapPT);
			this.pnlTimKiemPM.Controls.Add(this.groupBox6);
			this.pnlTimKiemPM.Controls.Add(this.btnTKPM_LapPM);
			this.pnlTimKiemPM.Controls.Add(this.btnTKPM_XemPMQH);
			this.pnlTimKiemPM.Controls.Add(this.btnTKPM_Xemtatca);
			this.pnlTimKiemPM.Controls.Add(this.groupBox7);
			this.pnlTimKiemPM.Location = new System.Drawing.Point(164, 55);
			this.pnlTimKiemPM.Name = "pnlTimKiemPM";
			this.pnlTimKiemPM.Size = new System.Drawing.Size(694, 398);
			this.pnlTimKiemPM.TabIndex = 26;
			this.pnlTimKiemPM.Visible = false;
			// 
			// btnTKPM_XemPMDT
			// 
			this.btnTKPM_XemPMDT.Location = new System.Drawing.Point(549, 137);
			this.btnTKPM_XemPMDT.Name = "btnTKPM_XemPMDT";
			this.btnTKPM_XemPMDT.Size = new System.Drawing.Size(114, 38);
			this.btnTKPM_XemPMDT.TabIndex = 25;
			this.btnTKPM_XemPMDT.Text = "Xem phiếu mượn đã trả";
			this.btnTKPM_XemPMDT.UseVisualStyleBackColor = true;
			this.btnTKPM_XemPMDT.Click += new System.EventHandler(this.btnTKPM_XemPMDT_Click);
			// 
			// btnTKPM_LapPT
			// 
			this.btnTKPM_LapPT.Location = new System.Drawing.Point(434, 49);
			this.btnTKPM_LapPT.Name = "btnTKPM_LapPT";
			this.btnTKPM_LapPT.Size = new System.Drawing.Size(229, 38);
			this.btnTKPM_LapPT.TabIndex = 24;
			this.btnTKPM_LapPT.Text = "Lập phiếu trả";
			this.btnTKPM_LapPT.UseVisualStyleBackColor = true;
			this.btnTKPM_LapPT.Click += new System.EventHandler(this.btnTKPM_LapPT_Click);
			// 
			// groupBox6
			// 
			this.groupBox6.Controls.Add(this.dgvDSPM);
			this.groupBox6.Location = new System.Drawing.Point(16, 180);
			this.groupBox6.Name = "groupBox6";
			this.groupBox6.Size = new System.Drawing.Size(648, 211);
			this.groupBox6.TabIndex = 23;
			this.groupBox6.TabStop = false;
			this.groupBox6.Text = "Danh sách phiếu mượn";
			// 
			// dgvDSPM
			// 
			this.dgvDSPM.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
			this.dgvDSPM.Location = new System.Drawing.Point(6, 19);
			this.dgvDSPM.Name = "dgvDSPM";
			this.dgvDSPM.ReadOnly = true;
			this.dgvDSPM.Size = new System.Drawing.Size(636, 186);
			this.dgvDSPM.TabIndex = 0;
			// 
			// btnTKPM_LapPM
			// 
			this.btnTKPM_LapPM.Location = new System.Drawing.Point(434, 7);
			this.btnTKPM_LapPM.Name = "btnTKPM_LapPM";
			this.btnTKPM_LapPM.Size = new System.Drawing.Size(229, 36);
			this.btnTKPM_LapPM.TabIndex = 22;
			this.btnTKPM_LapPM.Text = "Lập phiếu mượn";
			this.btnTKPM_LapPM.UseVisualStyleBackColor = true;
			this.btnTKPM_LapPM.Click += new System.EventHandler(this.btnTKPM_LapPM_Click);
			// 
			// btnTKPM_XemPMQH
			// 
			this.btnTKPM_XemPMQH.Location = new System.Drawing.Point(434, 93);
			this.btnTKPM_XemPMQH.Name = "btnTKPM_XemPMQH";
			this.btnTKPM_XemPMQH.Size = new System.Drawing.Size(229, 38);
			this.btnTKPM_XemPMQH.TabIndex = 21;
			this.btnTKPM_XemPMQH.Text = "Xem phiếu mượn quá hạn";
			this.btnTKPM_XemPMQH.UseVisualStyleBackColor = true;
			this.btnTKPM_XemPMQH.Click += new System.EventHandler(this.btnTKPM_XemPMQH_Click);
			// 
			// btnTKPM_Xemtatca
			// 
			this.btnTKPM_Xemtatca.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.btnTKPM_Xemtatca.Location = new System.Drawing.Point(434, 137);
			this.btnTKPM_Xemtatca.Name = "btnTKPM_Xemtatca";
			this.btnTKPM_Xemtatca.Size = new System.Drawing.Size(109, 38);
			this.btnTKPM_Xemtatca.TabIndex = 20;
			this.btnTKPM_Xemtatca.Text = "Xem tất cả";
			this.btnTKPM_Xemtatca.UseVisualStyleBackColor = true;
			this.btnTKPM_Xemtatca.Click += new System.EventHandler(this.btnTKPM_Xemtatca_Click);
			// 
			// groupBox7
			// 
			this.groupBox7.Controls.Add(this.rdoTKPM_MaDG);
			this.groupBox7.Controls.Add(this.rdoTKPM_MaPM);
			this.groupBox7.Controls.Add(this.btnTKPM_Timkiem);
			this.groupBox7.Controls.Add(this.txtTKPM_MaDG);
			this.groupBox7.Controls.Add(this.txtTKPM_MaPM);
			this.groupBox7.Location = new System.Drawing.Point(16, 6);
			this.groupBox7.Name = "groupBox7";
			this.groupBox7.Size = new System.Drawing.Size(384, 168);
			this.groupBox7.TabIndex = 19;
			this.groupBox7.TabStop = false;
			this.groupBox7.Text = "Thông tin phiếu mượn";
			// 
			// rdoTKPM_MaDG
			// 
			this.rdoTKPM_MaDG.AutoSize = true;
			this.rdoTKPM_MaDG.Location = new System.Drawing.Point(45, 76);
			this.rdoTKPM_MaDG.Name = "rdoTKPM_MaDG";
			this.rdoTKPM_MaDG.Size = new System.Drawing.Size(79, 17);
			this.rdoTKPM_MaDG.TabIndex = 8;
			this.rdoTKPM_MaDG.Text = "Mã độc giả";
			this.rdoTKPM_MaDG.UseVisualStyleBackColor = true;
			this.rdoTKPM_MaDG.CheckedChanged += new System.EventHandler(this.rdoTKPM_MaDG_CheckedChanged);
			// 
			// rdoTKPM_MaPM
			// 
			this.rdoTKPM_MaPM.AutoSize = true;
			this.rdoTKPM_MaPM.Checked = true;
			this.rdoTKPM_MaPM.Location = new System.Drawing.Point(45, 38);
			this.rdoTKPM_MaPM.Name = "rdoTKPM_MaPM";
			this.rdoTKPM_MaPM.Size = new System.Drawing.Size(98, 17);
			this.rdoTKPM_MaPM.TabIndex = 7;
			this.rdoTKPM_MaPM.TabStop = true;
			this.rdoTKPM_MaPM.Text = "Mã phiếu mượn";
			this.rdoTKPM_MaPM.UseVisualStyleBackColor = true;
			this.rdoTKPM_MaPM.CheckedChanged += new System.EventHandler(this.rdoTKPM_MaPM_CheckedChanged);
			// 
			// btnTKPM_Timkiem
			// 
			this.btnTKPM_Timkiem.Location = new System.Drawing.Point(29, 115);
			this.btnTKPM_Timkiem.Name = "btnTKPM_Timkiem";
			this.btnTKPM_Timkiem.Size = new System.Drawing.Size(287, 23);
			this.btnTKPM_Timkiem.TabIndex = 6;
			this.btnTKPM_Timkiem.Text = "Tìm kiếm";
			this.btnTKPM_Timkiem.UseVisualStyleBackColor = true;
			this.btnTKPM_Timkiem.Click += new System.EventHandler(this.btnTKPM_Timkiem_Click);
			// 
			// txtTKPM_MaDG
			// 
			this.txtTKPM_MaDG.Enabled = false;
			this.txtTKPM_MaDG.Location = new System.Drawing.Point(147, 75);
			this.txtTKPM_MaDG.Name = "txtTKPM_MaDG";
			this.txtTKPM_MaDG.Size = new System.Drawing.Size(169, 20);
			this.txtTKPM_MaDG.TabIndex = 4;
			// 
			// txtTKPM_MaPM
			// 
			this.txtTKPM_MaPM.Location = new System.Drawing.Point(147, 37);
			this.txtTKPM_MaPM.Name = "txtTKPM_MaPM";
			this.txtTKPM_MaPM.Size = new System.Drawing.Size(169, 20);
			this.txtTKPM_MaPM.TabIndex = 3;
			// 
			// pnlLapPT
			// 
			this.pnlLapPT.BackColor = System.Drawing.Color.Silver;
			this.pnlLapPT.Controls.Add(this.groupBox17);
			this.pnlLapPT.Controls.Add(this.btnLPT_LapPhieu);
			this.pnlLapPT.Controls.Add(this.groupBox8);
			this.pnlLapPT.Location = new System.Drawing.Point(167, 55);
			this.pnlLapPT.Name = "pnlLapPT";
			this.pnlLapPT.Size = new System.Drawing.Size(694, 398);
			this.pnlLapPT.TabIndex = 27;
			this.pnlLapPT.Visible = false;
			this.pnlLapPT.VisibleChanged += new System.EventHandler(this.pnlLapPT_VisibleChanged);
			// 
			// groupBox17
			// 
			this.groupBox17.Controls.Add(this.dgvTaiLieuDaMuon);
			this.groupBox17.Location = new System.Drawing.Point(7, 195);
			this.groupBox17.Name = "groupBox17";
			this.groupBox17.Size = new System.Drawing.Size(677, 198);
			this.groupBox17.TabIndex = 27;
			this.groupBox17.TabStop = false;
			this.groupBox17.Text = "Tài liệu";
			// 
			// dgvTaiLieuDaMuon
			// 
			this.dgvTaiLieuDaMuon.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
			this.dgvTaiLieuDaMuon.Location = new System.Drawing.Point(7, 16);
			this.dgvTaiLieuDaMuon.Name = "dgvTaiLieuDaMuon";
			this.dgvTaiLieuDaMuon.Size = new System.Drawing.Size(664, 179);
			this.dgvTaiLieuDaMuon.TabIndex = 0;
			this.dgvTaiLieuDaMuon.CellValueChanged += new System.Windows.Forms.DataGridViewCellEventHandler(this.dgvTaiLieuDaMuon_CellValueChanged);
			// 
			// btnLPT_LapPhieu
			// 
			this.btnLPT_LapPhieu.Location = new System.Drawing.Point(533, 68);
			this.btnLPT_LapPhieu.Name = "btnLPT_LapPhieu";
			this.btnLPT_LapPhieu.Size = new System.Drawing.Size(108, 59);
			this.btnLPT_LapPhieu.TabIndex = 25;
			this.btnLPT_LapPhieu.Text = "Lập phiếu";
			this.btnLPT_LapPhieu.UseVisualStyleBackColor = true;
			this.btnLPT_LapPhieu.Click += new System.EventHandler(this.btnLPT_LapPhieu_Click);
			// 
			// groupBox8
			// 
			this.groupBox8.Controls.Add(this.ltbPhieuMuon);
			this.groupBox8.Location = new System.Drawing.Point(0, 0);
			this.groupBox8.Name = "groupBox8";
			this.groupBox8.Size = new System.Drawing.Size(483, 181);
			this.groupBox8.TabIndex = 24;
			this.groupBox8.TabStop = false;
			this.groupBox8.Text = "Phiếu mượn";
			// 
			// ltbPhieuMuon
			// 
			this.ltbPhieuMuon.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.ltbPhieuMuon.FormattingEnabled = true;
			this.ltbPhieuMuon.ItemHeight = 16;
			this.ltbPhieuMuon.Location = new System.Drawing.Point(10, 16);
			this.ltbPhieuMuon.Name = "ltbPhieuMuon";
			this.ltbPhieuMuon.Size = new System.Drawing.Size(465, 148);
			this.ltbPhieuMuon.TabIndex = 0;
			this.ltbPhieuMuon.SelectedIndexChanged += new System.EventHandler(this.ltbPhieuMuon_SelectedIndexChanged);
			// 
			// pnlTimKiemPT
			// 
			this.pnlTimKiemPT.BackColor = System.Drawing.Color.Silver;
			this.pnlTimKiemPT.Controls.Add(this.btnTKPT_LapPT);
			this.pnlTimKiemPT.Controls.Add(this.groupBox9);
			this.pnlTimKiemPT.Controls.Add(this.btnTKPT_Xemtatca);
			this.pnlTimKiemPT.Controls.Add(this.groupBox10);
			this.pnlTimKiemPT.Location = new System.Drawing.Point(164, 55);
			this.pnlTimKiemPT.Name = "pnlTimKiemPT";
			this.pnlTimKiemPT.Size = new System.Drawing.Size(694, 398);
			this.pnlTimKiemPT.TabIndex = 28;
			this.pnlTimKiemPT.Visible = false;
			// 
			// btnTKPT_LapPT
			// 
			this.btnTKPT_LapPT.Location = new System.Drawing.Point(438, 48);
			this.btnTKPT_LapPT.Name = "btnTKPT_LapPT";
			this.btnTKPT_LapPT.Size = new System.Drawing.Size(199, 36);
			this.btnTKPT_LapPT.TabIndex = 30;
			this.btnTKPT_LapPT.Text = "Lập phiếu trả";
			this.btnTKPT_LapPT.UseVisualStyleBackColor = true;
			this.btnTKPT_LapPT.Click += new System.EventHandler(this.btnTKPT_LapPT_Click);
			// 
			// groupBox9
			// 
			this.groupBox9.Controls.Add(this.dgvDspt);
			this.groupBox9.Location = new System.Drawing.Point(16, 180);
			this.groupBox9.Name = "groupBox9";
			this.groupBox9.Size = new System.Drawing.Size(666, 211);
			this.groupBox9.TabIndex = 29;
			this.groupBox9.TabStop = false;
			this.groupBox9.Text = "Danh sách phiếu trả";
			// 
			// dgvDspt
			// 
			this.dgvDspt.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
			this.dgvDspt.Location = new System.Drawing.Point(7, 20);
			this.dgvDspt.Name = "dgvDspt";
			this.dgvDspt.ReadOnly = true;
			this.dgvDspt.Size = new System.Drawing.Size(653, 185);
			this.dgvDspt.TabIndex = 0;
			// 
			// btnTKPT_Xemtatca
			// 
			this.btnTKPT_Xemtatca.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.btnTKPT_Xemtatca.Location = new System.Drawing.Point(438, 108);
			this.btnTKPT_Xemtatca.Name = "btnTKPT_Xemtatca";
			this.btnTKPT_Xemtatca.Size = new System.Drawing.Size(199, 36);
			this.btnTKPT_Xemtatca.TabIndex = 27;
			this.btnTKPT_Xemtatca.Text = "Xem tất cả";
			this.btnTKPT_Xemtatca.UseVisualStyleBackColor = true;
			this.btnTKPT_Xemtatca.Click += new System.EventHandler(this.btnTKPT_Xemtatca_Click);
			// 
			// groupBox10
			// 
			this.groupBox10.Controls.Add(this.rdoTKPT_MaDG);
			this.groupBox10.Controls.Add(this.rdoTKPT_MaPM);
			this.groupBox10.Controls.Add(this.rdoTKPT_MaPT);
			this.groupBox10.Controls.Add(this.txtTKPT_MaPT);
			this.groupBox10.Controls.Add(this.btnTKPT_TimKiem);
			this.groupBox10.Controls.Add(this.txtTKPT_MaDG);
			this.groupBox10.Controls.Add(this.txtTKPT_MaPM);
			this.groupBox10.Location = new System.Drawing.Point(16, 6);
			this.groupBox10.Name = "groupBox10";
			this.groupBox10.Size = new System.Drawing.Size(343, 168);
			this.groupBox10.TabIndex = 26;
			this.groupBox10.TabStop = false;
			this.groupBox10.Text = "Thông tin phiếu trả";
			// 
			// rdoTKPT_MaDG
			// 
			this.rdoTKPT_MaDG.AutoSize = true;
			this.rdoTKPT_MaDG.Location = new System.Drawing.Point(29, 88);
			this.rdoTKPT_MaDG.Name = "rdoTKPT_MaDG";
			this.rdoTKPT_MaDG.Size = new System.Drawing.Size(79, 17);
			this.rdoTKPT_MaDG.TabIndex = 10;
			this.rdoTKPT_MaDG.Text = "Mã độc giả";
			this.rdoTKPT_MaDG.UseVisualStyleBackColor = true;
			this.rdoTKPT_MaDG.CheckedChanged += new System.EventHandler(this.rdoTKPT_MaDG_CheckedChanged);
			// 
			// rdoTKPT_MaPM
			// 
			this.rdoTKPT_MaPM.AutoSize = true;
			this.rdoTKPT_MaPM.Location = new System.Drawing.Point(29, 62);
			this.rdoTKPT_MaPM.Name = "rdoTKPT_MaPM";
			this.rdoTKPT_MaPM.Size = new System.Drawing.Size(98, 17);
			this.rdoTKPT_MaPM.TabIndex = 9;
			this.rdoTKPT_MaPM.Text = "Mã phiếu mượn";
			this.rdoTKPT_MaPM.UseVisualStyleBackColor = true;
			this.rdoTKPT_MaPM.CheckedChanged += new System.EventHandler(this.rdoTKPT_MaPM_CheckedChanged);
			// 
			// rdoTKPT_MaPT
			// 
			this.rdoTKPT_MaPT.AutoSize = true;
			this.rdoTKPT_MaPT.Checked = true;
			this.rdoTKPT_MaPT.Location = new System.Drawing.Point(29, 35);
			this.rdoTKPT_MaPT.Name = "rdoTKPT_MaPT";
			this.rdoTKPT_MaPT.Size = new System.Drawing.Size(84, 17);
			this.rdoTKPT_MaPT.TabIndex = 8;
			this.rdoTKPT_MaPT.TabStop = true;
			this.rdoTKPT_MaPT.Text = "Mã phiếu trả";
			this.rdoTKPT_MaPT.UseVisualStyleBackColor = true;
			this.rdoTKPT_MaPT.CheckedChanged += new System.EventHandler(this.rdoTKPT_MaPT_CheckedChanged);
			// 
			// txtTKPT_MaPT
			// 
			this.txtTKPT_MaPT.Location = new System.Drawing.Point(138, 34);
			this.txtTKPT_MaPT.Name = "txtTKPT_MaPT";
			this.txtTKPT_MaPT.Size = new System.Drawing.Size(136, 20);
			this.txtTKPT_MaPT.TabIndex = 7;
			// 
			// btnTKPT_TimKiem
			// 
			this.btnTKPT_TimKiem.Location = new System.Drawing.Point(29, 130);
			this.btnTKPT_TimKiem.Name = "btnTKPT_TimKiem";
			this.btnTKPT_TimKiem.Size = new System.Drawing.Size(263, 23);
			this.btnTKPT_TimKiem.TabIndex = 6;
			this.btnTKPT_TimKiem.Text = "Tìm kiếm";
			this.btnTKPT_TimKiem.UseVisualStyleBackColor = true;
			this.btnTKPT_TimKiem.Click += new System.EventHandler(this.btnTKPT_TimKiem_Click);
			// 
			// txtTKPT_MaDG
			// 
			this.txtTKPT_MaDG.Enabled = false;
			this.txtTKPT_MaDG.Location = new System.Drawing.Point(138, 86);
			this.txtTKPT_MaDG.Name = "txtTKPT_MaDG";
			this.txtTKPT_MaDG.Size = new System.Drawing.Size(136, 20);
			this.txtTKPT_MaDG.TabIndex = 4;
			// 
			// txtTKPT_MaPM
			// 
			this.txtTKPT_MaPM.Enabled = false;
			this.txtTKPT_MaPM.Location = new System.Drawing.Point(138, 60);
			this.txtTKPT_MaPM.Name = "txtTKPT_MaPM";
			this.txtTKPT_MaPM.Size = new System.Drawing.Size(136, 20);
			this.txtTKPT_MaPM.TabIndex = 3;
			// 
			// pnlTimKiemTL
			// 
			this.pnlTimKiemTL.BackColor = System.Drawing.Color.Silver;
			this.pnlTimKiemTL.Controls.Add(this.btnYeuCauThemTL);
			this.pnlTimKiemTL.Controls.Add(this.btnTKTL_LapPM);
			this.pnlTimKiemTL.Controls.Add(this.groupBox11);
			this.pnlTimKiemTL.Controls.Add(this.btnTKTL_Xemtatca);
			this.pnlTimKiemTL.Controls.Add(this.groupBox12);
			this.pnlTimKiemTL.Location = new System.Drawing.Point(164, 55);
			this.pnlTimKiemTL.Name = "pnlTimKiemTL";
			this.pnlTimKiemTL.Size = new System.Drawing.Size(694, 398);
			this.pnlTimKiemTL.TabIndex = 30;
			this.pnlTimKiemTL.Visible = false;
			// 
			// btnYeuCauThemTL
			// 
			this.btnYeuCauThemTL.Location = new System.Drawing.Point(406, 70);
			this.btnYeuCauThemTL.Name = "btnYeuCauThemTL";
			this.btnYeuCauThemTL.Size = new System.Drawing.Size(248, 32);
			this.btnYeuCauThemTL.TabIndex = 19;
			this.btnYeuCauThemTL.Text = "Yêu cầu tài liệu mới";
			this.btnYeuCauThemTL.UseVisualStyleBackColor = true;
			this.btnYeuCauThemTL.Click += new System.EventHandler(this.btnYeuCauThemTL_Click);
			// 
			// btnTKTL_LapPM
			// 
			this.btnTKTL_LapPM.Location = new System.Drawing.Point(406, 33);
			this.btnTKTL_LapPM.Name = "btnTKTL_LapPM";
			this.btnTKTL_LapPM.Size = new System.Drawing.Size(248, 31);
			this.btnTKTL_LapPM.TabIndex = 17;
			this.btnTKTL_LapPM.Text = "Lập phiếu mượn";
			this.btnTKTL_LapPM.UseVisualStyleBackColor = true;
			this.btnTKTL_LapPM.Click += new System.EventHandler(this.btnTKTL_LapPM_Click);
			// 
			// groupBox11
			// 
			this.groupBox11.Controls.Add(this.dgvDstl);
			this.groupBox11.Location = new System.Drawing.Point(6, 180);
			this.groupBox11.Name = "groupBox11";
			this.groupBox11.Size = new System.Drawing.Size(670, 214);
			this.groupBox11.TabIndex = 16;
			this.groupBox11.TabStop = false;
			this.groupBox11.Text = "Danh sách tài liệu";
			// 
			// dgvDstl
			// 
			this.dgvDstl.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
			this.dgvDstl.Location = new System.Drawing.Point(7, 19);
			this.dgvDstl.Name = "dgvDstl";
			this.dgvDstl.ReadOnly = true;
			this.dgvDstl.Size = new System.Drawing.Size(657, 189);
			this.dgvDstl.TabIndex = 0;
			// 
			// btnTKTL_Xemtatca
			// 
			this.btnTKTL_Xemtatca.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.btnTKTL_Xemtatca.Location = new System.Drawing.Point(406, 108);
			this.btnTKTL_Xemtatca.Name = "btnTKTL_Xemtatca";
			this.btnTKTL_Xemtatca.Size = new System.Drawing.Size(248, 47);
			this.btnTKTL_Xemtatca.TabIndex = 15;
			this.btnTKTL_Xemtatca.Text = "Xem tất cả";
			this.btnTKTL_Xemtatca.UseVisualStyleBackColor = true;
			this.btnTKTL_Xemtatca.Click += new System.EventHandler(this.btnTKTL_Xemtatca_Click);
			// 
			// groupBox12
			// 
			this.groupBox12.Controls.Add(this.pnlLoaiTimKiem);
			this.groupBox12.Controls.Add(this.rdoTKTL_TenTL);
			this.groupBox12.Controls.Add(this.rdoTKTL_MaTL);
			this.groupBox12.Controls.Add(this.btnLoaiTimKiem);
			this.groupBox12.Controls.Add(this.btnTKTL_TimKiem);
			this.groupBox12.Controls.Add(this.txtTKTL_TenTL);
			this.groupBox12.Controls.Add(this.txtTKTL_MaTL);
			this.groupBox12.Location = new System.Drawing.Point(10, 16);
			this.groupBox12.Name = "groupBox12";
			this.groupBox12.Size = new System.Drawing.Size(358, 151);
			this.groupBox12.TabIndex = 14;
			this.groupBox12.TabStop = false;
			this.groupBox12.Text = "Thông tin tài liệu";
			// 
			// pnlLoaiTimKiem
			// 
			this.pnlLoaiTimKiem.Controls.Add(this.cboTKTL_LoaiTL);
			this.pnlLoaiTimKiem.Controls.Add(this.cboLinhVuc);
			this.pnlLoaiTimKiem.Controls.Add(this.rdoLinhVuc);
			this.pnlLoaiTimKiem.Controls.Add(this.rdoLoaiTL);
			this.pnlLoaiTimKiem.Location = new System.Drawing.Point(7, 19);
			this.pnlLoaiTimKiem.Name = "pnlLoaiTimKiem";
			this.pnlLoaiTimKiem.Size = new System.Drawing.Size(309, 64);
			this.pnlLoaiTimKiem.TabIndex = 12;
			this.pnlLoaiTimKiem.Visible = false;
			// 
			// cboLinhVuc
			// 
			this.cboLinhVuc.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
			this.cboLinhVuc.Enabled = false;
			this.cboLinhVuc.FormattingEnabled = true;
			this.cboLinhVuc.Location = new System.Drawing.Point(110, 35);
			this.cboLinhVuc.Name = "cboLinhVuc";
			this.cboLinhVuc.Size = new System.Drawing.Size(151, 21);
			this.cboLinhVuc.TabIndex = 16;
			// 
			// rdoLinhVuc
			// 
			this.rdoLinhVuc.AutoSize = true;
			this.rdoLinhVuc.Location = new System.Drawing.Point(16, 37);
			this.rdoLinhVuc.Name = "rdoLinhVuc";
			this.rdoLinhVuc.Size = new System.Drawing.Size(69, 17);
			this.rdoLinhVuc.TabIndex = 15;
			this.rdoLinhVuc.Text = "Lĩnh vực";
			this.rdoLinhVuc.UseVisualStyleBackColor = true;
			this.rdoLinhVuc.CheckedChanged += new System.EventHandler(this.rdoLinhVuc_CheckedChanged);
			// 
			// rdoLoaiTL
			// 
			this.rdoLoaiTL.AutoSize = true;
			this.rdoLoaiTL.Checked = true;
			this.rdoLoaiTL.Location = new System.Drawing.Point(16, 11);
			this.rdoLoaiTL.Name = "rdoLoaiTL";
			this.rdoLoaiTL.Size = new System.Drawing.Size(78, 17);
			this.rdoLoaiTL.TabIndex = 14;
			this.rdoLoaiTL.TabStop = true;
			this.rdoLoaiTL.Text = "Loại tài liệu";
			this.rdoLoaiTL.UseVisualStyleBackColor = true;
			this.rdoLoaiTL.CheckedChanged += new System.EventHandler(this.rdoLoaiTL_CheckedChanged);
			// 
			// rdoTKTL_TenTL
			// 
			this.rdoTKTL_TenTL.AutoSize = true;
			this.rdoTKTL_TenTL.Location = new System.Drawing.Point(23, 56);
			this.rdoTKTL_TenTL.Name = "rdoTKTL_TenTL";
			this.rdoTKTL_TenTL.Size = new System.Drawing.Size(77, 17);
			this.rdoTKTL_TenTL.TabIndex = 11;
			this.rdoTKTL_TenTL.Text = "Tên tài liệu";
			this.rdoTKTL_TenTL.UseVisualStyleBackColor = true;
			this.rdoTKTL_TenTL.CheckedChanged += new System.EventHandler(this.rdoTKTL_TenTL_CheckedChanged);
			// 
			// rdoTKTL_MaTL
			// 
			this.rdoTKTL_MaTL.AutoSize = true;
			this.rdoTKTL_MaTL.Checked = true;
			this.rdoTKTL_MaTL.Location = new System.Drawing.Point(23, 30);
			this.rdoTKTL_MaTL.Name = "rdoTKTL_MaTL";
			this.rdoTKTL_MaTL.Size = new System.Drawing.Size(73, 17);
			this.rdoTKTL_MaTL.TabIndex = 10;
			this.rdoTKTL_MaTL.TabStop = true;
			this.rdoTKTL_MaTL.Text = "Mã tài liệu";
			this.rdoTKTL_MaTL.UseVisualStyleBackColor = true;
			this.rdoTKTL_MaTL.CheckedChanged += new System.EventHandler(this.rdoTKTL_MaTL_CheckedChanged);
			// 
			// btnLoaiTimKiem
			// 
			this.btnLoaiTimKiem.Location = new System.Drawing.Point(32, 117);
			this.btnLoaiTimKiem.Name = "btnLoaiTimKiem";
			this.btnLoaiTimKiem.Size = new System.Drawing.Size(290, 23);
			this.btnLoaiTimKiem.TabIndex = 9;
			this.btnLoaiTimKiem.Text = "Tìm kiếm nâng cao";
			this.btnLoaiTimKiem.UseVisualStyleBackColor = true;
			this.btnLoaiTimKiem.Click += new System.EventHandler(this.btnLoaiTimKiem_Click);
			// 
			// btnTKTL_TimKiem
			// 
			this.btnTKTL_TimKiem.Location = new System.Drawing.Point(32, 88);
			this.btnTKTL_TimKiem.Name = "btnTKTL_TimKiem";
			this.btnTKTL_TimKiem.Size = new System.Drawing.Size(290, 23);
			this.btnTKTL_TimKiem.TabIndex = 8;
			this.btnTKTL_TimKiem.Text = "Tìm kiếm";
			this.btnTKTL_TimKiem.UseVisualStyleBackColor = true;
			this.btnTKTL_TimKiem.Click += new System.EventHandler(this.btnTKTL_TimKiem_Click);
			// 
			// txtTKTL_TenTL
			// 
			this.txtTKTL_TenTL.Enabled = false;
			this.txtTKTL_TenTL.Location = new System.Drawing.Point(117, 55);
			this.txtTKTL_TenTL.Name = "txtTKTL_TenTL";
			this.txtTKTL_TenTL.Size = new System.Drawing.Size(151, 20);
			this.txtTKTL_TenTL.TabIndex = 1;
			// 
			// txtTKTL_MaTL
			// 
			this.txtTKTL_MaTL.Location = new System.Drawing.Point(117, 29);
			this.txtTKTL_MaTL.Name = "txtTKTL_MaTL";
			this.txtTKTL_MaTL.Size = new System.Drawing.Size(151, 20);
			this.txtTKTL_MaTL.TabIndex = 0;
			// 
			// pnlChinhSua
			// 
			this.pnlChinhSua.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(0)))), ((int)(((byte)(64)))));
			this.pnlChinhSua.Controls.Add(this.btnChinhSua);
			this.pnlChinhSua.Location = new System.Drawing.Point(529, 454);
			this.pnlChinhSua.Name = "pnlChinhSua";
			this.pnlChinhSua.Size = new System.Drawing.Size(165, 53);
			this.pnlChinhSua.TabIndex = 17;
			this.pnlChinhSua.Visible = false;
			// 
			// btnChinhSua
			// 
			this.btnChinhSua.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(128)))));
			this.btnChinhSua.FlatAppearance.BorderColor = System.Drawing.Color.Black;
			this.btnChinhSua.FlatAppearance.BorderSize = 2;
			this.btnChinhSua.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
			this.btnChinhSua.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.btnChinhSua.ForeColor = System.Drawing.Color.Black;
			this.btnChinhSua.Location = new System.Drawing.Point(3, 9);
			this.btnChinhSua.Name = "btnChinhSua";
			this.btnChinhSua.Size = new System.Drawing.Size(158, 34);
			this.btnChinhSua.TabIndex = 18;
			this.btnChinhSua.Text = "Chỉnh sửa";
			this.btnChinhSua.UseVisualStyleBackColor = false;
			this.btnChinhSua.Click += new System.EventHandler(this.btnChinhSua_Click);
			// 
			// pnlEdit
			// 
			this.pnlEdit.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(64)))), ((int)(((byte)(0)))), ((int)(((byte)(64)))));
			this.pnlEdit.Controls.Add(this.btnLuu);
			this.pnlEdit.Controls.Add(this.btnHuyChinhSua);
			this.pnlEdit.Location = new System.Drawing.Point(163, 455);
			this.pnlEdit.Name = "pnlEdit";
			this.pnlEdit.Size = new System.Drawing.Size(359, 53);
			this.pnlEdit.TabIndex = 19;
			this.pnlEdit.Visible = false;
			// 
			// btnLuu
			// 
			this.btnLuu.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(128)))));
			this.btnLuu.FlatAppearance.BorderColor = System.Drawing.Color.Black;
			this.btnLuu.FlatAppearance.BorderSize = 2;
			this.btnLuu.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
			this.btnLuu.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.btnLuu.ForeColor = System.Drawing.Color.Black;
			this.btnLuu.Location = new System.Drawing.Point(42, 9);
			this.btnLuu.Name = "btnLuu";
			this.btnLuu.Size = new System.Drawing.Size(89, 34);
			this.btnLuu.TabIndex = 20;
			this.btnLuu.Text = "Lưu";
			this.btnLuu.UseVisualStyleBackColor = false;
			this.btnLuu.Click += new System.EventHandler(this.btnLuu_Click);
			// 
			// btnHuyChinhSua
			// 
			this.btnHuyChinhSua.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(255)))), ((int)(((byte)(192)))), ((int)(((byte)(128)))));
			this.btnHuyChinhSua.FlatAppearance.BorderColor = System.Drawing.Color.Black;
			this.btnHuyChinhSua.FlatAppearance.BorderSize = 2;
			this.btnHuyChinhSua.FlatStyle = System.Windows.Forms.FlatStyle.Flat;
			this.btnHuyChinhSua.Font = new System.Drawing.Font("Microsoft Sans Serif", 9.75F, System.Drawing.FontStyle.Regular, System.Drawing.GraphicsUnit.Point, ((byte)(0)));
			this.btnHuyChinhSua.ForeColor = System.Drawing.Color.Black;
			this.btnHuyChinhSua.Location = new System.Drawing.Point(212, 9);
			this.btnHuyChinhSua.Name = "btnHuyChinhSua";
			this.btnHuyChinhSua.Size = new System.Drawing.Size(79, 34);
			this.btnHuyChinhSua.TabIndex = 19;
			this.btnHuyChinhSua.Text = "Hủy";
			this.btnHuyChinhSua.UseVisualStyleBackColor = false;
			this.btnHuyChinhSua.Click += new System.EventHandler(this.btnHuyChinhSua_Click);
			// 
			// pnlChiTietDG
			// 
			this.pnlChiTietDG.BackColor = System.Drawing.Color.Silver;
			this.pnlChiTietDG.Controls.Add(this.groupBox13);
			this.pnlChiTietDG.Location = new System.Drawing.Point(164, 55);
			this.pnlChiTietDG.Name = "pnlChiTietDG";
			this.pnlChiTietDG.Size = new System.Drawing.Size(694, 398);
			this.pnlChiTietDG.TabIndex = 25;
			this.pnlChiTietDG.Visible = false;
			// 
			// groupBox13
			// 
			this.groupBox13.Controls.Add(this.dgvChiTietDG);
			this.groupBox13.Location = new System.Drawing.Point(13, 16);
			this.groupBox13.Name = "groupBox13";
			this.groupBox13.Size = new System.Drawing.Size(669, 369);
			this.groupBox13.TabIndex = 0;
			this.groupBox13.TabStop = false;
			this.groupBox13.Text = "Chi tiết độc giả";
			// 
			// dgvChiTietDG
			// 
			this.dgvChiTietDG.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
			this.dgvChiTietDG.Location = new System.Drawing.Point(9, 15);
			this.dgvChiTietDG.Name = "dgvChiTietDG";
			this.dgvChiTietDG.ReadOnly = true;
			this.dgvChiTietDG.Size = new System.Drawing.Size(654, 348);
			this.dgvChiTietDG.TabIndex = 0;
			// 
			// pnlChiTietTL
			// 
			this.pnlChiTietTL.BackColor = System.Drawing.Color.Silver;
			this.pnlChiTietTL.Controls.Add(this.groupBox14);
			this.pnlChiTietTL.Location = new System.Drawing.Point(165, 54);
			this.pnlChiTietTL.Name = "pnlChiTietTL";
			this.pnlChiTietTL.Size = new System.Drawing.Size(694, 398);
			this.pnlChiTietTL.TabIndex = 26;
			this.pnlChiTietTL.Visible = false;
			// 
			// groupBox14
			// 
			this.groupBox14.Controls.Add(this.dgvChiTietTL);
			this.groupBox14.Location = new System.Drawing.Point(13, 16);
			this.groupBox14.Name = "groupBox14";
			this.groupBox14.Size = new System.Drawing.Size(669, 369);
			this.groupBox14.TabIndex = 0;
			this.groupBox14.TabStop = false;
			this.groupBox14.Text = "Chi tiết tài liệu";
			// 
			// dgvChiTietTL
			// 
			this.dgvChiTietTL.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
			this.dgvChiTietTL.Location = new System.Drawing.Point(9, 15);
			this.dgvChiTietTL.Name = "dgvChiTietTL";
			this.dgvChiTietTL.ReadOnly = true;
			this.dgvChiTietTL.Size = new System.Drawing.Size(654, 348);
			this.dgvChiTietTL.TabIndex = 0;
			// 
			// pnlChiTietPM
			// 
			this.pnlChiTietPM.BackColor = System.Drawing.Color.Silver;
			this.pnlChiTietPM.Controls.Add(this.groupBox16);
			this.pnlChiTietPM.Location = new System.Drawing.Point(164, 54);
			this.pnlChiTietPM.Name = "pnlChiTietPM";
			this.pnlChiTietPM.Size = new System.Drawing.Size(694, 398);
			this.pnlChiTietPM.TabIndex = 26;
			this.pnlChiTietPM.Visible = false;
			// 
			// groupBox16
			// 
			this.groupBox16.Controls.Add(this.dgvChiTietPM);
			this.groupBox16.Location = new System.Drawing.Point(13, 16);
			this.groupBox16.Name = "groupBox16";
			this.groupBox16.Size = new System.Drawing.Size(669, 369);
			this.groupBox16.TabIndex = 0;
			this.groupBox16.TabStop = false;
			this.groupBox16.Text = "Chi tiết phiếu mượn";
			// 
			// dgvChiTietPM
			// 
			this.dgvChiTietPM.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
			this.dgvChiTietPM.Location = new System.Drawing.Point(9, 15);
			this.dgvChiTietPM.Name = "dgvChiTietPM";
			this.dgvChiTietPM.ReadOnly = true;
			this.dgvChiTietPM.Size = new System.Drawing.Size(654, 348);
			this.dgvChiTietPM.TabIndex = 0;
			// 
			// pnlChiTietPT
			// 
			this.pnlChiTietPT.BackColor = System.Drawing.Color.Silver;
			this.pnlChiTietPT.Controls.Add(this.groupBox18);
			this.pnlChiTietPT.Location = new System.Drawing.Point(164, 54);
			this.pnlChiTietPT.Name = "pnlChiTietPT";
			this.pnlChiTietPT.Size = new System.Drawing.Size(694, 398);
			this.pnlChiTietPT.TabIndex = 30;
			this.pnlChiTietPT.Visible = false;
			// 
			// groupBox18
			// 
			this.groupBox18.Controls.Add(this.dgvChiTietPT);
			this.groupBox18.Location = new System.Drawing.Point(13, 16);
			this.groupBox18.Name = "groupBox18";
			this.groupBox18.Size = new System.Drawing.Size(669, 369);
			this.groupBox18.TabIndex = 0;
			this.groupBox18.TabStop = false;
			this.groupBox18.Text = "Chi tiết phiếu trả";
			// 
			// dgvChiTietPT
			// 
			this.dgvChiTietPT.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
			this.dgvChiTietPT.Location = new System.Drawing.Point(9, 15);
			this.dgvChiTietPT.Name = "dgvChiTietPT";
			this.dgvChiTietPT.ReadOnly = true;
			this.dgvChiTietPT.Size = new System.Drawing.Size(654, 348);
			this.dgvChiTietPT.TabIndex = 0;
			// 
			// pnlBaoBieu
			// 
			this.pnlBaoBieu.BackColor = System.Drawing.Color.Silver;
			this.pnlBaoBieu.Controls.Add(this.groupBox19);
			this.pnlBaoBieu.Location = new System.Drawing.Point(164, 52);
			this.pnlBaoBieu.Name = "pnlBaoBieu";
			this.pnlBaoBieu.Size = new System.Drawing.Size(694, 401);
			this.pnlBaoBieu.TabIndex = 26;
			this.pnlBaoBieu.Visible = false;
			// 
			// groupBox19
			// 
			this.groupBox19.Controls.Add(this.cboDSTL_Nam);
			this.groupBox19.Controls.Add(this.cboDSDG_Nam);
			this.groupBox19.Controls.Add(this.cboDSTL_TenDG);
			this.groupBox19.Controls.Add(this.cboDSTL_Thang);
			this.groupBox19.Controls.Add(this.cboDSDG_Thang);
			this.groupBox19.Controls.Add(this.btnLapBaoBieu);
			this.groupBox19.Controls.Add(this.rdoDSTLMaDG);
			this.groupBox19.Controls.Add(this.rdoDSTLThang);
			this.groupBox19.Controls.Add(this.rdoDSDGThang);
			this.groupBox19.Location = new System.Drawing.Point(12, 9);
			this.groupBox19.Name = "groupBox19";
			this.groupBox19.Size = new System.Drawing.Size(669, 182);
			this.groupBox19.TabIndex = 0;
			this.groupBox19.TabStop = false;
			this.groupBox19.Text = "Báo biểu";
			// 
			// cboDSTL_Nam
			// 
			this.cboDSTL_Nam.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
			this.cboDSTL_Nam.Enabled = false;
			this.cboDSTL_Nam.FormattingEnabled = true;
			this.cboDSTL_Nam.Location = new System.Drawing.Point(394, 78);
			this.cboDSTL_Nam.Name = "cboDSTL_Nam";
			this.cboDSTL_Nam.Size = new System.Drawing.Size(80, 21);
			this.cboDSTL_Nam.TabIndex = 11;
			// 
			// cboDSDG_Nam
			// 
			this.cboDSDG_Nam.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
			this.cboDSDG_Nam.FormattingEnabled = true;
			this.cboDSDG_Nam.Location = new System.Drawing.Point(394, 33);
			this.cboDSDG_Nam.Name = "cboDSDG_Nam";
			this.cboDSDG_Nam.Size = new System.Drawing.Size(80, 21);
			this.cboDSDG_Nam.TabIndex = 10;
			// 
			// cboDSTL_TenDG
			// 
			this.cboDSTL_TenDG.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
			this.cboDSTL_TenDG.Enabled = false;
			this.cboDSTL_TenDG.FormattingEnabled = true;
			this.cboDSTL_TenDG.Location = new System.Drawing.Point(307, 123);
			this.cboDSTL_TenDG.Name = "cboDSTL_TenDG";
			this.cboDSTL_TenDG.Size = new System.Drawing.Size(167, 21);
			this.cboDSTL_TenDG.TabIndex = 9;
			// 
			// cboDSTL_Thang
			// 
			this.cboDSTL_Thang.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
			this.cboDSTL_Thang.Enabled = false;
			this.cboDSTL_Thang.FormattingEnabled = true;
			this.cboDSTL_Thang.Location = new System.Drawing.Point(307, 78);
			this.cboDSTL_Thang.Name = "cboDSTL_Thang";
			this.cboDSTL_Thang.Size = new System.Drawing.Size(65, 21);
			this.cboDSTL_Thang.TabIndex = 8;
			// 
			// cboDSDG_Thang
			// 
			this.cboDSDG_Thang.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
			this.cboDSDG_Thang.FormattingEnabled = true;
			this.cboDSDG_Thang.Location = new System.Drawing.Point(307, 33);
			this.cboDSDG_Thang.Name = "cboDSDG_Thang";
			this.cboDSDG_Thang.Size = new System.Drawing.Size(65, 21);
			this.cboDSDG_Thang.TabIndex = 7;
			// 
			// btnLapBaoBieu
			// 
			this.btnLapBaoBieu.Location = new System.Drawing.Point(515, 59);
			this.btnLapBaoBieu.Name = "btnLapBaoBieu";
			this.btnLapBaoBieu.Size = new System.Drawing.Size(102, 55);
			this.btnLapBaoBieu.TabIndex = 6;
			this.btnLapBaoBieu.Text = "Lập báo biểu";
			this.btnLapBaoBieu.UseVisualStyleBackColor = true;
			this.btnLapBaoBieu.Click += new System.EventHandler(this.btnLapBaoBieu_Click);
			// 
			// rdoDSTLMaDG
			// 
			this.rdoDSTLMaDG.AutoSize = true;
			this.rdoDSTLMaDG.Location = new System.Drawing.Point(33, 127);
			this.rdoDSTLMaDG.Name = "rdoDSTLMaDG";
			this.rdoDSTLMaDG.Size = new System.Drawing.Size(215, 17);
			this.rdoDSTLMaDG.TabIndex = 2;
			this.rdoDSTLMaDG.TabStop = true;
			this.rdoDSTLMaDG.Text = "Danh sách tài liệu đã mượn của độc giả";
			this.rdoDSTLMaDG.UseVisualStyleBackColor = true;
			this.rdoDSTLMaDG.CheckedChanged += new System.EventHandler(this.rdoDSTLMaDG_CheckedChanged);
			// 
			// rdoDSTLThang
			// 
			this.rdoDSTLThang.AutoSize = true;
			this.rdoDSTLThang.Location = new System.Drawing.Point(33, 81);
			this.rdoDSTLThang.Name = "rdoDSTLThang";
			this.rdoDSTLThang.Size = new System.Drawing.Size(215, 17);
			this.rdoDSTLThang.TabIndex = 1;
			this.rdoDSTLThang.TabStop = true;
			this.rdoDSTLThang.Text = "Danh sách tài liệu đã mượn trong tháng:";
			this.rdoDSTLThang.UseVisualStyleBackColor = true;
			this.rdoDSTLThang.CheckedChanged += new System.EventHandler(this.rdoDSTLThang_CheckedChanged);
			// 
			// rdoDSDGThang
			// 
			this.rdoDSDGThang.AutoSize = true;
			this.rdoDSDGThang.Checked = true;
			this.rdoDSDGThang.Location = new System.Drawing.Point(33, 37);
			this.rdoDSDGThang.Name = "rdoDSDGThang";
			this.rdoDSDGThang.Size = new System.Drawing.Size(218, 17);
			this.rdoDSDGThang.TabIndex = 0;
			this.rdoDSDGThang.TabStop = true;
			this.rdoDSDGThang.Text = "Danh sách độc giả đăng ký trong tháng:";
			this.rdoDSDGThang.UseVisualStyleBackColor = true;
			this.rdoDSDGThang.CheckedChanged += new System.EventHandler(this.rdoDSDGThang_CheckedChanged);
			// 
			// pnlTaiKhoan
			// 
			this.pnlTaiKhoan.BackColor = System.Drawing.Color.Silver;
			this.pnlTaiKhoan.Controls.Add(this.pnlAdmin);
			this.pnlTaiKhoan.Controls.Add(this.btnAdmin);
			this.pnlTaiKhoan.Controls.Add(this.gbxMatKhau);
			this.pnlTaiKhoan.Controls.Add(this.gbxThongTin);
			this.pnlTaiKhoan.Controls.Add(this.btnDoiMK);
			this.pnlTaiKhoan.Controls.Add(this.btnDoiHoTen);
			this.pnlTaiKhoan.Controls.Add(this.groupBox20);
			this.pnlTaiKhoan.Location = new System.Drawing.Point(164, 54);
			this.pnlTaiKhoan.Name = "pnlTaiKhoan";
			this.pnlTaiKhoan.Size = new System.Drawing.Size(694, 401);
			this.pnlTaiKhoan.TabIndex = 27;
			this.pnlTaiKhoan.Visible = false;
			// 
			// pnlAdmin
			// 
			this.pnlAdmin.BackColor = System.Drawing.Color.Silver;
			this.pnlAdmin.Controls.Add(this.pnlThemTL);
			this.pnlAdmin.Controls.Add(this.gbxThemLDG);
			this.pnlAdmin.Controls.Add(this.gbxDoiQD);
			this.pnlAdmin.Controls.Add(this.btnAdmin_ThemTL);
			this.pnlAdmin.Controls.Add(this.btnAdmin_TaiKhoan);
			this.pnlAdmin.Location = new System.Drawing.Point(0, 167);
			this.pnlAdmin.Name = "pnlAdmin";
			this.pnlAdmin.Size = new System.Drawing.Size(691, 230);
			this.pnlAdmin.TabIndex = 28;
			this.pnlAdmin.Visible = false;
			// 
			// pnlThemTL
			// 
			this.pnlThemTL.Controls.Add(this.groupBox21);
			this.pnlThemTL.Controls.Add(this.dgvThemTL);
			this.pnlThemTL.Location = new System.Drawing.Point(1, 0);
			this.pnlThemTL.Name = "pnlThemTL";
			this.pnlThemTL.Size = new System.Drawing.Size(689, 176);
			this.pnlThemTL.TabIndex = 9;
			this.pnlThemTL.Visible = false;
			// 
			// groupBox21
			// 
			this.groupBox21.Controls.Add(this.btnThemTL);
			this.groupBox21.Controls.Add(this.cboThemLinhVuc);
			this.groupBox21.Controls.Add(this.cboLoaiTL);
			this.groupBox21.Controls.Add(this.cboMaNguonTL);
			this.groupBox21.Controls.Add(this.cboTenTL);
			this.groupBox21.Controls.Add(this.label25);
			this.groupBox21.Controls.Add(this.label24);
			this.groupBox21.Controls.Add(this.label23);
			this.groupBox21.Controls.Add(this.label22);
			this.groupBox21.Location = new System.Drawing.Point(336, 4);
			this.groupBox21.Name = "groupBox21";
			this.groupBox21.Size = new System.Drawing.Size(350, 166);
			this.groupBox21.TabIndex = 1;
			this.groupBox21.TabStop = false;
			this.groupBox21.Text = "Thông tin tài liệu";
			// 
			// btnThemTL
			// 
			this.btnThemTL.Location = new System.Drawing.Point(210, 132);
			this.btnThemTL.Name = "btnThemTL";
			this.btnThemTL.Size = new System.Drawing.Size(103, 23);
			this.btnThemTL.TabIndex = 8;
			this.btnThemTL.Text = "Thêm tài liệu";
			this.btnThemTL.UseVisualStyleBackColor = true;
			this.btnThemTL.Click += new System.EventHandler(this.btnThemTL_Click);
			// 
			// cboThemLinhVuc
			// 
			this.cboThemLinhVuc.FormattingEnabled = true;
			this.cboThemLinhVuc.Location = new System.Drawing.Point(192, 97);
			this.cboThemLinhVuc.Name = "cboThemLinhVuc";
			this.cboThemLinhVuc.Size = new System.Drawing.Size(121, 21);
			this.cboThemLinhVuc.TabIndex = 7;
			// 
			// cboLoaiTL
			// 
			this.cboLoaiTL.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
			this.cboLoaiTL.FormattingEnabled = true;
			this.cboLoaiTL.Location = new System.Drawing.Point(17, 97);
			this.cboLoaiTL.Name = "cboLoaiTL";
			this.cboLoaiTL.Size = new System.Drawing.Size(121, 21);
			this.cboLoaiTL.TabIndex = 6;
			// 
			// cboMaNguonTL
			// 
			this.cboMaNguonTL.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
			this.cboMaNguonTL.FormattingEnabled = true;
			this.cboMaNguonTL.Location = new System.Drawing.Point(192, 46);
			this.cboMaNguonTL.Name = "cboMaNguonTL";
			this.cboMaNguonTL.Size = new System.Drawing.Size(121, 21);
			this.cboMaNguonTL.TabIndex = 5;
			// 
			// cboTenTL
			// 
			this.cboTenTL.FormattingEnabled = true;
			this.cboTenTL.Location = new System.Drawing.Point(17, 46);
			this.cboTenTL.Name = "cboTenTL";
			this.cboTenTL.Size = new System.Drawing.Size(121, 21);
			this.cboTenTL.TabIndex = 4;
			// 
			// label25
			// 
			this.label25.AutoSize = true;
			this.label25.Location = new System.Drawing.Point(189, 77);
			this.label25.Name = "label25";
			this.label25.Size = new System.Drawing.Size(51, 13);
			this.label25.TabIndex = 3;
			this.label25.Text = "Lĩnh vực";
			// 
			// label24
			// 
			this.label24.AutoSize = true;
			this.label24.Location = new System.Drawing.Point(14, 77);
			this.label24.Name = "label24";
			this.label24.Size = new System.Drawing.Size(60, 13);
			this.label24.TabIndex = 2;
			this.label24.Text = "Loại tài liệu";
			// 
			// label23
			// 
			this.label23.AutoSize = true;
			this.label23.Location = new System.Drawing.Point(189, 23);
			this.label23.Name = "label23";
			this.label23.Size = new System.Drawing.Size(88, 13);
			this.label23.TabIndex = 1;
			this.label23.Text = "Mã nguồn tài liệu";
			// 
			// label22
			// 
			this.label22.AutoSize = true;
			this.label22.Location = new System.Drawing.Point(14, 23);
			this.label22.Name = "label22";
			this.label22.Size = new System.Drawing.Size(59, 13);
			this.label22.TabIndex = 0;
			this.label22.Text = "Tên tài liệu";
			// 
			// dgvThemTL
			// 
			this.dgvThemTL.ColumnHeadersHeightSizeMode = System.Windows.Forms.DataGridViewColumnHeadersHeightSizeMode.AutoSize;
			this.dgvThemTL.Location = new System.Drawing.Point(3, 4);
			this.dgvThemTL.Name = "dgvThemTL";
			this.dgvThemTL.ReadOnly = true;
			this.dgvThemTL.Size = new System.Drawing.Size(326, 166);
			this.dgvThemTL.TabIndex = 0;
			// 
			// gbxThemLDG
			// 
			this.gbxThemLDG.Controls.Add(this.label17);
			this.gbxThemLDG.Controls.Add(this.txtThemLDG_SoNgayToiDa);
			this.gbxThemLDG.Controls.Add(this.label16);
			this.gbxThemLDG.Controls.Add(this.txtThemLDG_PhiThuongNien);
			this.gbxThemLDG.Controls.Add(this.label3);
			this.gbxThemLDG.Controls.Add(this.txtThemLDG_SoSachToiDa);
			this.gbxThemLDG.Controls.Add(this.label2);
			this.gbxThemLDG.Controls.Add(this.txtThemLDG_LoaiDG);
			this.gbxThemLDG.Controls.Add(this.btnAdmin_ThemLoaiDG);
			this.gbxThemLDG.Location = new System.Drawing.Point(10, 4);
			this.gbxThemLDG.Name = "gbxThemLDG";
			this.gbxThemLDG.Size = new System.Drawing.Size(334, 172);
			this.gbxThemLDG.TabIndex = 4;
			this.gbxThemLDG.TabStop = false;
			this.gbxThemLDG.Text = "Thêm loại độc giả";
			// 
			// label17
			// 
			this.label17.AutoSize = true;
			this.label17.Location = new System.Drawing.Point(174, 69);
			this.label17.Name = "label17";
			this.label17.Size = new System.Drawing.Size(105, 13);
			this.label17.TabIndex = 8;
			this.label17.Text = "Số ngày mượn tối đa";
			// 
			// txtThemLDG_SoNgayToiDa
			// 
			this.txtThemLDG_SoNgayToiDa.Location = new System.Drawing.Point(176, 86);
			this.txtThemLDG_SoNgayToiDa.Name = "txtThemLDG_SoNgayToiDa";
			this.txtThemLDG_SoNgayToiDa.Size = new System.Drawing.Size(151, 20);
			this.txtThemLDG_SoNgayToiDa.TabIndex = 7;
			// 
			// label16
			// 
			this.label16.AutoSize = true;
			this.label16.Location = new System.Drawing.Point(174, 26);
			this.label16.Name = "label16";
			this.label16.Size = new System.Drawing.Size(83, 13);
			this.label16.TabIndex = 6;
			this.label16.Text = "Phí thường niên";
			// 
			// txtThemLDG_PhiThuongNien
			// 
			this.txtThemLDG_PhiThuongNien.Location = new System.Drawing.Point(176, 43);
			this.txtThemLDG_PhiThuongNien.Name = "txtThemLDG_PhiThuongNien";
			this.txtThemLDG_PhiThuongNien.Size = new System.Drawing.Size(151, 20);
			this.txtThemLDG_PhiThuongNien.TabIndex = 5;
			// 
			// label3
			// 
			this.label3.AutoSize = true;
			this.label3.Location = new System.Drawing.Point(8, 69);
			this.label3.Name = "label3";
			this.label3.Size = new System.Drawing.Size(105, 13);
			this.label3.TabIndex = 4;
			this.label3.Text = "Số sách mượn tối đa";
			// 
			// txtThemLDG_SoSachToiDa
			// 
			this.txtThemLDG_SoSachToiDa.Location = new System.Drawing.Point(10, 86);
			this.txtThemLDG_SoSachToiDa.Name = "txtThemLDG_SoSachToiDa";
			this.txtThemLDG_SoSachToiDa.Size = new System.Drawing.Size(151, 20);
			this.txtThemLDG_SoSachToiDa.TabIndex = 3;
			// 
			// label2
			// 
			this.label2.AutoSize = true;
			this.label2.Location = new System.Drawing.Point(8, 26);
			this.label2.Name = "label2";
			this.label2.Size = new System.Drawing.Size(66, 13);
			this.label2.TabIndex = 2;
			this.label2.Text = "Loại độc giả";
			// 
			// txtThemLDG_LoaiDG
			// 
			this.txtThemLDG_LoaiDG.Location = new System.Drawing.Point(10, 43);
			this.txtThemLDG_LoaiDG.Name = "txtThemLDG_LoaiDG";
			this.txtThemLDG_LoaiDG.Size = new System.Drawing.Size(151, 20);
			this.txtThemLDG_LoaiDG.TabIndex = 1;
			// 
			// btnAdmin_ThemLoaiDG
			// 
			this.btnAdmin_ThemLoaiDG.Location = new System.Drawing.Point(9, 127);
			this.btnAdmin_ThemLoaiDG.Name = "btnAdmin_ThemLoaiDG";
			this.btnAdmin_ThemLoaiDG.Size = new System.Drawing.Size(95, 23);
			this.btnAdmin_ThemLoaiDG.TabIndex = 0;
			this.btnAdmin_ThemLoaiDG.Text = "Thêm";
			this.btnAdmin_ThemLoaiDG.UseVisualStyleBackColor = true;
			this.btnAdmin_ThemLoaiDG.Click += new System.EventHandler(this.btnAdmin_ThemLoaiDG_Click);
			// 
			// gbxDoiQD
			// 
			this.gbxDoiQD.Controls.Add(this.cboDoiQD_LoaiDG);
			this.gbxDoiQD.Controls.Add(this.label18);
			this.gbxDoiQD.Controls.Add(this.txtDoiQD_SoNgayToiDa);
			this.gbxDoiQD.Controls.Add(this.label19);
			this.gbxDoiQD.Controls.Add(this.txtDoiQD_PhiThuongNien);
			this.gbxDoiQD.Controls.Add(this.label20);
			this.gbxDoiQD.Controls.Add(this.txtDoiQD_SoSachToiDa);
			this.gbxDoiQD.Controls.Add(this.label21);
			this.gbxDoiQD.Controls.Add(this.btnAdmin_ThayDoiQD);
			this.gbxDoiQD.Location = new System.Drawing.Point(350, 4);
			this.gbxDoiQD.Name = "gbxDoiQD";
			this.gbxDoiQD.Size = new System.Drawing.Size(333, 172);
			this.gbxDoiQD.TabIndex = 5;
			this.gbxDoiQD.TabStop = false;
			this.gbxDoiQD.Text = "Thay đổi quy định";
			// 
			// cboDoiQD_LoaiDG
			// 
			this.cboDoiQD_LoaiDG.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
			this.cboDoiQD_LoaiDG.FormattingEnabled = true;
			this.cboDoiQD_LoaiDG.Location = new System.Drawing.Point(9, 44);
			this.cboDoiQD_LoaiDG.Name = "cboDoiQD_LoaiDG";
			this.cboDoiQD_LoaiDG.Size = new System.Drawing.Size(152, 21);
			this.cboDoiQD_LoaiDG.TabIndex = 9;
			this.cboDoiQD_LoaiDG.SelectedIndexChanged += new System.EventHandler(this.cboDoiQD_LoaiDG_SelectedIndexChanged);
			// 
			// label18
			// 
			this.label18.AutoSize = true;
			this.label18.Location = new System.Drawing.Point(174, 69);
			this.label18.Name = "label18";
			this.label18.Size = new System.Drawing.Size(105, 13);
			this.label18.TabIndex = 8;
			this.label18.Text = "Số ngày mượn tối đa";
			// 
			// txtDoiQD_SoNgayToiDa
			// 
			this.txtDoiQD_SoNgayToiDa.Location = new System.Drawing.Point(176, 86);
			this.txtDoiQD_SoNgayToiDa.Name = "txtDoiQD_SoNgayToiDa";
			this.txtDoiQD_SoNgayToiDa.Size = new System.Drawing.Size(151, 20);
			this.txtDoiQD_SoNgayToiDa.TabIndex = 7;
			// 
			// label19
			// 
			this.label19.AutoSize = true;
			this.label19.Location = new System.Drawing.Point(174, 26);
			this.label19.Name = "label19";
			this.label19.Size = new System.Drawing.Size(83, 13);
			this.label19.TabIndex = 6;
			this.label19.Text = "Phí thường niên";
			// 
			// txtDoiQD_PhiThuongNien
			// 
			this.txtDoiQD_PhiThuongNien.Location = new System.Drawing.Point(176, 43);
			this.txtDoiQD_PhiThuongNien.Name = "txtDoiQD_PhiThuongNien";
			this.txtDoiQD_PhiThuongNien.Size = new System.Drawing.Size(151, 20);
			this.txtDoiQD_PhiThuongNien.TabIndex = 5;
			// 
			// label20
			// 
			this.label20.AutoSize = true;
			this.label20.Location = new System.Drawing.Point(8, 69);
			this.label20.Name = "label20";
			this.label20.Size = new System.Drawing.Size(105, 13);
			this.label20.TabIndex = 4;
			this.label20.Text = "Số sách mượn tối đa";
			// 
			// txtDoiQD_SoSachToiDa
			// 
			this.txtDoiQD_SoSachToiDa.Location = new System.Drawing.Point(10, 86);
			this.txtDoiQD_SoSachToiDa.Name = "txtDoiQD_SoSachToiDa";
			this.txtDoiQD_SoSachToiDa.Size = new System.Drawing.Size(151, 20);
			this.txtDoiQD_SoSachToiDa.TabIndex = 3;
			// 
			// label21
			// 
			this.label21.AutoSize = true;
			this.label21.Location = new System.Drawing.Point(8, 26);
			this.label21.Name = "label21";
			this.label21.Size = new System.Drawing.Size(66, 13);
			this.label21.TabIndex = 2;
			this.label21.Text = "Loại độc giả";
			// 
			// btnAdmin_ThayDoiQD
			// 
			this.btnAdmin_ThayDoiQD.Location = new System.Drawing.Point(9, 127);
			this.btnAdmin_ThayDoiQD.Name = "btnAdmin_ThayDoiQD";
			this.btnAdmin_ThayDoiQD.Size = new System.Drawing.Size(95, 23);
			this.btnAdmin_ThayDoiQD.TabIndex = 0;
			this.btnAdmin_ThayDoiQD.Text = "Thay đổi";
			this.btnAdmin_ThayDoiQD.UseVisualStyleBackColor = true;
			this.btnAdmin_ThayDoiQD.Click += new System.EventHandler(this.btnAdmin_ThayDoiQD_Click);
			// 
			// btnAdmin_ThemTL
			// 
			this.btnAdmin_ThemTL.Location = new System.Drawing.Point(514, 179);
			this.btnAdmin_ThemTL.Name = "btnAdmin_ThemTL";
			this.btnAdmin_ThemTL.Size = new System.Drawing.Size(161, 33);
			this.btnAdmin_ThemTL.TabIndex = 10;
			this.btnAdmin_ThemTL.Text = "Thêm tài liệu";
			this.btnAdmin_ThemTL.UseVisualStyleBackColor = true;
			this.btnAdmin_ThemTL.Click += new System.EventHandler(this.btnAdmin_ThemTL_Click);
			// 
			// btnAdmin_TaiKhoan
			// 
			this.btnAdmin_TaiKhoan.Location = new System.Drawing.Point(22, 179);
			this.btnAdmin_TaiKhoan.Name = "btnAdmin_TaiKhoan";
			this.btnAdmin_TaiKhoan.Size = new System.Drawing.Size(161, 33);
			this.btnAdmin_TaiKhoan.TabIndex = 2;
			this.btnAdmin_TaiKhoan.Text = "Tài khoản";
			this.btnAdmin_TaiKhoan.UseVisualStyleBackColor = true;
			this.btnAdmin_TaiKhoan.Click += new System.EventHandler(this.btnAdmin_TaiKhoan_Click);
			// 
			// btnAdmin
			// 
			this.btnAdmin.Location = new System.Drawing.Point(22, 346);
			this.btnAdmin.Name = "btnAdmin";
			this.btnAdmin.Size = new System.Drawing.Size(161, 33);
			this.btnAdmin.TabIndex = 1;
			this.btnAdmin.Text = "Admin";
			this.btnAdmin.UseVisualStyleBackColor = true;
			this.btnAdmin.Click += new System.EventHandler(this.btnAdmin_Click);
			// 
			// gbxMatKhau
			// 
			this.gbxMatKhau.Controls.Add(this.label1);
			this.gbxMatKhau.Controls.Add(this.lblMKMoi);
			this.gbxMatKhau.Controls.Add(this.btnMK_Doi);
			this.gbxMatKhau.Controls.Add(this.txtXacNhanMK);
			this.gbxMatKhau.Controls.Add(this.txtMKMoi);
			this.gbxMatKhau.Location = new System.Drawing.Point(9, 210);
			this.gbxMatKhau.Name = "gbxMatKhau";
			this.gbxMatKhau.Size = new System.Drawing.Size(325, 115);
			this.gbxMatKhau.TabIndex = 3;
			this.gbxMatKhau.TabStop = false;
			this.gbxMatKhau.Text = "Mật khẩu";
			this.gbxMatKhau.Visible = false;
			// 
			// label1
			// 
			this.label1.AutoSize = true;
			this.label1.Location = new System.Drawing.Point(8, 69);
			this.label1.Name = "label1";
			this.label1.Size = new System.Drawing.Size(100, 13);
			this.label1.TabIndex = 7;
			this.label1.Text = "Xác nhận mật khẩu";
			// 
			// lblMKMoi
			// 
			this.lblMKMoi.AutoSize = true;
			this.lblMKMoi.Location = new System.Drawing.Point(7, 25);
			this.lblMKMoi.Name = "lblMKMoi";
			this.lblMKMoi.Size = new System.Drawing.Size(71, 13);
			this.lblMKMoi.TabIndex = 6;
			this.lblMKMoi.Text = "Mật khẩu mới";
			// 
			// btnMK_Doi
			// 
			this.btnMK_Doi.Location = new System.Drawing.Point(201, 41);
			this.btnMK_Doi.Name = "btnMK_Doi";
			this.btnMK_Doi.Size = new System.Drawing.Size(118, 64);
			this.btnMK_Doi.TabIndex = 5;
			this.btnMK_Doi.Text = "Thay đổi";
			this.btnMK_Doi.UseVisualStyleBackColor = true;
			this.btnMK_Doi.Click += new System.EventHandler(this.btnMK_Doi_Click);
			// 
			// txtXacNhanMK
			// 
			this.txtXacNhanMK.Location = new System.Drawing.Point(10, 85);
			this.txtXacNhanMK.Name = "txtXacNhanMK";
			this.txtXacNhanMK.Size = new System.Drawing.Size(152, 20);
			this.txtXacNhanMK.TabIndex = 4;
			// 
			// txtMKMoi
			// 
			this.txtMKMoi.Location = new System.Drawing.Point(10, 41);
			this.txtMKMoi.Name = "txtMKMoi";
			this.txtMKMoi.Size = new System.Drawing.Size(152, 20);
			this.txtMKMoi.TabIndex = 3;
			// 
			// gbxThongTin
			// 
			this.gbxThongTin.Controls.Add(this.lblXacNhanHoTen);
			this.gbxThongTin.Controls.Add(this.lblHoTenMoi);
			this.gbxThongTin.Controls.Add(this.txtXacNhanHoTen);
			this.gbxThongTin.Controls.Add(this.txtHoTenMoi);
			this.gbxThongTin.Controls.Add(this.btnHoTen_Doi);
			this.gbxThongTin.Location = new System.Drawing.Point(353, 210);
			this.gbxThongTin.Name = "gbxThongTin";
			this.gbxThongTin.Size = new System.Drawing.Size(334, 115);
			this.gbxThongTin.TabIndex = 4;
			this.gbxThongTin.TabStop = false;
			this.gbxThongTin.Text = "Thông tin";
			this.gbxThongTin.Visible = false;
			// 
			// lblXacNhanHoTen
			// 
			this.lblXacNhanHoTen.AutoSize = true;
			this.lblXacNhanHoTen.Location = new System.Drawing.Point(9, 68);
			this.lblXacNhanHoTen.Name = "lblXacNhanHoTen";
			this.lblXacNhanHoTen.Size = new System.Drawing.Size(86, 13);
			this.lblXacNhanHoTen.TabIndex = 10;
			this.lblXacNhanHoTen.Text = "Xác nhận họ tên";
			// 
			// lblHoTenMoi
			// 
			this.lblHoTenMoi.AutoSize = true;
			this.lblHoTenMoi.Location = new System.Drawing.Point(9, 23);
			this.lblHoTenMoi.Name = "lblHoTenMoi";
			this.lblHoTenMoi.Size = new System.Drawing.Size(58, 13);
			this.lblHoTenMoi.TabIndex = 9;
			this.lblHoTenMoi.Text = "Họ tên mới";
			// 
			// txtXacNhanHoTen
			// 
			this.txtXacNhanHoTen.Location = new System.Drawing.Point(12, 84);
			this.txtXacNhanHoTen.Name = "txtXacNhanHoTen";
			this.txtXacNhanHoTen.Size = new System.Drawing.Size(152, 20);
			this.txtXacNhanHoTen.TabIndex = 8;
			// 
			// txtHoTenMoi
			// 
			this.txtHoTenMoi.Location = new System.Drawing.Point(12, 40);
			this.txtHoTenMoi.Name = "txtHoTenMoi";
			this.txtHoTenMoi.Size = new System.Drawing.Size(152, 20);
			this.txtHoTenMoi.TabIndex = 7;
			// 
			// btnHoTen_Doi
			// 
			this.btnHoTen_Doi.Location = new System.Drawing.Point(205, 41);
			this.btnHoTen_Doi.Name = "btnHoTen_Doi";
			this.btnHoTen_Doi.Size = new System.Drawing.Size(118, 64);
			this.btnHoTen_Doi.TabIndex = 6;
			this.btnHoTen_Doi.Text = "Thay đổi";
			this.btnHoTen_Doi.UseVisualStyleBackColor = true;
			this.btnHoTen_Doi.Click += new System.EventHandler(this.btnHoTen_Doi_Click);
			// 
			// btnDoiMK
			// 
			this.btnDoiMK.Location = new System.Drawing.Point(22, 166);
			this.btnDoiMK.Name = "btnDoiMK";
			this.btnDoiMK.Size = new System.Drawing.Size(305, 36);
			this.btnDoiMK.TabIndex = 2;
			this.btnDoiMK.Text = "Đổi mật khẩu";
			this.btnDoiMK.UseVisualStyleBackColor = true;
			this.btnDoiMK.Click += new System.EventHandler(this.btnDoiMK_Click);
			// 
			// btnDoiHoTen
			// 
			this.btnDoiHoTen.Location = new System.Drawing.Point(363, 166);
			this.btnDoiHoTen.Name = "btnDoiHoTen";
			this.btnDoiHoTen.Size = new System.Drawing.Size(312, 36);
			this.btnDoiHoTen.TabIndex = 2;
			this.btnDoiHoTen.Text = "Đổi họ tên";
			this.btnDoiHoTen.UseVisualStyleBackColor = true;
			this.btnDoiHoTen.Click += new System.EventHandler(this.btnDoiHoTen_Click);
			// 
			// groupBox20
			// 
			this.groupBox20.BackColor = System.Drawing.Color.Gainsboro;
			this.groupBox20.Controls.Add(this.lblTenTK);
			this.groupBox20.Controls.Add(this.lblHoTen);
			this.groupBox20.Controls.Add(this.pictureBox1);
			this.groupBox20.Location = new System.Drawing.Point(10, 6);
			this.groupBox20.Name = "groupBox20";
			this.groupBox20.Size = new System.Drawing.Size(677, 154);
			this.groupBox20.TabIndex = 0;
			this.groupBox20.TabStop = false;
			// 
			// lblTenTK
			// 
			this.lblTenTK.Font = new System.Drawing.Font("Tahoma", 14.25F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(163)));
			this.lblTenTK.ForeColor = System.Drawing.Color.BlueViolet;
			this.lblTenTK.Location = new System.Drawing.Point(182, 96);
			this.lblTenTK.Name = "lblTenTK";
			this.lblTenTK.Size = new System.Drawing.Size(430, 24);
			this.lblTenTK.TabIndex = 2;
			this.lblTenTK.Text = "Tên tài khoản";
			this.lblTenTK.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
			// 
			// lblHoTen
			// 
			this.lblHoTen.Font = new System.Drawing.Font("Times New Roman", 24F, System.Drawing.FontStyle.Bold, System.Drawing.GraphicsUnit.Point, ((byte)(163)));
			this.lblHoTen.ForeColor = System.Drawing.Color.DarkSlateBlue;
			this.lblHoTen.Location = new System.Drawing.Point(180, 40);
			this.lblHoTen.Name = "lblHoTen";
			this.lblHoTen.Size = new System.Drawing.Size(432, 36);
			this.lblHoTen.TabIndex = 1;
			this.lblHoTen.Text = "Họ tên";
			this.lblHoTen.TextAlign = System.Drawing.ContentAlignment.MiddleCenter;
			// 
			// pictureBox1
			// 
			this.pictureBox1.Image = global::DoAnCK.Properties.Resources.account1;
			this.pictureBox1.Location = new System.Drawing.Point(10, 13);
			this.pictureBox1.Name = "pictureBox1";
			this.pictureBox1.Size = new System.Drawing.Size(125, 133);
			this.pictureBox1.TabIndex = 0;
			this.pictureBox1.TabStop = false;
			// 
			// cboTKTL_LoaiTL
			// 
			this.cboTKTL_LoaiTL.DropDownStyle = System.Windows.Forms.ComboBoxStyle.DropDownList;
			this.cboTKTL_LoaiTL.Enabled = false;
			this.cboTKTL_LoaiTL.FormattingEnabled = true;
			this.cboTKTL_LoaiTL.Location = new System.Drawing.Point(110, 7);
			this.cboTKTL_LoaiTL.Name = "cboTKTL_LoaiTL";
			this.cboTKTL_LoaiTL.Size = new System.Drawing.Size(151, 21);
			this.cboTKTL_LoaiTL.TabIndex = 17;
			// 
			// fMain
			// 
			this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
			this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
			this.BackColor = System.Drawing.Color.FromArgb(((int)(((byte)(224)))), ((int)(((byte)(224)))), ((int)(((byte)(224)))));
			this.ClientSize = new System.Drawing.Size(858, 505);
			this.Controls.Add(this.pnlTimKiemTL);
			this.Controls.Add(this.pnlTaiKhoan);
			this.Controls.Add(this.pnlTrangChu);
			this.Controls.Add(this.pnlTimKiemDG);
			this.Controls.Add(this.pnlBaoBieu);
			this.Controls.Add(this.pnlLapPM);
			this.Controls.Add(this.pnlLapPT);
			this.Controls.Add(this.pnlTimKiemPT);
			this.Controls.Add(this.pnlTimKiemPM);
			this.Controls.Add(this.pnlChiTietPT);
			this.Controls.Add(this.pnlChiTietPM);
			this.Controls.Add(this.pnlChiTietTL);
			this.Controls.Add(this.pnlEdit);
			this.Controls.Add(this.pnlChinhSua);
			this.Controls.Add(this.pnlChiTietDG);
			this.Controls.Add(this.pnlDangKyDG);
			this.Controls.Add(this.panelTitle);
			this.Controls.Add(this.panel1);
			this.Controls.Add(this.panel4);
			this.Controls.Add(this.panelFooter);
			this.FormBorderStyle = System.Windows.Forms.FormBorderStyle.None;
			this.Icon = ((System.Drawing.Icon)(resources.GetObject("$this.Icon")));
			this.Name = "fMain";
			this.StartPosition = System.Windows.Forms.FormStartPosition.CenterScreen;
			this.Text = "Quản lý thư viện";
			this.Load += new System.EventHandler(this.Form1_Load);
			this.panel1.ResumeLayout(false);
			this.panel3.ResumeLayout(false);
			this.panel4.ResumeLayout(false);
			this.panelTitle.ResumeLayout(false);
			this.panelTitle.PerformLayout();
			this.panelFooter.ResumeLayout(false);
			this.pnlTool.ResumeLayout(false);
			this.pnlTrangChu.ResumeLayout(false);
			this.groupBox27.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.pictureBox2)).EndInit();
			this.groupBox26.ResumeLayout(false);
			this.groupBox25.ResumeLayout(false);
			this.groupBox24.ResumeLayout(false);
			this.groupBox23.ResumeLayout(false);
			this.groupBox22.ResumeLayout(false);
			this.pnlDangKyDG.ResumeLayout(false);
			this.pnlDangKyDG.PerformLayout();
			this.groupBox2.ResumeLayout(false);
			this.groupBox2.PerformLayout();
			this.groupBox1.ResumeLayout(false);
			this.groupBox1.PerformLayout();
			this.pnlTimKiemDG.ResumeLayout(false);
			this.groupBox3.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.dgvDsdg)).EndInit();
			this.groupBox4.ResumeLayout(false);
			this.groupBox4.PerformLayout();
			this.pnlLapPM.ResumeLayout(false);
			this.groupBox15.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.dgvTaiLieuMuon)).EndInit();
			this.groupBox5.ResumeLayout(false);
			this.pnlTimKiemPM.ResumeLayout(false);
			this.groupBox6.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.dgvDSPM)).EndInit();
			this.groupBox7.ResumeLayout(false);
			this.groupBox7.PerformLayout();
			this.pnlLapPT.ResumeLayout(false);
			this.groupBox17.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.dgvTaiLieuDaMuon)).EndInit();
			this.groupBox8.ResumeLayout(false);
			this.pnlTimKiemPT.ResumeLayout(false);
			this.groupBox9.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.dgvDspt)).EndInit();
			this.groupBox10.ResumeLayout(false);
			this.groupBox10.PerformLayout();
			this.pnlTimKiemTL.ResumeLayout(false);
			this.groupBox11.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.dgvDstl)).EndInit();
			this.groupBox12.ResumeLayout(false);
			this.groupBox12.PerformLayout();
			this.pnlLoaiTimKiem.ResumeLayout(false);
			this.pnlLoaiTimKiem.PerformLayout();
			this.pnlChinhSua.ResumeLayout(false);
			this.pnlEdit.ResumeLayout(false);
			this.pnlChiTietDG.ResumeLayout(false);
			this.groupBox13.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.dgvChiTietDG)).EndInit();
			this.pnlChiTietTL.ResumeLayout(false);
			this.groupBox14.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.dgvChiTietTL)).EndInit();
			this.pnlChiTietPM.ResumeLayout(false);
			this.groupBox16.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.dgvChiTietPM)).EndInit();
			this.pnlChiTietPT.ResumeLayout(false);
			this.groupBox18.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.dgvChiTietPT)).EndInit();
			this.pnlBaoBieu.ResumeLayout(false);
			this.groupBox19.ResumeLayout(false);
			this.groupBox19.PerformLayout();
			this.pnlTaiKhoan.ResumeLayout(false);
			this.pnlAdmin.ResumeLayout(false);
			this.pnlThemTL.ResumeLayout(false);
			this.groupBox21.ResumeLayout(false);
			this.groupBox21.PerformLayout();
			((System.ComponentModel.ISupportInitialize)(this.dgvThemTL)).EndInit();
			this.gbxThemLDG.ResumeLayout(false);
			this.gbxThemLDG.PerformLayout();
			this.gbxDoiQD.ResumeLayout(false);
			this.gbxDoiQD.PerformLayout();
			this.gbxMatKhau.ResumeLayout(false);
			this.gbxMatKhau.PerformLayout();
			this.gbxThongTin.ResumeLayout(false);
			this.gbxThongTin.PerformLayout();
			this.groupBox20.ResumeLayout(false);
			((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
			this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.Panel panel1;
        private System.Windows.Forms.Panel panelTitle;
        private System.Windows.Forms.Panel panel3;
        private System.Windows.Forms.Label lblClose;
        private System.Windows.Forms.Label lblTitle;
        private System.Windows.Forms.Label lblMinimize;
        private System.Windows.Forms.Label label4;
        private System.Windows.Forms.Button btnPhieuTra;
        private System.Windows.Forms.Button btnPhieuMuon;
        private System.Windows.Forms.Panel panel4;
        private System.Windows.Forms.Button btnTrangChu;
        private System.Windows.Forms.Button btnDocGia;
        private System.Windows.Forms.Button btnThoat;
        private System.Windows.Forms.Panel panelFooter;
        private System.Windows.Forms.Panel pnlTrangChu;
        private System.Windows.Forms.Button btnTimPT;
        private System.Windows.Forms.Button btnTimPM;
        private System.Windows.Forms.Button btnLapPT;
        private System.Windows.Forms.Button btnLapPM;
        private System.Windows.Forms.Button btnTimTL;
        private System.Windows.Forms.Button btnTimDG;
        private System.Windows.Forms.Button btnDangKy;
        private System.Windows.Forms.Panel pnlDangKyDG;
        private System.Windows.Forms.Label label8;
        private System.Windows.Forms.TextBox txtMscb;
        private System.Windows.Forms.Label label7;
        private System.Windows.Forms.TextBox txtMssv;
        private System.Windows.Forms.Button btnHuy;
        private System.Windows.Forms.Button button1;
        private System.Windows.Forms.GroupBox groupBox2;
        private System.Windows.Forms.Label label11;
        private System.Windows.Forms.Label label10;
        private System.Windows.Forms.ComboBox cboThang;
        private System.Windows.Forms.ComboBox cboNgay;
        private System.Windows.Forms.Label label9;
        private System.Windows.Forms.Label label5;
        private System.Windows.Forms.Label label6;
        private System.Windows.Forms.TextBox txtDiaChi;
        private System.Windows.Forms.Label label12;
        private System.Windows.Forms.TextBox txtSdt;
        private System.Windows.Forms.Label label13;
        private System.Windows.Forms.TextBox txtNamSinh;
        private System.Windows.Forms.Label label14;
        private System.Windows.Forms.TextBox txtEmail;
        private System.Windows.Forms.Label label15;
        private System.Windows.Forms.TextBox txtHoTen;
        private System.Windows.Forms.TextBox txtCmnd;
        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.RadioButton rdoSinhVien;
        private System.Windows.Forms.RadioButton rdoKhach;
        private System.Windows.Forms.RadioButton rdoGiangVien;
        private System.Windows.Forms.Panel pnlTimKiemDG;
        private System.Windows.Forms.Button btnTKDG_DangKyDG;
        private System.Windows.Forms.Button btnLapPhieuCanhCao;
        private System.Windows.Forms.Button btnTKDG_LapPT;
        private System.Windows.Forms.Button btnTKDG_LapPM;
        private System.Windows.Forms.GroupBox groupBox3;
        private System.Windows.Forms.DataGridView dgvDsdg;
        private System.Windows.Forms.Button btnTKDG_Xemtatca;
        private System.Windows.Forms.GroupBox groupBox4;
        private System.Windows.Forms.Button btnTKDG_Timkiem;
        private System.Windows.Forms.TextBox txtTKDG_Maso;
        private System.Windows.Forms.TextBox txtTKDG_TenDG;
        private System.Windows.Forms.TextBox txtMaDG;
        private System.Windows.Forms.Panel pnlLapPM;
        private System.Windows.Forms.Button btnLPM_LapPhieu;
        private System.Windows.Forms.GroupBox groupBox5;
        private System.Windows.Forms.Panel pnlTimKiemPM;
        private System.Windows.Forms.Button btnTKPM_LapPT;
        private System.Windows.Forms.GroupBox groupBox6;
        private System.Windows.Forms.DataGridView dgvDSPM;
        private System.Windows.Forms.Button btnTKPM_LapPM;
        private System.Windows.Forms.Button btnTKPM_XemPMQH;
        private System.Windows.Forms.Button btnTKPM_Xemtatca;
        private System.Windows.Forms.GroupBox groupBox7;
        private System.Windows.Forms.Button btnTKPM_Timkiem;
        private System.Windows.Forms.TextBox txtTKPM_MaDG;
        private System.Windows.Forms.TextBox txtTKPM_MaPM;
        private System.Windows.Forms.Panel pnlLapPT;
        private System.Windows.Forms.Button btnLPT_LapPhieu;
        private System.Windows.Forms.GroupBox groupBox8;
        private System.Windows.Forms.Panel pnlTimKiemPT;
        private System.Windows.Forms.GroupBox groupBox9;
        private System.Windows.Forms.DataGridView dgvDspt;
        private System.Windows.Forms.Button btnTKPT_Xemtatca;
        private System.Windows.Forms.GroupBox groupBox10;
        private System.Windows.Forms.Button btnTKPT_TimKiem;
        private System.Windows.Forms.Panel pnlTimKiemTL;
        private System.Windows.Forms.Button btnYeuCauThemTL;
        private System.Windows.Forms.Button btnTKTL_LapPM;
        private System.Windows.Forms.GroupBox groupBox11;
        private System.Windows.Forms.DataGridView dgvDstl;
        private System.Windows.Forms.Button btnTKTL_Xemtatca;
        private System.Windows.Forms.GroupBox groupBox12;
        private System.Windows.Forms.Button btnLoaiTimKiem;
        private System.Windows.Forms.Button btnTKTL_TimKiem;
        private System.Windows.Forms.TextBox txtTKTL_TenTL;
        private System.Windows.Forms.TextBox txtTKTL_MaTL;
        private System.Windows.Forms.RadioButton rdoMaso;
        private System.Windows.Forms.RadioButton rdoTenDG;
        private System.Windows.Forms.RadioButton rdoMaDG;
        private System.Windows.Forms.Panel pnlTool;
        private System.Windows.Forms.Button btnXoa;
        private System.Windows.Forms.Button btnXemChiTiet;
        private System.Windows.Forms.Panel pnlChinhSua;
        private System.Windows.Forms.Button btnChinhSua;
        private System.Windows.Forms.Panel pnlEdit;
        private System.Windows.Forms.Button btnLuu;
        private System.Windows.Forms.Button btnHuyChinhSua;
        private System.Windows.Forms.Panel pnlChiTietDG;
        private System.Windows.Forms.GroupBox groupBox13;
        private System.Windows.Forms.DataGridView dgvChiTietDG;
        private System.Windows.Forms.RadioButton rdoTKTL_TenTL;
        private System.Windows.Forms.RadioButton rdoTKTL_MaTL;
        private System.Windows.Forms.Panel pnlChiTietTL;
        private System.Windows.Forms.GroupBox groupBox14;
        private System.Windows.Forms.DataGridView dgvChiTietTL;
        private System.Windows.Forms.ListBox ltbDocGia;
        private System.Windows.Forms.GroupBox groupBox15;
        private System.Windows.Forms.DataGridView dgvTaiLieuMuon;
        private System.Windows.Forms.RadioButton rdoTKPM_MaDG;
        private System.Windows.Forms.RadioButton rdoTKPM_MaPM;
        private System.Windows.Forms.Panel pnlChiTietPM;
        private System.Windows.Forms.GroupBox groupBox16;
        private System.Windows.Forms.DataGridView dgvChiTietPM;
        private System.Windows.Forms.GroupBox groupBox17;
        private System.Windows.Forms.DataGridView dgvTaiLieuDaMuon;
        private System.Windows.Forms.ListBox ltbPhieuMuon;
        private System.Windows.Forms.RadioButton rdoTKPT_MaDG;
        private System.Windows.Forms.RadioButton rdoTKPT_MaPM;
        private System.Windows.Forms.RadioButton rdoTKPT_MaPT;
        private System.Windows.Forms.TextBox txtTKPT_MaPT;
        private System.Windows.Forms.TextBox txtTKPT_MaDG;
        private System.Windows.Forms.TextBox txtTKPT_MaPM;
        private System.Windows.Forms.Panel pnlChiTietPT;
        private System.Windows.Forms.GroupBox groupBox18;
        private System.Windows.Forms.DataGridView dgvChiTietPT;
        private System.Windows.Forms.Button btnTKPT_LapPT;
        private System.Windows.Forms.Button btnTaiLieu;
        private System.Windows.Forms.Panel pnlLoaiTimKiem;
        private System.Windows.Forms.RadioButton rdoLinhVuc;
        private System.Windows.Forms.RadioButton rdoLoaiTL;
        private System.Windows.Forms.ComboBox cboLinhVuc;
        private System.Windows.Forms.Button btnDangXuat;
        private System.Windows.Forms.Button btnBaoBieu;
        private System.Windows.Forms.Panel pnlBaoBieu;
        private System.Windows.Forms.GroupBox groupBox19;
        private System.Windows.Forms.ComboBox cboDSTL_Thang;
        private System.Windows.Forms.ComboBox cboDSDG_Thang;
        private System.Windows.Forms.Button btnLapBaoBieu;
        private System.Windows.Forms.RadioButton rdoDSTLMaDG;
        private System.Windows.Forms.RadioButton rdoDSTLThang;
        private System.Windows.Forms.RadioButton rdoDSDGThang;
        private System.Windows.Forms.ComboBox cboDSTL_TenDG;
        private System.Windows.Forms.Button btnTaiKhoan;
        private System.Windows.Forms.Panel pnlTaiKhoan;
        private System.Windows.Forms.GroupBox groupBox20;
        private System.Windows.Forms.PictureBox pictureBox1;
        private System.Windows.Forms.Button btnDoiMK;
        private System.Windows.Forms.Button btnAdmin;
        private System.Windows.Forms.Label lblTenTK;
        private System.Windows.Forms.Label lblHoTen;
        private System.Windows.Forms.GroupBox gbxMatKhau;
        private System.Windows.Forms.GroupBox gbxThongTin;
        private System.Windows.Forms.TextBox txtXacNhanHoTen;
        private System.Windows.Forms.TextBox txtHoTenMoi;
        private System.Windows.Forms.Button btnHoTen_Doi;
        private System.Windows.Forms.Button btnDoiHoTen;
        private System.Windows.Forms.Button btnMK_Doi;
        private System.Windows.Forms.TextBox txtXacNhanMK;
        private System.Windows.Forms.TextBox txtMKMoi;
        private System.Windows.Forms.Label lblXacNhanHoTen;
        private System.Windows.Forms.Label lblHoTenMoi;
        private System.Windows.Forms.Label label1;
        private System.Windows.Forms.Label lblMKMoi;
        private System.Windows.Forms.Panel pnlAdmin;
        private System.Windows.Forms.Button btnAdmin_TaiKhoan;
        private System.Windows.Forms.GroupBox gbxThemLDG;
        private System.Windows.Forms.Label label17;
        private System.Windows.Forms.TextBox txtThemLDG_SoNgayToiDa;
        private System.Windows.Forms.Label label16;
        private System.Windows.Forms.TextBox txtThemLDG_PhiThuongNien;
        private System.Windows.Forms.Label label3;
        private System.Windows.Forms.TextBox txtThemLDG_SoSachToiDa;
        private System.Windows.Forms.Label label2;
        private System.Windows.Forms.TextBox txtThemLDG_LoaiDG;
        private System.Windows.Forms.Button btnAdmin_ThemLoaiDG;
        private System.Windows.Forms.GroupBox gbxDoiQD;
        private System.Windows.Forms.ComboBox cboDoiQD_LoaiDG;
        private System.Windows.Forms.Label label18;
        private System.Windows.Forms.TextBox txtDoiQD_SoNgayToiDa;
        private System.Windows.Forms.Label label19;
        private System.Windows.Forms.TextBox txtDoiQD_PhiThuongNien;
        private System.Windows.Forms.Label label20;
        private System.Windows.Forms.TextBox txtDoiQD_SoSachToiDa;
        private System.Windows.Forms.Label label21;
        private System.Windows.Forms.Button btnAdmin_ThayDoiQD;
        private System.Windows.Forms.ComboBox cboDSTL_Nam;
        private System.Windows.Forms.ComboBox cboDSDG_Nam;
        private System.Windows.Forms.Button btnDGPhat;
        private System.Windows.Forms.Button btnTKPM_XemPMDT;
        private System.Windows.Forms.Button btnAdmin_ThemTL;
        private System.Windows.Forms.Panel pnlThemTL;
        private System.Windows.Forms.GroupBox groupBox21;
        private System.Windows.Forms.Button btnThemTL;
        private System.Windows.Forms.ComboBox cboThemLinhVuc;
        private System.Windows.Forms.ComboBox cboLoaiTL;
        private System.Windows.Forms.ComboBox cboMaNguonTL;
        private System.Windows.Forms.ComboBox cboTenTL;
        private System.Windows.Forms.Label label25;
        private System.Windows.Forms.Label label24;
        private System.Windows.Forms.Label label23;
        private System.Windows.Forms.Label label22;
        private System.Windows.Forms.DataGridView dgvThemTL;
        private System.Windows.Forms.GroupBox groupBox27;
        private System.Windows.Forms.PictureBox pictureBox2;
        private System.Windows.Forms.GroupBox groupBox26;
        private System.Windows.Forms.GroupBox groupBox25;
        private System.Windows.Forms.GroupBox groupBox24;
        private System.Windows.Forms.GroupBox groupBox23;
        private System.Windows.Forms.GroupBox groupBox22;
		private System.Windows.Forms.ComboBox cboTKTL_LoaiTL;
	}
}

