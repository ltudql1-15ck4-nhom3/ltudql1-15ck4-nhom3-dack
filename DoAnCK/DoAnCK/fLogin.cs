﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using DTO;
using BLL;

namespace DoAnCK
{
	public partial class fLogin : Form
	{
        //DTO_TaiKhoan tk = new DTO_TaiKhoan();
        //fMain fMain = new fMain();
        BLL_TaiKhoan tk = new BLL_TaiKhoan();

		public fLogin()
		{
			InitializeComponent();
			StylesForm();
		}

		private bool isVailUser = false;
		private bool isVailPas = false;
		void isVailAllTextBox()
		{
			//Đặt khoảng cách giữa icon đến textbox
			epUser.SetIconPadding(txtUsername, 3);
			epPass.SetIconPadding(txtPassword, 3);

			if (isVailUser == true && isVailPas == true)
			{
				btnLogin.Enabled = true;
			}
			else
			{
				btnLogin.Enabled = false;
			}
		}

		void StylesForm()
		{
			//Load icon từ Resources
			this.Icon = DoAnCK.Properties.Resources.Login_icon;

			//disable nút đăng nhập lúc khởi chạy
			btnLogin.Enabled = false;

			////giới hạn ký tự cho Textbox
			txtUsername.MaxLength = 20;
			txtPassword.PasswordChar = '*';
			txtPassword.MaxLength = 32;

			//chỉnh sửa comboBox

		}

		private void btnLogin_Click(object sender, EventArgs e)
		{
            string id = txtUsername.Text;
            string pass = txtPassword.Text;
            if (!tk.KiemTraTK(id))
                MessageBox.Show("Tài khoản không tồn tại", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            else
            {
                if (!tk.KiemTraMK(id, pass))
                    MessageBox.Show("Mật khẩu không đúng", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                else
                {
                    DTO_TaiKhoan tttk = new DTO_TaiKhoan();
                    tttk = tk.xemTK(id);
                    fMain fMain = new fMain(tttk);
                    this.Hide();
                    fMain.ShowDialog();

                    if (!this.IsDisposed)
                    {
                        txtUsername.Text = string.Empty;
                        txtPassword.Text = string.Empty;
                        //Hiện lại form khi thoát, giúp đăng nhập lại

                        this.Show();
                    }

					//Tắt fMain thì from này tự tắt luôn khỏi cần đăng nhập lại
					//this.Close();
				}
            }
		}

		private void btnRig_Click(object sender, EventArgs e)
		{
			fRegistration fRegistration=new fRegistration();
			fRegistration.ShowDialog();
			txtUsername.Focus();
		}


		private void txtUsername_TextChanged(object sender, EventArgs e)
		{
			if (string.IsNullOrEmpty(txtUsername.Text))
			{
				epUser.SetError(txtUsername, "Tài khoản không được để trống");
				isVailUser = false;
			}
			else if (txtUsername.Text.Contains(" "))
			{
				epUser.SetError(txtUsername,"Tài khoản không được chứa dấu khoảng trắng");
				isVailUser = false;
			}
			else
			{
				epUser.SetError(txtUsername, string.Empty);
				epUser.Clear();
				isVailUser = true;
			}
			isVailAllTextBox();
		}

		private void txtPassword_TextChanged(object sender, EventArgs e)
		{
			if (string.IsNullOrEmpty(txtPassword.Text))
			{
				epPass.SetError(txtPassword, "Mật khẩu không được để trống");
				isVailPas = false;
			}
			else if (txtPassword.Text.Contains(" "))
			{
				epPass.SetError(txtPassword, "Mật khẩu không được chứa dấu khoảng trắng");
				isVailPas = false;
			}
			else
			{
				epPass.SetError(txtPassword, string.Empty);
				epPass.Clear();
				isVailPas = true;
			}
			isVailAllTextBox();
		}

		//Nhấn ESC để tắt Form
		protected override bool ProcessCmdKey(ref Message msg, Keys keyData)
		{
			if (keyData == Keys.Escape)
			{
				this.Close();
				return true;
			}
			return base.ProcessCmdKey(ref msg, keyData);
		}
	}
}
