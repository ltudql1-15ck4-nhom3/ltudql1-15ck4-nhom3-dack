﻿namespace DoAnCK
{
    partial class fRequest
    {
        /// <summary>
        /// Required designer variable.
        /// </summary>
        private System.ComponentModel.IContainer components = null;

        /// <summary>
        /// Clean up any resources being used.
        /// </summary>
        /// <param name="disposing">true if managed resources should be disposed; otherwise, false.</param>
        protected override void Dispose(bool disposing)
        {
            if (disposing && (components != null))
            {
                components.Dispose();
            }
            base.Dispose(disposing);
        }

        #region Windows Form Designer generated code

        /// <summary>
        /// Required method for Designer support - do not modify
        /// the contents of this method with the code editor.
        /// </summary>
        private void InitializeComponent()
        {
            this.groupBox1 = new System.Windows.Forms.GroupBox();
            this.pictureBox1 = new System.Windows.Forms.PictureBox();
            this.btnHuy = new System.Windows.Forms.Button();
            this.btnGuiYeuCau = new System.Windows.Forms.Button();
            this.txtLinhVuc = new System.Windows.Forms.TextBox();
            this.txtTenTL = new System.Windows.Forms.TextBox();
            this.lblLinhvuc = new System.Windows.Forms.Label();
            this.lblTentl = new System.Windows.Forms.Label();
            this.groupBox1.SuspendLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).BeginInit();
            this.SuspendLayout();
            // 
            // groupBox1
            // 
            this.groupBox1.Controls.Add(this.pictureBox1);
            this.groupBox1.Controls.Add(this.btnHuy);
            this.groupBox1.Controls.Add(this.btnGuiYeuCau);
            this.groupBox1.Controls.Add(this.txtLinhVuc);
            this.groupBox1.Controls.Add(this.txtTenTL);
            this.groupBox1.Controls.Add(this.lblLinhvuc);
            this.groupBox1.Controls.Add(this.lblTentl);
            this.groupBox1.Location = new System.Drawing.Point(13, 13);
            this.groupBox1.Name = "groupBox1";
            this.groupBox1.Size = new System.Drawing.Size(366, 159);
            this.groupBox1.TabIndex = 0;
            this.groupBox1.TabStop = false;
            // 
            // pictureBox1
            // 
            this.pictureBox1.Image = global::DoAnCK.Properties.Resources.envelope;
            this.pictureBox1.Location = new System.Drawing.Point(17, 24);
            this.pictureBox1.Name = "pictureBox1";
            this.pictureBox1.Size = new System.Drawing.Size(105, 106);
            this.pictureBox1.SizeMode = System.Windows.Forms.PictureBoxSizeMode.StretchImage;
            this.pictureBox1.TabIndex = 6;
            this.pictureBox1.TabStop = false;
            // 
            // btnHuy
            // 
            this.btnHuy.Location = new System.Drawing.Point(265, 117);
            this.btnHuy.Name = "btnHuy";
            this.btnHuy.Size = new System.Drawing.Size(95, 23);
            this.btnHuy.TabIndex = 5;
            this.btnHuy.Text = "Hủy bỏ";
            this.btnHuy.UseVisualStyleBackColor = true;
            this.btnHuy.Click += new System.EventHandler(this.btnHuy_Click);
            // 
            // btnGuiYeuCau
            // 
            this.btnGuiYeuCau.Location = new System.Drawing.Point(141, 117);
            this.btnGuiYeuCau.Name = "btnGuiYeuCau";
            this.btnGuiYeuCau.Size = new System.Drawing.Size(95, 23);
            this.btnGuiYeuCau.TabIndex = 4;
            this.btnGuiYeuCau.Text = "Gửi yêu cầu";
            this.btnGuiYeuCau.UseVisualStyleBackColor = true;
            this.btnGuiYeuCau.Click += new System.EventHandler(this.btnGuiYeuCau_Click);
            // 
            // txtLinhVuc
            // 
            this.txtLinhVuc.Location = new System.Drawing.Point(203, 71);
            this.txtLinhVuc.Name = "txtLinhVuc";
            this.txtLinhVuc.Size = new System.Drawing.Size(157, 20);
            this.txtLinhVuc.TabIndex = 3;
            // 
            // txtTenTL
            // 
            this.txtTenTL.Location = new System.Drawing.Point(203, 31);
            this.txtTenTL.Name = "txtTenTL";
            this.txtTenTL.Size = new System.Drawing.Size(157, 20);
            this.txtTenTL.TabIndex = 2;
            // 
            // lblLinhvuc
            // 
            this.lblLinhvuc.AutoSize = true;
            this.lblLinhvuc.Location = new System.Drawing.Point(138, 71);
            this.lblLinhvuc.Name = "lblLinhvuc";
            this.lblLinhvuc.Size = new System.Drawing.Size(51, 13);
            this.lblLinhvuc.TabIndex = 1;
            this.lblLinhvuc.Text = "Lĩnh vực";
            // 
            // lblTentl
            // 
            this.lblTentl.AutoSize = true;
            this.lblTentl.Location = new System.Drawing.Point(138, 34);
            this.lblTentl.Name = "lblTentl";
            this.lblTentl.Size = new System.Drawing.Size(59, 13);
            this.lblTentl.TabIndex = 0;
            this.lblTentl.Text = "Tên tài liệu";
            // 
            // fRequest
            // 
            this.AutoScaleDimensions = new System.Drawing.SizeF(6F, 13F);
            this.AutoScaleMode = System.Windows.Forms.AutoScaleMode.Font;
            this.ClientSize = new System.Drawing.Size(391, 184);
            this.Controls.Add(this.groupBox1);
            this.Name = "fRequest";
            this.StartPosition = System.Windows.Forms.FormStartPosition.CenterParent;
            this.Text = "Yêu cầu tài liệu";
            this.groupBox1.ResumeLayout(false);
            this.groupBox1.PerformLayout();
            ((System.ComponentModel.ISupportInitialize)(this.pictureBox1)).EndInit();
            this.ResumeLayout(false);

        }

        #endregion

        private System.Windows.Forms.GroupBox groupBox1;
        private System.Windows.Forms.Label lblLinhvuc;
        private System.Windows.Forms.Label lblTentl;
        private System.Windows.Forms.TextBox txtTenTL;
        private System.Windows.Forms.Button btnHuy;
        private System.Windows.Forms.Button btnGuiYeuCau;
        private System.Windows.Forms.TextBox txtLinhVuc;
        private System.Windows.Forms.PictureBox pictureBox1;
    }
}