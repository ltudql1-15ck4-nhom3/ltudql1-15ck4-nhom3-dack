﻿using System;
using System.Collections.Generic;
using System.ComponentModel;
using System.Data;
using System.Drawing;
using System.Linq;
using System.Text;
using System.Windows.Forms;
using System.Data.SqlClient;
using DTO;
using BLL;
using System.Collections;
using System.Globalization;

namespace DoAnCK
{
    public partial class fMain : Form
    {
        Dictionary<string, string> tlMuon = new Dictionary<string, string>();

        BLL_DocGia bll_DG = new BLL_DocGia();
        BLL_TaiLieu bll_TL = new BLL_TaiLieu();
        BLL_PhieuMuon bll_PM = new BLL_PhieuMuon();
        BLL_PhieuTra bll_PT = new BLL_PhieuTra();
        BLL_LoaiDG bll_LDG = new BLL_LoaiDG();
        BLL_TaiKhoan bll_TK = new BLL_TaiKhoan();

        DTO_TaiKhoan tk = new DTO_TaiKhoan();
        public const int WM_NCLBUTTONDOWN = 0xA1;
        public const int HT_CAPTION = 0x2;

        [System.Runtime.InteropServices.DllImportAttribute("user32.dll")]
        public static extern int SendMessage(IntPtr hWnd, int Msg, int wParam, int lParam);
        [System.Runtime.InteropServices.DllImportAttribute("user32.dll")]
        public static extern bool ReleaseCapture();


        public fMain(DTO_TaiKhoan tttk)
        {
            InitializeComponent();
            tk = tttk;
        }

        private void AddDate()
        {
            string value = cboThang.Text;
            switch (value)
            {
                case "1":
                case "3":
                case "5":
                case "7":
                case "8":
                case "10":
                case "12":
                    cboNgay.Items.Clear();
                    for (int i = 1; i < 32; i++)
                        cboNgay.Items.Add(i);
                    return;
                case "2":
                    cboNgay.Items.Clear();
                    for (int i = 1; i <= 29; i++)
                        cboNgay.Items.Add(i);
                    return;
                case "4":
                case "6":
                case "9":
                case "11":
                    cboNgay.Items.Clear();
                    for (int i = 1; i < 31; i++)
                        cboNgay.Items.Add(i);
                    return;
            }
        }

        void ClearTextboxes()
        {
            foreach (Control X in this.Controls)
            {
                if (X is TextBox)
                    X.Text = string.Empty;
            }
            foreach (Control X in groupBox2.Controls)
            {
                if (X is TextBox)
                    X.Text = string.Empty;
            }
        }

        private void button1_Click(object sender, EventArgs e)
        {
            long temp;
            int tam;
            if (txtHoTen.Text.Length == 0)
                MessageBox.Show("Họ tên không được để trống", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            else
                if (cboThang.SelectedItem == null)
                    MessageBox.Show("Chưa chọn tháng sinh", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                else
                    if (cboNgay.SelectedItem == null)
                        MessageBox.Show("Chưa chọn ngày sinh", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    else
                        if (txtNamSinh.Text.Length == 0)
                            MessageBox.Show("Năm sinh không được để trống", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        else
                            if (!long.TryParse(txtNamSinh.Text, out temp))
                                MessageBox.Show("Năm sinh phải là số", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
							else
								if (Int32.TryParse(txtNamSinh.Text, out tam) && (tam < 1900 || tam > 2018))
									MessageBox.Show("Năm sinh phải trong giới hạn", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
							else
                                if (!long.TryParse(txtSdt.Text, out temp))
                                    MessageBox.Show("Số điện thoại phải là số", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                                else
                                    if (txtDiaChi.Text.Length == 0)
                                        MessageBox.Show("Địa chỉ không được để trống", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                                    else
                                        if (txtCmnd.Text.Length == 0)
                                            MessageBox.Show("CMND không được để trống", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                                        else
                                            if (!long.TryParse(txtCmnd.Text, out temp) || txtCmnd.Text.Length > 12 || txtCmnd.Text.Length < 9)
                                                MessageBox.Show("CMND có định dạng không đúng", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                                            else
                                                if (!long.TryParse(txtMscb.Text, out temp) && rdoGiangVien.Checked == true)
                                                    MessageBox.Show("MSGV có định dạng không đúng", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                                                else
                                                    if (!long.TryParse(txtMssv.Text, out temp) && rdoSinhVien.Checked == true || txtMssv.Text.Length > 10)
                                                        MessageBox.Show("MSSV có định dạng không đúng", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                                                    else
                                                    {
                                                        DTO_DocGia dg = new DTO_DocGia();
                                                        dg.CMND = txtCmnd.Text;
                                                        dg.HoTen = txtHoTen.Text;
                                                        dg.SDT = txtSdt.Text;
                                                        dg.Email = txtEmail.Text;
                                                        dg.NgaySinh = cboThang.Text + "/" + cboNgay.Text + "/" + txtNamSinh.Text;
                                                        dg.DiaChi = txtDiaChi.Text;
                                                        if (rdoGiangVien.Checked == true)
                                                        {
                                                            dg.Loai = rdoGiangVien.Text;
                                                            dg.MaSo = txtMscb.Text;
                                                        }
                                                        else
                                                            if (rdoKhach.Checked == true)
                                                            {
                                                                dg.Loai = rdoKhach.Text;
                                                                dg.MaSo = "NULL";
                                                            }
                                                            else
                                                            {
                                                                dg.Loai = rdoSinhVien.Text;
                                                                dg.MaSo = txtMssv.Text;
                                                            }
                                                        bll_DG.ThemDG(dg);

                                                        dg = bll_DG.timDGTheoCMND(dg.CMND);

                                                        string madg = dg.MaDG;

                                                        //Delete text after register
                                                        ClearTextboxes();
                                                        cboNgay.Text = string.Empty;
                                                        cboThang.Text = string.Empty;
                                                        cboNgay.Items.Clear();
                                                        MessageBox.Show("Đăng ký độc giả thành công\nMã độc giả: " + madg, "Information");
                                                    }
        }

        private void btnTimTL_Click(object sender, EventArgs e)
        {
            foreach (Control ct in this.Controls)
            {
                if (ct is Panel && ct.Name.Contains("pnl"))
                {
                    ct.Visible = false;
                }
            }
            pnlTimKiemTL.Visible = true;
            pnlTool.Visible = true;
            lblTitle.Text = "Tìm kiếm tài liệu";

            dgvDstl.Rows.Clear();
            dgvDstl.Refresh();
        }

        private void btnDangKy_Click(object sender, EventArgs e)
        {
            foreach (Control ct in this.Controls)
            {
                if (ct is Panel && ct.Name.Contains("pnl"))
                {
                    ct.Visible = false;
                }
            }
            pnlDangKyDG.Visible = true;
            pnlTool.Visible = false;
            lblTitle.Text = "Đăng ký độc giả";
        }

        private void btnTimDG_Click(object sender, EventArgs e)
        {
            dgvDsdg.Rows.Clear();
            dgvDsdg.Refresh();
            foreach (Control ct in this.Controls)
            {
                if (ct is Panel && ct.Name.Contains("pnl"))
                {
                    ct.Visible = false;
                }
            }
            pnlTimKiemDG.Visible = true;
            pnlTool.Visible = true;
            lblTitle.Text = "Tìm kiếm độc giả";
        }

        private void btnLapPM_Click(object sender, EventArgs e)
        {
            foreach (Control ct in this.Controls)
            {
                if (ct is Panel && ct.Name.Contains("pnl"))
                {
                    ct.Visible = false;
                }
            }
            pnlLapPM.Visible = true;
            pnlTool.Visible = false;
            lblTitle.Text = "Lập phiếu mượn";
        }

        private void btnTimPM_Click(object sender, EventArgs e)
        {
            dgvDSPM.Rows.Clear();
            dgvDSPM.Refresh();
            foreach (Control ct in this.Controls)
            {
                if (ct is Panel && ct.Name.Contains("pnl"))
                {
                    ct.Visible = false;
                }
            }
            pnlTimKiemPM.Visible = true;
            pnlTool.Visible = true;
            lblTitle.Text = "Tìm kiếm phiếu mượn";
        }

        private void btnLapPT_Click(object sender, EventArgs e)
        {
            tlMuon.Clear();

            foreach (Control ct in this.Controls)
            {
                if (ct is Panel && ct.Name.Contains("pnl"))
                {
                    ct.Visible = false;
                }
            }
            pnlLapPT.Visible = true;
            pnlTool.Visible = false;
            lblTitle.Text = "Lập phiếu trả";
        }

        private void btnTimPT_Click(object sender, EventArgs e)
        {
            dgvDspt.Rows.Clear();
            dgvDspt.Refresh();
            foreach (Control ct in this.Controls)
            {
                if (ct is Panel && ct.Name.Contains("pnl"))
                {
                    ct.Visible = false;
                }
            }
            pnlTimKiemPT.Visible = true;
            pnlTool.Visible = true;
            lblTitle.Text = "Tìm kiếm phiếu trả";
        }

        private void pnlTitle_MouseMove_1(object sender, MouseEventArgs e)
        {
            ReleaseCapture();
            SendMessage(Handle, WM_NCLBUTTONDOWN, HT_CAPTION, 0);
        }

        private void lblClose_Click(object sender, EventArgs e)
        {
            ArrayList RemoveList = new ArrayList();
            foreach (Form f in Application.OpenForms)
            {
                //f.Close();
                RemoveList.Add(f);
            }
            foreach (Form f in RemoveList)
                f.Close();
        }

        private void btnThoat_Click(object sender, EventArgs e)
        {
            ArrayList RemoveList = new ArrayList();
            foreach (Form f in Application.OpenForms)
            {
                RemoveList.Add(f);
            }
            foreach (Form f in RemoveList)
                f.Close();
        }

        private void btnTrangChu_Click(object sender, EventArgs e)
        {
            foreach (Control ct in this.Controls)
            {
                if (ct is Panel && ct.Name.Contains("pnl"))
                {
                    ct.Visible = false;
                }
            }
            pnlTool.Visible = false;
            pnlTrangChu.Visible = true;
            lblTitle.Text = "Trang chủ";
        }

        private void rdoSinhVien_CheckedChanged(object sender, EventArgs e)
        {
            txtMssv.Enabled = true;
            txtMscb.Enabled = false;
            txtMscb.Text = string.Empty;
        }

        private void rdoGiangVien_CheckedChanged(object sender, EventArgs e)
        {
            txtMssv.Enabled = false;
            txtMscb.Enabled = true;
            txtMssv.Text = string.Empty;
        }

        private void rdoKhach_CheckedChanged(object sender, EventArgs e)
        {
            txtMssv.Enabled = false;
            txtMscb.Enabled = false;
            txtMscb.Text = string.Empty;
            txtMssv.Text = string.Empty;
        }

        private void cboThang_SelectedIndexChanged(object sender, EventArgs e)
        {
            AddDate();
        }

        private void Form1_Load(object sender, EventArgs e)
        {
            AddCol_DSDG();
            AddCol_DSTL();
            Addcol_DSPM();
            Addcol_DSPT();
            AddCol_ThemTL();

            StyleForm();
        }

        private void AddCol_DSDG()
        {
            dgvDsdg.ColumnCount = 7;
            dgvDsdg.Columns[0].Name = "STT";
            dgvDsdg.Columns[0].Width = 50;
            dgvDsdg.Columns[1].Name = "Mã độc giả";
            dgvDsdg.Columns[2].Name = "Họ và tên";
            dgvDsdg.Columns[3].Name = "CMND";
            dgvDsdg.Columns[4].Name = "Loại độc giả";
            dgvDsdg.Columns[5].Name = "Số sách đang mượn";
            dgvDsdg.Columns[6].Name = "Số sách quá hạn";
        }

        private void AddCol_DSTL()
        {
            dgvDstl.ColumnCount = 4;
            dgvDstl.Columns[0].Name = "STT";
            dgvDstl.Columns[0].Width = 50;
            dgvDstl.Columns[1].Name = "Mã tài liệu";
            dgvDstl.Columns[2].Name = "Tên tài liệu";
            dgvDstl.Columns[3].Name = "Loại tài liệu";
        }

        private void Addcol_DSPM()
        {
            dgvDSPM.ColumnCount = 3;
            dgvDSPM.Columns[0].Name = "STT";
            dgvDSPM.Columns[0].Width = 50;
            dgvDSPM.Columns[1].Name = "Mã phiếu mượn";
            dgvDSPM.Columns[1].Width = 120;
            dgvDSPM.Columns[2].Name = "Mã độc giả";
        }

        private void Addcol_DSPT()
        {
            dgvDspt.ColumnCount = 2;
            dgvDspt.Columns[0].Name = "STT";
            dgvDspt.Columns[0].Width = 50;
            dgvDspt.Columns[1].Name = "Mã phiếu trả";
        }

        private void AddCol_CTDG()
        {
            dgvChiTietDG.ColumnCount = 9;
            dgvChiTietDG.Columns[0].Name = "Mã độc giả";
            dgvChiTietDG.Columns[1].Name = "Họ và tên";
            dgvChiTietDG.Columns[2].Name = "CMND";
            dgvChiTietDG.Columns[3].Name = "Số điện thoại";
            dgvChiTietDG.Columns[4].Name = "Email";
            dgvChiTietDG.Columns[5].Name = "Ngày sinh";
            dgvChiTietDG.Columns[6].Name = "Địa chỉ";
            dgvChiTietDG.Columns[7].Name = "Loại độc giả";
            dgvChiTietDG.Columns[8].Name = "Mã số";
        }

        private void AddCol_CTTL()
        {
            dgvChiTietTL.ColumnCount = 6;
            dgvChiTietTL.Columns[0].Name = "Mã nguồn tài liệu";
            dgvChiTietTL.Columns[1].Name = "Mã tài liệu";
            dgvChiTietTL.Columns[2].Name = "Tên tài liệu";
            dgvChiTietTL.Columns[3].Name = "Loại tài liệu";
            dgvChiTietTL.Columns[4].Name = "Lĩnh vực";
            dgvChiTietTL.Columns[5].Name = "Trạng thái";
        }

        private void AddCol_CTPM()
        {
            dgvChiTietPM.ColumnCount = 5;
            dgvChiTietPM.Columns[0].Name = "Mã phiếu mượn";
            dgvChiTietPM.Columns[1].Name = "Ngày lập phiếu mượn";
            dgvChiTietPM.Columns[2].Name = "Hạn chót trả";
            dgvChiTietPM.Columns[3].Name = "Mã tài liệu";
            dgvChiTietPM.Columns[4].Name = "Trạng thái";
        }

        private void AddCol_CTPT()
        {
            dgvChiTietPT.ColumnCount = 4;
            dgvChiTietPT.Columns[0].Name = "Mã phiếu trả";
            dgvChiTietPT.Columns[1].Name = "Mã phiếu mượn";
            dgvChiTietPT.Columns[2].Name = "Mã tài liệu";
            dgvChiTietPT.Columns[3].Name = "Ngày lập phiếu trả";
        }

        private void btnTKDG_Xemtatca_Click(object sender, EventArgs e)
        {
            dgvDsdg.Rows.Clear();
            dgvDsdg.Refresh();
            ReadData_DSDG();
        }

        private void ReadData_DSDG()
        {
            List<DTO_DocGia> dg = bll_DG.xemTatCaDG();
            int i = 1;
            foreach (DTO_DocGia row in dg)
            {
                string[] dgRow = new string[] { i++.ToString(), row.MaDG, row.HoTen, row.CMND, row.Loai, row.SoSachDangMuon.ToString(), row.SoSachQuaHan.ToString() };
                dgvDsdg.Rows.Add(dgRow);
            }
        }

        private void rdoMaDG_CheckedChanged(object sender, EventArgs e)
        {
            txtMaDG.Enabled = true;
            txtTKDG_TenDG.Enabled = false;
            txtTKDG_Maso.Enabled = false;

            txtTKDG_TenDG.Text = string.Empty;
            txtTKDG_Maso.Text = string.Empty;
        }

        private void rdoCmnd_CheckedChanged(object sender, EventArgs e)
        {
            txtMaDG.Enabled = false;
            txtTKDG_TenDG.Enabled = true;
            txtTKDG_Maso.Enabled = false;

            txtMaDG.Text = string.Empty;
            txtTKDG_Maso.Text = string.Empty;
        }

        private void rdoMaso_CheckedChanged(object sender, EventArgs e)
        {
            txtMaDG.Enabled = false;
            txtTKDG_TenDG.Enabled = false;
            txtTKDG_Maso.Enabled = true;

            txtMaDG.Text = string.Empty;
            txtTKDG_TenDG.Text = string.Empty;
        }

        private void btnTKDG_Timkiem_Click(object sender, EventArgs e)
        {
            dgvDsdg.Rows.Clear();
            dgvDsdg.Refresh();
            if (rdoMaDG.Checked)
            {
                if (txtMaDG.Text.Length > 0)
                {
                    DTO_DocGia dg = new DTO_DocGia();
                    dg = bll_DG.timDGTheoMaDG(txtMaDG.Text);
                    if (dg.MaDG != null)
                    {
                        string[] dgRow = new string[] { "1", dg.MaDG, dg.HoTen, dg.CMND, dg.Loai, dg.SoSachDangMuon.ToString(), dg.SoSachQuaHan.ToString() };
                        dgvDsdg.Rows.Add(dgRow);
                    }
                }
            }
            if (rdoTenDG.Checked)
            {
                if (txtTKDG_TenDG.Text.Length > 0)
                {
                    List<DTO_DocGia> dg = bll_DG.timDGTheoHoTen(txtTKDG_TenDG.Text);
                    int i = 1;
                    foreach (DTO_DocGia row in dg)
                    {
                        string[] dgRow = new string[] { i++.ToString(), row.MaDG, row.HoTen, row.CMND, row.Loai, row.SoSachDangMuon.ToString(), row.SoSachQuaHan.ToString() };
                        dgvDsdg.Rows.Add(dgRow);
                    }
                }
            }

            if (rdoMaso.Checked)
            {
                if (txtTKDG_Maso.Text.Length > 0)
                {
                    DTO_DocGia dg = new DTO_DocGia();
                    dg = bll_DG.timDGTheoMaSo(txtTKDG_Maso.Text);
                    if (dg.MaDG != null)
                    {
                        string[] dgRow = new string[] { "1", dg.MaDG, dg.HoTen, dg.CMND, dg.Loai, dg.SoSachDangMuon.ToString(), dg.SoSachQuaHan.ToString() };
                        dgvDsdg.Rows.Add(dgRow);
                    }

                    dg = new DTO_DocGia();
                    dg = bll_DG.timDGTheoCMND(txtTKDG_Maso.Text);
                    if (dg.MaDG != null)
                    {
                        string[] dgRow = new string[] { "1", dg.MaDG, dg.HoTen, dg.CMND, dg.Loai, dg.SoSachDangMuon.ToString(), dg.SoSachQuaHan.ToString() };
                        dgvDsdg.Rows.Add(dgRow);
                    }
                }
            }
        }

        private void ChinhSuaDG()
        {
            pnlEdit.Visible = true;
            dgvChiTietDG.ReadOnly = false;
            dgvChiTietDG.Columns["Mã độc giả"].ReadOnly = true;
            dgvChiTietDG.Rows[dgvChiTietDG.Rows.Count-1].ReadOnly = true;
        }

        private void ChinhSuaTL()
        {
            pnlEdit.Visible = true;
            dgvChiTietTL.ReadOnly = false;
            dgvChiTietTL.Columns["Mã nguồn tài liệu"].ReadOnly = true;
            dgvChiTietTL.Columns["Mã tài liệu"].ReadOnly = true;
            dgvChiTietTL.Rows[dgvChiTietTL.Rows.Count - 1].ReadOnly = true;
        }

        private void ChinhSuaPM()
        {
            pnlEdit.Visible = true;
            dgvChiTietPM.ReadOnly = false;
            dgvChiTietPM.Columns["Mã phiếu mượn"].ReadOnly = true;
            dgvChiTietPM.Columns["Mã tài liệu"].ReadOnly = true;
            dgvChiTietPM.Rows[dgvChiTietPM.Rows.Count - 1].ReadOnly = true;
        }

        private void ChinhSuaPT()
        {
            pnlEdit.Visible = true;
            dgvChiTietPT.ReadOnly = false;
            dgvChiTietPT.Columns["Mã phiếu trả"].ReadOnly = true;
            dgvChiTietPT.Columns["Mã phiếu mượn"].ReadOnly = true;
            dgvChiTietPT.Columns["Mã tài liệu"].ReadOnly = true;
            dgvChiTietPT.Rows[dgvChiTietPT.Rows.Count - 1].ReadOnly = true;
        }

        private void btnChinhSua_Click(object sender, EventArgs e)
        {
            if (pnlChiTietDG.Visible == true)
                ChinhSuaDG();
            if (pnlChiTietTL.Visible == true)
                ChinhSuaTL();
            if (pnlChiTietPM.Visible == true)
                ChinhSuaPM();
            if (pnlChiTietPT.Visible == true)
                ChinhSuaPT();
        }

        private void HuyChinhSuaDG()
        {
            pnlChiTietDG.Visible = false;
            pnlChinhSua.Visible = false;
            pnlTimKiemDG.Visible = true;
            pnlTool.Visible = true;
            pnlEdit.Visible = false;
            lblTitle.Text = "Tìm kiếm độc giả";
        }

        private void HuyChinhSuaTL()
        {
            pnlChiTietTL.Visible = false;
            pnlChinhSua.Visible = false;
            pnlTimKiemTL.Visible = true;
            pnlTool.Visible = true;
            pnlEdit.Visible = false;
            lblTitle.Text = "Tìm kiếm tài liệu";
        }

        private void HuyChinhSuaPM()
        {
            pnlChiTietPM.Visible = false;
            pnlChinhSua.Visible = false;
            pnlTimKiemPM.Visible = true;
            pnlTool.Visible = true;
            pnlEdit.Visible = false;
            lblTitle.Text = "Tìm kiếm phiếu mượn";
        }

        private void HuyChinhSuaPT()
        {
            pnlChiTietPT.Visible = false;
            pnlChinhSua.Visible = false;
            pnlTimKiemPT.Visible = true;
            pnlTool.Visible = true;
            pnlEdit.Visible = false;
            lblTitle.Text = "Tìm kiếm phiếu trả";
        }

        private void btnHuyChinhSua_Click(object sender, EventArgs e)
        {
            if (pnlChiTietDG.Visible == true)
                HuyChinhSuaDG();
            if (pnlChiTietTL.Visible == true)
                HuyChinhSuaTL();
            if (pnlChiTietPM.Visible == true)
                HuyChinhSuaPM();
            if (pnlChiTietPT.Visible == true)
                HuyChinhSuaPT();

        }

        private void LuuDG()
        {
            foreach (DataGridViewRow a in dgvChiTietDG.Rows)
            {
                if (a.Cells[0].Value == null)
                    break;
                DTO_DocGia dg = new DTO_DocGia();
                dg.MaDG = a.Cells[0].Value.ToString();
                dg.HoTen = a.Cells[1].Value.ToString();
                dg.CMND = a.Cells[2].Value.ToString();
                dg.SDT = a.Cells[3].Value.ToString();
                dg.Email = a.Cells[4].Value.ToString();

	            DateTime date = new DateTime();
	            if(DateTime.TryParseExact(a.Cells[5].Value.ToString(), "dd/MM/yyyy", CultureInfo.InvariantCulture, DateTimeStyles.None, out date))
					dg.NgaySinh = a.Cells[5].Value.ToString();
	            else
	            {
					MessageBox.Show("Ngày sinh có định dạng không đúng", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
		            return;
	            }
                dg.DiaChi = a.Cells[6].Value.ToString();
                dg.Loai = a.Cells[7].Value.ToString();
                dg.MaSo = a.Cells[8].Value.ToString();
                bll_DG.chinhSuaDG(dg);
            }

            dgvChiTietDG.ReadOnly = true;
            pnlChiTietDG.Visible = false;
            pnlChinhSua.Visible = false;
            pnlTimKiemDG.Visible = true;
            pnlTool.Visible = true;
            pnlEdit.Visible = false;
            lblTitle.Text = "Tìm kiếm độc giả";
        }

        private void LuuTL()
        {
            foreach (DataGridViewRow a in dgvChiTietTL.Rows)
            {
                if (a.Cells[0].Value == null)
                    break;
                for (int i = 0; i < a.Cells.Count; i++)
                {
                    if (a.Cells[i].Value == null)
                    {
                        MessageBox.Show("Dữ liệu chỉnh sửa không được để trống", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        return;
                    }
                }
                if (String.Compare(a.Cells[5].Value.ToString(), "Đã mượn") != 0 && String.Compare(a.Cells[5].Value.ToString(), "Có thể mượn") != 0)
                {
                    MessageBox.Show("Trạng thái chỉ có thể là 'Đã mượn' hoặc 'Có thể mượn'", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }
                if (String.Compare(a.Cells[3].Value.ToString(), "Thường") != 0 && String.Compare(a.Cells[3].Value.ToString(), "Đặc biệt") != 0 && String.Compare(a.Cells[3].Value.ToString(), "Hiếm") != 0)
                {
                    MessageBox.Show("Loại tài liệu chỉ có thể là 'Thường', 'Đặc biệt' hoặc 'Hiếm'", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }

                DTO_TaiLieu tl = new DTO_TaiLieu();
                tl.MaNguonTL = a.Cells[0].Value.ToString();
                tl.MaTL = a.Cells[1].Value.ToString();
                tl.TenTL = a.Cells[2].Value.ToString();
                tl.LoaiTL = a.Cells[3].Value.ToString();
	            tl.LinhVuc = a.Cells[4].Value.ToString();
				tl.TrangThai = a.Cells[5].Value.ToString();
                bll_TL.chinhSuaTL(tl);
            }


            dgvChiTietTL.ReadOnly = true;
            pnlChiTietTL.Visible = false;
            pnlChinhSua.Visible = false;
            pnlTimKiemTL.Visible = true;
            pnlTool.Visible = true;
            pnlEdit.Visible = false;
            lblTitle.Text = "Tìm kiếm tài liệu";
        }

        private void LuuPM()
        {
            foreach (DataGridViewRow a in dgvChiTietPM.Rows)
            {
                if (a.Cells[0].Value == null)
                    break;
                for (int i = 0; i < a.Cells.Count; i++)
                {
                    if (a.Cells[i].Value == null)
                    {
                        MessageBox.Show("Dữ liệu chỉnh sửa không được để trống", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        return;
                    }
                }
                if (String.Compare(a.Cells[4].Value.ToString(), "Chưa trả") != 0 && String.Compare(a.Cells[4].Value.ToString(), "Đã trả") != 0)
                {
                    MessageBox.Show("Trạng thái chỉ có thể là 'Chưa trả' hoặc 'Đã trả'", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    return;
                }

                DTO_PhieuMuon pm = new DTO_PhieuMuon();
                pm.MaPM = a.Cells[0].Value.ToString();

	            DateTime date = new DateTime();
	            if (DateTime.TryParseExact(a.Cells[1].Value.ToString(), "dd/MM/yyyy", CultureInfo.InvariantCulture,
		            DateTimeStyles.None, out date))
		            pm.NgayLap = a.Cells[1].Value.ToString();
	            else
	            {
					MessageBox.Show("Ngày lập phiếu có định dạng không đúng", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
		            return;
	            }
	            if (DateTime.TryParseExact(a.Cells[2].Value.ToString(), "dd/MM/yyyy", CultureInfo.InvariantCulture,
		            DateTimeStyles.None, out date))
					pm.HanChotTra = a.Cells[2].Value.ToString();
	            else
	            {
					MessageBox.Show("Hạn chót trả có định dạng không đúng", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
		            return;
				}
                pm.MaTL = a.Cells[3].Value.ToString();
                pm.TrangThai = a.Cells[4].Value.ToString();
                bll_PM.chinhSuaPM(pm);
            }


            dgvChiTietPM.ReadOnly = true;
            pnlChiTietPM.Visible = false;
            pnlChinhSua.Visible = false;
            pnlTimKiemPM.Visible = true;
            pnlTool.Visible = true;
            pnlEdit.Visible = false;
            lblTitle.Text = "Tìm kiếm phiếu mượn";
        }

        private void LuuPT()
        {
            foreach (DataGridViewRow a in dgvChiTietPT.Rows)
            {
                if (a.Cells[0].Value == null)
                    break;
                for (int i = 0; i < a.Cells.Count; i++)
                {
                    if (a.Cells[i].Value == null)
                    {
                        MessageBox.Show("Dữ liệu chỉnh sửa không được để trống", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        return;
                    }
                }

                DTO_PhieuTra pt = new DTO_PhieuTra();
                pt.MaPT = a.Cells[0].Value.ToString();
                pt.MaPM = a.Cells[1].Value.ToString();
                pt.MaTL = a.Cells[2].Value.ToString();

	            DateTime date = new DateTime();
	            if (DateTime.TryParseExact(a.Cells[3].Value.ToString(), "dd/MM/yyyy", CultureInfo.InvariantCulture,
		            DateTimeStyles.None, out date))
					pt.NgayLapPT = a.Cells[3].Value.ToString();
	            else
	            {
					MessageBox.Show("Ngày lập phiếu có định dạng không đúng", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
		            return;
				}

                bll_PT.chinhSuaPT(pt);
            }


            dgvChiTietPT.ReadOnly = true;
            pnlChiTietPT.Visible = false;
            pnlChinhSua.Visible = false;
            pnlTimKiemPT.Visible = true;
            pnlTool.Visible = true;
            pnlEdit.Visible = false;
            lblTitle.Text = "Tìm kiếm phiếu trả";
        }

        private void btnLuu_Click(object sender, EventArgs e)
        {
            if (pnlChiTietDG.Visible == true)
                LuuDG();
            if (pnlChiTietTL.Visible == true)
                LuuTL();
            if (pnlChiTietPM.Visible == true)
                LuuPM();
            if (pnlChiTietPT.Visible == true)
                LuuPT();
        }

        private void XemChiTietDG()
        {
            if (dgvDsdg.SelectedRows.Count == 0 || dgvDsdg.SelectedRows[0].Cells[0].Value == null)
            {
                MessageBox.Show("Chưa chọn độc giả nào để xem chi tiết", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            else
            {
                dgvChiTietDG.ReadOnly = true;

                AddCol_CTDG();
                dgvChiTietDG.Rows.Clear();
                dgvChiTietDG.Refresh();

                foreach (Control ct in this.Controls)
                {
                    if (ct is Panel && ct.Name.Contains("pnl"))
                    {
                        ct.Visible = false;
                    }
                }
                pnlTool.Visible = false;
                pnlChinhSua.Visible = true;
                pnlChiTietDG.Visible = true;
                lblTitle.Text = "Chi tiết độc giả";

                foreach (DataGridViewRow row in dgvDsdg.SelectedRows)
                {
                    DTO_DocGia dg = new DTO_DocGia();
                    dg = bll_DG.timDGTheoMaDG(row.Cells[1].Value.ToString());

                    string[] dgRow = new string[] { dg.MaDG, dg.HoTen, dg.CMND, dg.SDT, dg.Email, dg.NgaySinh, dg.DiaChi, dg.Loai, dg.MaSo };
                    dgvChiTietDG.Rows.Add(dgRow);
                }
            }
        }

        private void XemChiTietTL()
        {
            if (dgvDstl.SelectedRows.Count == 0 || dgvDstl.SelectedRows[0].Cells[0].Value == null)
            {
                MessageBox.Show("Chưa chọn tài liệu nào để xem chi tiết", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            else
            {
                dgvChiTietTL.ReadOnly = true;

                AddCol_CTTL();
                dgvChiTietTL.Rows.Clear();
                dgvChiTietTL.Refresh();

                foreach (Control ct in this.Controls)
                {
                    if (ct is Panel && ct.Name.Contains("pnl"))
                    {
                        ct.Visible = false;
                    }
                }
                pnlTool.Visible = false;
                pnlChinhSua.Visible = true;
                pnlChiTietTL.Visible = true;
                lblTitle.Text = "Chi tiết tài liệu";

                foreach (DataGridViewRow row in dgvDstl.SelectedRows)
                {
                    DTO_TaiLieu tl = new DTO_TaiLieu();
                    tl = bll_TL.timTLTheoMaTL(row.Cells[1].Value.ToString());

                    string[] dgRow = new string[] { tl.MaNguonTL, tl.MaTL, tl.TenTL, tl.LoaiTL, tl.LinhVuc, tl.TrangThai };
                    dgvChiTietTL.Rows.Add(dgRow);
                }
            }
        }

        private void XemChiTietPM()
        {
            if (dgvDSPM.SelectedRows.Count == 0 || dgvDSPM.SelectedRows[0].Cells[0].Value == null)
            {
                MessageBox.Show("Chưa chọn phiếu mượn nào để xem chi tiết", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            else
            {
                dgvChiTietPM.ReadOnly = true;

                AddCol_CTPM();
                dgvChiTietPM.Rows.Clear();
                dgvChiTietPM.Refresh();

                foreach (Control ct in this.Controls)
                {
                    if (ct is Panel && ct.Name.Contains("pnl"))
                    {
                        ct.Visible = false;
                    }
                }
                pnlTool.Visible = false;
                pnlChinhSua.Visible = true;
                pnlChiTietPM.Visible = true;
                lblTitle.Text = "Chi tiết phiếu mượn";

                foreach (DataGridViewRow row in dgvDSPM.SelectedRows)
                {
                    List<DTO_PhieuMuon> pm = new List<DTO_PhieuMuon>();
                    pm = bll_PM.timCTPMTheoMaPM(row.Cells[1].Value.ToString());
                    foreach (DTO_PhieuMuon ctpm in pm)
                    {
                        string[] dgRow = new string[] { ctpm.MaPM, ctpm.NgayLap, ctpm.HanChotTra, ctpm.MaTL, ctpm.TrangThai };
                        dgvChiTietPM.Rows.Add(dgRow);
                    }
                }
            }
        }

        private void XemChiTietPT()
        {
            if (dgvDspt.SelectedRows.Count == 0 || dgvDspt.SelectedRows[0].Cells[0].Value == null)
            {
                MessageBox.Show("Chưa chọn phiếu trả nào để xem chi tiết", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            else
            {
                dgvChiTietPT.ReadOnly = true;

                AddCol_CTPT();
                dgvChiTietPT.Rows.Clear();
                dgvChiTietPT.Refresh();

                foreach (Control ct in this.Controls)
                {
                    if (ct is Panel && ct.Name.Contains("pnl"))
                    {
                        ct.Visible = false;
                    }
                }
                pnlTool.Visible = false;
                pnlChinhSua.Visible = true;
                pnlChiTietPT.Visible = true;
                lblTitle.Text = "Chi tiết phiếu trả";

                foreach (DataGridViewRow row in dgvDspt.SelectedRows)
                {
                    List<DTO_PhieuTra> pt = new List<DTO_PhieuTra>();
                    pt = bll_PT.timCTPTTheoMaPT(row.Cells[1].Value.ToString());
                    foreach (DTO_PhieuTra ctpt in pt)
                    {
                        string[] dgRow = new string[] { ctpt.MaPT, ctpt.MaPM, ctpt.MaTL, ctpt.NgayLapPT };
                        dgvChiTietPT.Rows.Add(dgRow);
                    }
                }
            }
        }

        private void btnXemChiTiet_Click(object sender, EventArgs e)
        {
            if (pnlTimKiemDG.Visible == true)
                XemChiTietDG();
            if (pnlTimKiemTL.Visible == true)
                XemChiTietTL();
            if (pnlTimKiemPM.Visible == true)
                XemChiTietPM();
            if (pnlTimKiemPT.Visible == true)
                XemChiTietPT();

        }

        private void btnTKDG_LapPM_Click(object sender, EventArgs e)
        {

            foreach (Control ct in this.Controls)
            {
                if (ct is Panel && ct.Name.Contains("pnl"))
                {
                    ct.Visible = false;
                }
            }
            pnlLapPM.Visible = true;
            pnlTool.Visible = false;
            lblTitle.Text = "Lập phiếu mượn";
        }

        private void btnTKDG_LapPT_Click(object sender, EventArgs e)
        {
            tlMuon.Clear();

            foreach (Control ct in this.Controls)
            {
                if (ct is Panel && ct.Name.Contains("pnl"))
                {
                    ct.Visible = false;
                }
            }
            pnlLapPT.Visible = true;
            pnlTool.Visible = false;
            lblTitle.Text = "Lập phiếu trả";
        }

        private void btnTKDG_DangKyDG_Click(object sender, EventArgs e)
        {
            foreach (Control ct in this.Controls)
            {
                if (ct is Panel && ct.Name.Contains("pnl"))
                {
                    ct.Visible = false;
                }
            }
            pnlDangKyDG.Visible = true;
            pnlTool.Visible = false;
            lblTitle.Text = "Đăng ký độc giả";
        }

        private void btnDocGia_Click(object sender, EventArgs e)
        {
            dgvDsdg.Rows.Clear();
            dgvDsdg.Refresh();
            foreach (Control ct in this.Controls)
            {
                if (ct is Panel && ct.Name.Contains("pnl"))
                {
                    ct.Visible = false;
                }
            }
            pnlTimKiemDG.Visible = true;
            pnlTool.Visible = true;
            lblTitle.Text = "Tìm kiếm độc giả";
        }

        private void XoaDG(object sender, EventArgs e)
        {
            if (dgvDsdg.SelectedRows.Count == 0 || dgvDsdg.SelectedRows[0].Cells[0].Value == null)
            {
                MessageBox.Show("Chưa chọn độc giả nào để xóa", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            else
            {
                DialogResult dr = MessageBox.Show("Bạn có chắc muốn xóa độc giả này?", "Confirmation", MessageBoxButtons.YesNo, MessageBoxIcon.Warning);
                if (dr == DialogResult.Yes)
                {

                    foreach (DataGridViewRow row in dgvDsdg.SelectedRows)
                    {
                        if (row.Cells[0].Value == null)
                            break;
                        bll_DG.xoaDG(row.Cells[1].Value.ToString());
                    }
                    btnTKDG_Xemtatca_Click(sender, e);
                }
            }
        }

        private void XoaTL(object sender, EventArgs e)
        {
            if (dgvDstl.SelectedRows.Count == 0 || dgvDstl.SelectedRows[0].Cells[0].Value == null)
            {
                MessageBox.Show("Chưa chọn tài liệu nào để xóa", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            else
            {
                DialogResult dr = MessageBox.Show("Bạn có chắc muốn xóa tài liệu này?", "Confirmation", MessageBoxButtons.YesNo, MessageBoxIcon.Warning);
                if (dr == DialogResult.Yes)
                {
                    foreach (DataGridViewRow row in dgvDstl.SelectedRows)
                    {
                        if (row.Cells[0].Value == null)
                            break;
                        bll_TL.xoaTL(row.Cells[1].Value.ToString());
                    }
                    btnTKTL_Xemtatca_Click(sender, e);
                }
            }
        }

        private void XoaPM(object sender, EventArgs e)
        {
            if (dgvDSPM.SelectedRows.Count == 0 || dgvDSPM.SelectedRows[0].Cells[0].Value == null)
            {
                MessageBox.Show("Chưa chọn phiếu mượn nào để xóa", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            else
            {
                DialogResult dr = MessageBox.Show("Bạn có chắc muốn xóa phiếu mượn này?", "Confirmation", MessageBoxButtons.YesNo, MessageBoxIcon.Warning);
                if (dr == DialogResult.Yes)
                {
                    foreach (DataGridViewRow row in dgvDSPM.SelectedRows)
                    {
                        if (row.Cells[0].Value == null)
                            break;
                        bll_PM.xoaPM(row.Cells[1].Value.ToString());
                    }
                    btnTKPM_Xemtatca_Click(sender, e);
                }
            }
        }

        private void XoaPT(object sender, EventArgs e)
        {
            if (dgvDspt.SelectedRows.Count == 0 || dgvDspt.SelectedRows[0].Cells[0].Value == null)
            {
                MessageBox.Show("Chưa chọn phiếu trả nào để xóa", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            else
            {
                DialogResult dr = MessageBox.Show("Bạn có chắc muốn xóa phiếu trả này?", "Confirmation", MessageBoxButtons.YesNo, MessageBoxIcon.Warning);
                if (dr == DialogResult.Yes)
                {
                    foreach (DataGridViewRow row in dgvDspt.SelectedRows)
                    {
                        if (row.Cells[0].Value == null)
                            break;
                        bll_PT.xoaPT(row.Cells[1].Value.ToString());
                    }
                    btnTKPT_Xemtatca_Click(sender, e);
                }
            }
        }

        private void btnXoa_Click(object sender, EventArgs e)
        {
            if (pnlTimKiemDG.Visible == true)
                XoaDG(sender, e);
            if (pnlTimKiemTL.Visible == true)
                XoaTL(sender, e);
            if (pnlTimKiemPM.Visible == true)
                XoaPM(sender, e);
            if (pnlTimKiemPT.Visible == true)
                XoaPT(sender, e);
        }

        private void rdoTKTL_TenTL_CheckedChanged(object sender, EventArgs e)
        {
            txtTKTL_TenTL.Enabled = true;
            txtTKTL_MaTL.Enabled = false;

            txtTKTL_MaTL.Text = string.Empty;
        }

        private void rdoTKTL_MaTL_CheckedChanged(object sender, EventArgs e)
        {
            txtTKTL_TenTL.Enabled = false;
            txtTKTL_MaTL.Enabled = true;

            txtTKTL_TenTL.Text = string.Empty;
        }

        private void btnTKTL_TimKiem_Click(object sender, EventArgs e)
        {
            dgvDstl.Rows.Clear();
            dgvDstl.Refresh();
            if (pnlLoaiTimKiem.Visible == false)
            {
                if (rdoTKTL_MaTL.Checked)
                {
                    DTO_TaiLieu tl = new DTO_TaiLieu();
                    tl = bll_TL.timTLTheoMaTL(txtTKTL_MaTL.Text);

                    if (tl.MaTL != null)
                    {
                        string[] dgRow = new string[] { "1", tl.MaTL, tl.TenTL, tl.LoaiTL };
                        dgvDstl.Rows.Add(dgRow);
                    }
                }
                if (rdoTKTL_TenTL.Checked)
                {
                    List<DTO_TaiLieu> tl = bll_TL.timTLTheoTen(txtTKTL_TenTL.Text);
                    int i = 1;
                    foreach (DTO_TaiLieu row in tl)
                    {
                        string[] dgRow = new string[] { i++.ToString(), row.MaTL, row.TenTL, row.LoaiTL };
                        dgvDstl.Rows.Add(dgRow);
                    }
                }
            }
            else
            {
                if (rdoLoaiTL.Checked)
                {
                    List<DTO_TaiLieu> tl = bll_TL.timTLTheoLoai(cboTKTL_LoaiTL.Text);
                    int i = 1;
                    foreach (DTO_TaiLieu row in tl)
                    {
                        string[] dgRow = new string[] { i++.ToString(), row.MaTL, row.TenTL, row.LoaiTL };
                        dgvDstl.Rows.Add(dgRow);
                    }
                }

                if (rdoLinhVuc.Checked)
                {
                    List<DTO_TaiLieu> tl = bll_TL.timTLTheoLinhVuc(cboLinhVuc.Text);
                    int i = 1;
                    foreach (DTO_TaiLieu row in tl)
                    {
                        string[] dgRow = new string[] { i++.ToString(), row.MaTL, row.TenTL, row.LoaiTL };
                        dgvDstl.Rows.Add(dgRow);
                    }
                }
            }
        }

        private void btnTKTL_Xemtatca_Click(object sender, EventArgs e)
        {
            dgvDstl.Rows.Clear();
            dgvDstl.Refresh();
            ReadData_DSTL();
        }

        private void ReadData_DSTL()
        {
            List<DTO_TaiLieu> tl = bll_TL.xemTatCaTL();
            int i = 1;
            foreach (DTO_TaiLieu row in tl)
            {
                string[] dgRow = new string[] { i++.ToString(), row.MaTL, row.TenTL, row.LoaiTL };
                dgvDstl.Rows.Add(dgRow);
            }
        }

        private void btnTKTL_LapPM_Click(object sender, EventArgs e)
        {
            foreach (Control ct in this.Controls)
            {
                if (ct is Panel && ct.Name.Contains("pnl"))
                {
                    ct.Visible = false;
                }
            }
            pnlLapPM.Visible = true;
            pnlTool.Visible = false;
            lblTitle.Text = "Lập phiếu mượn";
        }

        private void pnlLapPM_VisibleChanged(object sender, EventArgs e)
        {
            if (pnlLapPM.Visible == true)
            {
                ShowDSDG();

                AddCol_TaiLieuMuon();
                ShowTLM();
            }
        }

        private void ShowDSDG()
        {
            ltbDocGia.Items.Clear();
            List<DTO_DocGia> dg = bll_DG.xemTatCaDG();
            foreach (DTO_DocGia row in dg)
            {
                string dgRow = row.MaDG + "          " + row.HoTen;
                ltbDocGia.Items.Add(dgRow);
            }
        }

        private void ShowTLM()
        {

            List<DTO_TaiLieu> tl = bll_TL.xemTLCoTheMuon();
            foreach (DTO_TaiLieu row in tl)
            {
                string[] tlRow = new string[] { false.ToString(), row.MaTL, row.TenTL, row.LoaiTL, row.TrangThai };
                dgvTaiLieuMuon.Rows.Add(tlRow);
            }
        }

        private void AddCol_TaiLieuMuon()
        {
            dgvTaiLieuMuon.Rows.Clear();
            dgvTaiLieuMuon.Columns.Clear();
            dgvTaiLieuMuon.Refresh();

            DataGridViewCheckBoxColumn checkColumn = new DataGridViewCheckBoxColumn();
            checkColumn.Width = 50;
            checkColumn.ReadOnly = false;
            checkColumn.FillWeight = 10;

            dgvTaiLieuMuon.Columns.Add(checkColumn);
            dgvTaiLieuMuon.Columns.Add(new DataGridViewTextBoxColumn { Name = "Mã tài liệu" });
            dgvTaiLieuMuon.Columns.Add(new DataGridViewTextBoxColumn { Name = "Tên tài liệu" });
            dgvTaiLieuMuon.Columns.Add(new DataGridViewTextBoxColumn { Name = "Loại tài liệu" });
            dgvTaiLieuMuon.Columns.Add(new DataGridViewTextBoxColumn { Name = "Trạng thái" });
            dgvTaiLieuMuon.Columns[1].ReadOnly = true;
            dgvTaiLieuMuon.Columns[2].ReadOnly = true;
            dgvTaiLieuMuon.Columns[3].ReadOnly = true;
            dgvTaiLieuMuon.Columns[4].ReadOnly = true;
            int temp = dgvTaiLieuMuon.RowCount;
            dgvTaiLieuMuon.Rows[temp - 1].ReadOnly = true;
        }

        private void ltbDocGia_SelectedIndexChanged(object sender, EventArgs e)
        {
            int temp = dgvTaiLieuMuon.RowCount;
            dgvTaiLieuMuon.Rows[temp - 1].ReadOnly = true;
            foreach (DataGridViewRow row in dgvTaiLieuMuon.Rows)
            {
                row.Cells[0].Value = "False";
            }
        }

        private void btnLPM_LapPhieu_Click(object sender, EventArgs e)
        {
            if (ltbDocGia.SelectedItem == null)
                MessageBox.Show("Chưa chọn độc giả để lập phiếu mượn", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            else
            {
                int soTL = 1;
                string madg = ltbDocGia.SelectedItem.ToString().Substring(0, ltbDocGia.SelectedItem.ToString().IndexOf("    "));
                if (bll_DG.kiemTraPhieuPhat(madg))
                {
                    MessageBox.Show("Độc giả này không thể mượn sách do trả sách trễ hạn và đang bị phạt", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                }
                else
                {

                    int flag = 0;

                    string mapm = bll_PM.LapPM(madg);

                    List<string> listMaTL = new List<string>();
                    foreach (DataGridViewRow row in dgvTaiLieuMuon.Rows)
                    {
                        DTO_PhieuMuon temp = new DTO_PhieuMuon();
                        if (row.Cells[0].Value.ToString() == "True")
                        {
                            DTO_DocGia dg = new DTO_DocGia();
                            dg = bll_DG.timDGTheoMaDG(madg);

                            if (row.Cells[3].Value.ToString() == "Đặc biệt" && dg.Loai != "Giảng viên")
                            {
                                flag = 2;
                                bll_PM.xoaPM(mapm);
                                MessageBox.Show("Chỉ giảng viên mới được mượn tài liệu đặc biệt", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                                break;
                            }
                            else
                            {

                                flag = 1;

                                string matl = row.Cells[1].Value.ToString();

                                DTO_LoaiDG loai = new DTO_LoaiDG();

                                loai = bll_LDG.xemChiTietLoaiDG(dg.Loai);


                                if (bll_DG.SoSachMuon(madg) + soTL > loai.SoSachMuonToiDa)
                                {
                                    flag = 2;

                                    bll_PM.xoaPM(mapm);
                                    MessageBox.Show("Số tài liệu mượn vượt giới hạn", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                                    break;
                                }
                                else
                                {
                                    bll_PM.LapTaiLieuPM(mapm, matl, loai.SoNgayMuonToiDa);
                                    listMaTL.Add(matl);
                                }
                            }
                        }
                    }
                    if (flag == 1)
                    {
                        foreach (string matl in listMaTL)
                        {
                            bll_TL.capNhatTrangThaiTL(matl);
                        }
                        MessageBox.Show("Lập phiếu mượn thành công", "Information", MessageBoxButtons.OK, MessageBoxIcon.Asterisk);

                        dgvTaiLieuMuon.Rows.Clear();
                        dgvTaiLieuMuon.Refresh();
                        ShowTLM();
                        ltbDocGia_SelectedIndexChanged(sender, e);
                    }
                    if (flag == 0)
                    {

                        bll_PM.xoaPM(mapm);
                        MessageBox.Show("Chưa chọn tài liệu để lập phiếu mượn", "Information", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    }
                }
            }
        }

        private void rdoTKPM_MaDG_CheckedChanged(object sender, EventArgs e)
        {
            txtTKPM_MaDG.Enabled = true;
            txtTKPM_MaPM.Enabled = false;
            txtTKPM_MaPM.Text = string.Empty;
        }

        private void rdoTKPM_MaPM_CheckedChanged(object sender, EventArgs e)
        {
            txtTKPM_MaDG.Enabled = false;
            txtTKPM_MaPM.Enabled = true;
            txtTKPM_MaDG.Text = string.Empty;
        }

        private void btnTKPM_Timkiem_Click(object sender, EventArgs e)
        {
            dgvDSPM.Rows.Clear();
            dgvDSPM.Refresh();
            if (rdoTKPM_MaPM.Checked)
            {
                DTO_PhieuMuon pm = new DTO_PhieuMuon();
                pm = bll_PM.timPMTheoMaPM(txtTKPM_MaPM.Text);
                if (pm.MaPM != null)
                {
                    string[] dgRow = new string[] { "1", pm.MaPM, pm.MaDG };
                    dgvDSPM.Rows.Add(dgRow);
                }
            }
            if (rdoTKPM_MaDG.Checked)
            {
                List<DTO_PhieuMuon> pm = bll_PM.timPMTheoMaDG(txtTKPM_MaDG.Text);
                int i = 1;
                foreach (DTO_PhieuMuon row in pm)
                {
                    string[] dgRow = new string[] { i++.ToString(), row.MaPM, row.MaDG };
                    dgvDSPM.Rows.Add(dgRow);
                }
            }
        }

        private void btnTKPM_Xemtatca_Click(object sender, EventArgs e)
        {
            dgvDSPM.Rows.Clear();
            dgvDSPM.Refresh();
            ReadData_DSPM();
        }

        private void ReadData_DSPM()
        {
            List<DTO_PhieuMuon> pm = bll_PM.xemTatCaPM();
            int i = 1;
            foreach (DTO_PhieuMuon row in pm)
            {
                string[] dgRow = new string[] { i++.ToString(), row.MaPM, row.MaDG };
                dgvDSPM.Rows.Add(dgRow);
            }
        }

        private void btnTKPM_XemPMQH_Click(object sender, EventArgs e)
        {
            dgvDSPM.Rows.Clear();
            dgvDSPM.Refresh();
            ReadData_DSPM_quaHan();
        }

        private void ReadData_DSPM_quaHan()
        {
            List<DTO_PhieuMuon> pm = bll_PM.xemTatCaPMQH();
            int i = 1;
            foreach (DTO_PhieuMuon row in pm)
            {
                string[] dgRow = new string[] { i++.ToString(), row.MaPM, row.MaDG };
                dgvDSPM.Rows.Add(dgRow);
            }
        }

        private void btnPhieuMuon_Click(object sender, EventArgs e)
        {
            dgvDSPM.Rows.Clear();
            dgvDSPM.Refresh();
            foreach (Control ct in this.Controls)
            {
                if (ct is Panel && ct.Name.Contains("pnl"))
                {
                    ct.Visible = false;
                }
            }
            pnlTimKiemPM.Visible = true;
            pnlTool.Visible = true;
            lblTitle.Text = "Tìm kiếm phiếu mượn";
        }

        private void btnTKPM_LapPT_Click(object sender, EventArgs e)
        {
            tlMuon.Clear();

            foreach (Control ct in this.Controls)
            {
                if (ct is Panel && ct.Name.Contains("pnl"))
                {
                    ct.Visible = false;
                }
            }
            pnlLapPT.Visible = true;
            pnlTool.Visible = false;
            lblTitle.Text = "Lập phiếu trả";
        }

        private void btnTKPM_LapPM_Click(object sender, EventArgs e)
        {
            foreach (Control ct in this.Controls)
            {
                if (ct is Panel && ct.Name.Contains("pnl"))
                {
                    ct.Visible = false;
                }
            }
            pnlLapPM.Visible = true;
            pnlTool.Visible = false;
            lblTitle.Text = "Lập phiếu mượn";
        }

        private void pnlLapPT_VisibleChanged(object sender, EventArgs e)
        {
            if (pnlLapPT.Visible == true)
            {
                ShowDSPM();

                AddCol_TaiLieuDaMuon();
            }
        }

        private void ShowDSPM()
        {
            ltbPhieuMuon.Items.Clear();
            List<DTO_PhieuMuon> pm = bll_PM.xemTatCaPM();
            foreach (DTO_PhieuMuon row in pm)
            {
                string pmRow = row.MaPM + "          " + row.MaDG;
                ltbPhieuMuon.Items.Add(pmRow);
            }
        }

        private void AddCol_TaiLieuDaMuon()
        {
            dgvTaiLieuDaMuon.Rows.Clear();
            dgvTaiLieuDaMuon.Columns.Clear();
            dgvTaiLieuDaMuon.Refresh();

            DataGridViewCheckBoxColumn checkColumn = new DataGridViewCheckBoxColumn();
            checkColumn.Width = 50;
            checkColumn.ReadOnly = false;
            checkColumn.FillWeight = 10;

            dgvTaiLieuDaMuon.Columns.Add(checkColumn);
            dgvTaiLieuDaMuon.Columns.Add(new DataGridViewTextBoxColumn { Name = "Mã tài liệu" });
            dgvTaiLieuDaMuon.Columns.Add(new DataGridViewTextBoxColumn { Name = "Tên tài liệu" });
            dgvTaiLieuDaMuon.Columns.Add(new DataGridViewTextBoxColumn { Name = "Loại tài liệu" });
            dgvTaiLieuDaMuon.Columns[1].ReadOnly = true;
            dgvTaiLieuDaMuon.Columns[2].ReadOnly = true;
            dgvTaiLieuDaMuon.Columns[3].ReadOnly = true;
            int temp = dgvTaiLieuDaMuon.RowCount;
            dgvTaiLieuDaMuon.Rows[temp - 1].ReadOnly = true;
        }

        private void ShowTLDaMuon()
        {
            if (ltbPhieuMuon.SelectedItem != null)
            {
                string mapm = ltbPhieuMuon.SelectedItem.ToString().Substring(0, ltbPhieuMuon.SelectedItem.ToString().IndexOf("    "));
                //dgvTaiLieuDaMuon.ReadOnly = true;

                if (ltbPhieuMuon.SelectedItem != null)
                {
                    List<DTO_TaiLieu> tl = bll_TL.xemTLChuaTra(mapm);
                    foreach (DTO_TaiLieu row in tl)
                    {
                        string[] tlRow = new string[] { false.ToString(), row.MaTL, row.TenTL, row.LoaiTL };
                        if (tlMuon.ContainsKey(row.MaTL))
                            tlRow[0] = true.ToString();
                        dgvTaiLieuDaMuon.Rows.Add(tlRow);
                    }
                }
            }
        }

        private void ltbPhieuMuon_SelectedIndexChanged(object sender, EventArgs e)
        {
            dgvTaiLieuDaMuon.Rows.Clear();
            dgvTaiLieuDaMuon.Refresh();

            ShowTLDaMuon();
            //dgvTaiLieuDaMuon.ReadOnly = false;
            int temp = dgvTaiLieuDaMuon.RowCount;
            dgvTaiLieuDaMuon.Rows[temp - 1].ReadOnly = true;
        }

        private void btnLPT_LapPhieu_Click(object sender, EventArgs e)
        {
            if (ltbPhieuMuon.SelectedItem == null)
                MessageBox.Show("Chưa chọn phiếu mượn để lập phiếu trả", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            else
            {
                int flag = 0;

                string mapt = bll_PT.LapPT();

                if (tlMuon.Count >= 1)
                {
                    flag = 1;
                    
                    foreach (var temp in tlMuon)
                    {
                        bll_PT.LapCTPT(mapt, temp.Value, temp.Key);
                        bll_PM.capNhatTrangThaiPM(temp.Value, temp.Key);
                        bll_TL.capNhatTrangThaiTLCoTheMuon(temp.Key);
                    }
                }
                if (flag == 1)
                {
                    MessageBox.Show("Lập phiếu trả thành công", "Information", MessageBoxButtons.OK, MessageBoxIcon.Asterisk);
                    dgvTaiLieuDaMuon.Rows.Clear();
                    dgvTaiLieuDaMuon.Refresh();
                    ShowTLDaMuon();
                }
                else
                {
                    MessageBox.Show("Chưa chọn tài liệu để lập phiếu trả", "Information", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    bll_PT.xoaPT(mapt);
                }
            }
            ShowDSPM();
            dgvTaiLieuDaMuon.Rows.Clear();
            dgvTaiLieuDaMuon.Refresh();
            tlMuon.Clear();
        }

        private void ReadData_DSPT()
        {
            List<DTO_PhieuTra> pt = bll_PT.xemTatCaPT();
            int i = 1;
            foreach (DTO_PhieuTra row in pt)
            {
                string[] dgRow = new string[] { i++.ToString(), row.MaPT };
                dgvDspt.Rows.Add(dgRow);
            }
        }

        private void btnTKPT_Xemtatca_Click(object sender, EventArgs e)
        {
            dgvDspt.Rows.Clear();
            dgvDspt.Refresh();
            ReadData_DSPT();
        }

        private void btnTKPT_TimKiem_Click(object sender, EventArgs e)
        {
            dgvDspt.Rows.Clear();
            dgvDspt.Refresh();
            if (rdoTKPT_MaPT.Checked)
            {
                DTO_PhieuTra pt = new DTO_PhieuTra();
                pt = bll_PT.timPTTheoMaPT(txtTKPT_MaPT.Text);
                if (pt.MaPT != null)
                {
                    string[] dgRow = new string[] { "1", pt.MaPT, pt.MaPM };
                    dgvDspt.Rows.Add(dgRow);
                }
            }
            if (rdoTKPT_MaPM.Checked)
            {
                DTO_PhieuTra pt = new DTO_PhieuTra();
                pt = bll_PT.timPTTheoMaPM(txtTKPT_MaPM.Text);
                if (pt.MaPT != null)
                {
                    string[] dgRow = new string[] { "1", pt.MaPT, pt.MaPM };
                    dgvDspt.Rows.Add(dgRow);
                }
            }

            if (rdoTKPT_MaDG.Checked)
            {
                List<DTO_PhieuTra> pt = new List<DTO_PhieuTra>();
                pt = bll_PT.timPTTheoMaDG(txtTKPT_MaDG.Text);
                int i = 1;
                foreach (DTO_PhieuTra row in pt)
                {
                    string[] dgRow = new string[] { i++.ToString(), row.MaPT, row.MaPM };
                    dgvDspt.Rows.Add(dgRow);
                }
            }
        }

        private void rdoTKPT_MaPT_CheckedChanged(object sender, EventArgs e)
        {
            txtTKPT_MaPT.Enabled = true;
            txtTKPT_MaPM.Enabled = false;
            txtTKPT_MaDG.Enabled = false;

            txtTKPT_MaPM.Text = string.Empty;
            txtTKPT_MaDG.Text = string.Empty;
        }

        private void rdoTKPT_MaPM_CheckedChanged(object sender, EventArgs e)
        {
            txtTKPT_MaPT.Enabled = false;
            txtTKPT_MaPM.Enabled = true;
            txtTKPT_MaDG.Enabled = false;

            txtTKPT_MaPT.Text = string.Empty;
            txtTKPT_MaDG.Text = string.Empty;
        }

        private void rdoTKPT_MaDG_CheckedChanged(object sender, EventArgs e)
        {
            txtTKPT_MaPT.Enabled = false;
            txtTKPT_MaPM.Enabled = false;
            txtTKPT_MaDG.Enabled = true;

            txtTKPT_MaPM.Text = string.Empty;
            txtTKPT_MaPT.Text = string.Empty;
        }

        private void btnPhieuTra_Click(object sender, EventArgs e)
        {
            dgvDspt.Rows.Clear();
            dgvDspt.Refresh();
            foreach (Control ct in this.Controls)
            {
                if (ct is Panel && ct.Name.Contains("pnl"))
                {
                    ct.Visible = false;
                }
            }
            pnlTimKiemPT.Visible = true;
            pnlTool.Visible = true;
            lblTitle.Text = "Tìm kiếm phiếu trả";
        }

        private void btnTKPT_LapPT_Click(object sender, EventArgs e)
        {
            tlMuon.Clear();

            foreach (Control ct in this.Controls)
            {
                if (ct is Panel && ct.Name.Contains("pnl"))
                {
                    ct.Visible = false;
                }
            }
            pnlLapPT.Visible = true;
            pnlTool.Visible = false;
            lblTitle.Text = "Lập phiếu trả";
        }

        private void btnTaiLieu_Click(object sender, EventArgs e)
        {
            dgvDstl.Rows.Clear();
            dgvDstl.Refresh();
            foreach (Control ct in this.Controls)
            {
                if (ct is Panel && ct.Name.Contains("pnl"))
                {
                    ct.Visible = false;
                }
            }
            pnlTimKiemTL.Visible = true;
            pnlTool.Visible = true;
            lblTitle.Text = "Tìm kiếm tài liệu";
        }

        private void btnYeuCauThemTL_Click(object sender, EventArgs e)
        {
            fRequest frequest = new fRequest();
            frequest.ShowDialog();
        }

        private void btnLoaiTimKiem_Click(object sender, EventArgs e)
        {
            if (btnLoaiTimKiem.Text == "Tìm kiếm nâng cao")
            {
                List<string> linhVuc = bll_TL.xemLinhVuc();
                cboLinhVuc.Items.Clear();
                foreach (string item in linhVuc)
                    cboLinhVuc.Items.Add(item);
	            List<string> loai = bll_TL.xemLoaiTL();
	            cboTKTL_LoaiTL.Items.Clear();
	            foreach (string item in loai)
		            cboTKTL_LoaiTL.Items.Add(item);
				if (rdoLinhVuc.Checked)
                    cboLinhVuc.Enabled = true;
                else
                    cboTKTL_LoaiTL.Enabled = true;
                btnLoaiTimKiem.Text = "Tìm kiếm thông thường";
                pnlLoaiTimKiem.Visible = true;
            }
            else
            {
                btnLoaiTimKiem.Text = "Tìm kiếm nâng cao";
                pnlLoaiTimKiem.Visible = false;
            }
        }

        private void rdoLoaiTL_CheckedChanged(object sender, EventArgs e)
        {
            cboTKTL_LoaiTL.Enabled = true;
            cboLinhVuc.Enabled = false;

            cboLinhVuc.Text = string.Empty;
        }

        private void rdoLinhVuc_CheckedChanged(object sender, EventArgs e)
        {
            cboTKTL_LoaiTL.Enabled = false;
            cboLinhVuc.Enabled = true;

            cboTKTL_LoaiTL.Text = string.Empty;
        }

        private void btnDangXuat_Click(object sender, EventArgs e)
        {
            this.Close();
        }

        private void btnBaoBieu_Click(object sender, EventArgs e)
        {
            foreach (Control ct in this.Controls)
            {
                if (ct is Panel && ct.Name.Contains("pnl"))
                {
                    ct.Visible = false;
                }
            }
            pnlBaoBieu.Visible = true;
            pnlTool.Visible = false;
            lblTitle.Text = "Lập báo biểu";

            cboDSTL_Thang.Text = string.Empty;
            cboDSDG_Thang.Text = string.Empty;
            cboDSTL_Nam.SelectedItem = null;
            cboDSDG_Nam.SelectedItem = null;
            cboDSTL_TenDG.SelectedItem = null;

            cboDSTL_Thang.Items.Clear();
            cboDSDG_Thang.Items.Clear();
            for (int i = 1; i <= 12; i++)
            {
                cboDSDG_Thang.Items.Add(i);
                cboDSTL_Thang.Items.Add(i);
            }
            string str = DateTime.Now.Year.ToString();
            for (int i = 2000; i <= int.Parse(str); i++)
            {
                cboDSTL_Nam.Items.Add(i);
                cboDSDG_Nam.Items.Add(i);
            }

            List<DTO_DocGia> listDG = new List<DTO_DocGia>();
            listDG = bll_DG.xemTatCaDG();
            for (int i = 0; i < listDG.Count; i++)
            {
                cboDSTL_TenDG.Items.Add(listDG[i].HoTen);
            }
        }

        private void rdoDSDGThang_CheckedChanged(object sender, EventArgs e)
        {
            cboDSDG_Thang.Enabled = true;
            cboDSDG_Nam.Enabled = true;
            cboDSTL_Thang.Enabled = false;
            cboDSTL_Nam.Enabled = false;
            cboDSTL_TenDG.Enabled = false;

            cboDSTL_Thang.SelectedItem = null;
            cboDSTL_Nam.SelectedItem = null;
            cboDSTL_TenDG.SelectedItem = null;
        }

        private void rdoDSTLThang_CheckedChanged(object sender, EventArgs e)
        {
            cboDSDG_Thang.Enabled = false;
            cboDSDG_Nam.Enabled = false;
            cboDSTL_Thang.Enabled = true;
            cboDSTL_Nam.Enabled = true;
            cboDSTL_TenDG.Enabled = false;

            cboDSDG_Thang.SelectedItem = null;
			cboDSDG_Nam.SelectedItem = null;
			cboDSTL_TenDG.SelectedItem = null;
		}

        private void rdoDSTLMaDG_CheckedChanged(object sender, EventArgs e)
        {
            cboDSDG_Thang.Enabled = false;
            cboDSDG_Nam.Enabled = false;
            cboDSTL_Thang.Enabled = false;
            cboDSTL_Nam.Enabled = false;
            cboDSTL_TenDG.Enabled = true;

            cboDSDG_Thang.SelectedItem = null;
            cboDSDG_Nam.SelectedItem = null;
            cboDSTL_Thang.SelectedItem = null;
            cboDSTL_Nam.SelectedItem = null;
        }

        private void btnLapBaoBieu_Click(object sender, EventArgs e)
        {
            if (rdoDSDGThang.Checked)
            {
                if (cboDSDG_Thang.Text.Length == 0)
                    MessageBox.Show("Chưa chọn tháng", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                else
                    if (cboDSDG_Nam.Text.Length == 0)
                        MessageBox.Show("Chưa chọn năm", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    else
                    {
                        DataTable dt = new DataTable();
                        dt = bll_DG.xemDGDangKyTheoThang(cboDSDG_Thang.Text, cboDSDG_Nam.Text);
                        BaoBieu_DG rp = new BaoBieu_DG();
                        rp.SetDataSource(dt);
                        fBaoBieu_DG rpt = new fBaoBieu_DG(rp);
                        rpt.ShowDialog();
                    }
            }
            if (rdoDSTLThang.Checked)
            {
                if (cboDSTL_Thang.Text.Length == 0)
                    MessageBox.Show("Chưa chọn tháng", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                else
                    if (cboDSTL_Nam.Text.Length == 0)
                        MessageBox.Show("Chưa chọn năm", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    else
                    {
                        DataTable dt = new DataTable();
                        dt = bll_TL.xemTLDaMuonTheoThang(cboDSTL_Thang.Text, cboDSTL_Nam.Text);
                        BaoBieu_TL rp = new BaoBieu_TL();
                        rp.SetDataSource(dt);
                        fBaoBieu_TL rpt = new fBaoBieu_TL(rp);
                        rpt.ShowDialog();
                    }
            }

            if (rdoDSTLMaDG.Checked)
            {
                if (cboDSTL_TenDG.Text.Length == 0)
                    MessageBox.Show("Chưa chọn độc giả", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                else
                {
                    DataTable dt = new DataTable();
                    dt = bll_TL.xemTLDocGiaDaMuon(cboDSTL_TenDG.Text);
                    BaoBieu_TLDGmuon rp = new BaoBieu_TLDGmuon();
                    rp.SetDataSource(dt);
                    fBaoBieu_TLDGmuon rpt = new fBaoBieu_TLDGmuon(rp);
                    rpt.ShowDialog();
                }
            }
        }

        private void dgvTaiLieuDaMuon_CellValueChanged(object sender, DataGridViewCellEventArgs e)
        {
            if (e.ColumnIndex == dgvTaiLieuDaMuon.Columns[0].Index && e.RowIndex != -1)
            {
                string mapm = ltbPhieuMuon.SelectedItem.ToString().Substring(0, ltbPhieuMuon.SelectedItem.ToString().IndexOf("    "));

                if (dgvTaiLieuDaMuon.Rows[e.RowIndex].Cells[e.ColumnIndex].Value.ToString() == "True")
                {

                    string matl = dgvTaiLieuDaMuon.Rows[e.RowIndex].Cells[e.ColumnIndex + 1].Value.ToString();
                    if (!tlMuon.ContainsKey(matl))
                        tlMuon.Add(matl, mapm);
                }
                else
                {
                    if (dgvTaiLieuDaMuon.Rows[e.RowIndex].Cells[e.ColumnIndex + 1].Value != null)
                    {
                        string matl = dgvTaiLieuDaMuon.Rows[e.RowIndex].Cells[e.ColumnIndex + 1].Value.ToString();
                        if (tlMuon.ContainsKey(matl))
                            tlMuon.Remove(matl);
                    }
                }
            }
        }

        private void btnTaiKhoan_Click(object sender, EventArgs e)
        {
            foreach (Control ct in this.Controls)
            {
                if (ct is Panel && ct.Name.Contains("pnl"))
                {
                    ct.Visible = false;
                }
            }
            pnlTaiKhoan.Visible = true;
            pnlTool.Visible = false;
            lblTitle.Text = "Tài khoản";

            lblHoTen.Text = tk.HoTen;
            lblTenTK.Text = tk.TenTK;

            pnlAdmin.Visible = false;
        }

        private void btnDoiMK_Click(object sender, EventArgs e)
        {
			if(!gbxMatKhau.Visible)
				gbxMatKhau.Visible = true;
			else
				gbxMatKhau.Visible = false;
		}

        private void btnDoiHoTen_Click(object sender, EventArgs e)
        {
			if(!gbxThongTin.Visible)
				gbxThongTin.Visible = true;
			else
				gbxThongTin.Visible = false;
		}

        private void btnMK_Doi_Click(object sender, EventArgs e)
        {
            if (txtMKMoi.Text.Length == 0)
            {
                MessageBox.Show("Mật khẩu không được để trống", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            else
                if (txtXacNhanMK.Text != txtMKMoi.Text)
                    MessageBox.Show("Mật khẩu xác nhận không khớp", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                else
                {
                    bll_TK.capNhatMK(tk.TenTK, txtMKMoi.Text);

                    MessageBox.Show("Cập nhật mật khẩu thành công", "Success", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    txtMKMoi.Text = string.Empty;
                    txtXacNhanMK.Text = string.Empty;
                    gbxMatKhau.Visible = false;

                    tk = bll_TK.xemTK(tk.TenTK);
                }
        }

        private void StyleForm()
        {
            txtMKMoi.PasswordChar = '*';
            txtXacNhanMK.PasswordChar = '*';
            txtMKMoi.MaxLength = 32;
            txtXacNhanMK.MaxLength = 32;

            txtHoTenMoi.MaxLength = 40;
            txtXacNhanHoTen.MaxLength = 40;
            if (tk.LoaiTK == "Quản trị")
                btnAdmin.Enabled = true;
            else
                btnAdmin.Enabled = false;
        }

        private void btnHoTen_Doi_Click(object sender, EventArgs e)
        {
            if (txtHoTenMoi.Text.Length == 0)
            {
                MessageBox.Show("Họ tên không được để trống", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            else
                if (txtXacNhanHoTen.Text != txtHoTenMoi.Text)
                    MessageBox.Show("Họ tên xác nhận không khớp", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                else
                {
                    bll_TK.capNhatHoTen(tk.TenTK, txtHoTenMoi.Text);

                    MessageBox.Show("Cập nhật họ tên thành công", "Success", MessageBoxButtons.OK, MessageBoxIcon.Information);
                    txtHoTenMoi.Text = string.Empty;
                    txtXacNhanHoTen.Text = string.Empty;
                    gbxThongTin.Visible = false;

                    tk = bll_TK.xemTK(tk.TenTK);
                    lblHoTen.Text = tk.HoTen;
                }
        }

        private void btnAdmin_Click(object sender, EventArgs e)
        {
            pnlAdmin.Visible = true;
            pnlThemTL.Visible = false;

            cboDoiQD_LoaiDG.Items.Clear();
            List<DTO_LoaiDG> ldg = new List<DTO_LoaiDG>();
            ldg = bll_LDG.xemLoaiDG();
            foreach (var item in ldg)
            {
                cboDoiQD_LoaiDG.Items.Add(item.LoaiDG);
            }

            foreach (Control control in gbxDoiQD.Controls)
            {
                if (control is TextBox)
                    control.Text = string.Empty;
            }
            foreach (Control control in gbxThemLDG.Controls)
            {
                if (control is TextBox)
                    control.Text = string.Empty;
            }
        }

        private void btnAdmin_TaiKhoan_Click(object sender, EventArgs e)
        {
            pnlAdmin.Visible = false;
        }

        private void btnAdmin_ThemLoaiDG_Click(object sender, EventArgs e)
        {
            int phiTN, soSachTD, soNgayTD;
            if (txtThemLDG_LoaiDG.Text.Length == 0)
                MessageBox.Show("Loại độc giả không được để trống", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            else
                if (!int.TryParse(txtThemLDG_PhiThuongNien.Text, out phiTN))
                    MessageBox.Show("Phí thường niên phải là số", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                else
                    if (!int.TryParse(txtThemLDG_SoSachToiDa.Text, out soSachTD))
                        MessageBox.Show("Số sách mượn tối đa phải là số", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    else
                        if (!int.TryParse(txtThemLDG_SoNgayToiDa.Text, out soNgayTD))
                            MessageBox.Show("Số ngày mượn tối đa phải là số", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        else
                        {
                            DTO_LoaiDG ldg = new DTO_LoaiDG();
                            ldg.LoaiDG = txtThemLDG_LoaiDG.Text;
                            ldg.PhiThuongNien = phiTN;
                            ldg.SoNgayMuonToiDa = soNgayTD;
                            ldg.SoSachMuonToiDa = soSachTD;

                            bll_LDG.themLoaiDG(ldg);
                            MessageBox.Show("Thêm loại độc giả thành công", "Success", MessageBoxButtons.OK, MessageBoxIcon.Information);
                            btnAdmin_Click(sender, e);
                        }
        }

        private void cboDoiQD_LoaiDG_SelectedIndexChanged(object sender, EventArgs e)
        {
            List<DTO_LoaiDG> ldg = new List<DTO_LoaiDG>();
            ldg = bll_LDG.xemLoaiDG();
            foreach (var item in ldg)
            {
                if (cboDoiQD_LoaiDG.SelectedItem.ToString() == item.LoaiDG)
                {
                    txtDoiQD_PhiThuongNien.Text = item.PhiThuongNien.ToString();
                    txtDoiQD_SoSachToiDa.Text = item.SoSachMuonToiDa.ToString();
                    txtDoiQD_SoNgayToiDa.Text = item.SoNgayMuonToiDa.ToString();
                }
            }
        }

        private void btnAdmin_ThayDoiQD_Click(object sender, EventArgs e)
        {
            int phiTN, soSachTD, soNgayTD;
            if (!int.TryParse(txtDoiQD_PhiThuongNien.Text, out phiTN))
                MessageBox.Show("Phí thường niên phải là số", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            else
                if (!int.TryParse(txtDoiQD_SoSachToiDa.Text, out soSachTD))
                    MessageBox.Show("Số sách mượn tối đa phải là số", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                else
                    if (!int.TryParse(txtDoiQD_SoNgayToiDa.Text, out soNgayTD))
                        MessageBox.Show("Số ngày muợn tối đa phải là số", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    else
                    {
                        DTO_LoaiDG ldg = new DTO_LoaiDG();
                        ldg.LoaiDG = cboDoiQD_LoaiDG.Text;
                        ldg.PhiThuongNien = phiTN;
                        ldg.SoNgayMuonToiDa = soNgayTD;
                        ldg.SoSachMuonToiDa = soSachTD;

                        bll_LDG.chinhSuaLoaiDG(ldg);
                        MessageBox.Show("Chỉnh sửa loại độc giả thành công", "Success", MessageBoxButtons.OK, MessageBoxIcon.Information);
                        btnAdmin_Click(sender, e);
                    }
        }

        private void btnLapPhieuCanhCao_Click(object sender, EventArgs e)
        {
            if (dgvDsdg.SelectedRows.Count == 0 || dgvDsdg.SelectedRows[0].Cells[0].Value == null)
            {
                MessageBox.Show("Chưa chọn độc giả nào để lập phiếu cảnh cáo", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            }
            else
            {
                foreach (DataGridViewRow row in dgvDsdg.SelectedRows)
                {
                    string madg = row.Cells[1].Value.ToString();
                    bll_DG.themPhieuCanhCao(madg);

                    MessageBox.Show("Lập phiếu cảnh cáo thành công", "Success", MessageBoxButtons.OK, MessageBoxIcon.Information);
                }
            }
        }

        private void lblMinimize_Click(object sender, EventArgs e)
        {
            this.WindowState = FormWindowState.Minimized;
        }

        private void btnDGPhat_Click(object sender, EventArgs e)
        {
            dgvDsdg.Rows.Clear();
            dgvDsdg.Refresh();

            List<DTO_DocGia> dg = bll_DG.xemDGBiPhat();
            int i = 1;
            foreach (DTO_DocGia row in dg)
            {
                string[] dgRow = new string[] { i++.ToString(), row.MaDG, row.HoTen, row.CMND, row.Loai, row.SoSachDangMuon.ToString(), row.SoSachQuaHan.ToString() };
                dgvDsdg.Rows.Add(dgRow);
            }
        }

        private void btnTKPM_XemPMDT_Click(object sender, EventArgs e)
        {
            dgvDSPM.Rows.Clear();
            dgvDSPM.Refresh();
            ReadData_DSPMDaTra();
        }

        private void ReadData_DSPMDaTra()
        {
            List<DTO_PhieuMuon> pm = bll_PM.xemTatCaPMDaTra();
            int i = 1;
            foreach (DTO_PhieuMuon row in pm)
            {
                string[] dgRow = new string[] { i++.ToString(), row.MaPM, row.MaDG };
                dgvDSPM.Rows.Add(dgRow);
            }
        }

        private void btnAdmin_ThemTL_Click(object sender, EventArgs e)
        {
            if (!pnlThemTL.Visible)
            {
                dgvThemTL.Rows.Clear();
                dgvThemTL.Refresh();
                ReadData_DSTLThem();

                pnlThemTL.Visible = true;

                List<string> linhVuc = bll_TL.xemLinhVuc();
                cboThemLinhVuc.Items.Clear();
                foreach (string item in linhVuc)
                    cboThemLinhVuc.Items.Add(item);
                List<string> loai = bll_TL.xemLoaiTL();
                cboLoaiTL.Items.Clear();
                foreach (string item in loai)
                    cboLoaiTL.Items.Add(item);
                List<string> manguon = bll_TL.xemMaNguonTL();
                cboMaNguonTL.Items.Clear();
                foreach (string item in manguon)
                    cboMaNguonTL.Items.Add(item);
                List<string> TenTL = bll_TL.xemTenTLThem();
                cboTenTL.Items.Clear();
                foreach (string item in TenTL)
                    cboTenTL.Items.Add(item);

	            cboThemLinhVuc.Text = string.Empty;
				cboLoaiTL.SelectedItem = null;
				cboMaNguonTL.SelectedItem = null;
	            cboTenTL.Text = string.Empty;

            }
            else
            {
                pnlThemTL.Visible = false;
            }
        }

        private void AddCol_ThemTL()
        {
            dgvThemTL.ColumnCount = 2;
            dgvThemTL.Columns[0].Name = "Tên tài liệu";
            dgvThemTL.Columns[1].Name = "Lĩnh vực";
        }

        private void ReadData_DSTLThem()
        {
            List<DTO_TaiLieu> tl = bll_TL.xemTatCaTLThem();
            foreach (DTO_TaiLieu row in tl)
            {
                string[] dgRow = new string[] { row.TenTL, row.LinhVuc };
                dgvThemTL.Rows.Add(dgRow);
            }
        }

        private void btnThemTL_Click(object sender, EventArgs e)
        {
            if(cboTenTL.Text.Length == 0)
                MessageBox.Show("Chưa chọn tên tài liệu", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
            else
                if(cboMaNguonTL.Text.Length == 0)
                    MessageBox.Show("Chưa chọn mã nguồn tài liệu", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                else
                    if(cboLoaiTL.Text.Length == 0)
                        MessageBox.Show("Chưa chọn loại tài liệu", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                    else
                        if (cboThemLinhVuc.Text.Length == 0)
                            MessageBox.Show("Chưa chọn lĩnh vực", "Error", MessageBoxButtons.OK, MessageBoxIcon.Error);
                        else
                        {
                            DTO_TaiLieu tl = new DTO_TaiLieu();
                            tl.MaNguonTL = cboMaNguonTL.Text;
                            tl.TenTL = cboTenTL.Text;
                            tl.LoaiTL = cboLoaiTL.Text;
                            tl.LinhVuc = cboThemLinhVuc.Text;
                            tl.TrangThai = "Có thể mượn";
                            bll_TL.ThemTLYeuCau(tl);

                            MessageBox.Show("Thêm tài liệu thành công", "Success", MessageBoxButtons.OK, MessageBoxIcon.Information);
                            dgvThemTL.Rows.Clear();
                            dgvThemTL.Refresh();
                            ReadData_DSTLThem();

							cboThemLinhVuc.Text = string.Empty;
	                        cboLoaiTL.SelectedItem = null;
	                        cboMaNguonTL.SelectedItem = null;
	                        cboTenTL.Text = string.Empty;
			}
        }
    }
}
