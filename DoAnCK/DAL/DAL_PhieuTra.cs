﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DTO;
using System.Data;
using System.Data.SqlClient;

namespace DAL
{
    public class DAL_PhieuTra
    {
        Provider p = new Provider();

        //public void LapPT()
        //{
        //    p.Connect();
        //    string strSql = "Insert into PhieuTra Default values";
        //    p.ExecuteNonQuery(CommandType.Text, strSql);
        //    p.Disconnect();
        //}

        public string LapPT()
        {
            p.Connect();
            //string strSql = "Insert into PhieuMuon values(@madg) Select distinct @@IDENTITY from PhieuMuon";
            string strSql = "Insert into PhieuTra Default values Select MaPT From PhieuTra Where stt = (Select distinct @@IDENTITY from PhieuTra)";
            //p.ExecuteNonQuery(CommandType.Text, strSql,
            //new SqlParameter { ParameterName = "@madg", Value = madg });
            DataTable dt = p.Select(CommandType.Text, strSql);
            p.Disconnect();
            string kq = "";
            foreach (DataRow row in dt.Rows)
                kq = row[0].ToString();
            return kq;
        }

        public DataTable MaPTMoiNhat()
        {
            p.Connect();
            string strSql = "Select Max(MaPT) from PhieuTra";
            DataTable dt = p.Select(CommandType.Text, strSql);
            p.Disconnect();
            return dt;
        }

        public void LapCTPT(string mapt, string mapm, string matl)
        {
            p.Connect();
            string strSql = "Insert into CTPhieuTra values(@mapt, Getdate(), @mapm, @matl)";
            p.ExecuteNonQuery(CommandType.Text, strSql,
                new SqlParameter { ParameterName = "@mapt", Value = mapt },
                new SqlParameter { ParameterName = "@mapm", Value = mapm },
                new SqlParameter { ParameterName = "@matl", Value = matl });
            p.Disconnect();
        }

        public DataTable xemTatCaPT()
        {
            p.Connect();

            DataTable dt = p.Select(CommandType.Text, "Exec XemTatCaPT");
            p.Disconnect();
            return dt;
        }

        public DataTable timPTTheoMaPT(string maPT)
        {
            string strSql = "Select distinct MaPT, MaPM From CTPhieuTra Where MaPT= " + "'" + maPT + "'";
            p.Connect();
            DataTable dt = p.Select(CommandType.Text, strSql);
            p.Disconnect();
            return dt;
        }

        public DataTable timPTTheoMaPM(string maPM)
        {
            string strSql = "Select distinct MaPT, MaPM From CTPhieuTra Where MaPM= " + "'" + maPM + "'";
            p.Connect();
            DataTable dt = p.Select(CommandType.Text, strSql);
            p.Disconnect();
            return dt;
        }

        public DataTable timPTTheoMaDG(string maDG)
        {
            string strSql = "Select distinct ct.MaPT, ct.MaPM From CTPhieuTra ct, PhieuMuon pm Where pm.MaPM=ct.MaPM and pm.MaDocGia= " + "'" + maDG + "'";
            p.Connect();
            DataTable dt = p.Select(CommandType.Text, strSql);
            p.Disconnect();
            return dt;
        }

        public DataTable timCTPTTheoMaPT(string maPT)
        {
            string strSql = "Select* From CTPhieuTra Where MaPT=" + "'" + maPT + "'";
            p.Connect();
            DataTable dt = p.Select(CommandType.Text, strSql);
            p.Disconnect();
            return dt;
        }

        public void chinhSuaPT(DTO_PhieuTra pt)
        {
            p.Connect();
            p.ExecuteNonQuery(CommandType.StoredProcedure, "ChinhSuaPT",
                    new SqlParameter { ParameterName = "@mapt", Value = pt.MaPT },
                    new SqlParameter { ParameterName = "@mapm", Value = pt.MaPM },
                    new SqlParameter { ParameterName = "@matl", Value = pt.MaTL },
                    new SqlParameter { ParameterName = "@ngaylap", Value = pt.NgayLapPT });
            p.Disconnect();
        }

        public void xoaPT(string maPT)
        {
            p.Connect();
            string strSql = "Delete PhieuTra Where MaPT = " + "'" + maPT + "'";
            p.ExecuteNonQuery(CommandType.Text, strSql);
            p.Disconnect();
        }
    }
}
