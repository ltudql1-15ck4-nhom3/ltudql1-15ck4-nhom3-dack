﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DTO;
using System.Data;
using System.Data.SqlClient;

namespace DAL
{
    public class DAL_PhieuMuon
    {
        Provider p = new Provider();

        public string LapPM(string madg)
        {
            p.Connect();
            //string strSql = "Insert into PhieuMuon values(@madg) Select distinct @@IDENTITY from PhieuMuon";
            string strSql = "Insert into PhieuMuon values('" + madg + "') Select MaPM From PhieuMuon Where stt = (Select distinct @@IDENTITY from PhieuMuon)";
            //p.ExecuteNonQuery(CommandType.Text, strSql,
                //new SqlParameter { ParameterName = "@madg", Value = madg });
            DataTable dt = p.Select(CommandType.Text, strSql);
            p.Disconnect();
            string kq="";
            foreach (DataRow row in dt.Rows)
                kq = row[0].ToString();
            return kq;
        }

        public void LapTaiLieuPM(string mapm, string matl, int dl)
        {
            p.Connect();
            string strSql = "Insert into CTPhieuMuon values(@mapm, Getdate(), Getdate()+" + dl.ToString() + ", @matl, "+ "N'Chưa trả')";
            p.ExecuteNonQuery(CommandType.Text, strSql,
                new SqlParameter { ParameterName = "@mapm", Value = mapm },
                new SqlParameter { ParameterName = "@matl", Value = matl });
            p.Disconnect();
        }

        public DataTable MaPMMoiNhat()
        {
            p.Connect();
            string strSql = "Select distinct @@IDENTITY from PhieuMuon";
            DataTable dt = p.Select(CommandType.Text, strSql);
            p.Disconnect();
            return dt;
        }

        public DataTable timPMTheoMaPM(string maPM)
        {
            string strSql = "Select* From PhieuMuon Where MaPM=" + "'" + maPM + "'";
            p.Connect();
            DataTable dt = p.Select(CommandType.Text, strSql);
            p.Disconnect();
            return dt;
        }

        public DataTable timPMTheoMaDG(string maDG)
        {
            string strSql = "Select * From PhieuMuon Where MaDocGia=" + "'" + maDG + "'";
            p.Connect();
            DataTable dt = p.Select(CommandType.Text, strSql);
            p.Disconnect();
            return dt;
        }

        public DataTable xemTatCaPM()
        {
            p.Connect();

            DataTable dt = p.Select(CommandType.Text, "Exec XemTatCaPM");
            p.Disconnect();
            return dt;
        }

        public DataTable xemTatCaPMDaTra()
        {
            p.Connect();

            DataTable dt = p.Select(CommandType.Text, "Exec XemTatCaPMDaTra");
            p.Disconnect();
            return dt;
        }

        public DataTable xemTatCaPMQH()
        {
            p.Connect();

            DataTable dt = p.Select(CommandType.Text, "Exec XemTatCaPMQH");
            p.Disconnect();
            return dt;
        }

        public DataTable timCTPMTheoMaPM(string maPM)
        {
            string strSql = "Select* From CTPhieuMuon Where MaPM=" + "'" + maPM + "'";
            p.Connect();
            DataTable dt = p.Select(CommandType.Text, strSql);
            p.Disconnect();
            return dt;
        }

        public void chinhSuaPM(DTO_PhieuMuon pm)
        {
            p.Connect();
            p.ExecuteNonQuery(CommandType.StoredProcedure, "ChinhSuaPM",
                    new SqlParameter { ParameterName = "@mapm", Value = pm.MaPM },
                    new SqlParameter { ParameterName = "@ngaylap", Value = pm.NgayLap },
                    new SqlParameter { ParameterName = "@hanchot", Value = pm.HanChotTra },
                    new SqlParameter { ParameterName = "@matl", Value = pm.MaTL },
                    new SqlParameter { ParameterName = "@trangthai", Value = pm.TrangThai });
            p.Disconnect();
        }

        public void xoaPM(string maPM)
        {
            p.Connect();
            string strSql = "Delete PhieuMuon Where MaPM = " + "'" + maPM + "'";
            p.ExecuteNonQuery(CommandType.Text, strSql);
            p.Disconnect();
        }

        public void capNhatTrangThaiPM(string mapm, string matl)
        {
            p.Connect();
            string strSql = "Update CTPhieuMuon Set TrangThai = N'Đã trả' Where MaPM = @mapm and MaTaiLieu = @matl";
            p.ExecuteNonQuery(CommandType.Text, strSql,
                new SqlParameter { ParameterName = "@mapm", Value = mapm },
                new SqlParameter { ParameterName = "@matl", Value = matl });
            p.Disconnect();
        }
    }
}
