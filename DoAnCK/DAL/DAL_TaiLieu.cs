﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DTO;
using System.Data;
using System.Data.SqlClient;

namespace DAL
{
    public class DAL_TaiLieu
    {
        Provider p = new Provider();

        public void chinhSuaTL(DTO_TaiLieu tl)
        {
            p.Connect();
            p.ExecuteNonQuery(CommandType.StoredProcedure, "ChinhSuaTL",
                    new SqlParameter { ParameterName = "@matl", Value = tl.MaTL },
                    new SqlParameter { ParameterName = "@tentl", Value = tl.TenTL },
                    new SqlParameter { ParameterName = "@loaitl", Value = tl.LoaiTL },
                    new SqlParameter { ParameterName = "@trangthai", Value = tl.TrangThai });
            p.Disconnect();
        }

        public DataTable timTLTheoMaTL(string maTL)
        {
            string strSql = "Select* From TaiLieu Where MaTaiLieu=" + "'" + maTL + "'";
            p.Connect();
            DataTable dt = p.Select(CommandType.Text, strSql);
            p.Disconnect();
            return dt;
        }

        public void xoaTL(string maTL)
        {
            p.Connect();
            string strSql = "Delete TaiLieu Where MaTaiLieu = " + "'" + maTL + "'";
            p.ExecuteNonQuery(CommandType.Text, strSql);
            p.Disconnect();
        }

        public DataTable timTLTheoTen(string ten)
        {
            string strSql = "Select MaTaiLieu, TenTaiLieu, LoaiTaiLieu From TaiLieu Where TenTaiLieu like " + "N'%" + ten + "%'";
            p.Connect();
            DataTable dt = p.Select(CommandType.Text, strSql);
            p.Disconnect();
            return dt;
        }

        public DataTable xemTatCaTL()
        {
            p.Connect();

            DataTable dt = p.Select(CommandType.Text, "Exec XemTatCaTL");
            p.Disconnect();
            return dt;
        }

        public DataTable xemTLCoTheMuon()
        {
            p.Connect();

            DataTable dt = p.Select(CommandType.Text, "Select MaTaiLieu, TenTaiLieu, LoaiTaiLieu, TrangThai from TaiLieu Where TrangThai = N'Có thể mượn' and LoaiTaiLieu <> N'Hiếm'");
            p.Disconnect();
            return dt;
        }

        public DataTable xemTLChuaTra(string mapm)
        {
            p.Connect();
            string strSql = "Select tl.MaTaiLieu, tl.TenTaiLieu, tl.LoaiTaiLieu, tl.TrangThai from TaiLieu tl, CTPhieuMuon ct Where ct.TrangThai = N'Chưa trả' and ct.MaTaiLieu = tl.MaTaiLieu and ct.MaPM =" + "'" + mapm + "'";
            DataTable dt = p.Select(CommandType.Text, strSql);
            p.Disconnect();
            return dt;
        }

        public void capNhatTrangThaiTL(string matl)
        {
            p.Connect();
            string strSql = "Update TaiLieu Set TrangThai = N'Đã mượn' Where MaTaiLieu = @matl ";
            p.ExecuteNonQuery(CommandType.Text, strSql, new SqlParameter { ParameterName = "@matl", Value = matl });
            p.Disconnect();
        }

        public void capNhatTrangThaiTLCoTheMuon(string matl)
        {
            p.Connect();
            string strSql = "Update TaiLieu Set TrangThai = N'Có thể mượn' Where MaTaiLieu = @matl ";
            p.ExecuteNonQuery(CommandType.Text, strSql, new SqlParameter { ParameterName = "@matl", Value = matl });
            p.Disconnect();
        }

        public void themYeuCauTL(string tentl, string linhvuc)
        {
            p.Connect();
            string strSql = "insert into TLYeuCau values(@tentl, @linhvuc)";
            p.ExecuteNonQuery(CommandType.Text, strSql,
                new SqlParameter { ParameterName = "@tentl", Value = tentl },
                new SqlParameter { ParameterName = "@linhvuc", Value = linhvuc });
            p.Disconnect();
        }

        public DataTable timTLTheoLoai(string loaiTL)
        {
            string strSql = "Select MaTaiLieu, TenTaiLieu, LoaiTaiLieu From TaiLieu Where LoaiTaiLieu = " + "N'" + loaiTL + "'";
            p.Connect();
            DataTable dt = p.Select(CommandType.Text, strSql);
            p.Disconnect();
            return dt;
        }

        public DataTable timTLTheoLinhVuc(string linhVuc)
        {
            string strSql = "Select MaTaiLieu, TenTaiLieu, LoaiTaiLieu From TaiLieu Where LinhVuc = " + "N'" + linhVuc + "'";
            p.Connect();
            DataTable dt = p.Select(CommandType.Text, strSql);
            p.Disconnect();
            return dt;
        }

        public DataTable xemLinhVuc()
        {
            string strSql = "Select distinct LinhVuc from TaiLieu";
            p.Connect();
            DataTable dt = p.Select(CommandType.Text, strSql);
            p.Disconnect();
            return dt;
        }

        public DataTable xemTLDaMuonTheoThang(string thang, string nam)
        {
            p.Connect();
            string strSql = "Select distinct * from TaiLieu tl, CTPhieuMuon ct Where tl.MaTaiLieu = ct.MaTaiLieu and Year(ct.NgayLapPM) = "+ nam + " and Month(ct.NgayLapPM) = "  + thang;
            DataTable dt = p.Select(CommandType.Text, strSql);
            p.Disconnect();
            return dt;
        }

        public DataTable xemTLDocGiaDaMuon(string tendg)
        {
            p.Connect();
            string strSql = "Select distinct tl.* from TaiLieu tl, CTPhieuMuon ct, DocGia dg, PhieuMuon pm Where dg.HoTen = N'" + tendg + "' and dg.MaDocGia = pm.MaDocGia and pm.MaPM = ct.MaPM and ct.MaTaiLieu = tl.MaTaiLieu";
            DataTable dt = p.Select(CommandType.Text, strSql);
            p.Disconnect();
            return dt;
        }

        public DataTable xemTatCaTLThem()
        {
            p.Connect();
            string strSql = "Select * from TLYeuCau";
            DataTable dt = p.Select(CommandType.Text, strSql);
            p.Disconnect();
            return dt;
        }

        public DataTable xemLoaiTL()
        {
            string strSql = "Select distinct LoaiTaiLieu from TaiLieu";
            p.Connect();
            DataTable dt = p.Select(CommandType.Text, strSql);
            p.Disconnect();
            return dt;
        }

        public DataTable xemMaNguonTL()
        {
            string strSql = "Select MaNguonTL from NguonTaiLieu";
            p.Connect();
            DataTable dt = p.Select(CommandType.Text, strSql);
            p.Disconnect();
            return dt;
        }

        public DataTable xemTenTLThem()
        {
            string strSql = "Select TenTL from TLYeuCau";
            p.Connect();
            DataTable dt = p.Select(CommandType.Text, strSql);
            p.Disconnect();
            return dt;
        }

        public void ThemTLYeuCau(DTO_TaiLieu tl)
        {
            p.Connect();
            string strSql = "Insert into TaiLieu values(@manguonTL, @tenTL, @loaiTL, @linhvuc, @trangthai)";
            p.ExecuteNonQuery(CommandType.Text, strSql,
                new SqlParameter { ParameterName = "@manguonTL", Value = tl.MaNguonTL },
                new SqlParameter { ParameterName = "@tenTL", Value = tl.TenTL },
                new SqlParameter { ParameterName = "@loaiTL", Value = tl.LoaiTL },
                new SqlParameter { ParameterName = "@linhvuc", Value = tl.LinhVuc },
                new SqlParameter { ParameterName = "@trangthai", Value = tl.TrangThai });
            p.Disconnect();
        }
    }
}
