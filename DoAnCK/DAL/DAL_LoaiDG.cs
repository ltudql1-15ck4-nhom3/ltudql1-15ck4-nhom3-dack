﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DTO;
using System.Data;
using System.Data.SqlClient;

namespace DAL
{
    public class DAL_LoaiDG
    {
        Provider p = new Provider();

        public DataTable xemChiTietLoaiDG(string loai)
        {
            p.Connect();
            string strSql = "Select * from LoaiDG where Loai = N'" + loai + "'";
            DataTable dt = p.Select(CommandType.Text, strSql);
            p.Disconnect();
            return dt;
        }

        public void themLoaiDG(DTO_LoaiDG ldg)
        {
            p.Connect();
            string strSql = "Insert into LoaiDG values(@loai, @phiTN, @sosachTD, @songayTD)";
            p.ExecuteNonQuery(CommandType.Text, strSql,
                new SqlParameter { ParameterName = "@loai", Value = ldg.LoaiDG },
                new SqlParameter { ParameterName = "@phiTN", Value = ldg.PhiThuongNien },
                new SqlParameter { ParameterName = "@sosachTD", Value = ldg.SoSachMuonToiDa },
                new SqlParameter { ParameterName = "@songayTD", Value = ldg.SoNgayMuonToiDa });
            p.Disconnect();
        }

        public DataTable xemLoaiDG()
        {
            p.Connect();
            string strSql = "Select * from LoaiDG";
            DataTable dt = p.Select(CommandType.Text, strSql);
            p.Disconnect();
            return dt;
        }

        public void chinhSuaLoaiDG(DTO_LoaiDG ldg)
        {
            p.Connect();
            string strSql = "Update LoaiDG Set PhiThuongNien = @phiTN, SoSachMuonToiDa = @soSachTD, SoNgayMuonToiDa = @soNgayTD Where Loai = @loai";
            p.ExecuteNonQuery(CommandType.Text, strSql,
                new SqlParameter { ParameterName = "@loai", Value = ldg.LoaiDG },
                new SqlParameter { ParameterName = "@phiTN", Value = ldg.PhiThuongNien },
                new SqlParameter { ParameterName = "@sosachTD", Value = ldg.SoSachMuonToiDa },
                new SqlParameter { ParameterName = "@songayTD", Value = ldg.SoNgayMuonToiDa });
            p.Disconnect();
        }
    }
}
