﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DTO;
using System.Data;
using System.Data.SqlClient;

namespace DAL
{
    public class DAL_TaiKhoan
    {
        Provider p = new Provider();

        public DataTable TimKiemTK(string id)
        {
            p.Connect();
            string strSql = "Select TenTK From TaiKhoan Where TenTK = " + "'" + id + "'";
            DataTable dt = p.Select(CommandType.Text, strSql);
            p.Disconnect();
            return dt;
        }

        public DataTable TimKiemMK(string id, string pass)
        {
            p.Connect();
            string strSql = "Select MatKhau From TaiKhoan Where MatKhau = " + "'" + pass + "' and TenTK = '"+id+"'";
            DataTable dt = p.Select(CommandType.Text, strSql);
            p.Disconnect();
            return dt;
        }

        public void LuuTK(string hoten, string id, string pass)
        {
            p.Connect();
            string strSql = "Insert into TaiKhoan values(@id, @pass, @hoten, @loai)";
            p.ExecuteNonQuery(CommandType.Text, strSql,
                new SqlParameter { ParameterName = "@id", Value = id },
                new SqlParameter { ParameterName = "@pass", Value = pass },
                new SqlParameter { ParameterName = "@hoten", Value = hoten },
                new SqlParameter { ParameterName = "@loai", Value = "Nhân viên" });
            p.Disconnect();
        }

        public DataTable xemTK(string id)
        {
            p.Connect();
            string strSql = "Select* from TaiKhoan Where TenTK = '" + id + "'";
            DataTable dt = p.Select(CommandType.Text, strSql);
            p.Disconnect();
            return dt;
        }

        public void capNhatMK(string id, string pass)
        {
            string strSql = "Update TaiKhoan Set MatKhau = @pass where TenTK = @id";
            p.Connect();
            p.ExecuteNonQuery(CommandType.Text, strSql,
                new SqlParameter { ParameterName = "@id", Value = id },
                new SqlParameter { ParameterName = "@pass", Value = pass });
            p.Disconnect();
        }

        public void capNhatHoTen(string id, string hoten)
        {
            p.Connect();
            string strSql = "Update TaiKhoan Set HoTen = @hoten where TenTK = @id";
            p.ExecuteNonQuery(CommandType.Text, strSql,
                new SqlParameter { ParameterName = "@id", Value = id },
                new SqlParameter { ParameterName = "@hoten", Value = hoten });
            p.Disconnect();
        }
    }
}
