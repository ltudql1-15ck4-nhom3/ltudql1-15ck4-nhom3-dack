﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DTO;
using System.Data;
using System.Data.SqlClient;

namespace DAL
{
    public class DAL_DocGia
    {
        Provider p = new Provider();

        public void ThemDG(DTO_DocGia dg)
        {
            p.Connect();
            string strSql = "Insert into DocGia values(@cmnd, @hoten, @sdt, @email, @ngaysinh, @diachi, @loai, @maso, getdate())";
            p.ExecuteNonQuery(CommandType.Text, strSql,
                new SqlParameter { ParameterName = "@cmnd", Value = dg.CMND },
                new SqlParameter { ParameterName = "@hoten", Value = dg.HoTen },
                new SqlParameter { ParameterName = "@sdt", Value = dg.SDT },
                new SqlParameter { ParameterName = "@email", Value = dg.Email },
                new SqlParameter { ParameterName = "@ngaysinh", Value = dg.NgaySinh },
                new SqlParameter { ParameterName = "@diachi", Value = dg.DiaChi },
                new SqlParameter { ParameterName = "@loai", Value = dg.Loai },
                new SqlParameter { ParameterName = "@maso", Value = dg.MaSo });
            p.Disconnect();
        }

        public DataTable timDGTheoCMND(string cmnd)
        {
            p.Connect();
            string sql = "Select* from DocGia where CMND = " + cmnd;
            DataTable reader = p.Select(CommandType.Text, sql);
            p.Disconnect();
            return reader;
        }

        public DataTable xemTatCaDG()
        {
            p.Connect();

            DataTable dt = p.Select(CommandType.Text, "Exec XemTatCaDG");
            p.Disconnect();
            return dt;
        }

        public int soSachMuon(string madg)
        {
            p.Connect();
            SqlParameter kq = new SqlParameter("@kq", SqlDbType.Int);
            kq.Direction = ParameterDirection.Output;
            p.ExecuteNonQuery(CommandType.StoredProcedure, "SoSachMuon",
                new SqlParameter { ParameterName = "madg", Value = madg }, kq);
            p.Disconnect();
            return int.Parse(kq.Value.ToString());      
        }

        public int soSachQuaHan(string madg)
        {
            p.Connect();
            SqlParameter kq = new SqlParameter("@kq", SqlDbType.Int);
            kq.Direction = ParameterDirection.Output;
            p.ExecuteNonQuery(CommandType.StoredProcedure, "SoSachQuaHan",
                new SqlParameter { ParameterName = "madg", Value = madg }, kq);
            p.Disconnect();
            return int.Parse(kq.Value.ToString());
        }

        public DataTable timDGTheoMaDG(string maDG)
        {
            string strSql = "Select* From DocGia Where MaDocGia=" + "'" + maDG + "'";
            p.Connect();
            DataTable dt = p.Select(CommandType.Text, strSql);
            p.Disconnect();
            return dt;
        }

        public DataTable timDGTheoMaSo(string maSo)
        {
            string strSql = "Select MaDocGia, HoTen, CMND, Loai From DocGia Where MaSo=" + "'" + maSo + "'";
            p.Connect();
            DataTable dt = p.Select(CommandType.Text, strSql);
            p.Disconnect();
            return dt;
        }

        public DataTable timDGTheoHoTen(string hoTen)
        {
            string strSql = "Select MaDocGia, HoTen, CMND, Loai From DocGia Where HoTen Like " + "N'%" + hoTen + "%'";
            p.Connect();
            DataTable dt = p.Select(CommandType.Text, strSql);
            p.Disconnect();
            return dt;
        }

        public void chinhSuaDG(DTO_DocGia dg)
        {
            p.Connect();
            p.ExecuteNonQuery(CommandType.StoredProcedure, "ChinhSuaDG",
                    new SqlParameter { ParameterName = "@madg", Value = dg.MaDG },
                    new SqlParameter { ParameterName = "@hoten", Value = dg.HoTen },
                    new SqlParameter { ParameterName = "@cmnd", Value = dg.CMND },
                    new SqlParameter { ParameterName = "@sdt", Value = dg.SDT },
                    new SqlParameter { ParameterName = "@email", Value = dg.Email },
                    new SqlParameter { ParameterName = "@ngaysinh", Value = dg.NgaySinh },
                    new SqlParameter { ParameterName = "@diachi", Value = dg.DiaChi },
                    new SqlParameter { ParameterName = "@loai", Value = dg.Loai },
                    new SqlParameter { ParameterName = "@maso", Value = dg.MaSo });
            p.Disconnect();
        }

        public void xoaDG(string maDG)
        {
            p.Connect();
            string strSql = "Delete DocGia Where MaDocGia = " + "'" + maDG + "'";
            p.ExecuteNonQuery(CommandType.Text, strSql);
            p.Disconnect();
        }

        public void themPhieuCanhCao(string madg)
        {
            p.Connect();
            string strSql = "Insert into PhieuCanhCao values(@madg)";
            p.ExecuteNonQuery(CommandType.Text, strSql,
                new SqlParameter { ParameterName = "@madg", Value = madg });
            p.Disconnect();
        }

        public DataTable kiemTraPhieuPhat(string madg)
        {
            p.Connect();
            string strSql = "Select * from PhieuPhat Where MaDocGia = '"+ madg + "'";
            DataTable dt = p.Select(CommandType.Text, strSql);
            p.Disconnect();
            return dt;
        }

        public DataTable xemDGDangKyTheoThang(string thang, string nam)
        {
            p.Connect();
            string strSql = "Select distinct * from DocGia Where Year(NgayDangKi) = " + nam + " and Month(NgayDangKi) = " + thang;
            DataTable dt = p.Select(CommandType.Text, strSql);
            p.Disconnect();
            return dt;
        }

        public DataTable xemDGBiPhat()
        {
            p.Connect();
            string strSql = "Select distinct dg.MaDocGia, dg.HoTen, dg.CMND, dg.Loai from DocGia dg, PhieuPhat pp Where pp.MaDocGia = dg.MaDocGia";
            DataTable dt = p.Select(CommandType.Text, strSql);
            p.Disconnect();
            return dt;
        }
    }
}
