﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DTO
{
    public class DTO_PhieuMuon
    {
        string _maPM;
        public string MaPM
        {
            get { return _maPM; }
            set { _maPM = value; }
        }

        string _maDG;
        public string MaDG
        {
            get { return _maDG; }
            set { _maDG = value; }
        }

        string _ngayLap;
        public string NgayLap
        {
            get { return _ngayLap; }
            set { _ngayLap = value; }
        }

        string _hanChotTra;
        public string HanChotTra
        {
            get { return _hanChotTra; }
            set { _hanChotTra = value; }
        }

        string _maTL;
        public string MaTL
        {
            get { return _maTL; }
            set { _maTL = value; }
        }

        string _trangThai;
        public string TrangThai
        {
            get { return _trangThai; }
            set { _trangThai = value; }
        }
    }
}
