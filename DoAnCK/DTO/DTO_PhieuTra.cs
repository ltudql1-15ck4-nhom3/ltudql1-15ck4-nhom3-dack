﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DTO
{
    public class DTO_PhieuTra
    {
        string _maPT;
        public string MaPT
        {
            get { return _maPT; }
            set { _maPT = value; }
        }

        string _ngayLapPT;
        public string NgayLapPT
        {
            get { return _ngayLapPT; }
            set { _ngayLapPT = value; }
        }

        string _maPM;
        public string MaPM
        {
            get { return _maPM; }
            set { _maPM = value; }
        }

        string _maTL;
        public string MaTL
        {
            get { return _maTL; }
            set { _maTL = value; }
        }
    }
}
