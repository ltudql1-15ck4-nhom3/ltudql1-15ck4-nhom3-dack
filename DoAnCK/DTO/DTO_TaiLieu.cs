﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DTO
{
    public class DTO_TaiLieu
    {
        string _maNguonTL;
        public string MaNguonTL
        {
            get { return _maNguonTL; }
            set { _maNguonTL = value; }
        }

        string _maTaiLieu;
        public string MaTL
        {
            get { return _maTaiLieu; }
            set { _maTaiLieu = value; }
        }

        string _tenTaiLieu;
        public string TenTL
        {
            get { return _tenTaiLieu; }
            set { _tenTaiLieu = value; }
        }

        string _loaiTaiLieu;
        public string LoaiTL
        {
            get { return _loaiTaiLieu; }
            set { _loaiTaiLieu = value; }
        }

        string _trangThai;
        public string TrangThai
        {
            get { return _trangThai; }
            set { _trangThai = value; }
        }

        string _linhVuc;
        public string LinhVuc
        {
            get { return _linhVuc; }
            set { _linhVuc = value; }
        }
    }
}
