﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DTO
{
    public class DTO_TaiKhoan
    {
        string _tenTK;
        public string TenTK
        {
            get { return _tenTK; }
            set { _tenTK = value; }
        }

        string _matKhau;
        public string MatKhau
        {
            get { return _matKhau; }
            set { _matKhau = value; }
        }

        string _hoTen;
        public string HoTen
        {
            get { return _hoTen; }
            set { _hoTen = value; }
        }

        string _loaiTK;
        public string LoaiTK
        {
            get { return _loaiTK; }
            set { _loaiTK = value; }
        }
    }
}
