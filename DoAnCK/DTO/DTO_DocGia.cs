﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DTO
{
    public class DTO_DocGia
    {
        private string _maDG;
        public string MaDG
        {
            get { return _maDG; }
            set { _maDG = value; }
        }
        private string _cmnd;
        public string CMND
        {
            get { return _cmnd; }
            set { _cmnd = value; }
        }
        private string _hoTen;
        public string HoTen
        {
            get { return _hoTen; }
            set { _hoTen = value; }
        }
        private string _sdt;
        public string SDT
        {
            get { return _sdt; }
            set { _sdt = value; }
        }
        private string _email;
        public string Email
        {
            get { return _email; }
            set { _email = value; }
        }
        private string _ngaySinh;
        public string NgaySinh
        {
            get { return _ngaySinh; }
            set { _ngaySinh = value; }
        }
        private string _diaChi;
        public string DiaChi
        {
            get { return _diaChi; }
            set { _diaChi = value; }
        }
        private string _loai;
        public string Loai
        {
            get { return _loai; }
            set { _loai = value; }
        }
        private string _maSo;
        public string MaSo
        {
            get { return _maSo; }
            set { _maSo = value; }
        }

        private int _soSachDangMuon;
        public int SoSachDangMuon
        {
            get { return _soSachDangMuon; }
            set { _soSachDangMuon = value; }
        }

        private int _soSachQuaHan;
        public int SoSachQuaHan
        {
            get { return _soSachQuaHan; }
            set { _soSachQuaHan = value; }
        }
    }
}
