﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;

namespace DTO
{
    public class DTO_LoaiDG
    {
        private string _loaiDG;
        public string LoaiDG
        {
            get { return _loaiDG; }
            set { _loaiDG = value; }
        }

        private int _phiThuongNien;
        public int PhiThuongNien
        {
            set { _phiThuongNien = value; }
            get
            {
                return _phiThuongNien;
            }
        }

        private int _soSachMuonToiDa;
        public int SoSachMuonToiDa
        {
            set { _soSachMuonToiDa = value; }
            get
            {
                return _soSachMuonToiDa;
            }
        }

        private int _soNgayMuonToiDa;
        public int SoNgayMuonToiDa
        {
            set { _soNgayMuonToiDa = value; }
            get
            {
                return _soNgayMuonToiDa;
            }
        }
    }
}
