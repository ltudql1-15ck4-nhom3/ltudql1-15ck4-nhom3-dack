﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DTO;
using DAL;
using System.Data;

namespace BLL
{
    public class BLL_LoaiDG
    {
        DAL_LoaiDG dal = new DAL_LoaiDG();

        public DTO_LoaiDG xemChiTietLoaiDG(string loai)
        {
            DataTable dt = dal.xemChiTietLoaiDG(loai);
            DTO_LoaiDG kq = new DTO_LoaiDG();
            foreach (DataRow row in dt.Rows)
            {
                kq.LoaiDG = loai;
                kq.PhiThuongNien = int.Parse(row[1].ToString());
                kq.SoSachMuonToiDa = int.Parse(row[2].ToString());
                kq.SoNgayMuonToiDa = int.Parse(row[3].ToString());
            }
            return kq;
        }

        public void themLoaiDG(DTO_LoaiDG ldg)
        {
            dal.themLoaiDG(ldg);
        }

        public List<DTO_LoaiDG> xemLoaiDG()
        {
            List<DTO_LoaiDG> kq = new List<DTO_LoaiDG>();
            DataTable dt = dal.xemLoaiDG();
            foreach (DataRow row in dt.Rows)
            {
                DTO_LoaiDG temp = new DTO_LoaiDG();
                temp.LoaiDG = row[0].ToString();
                temp.PhiThuongNien = int.Parse(row[1].ToString());
                temp.SoSachMuonToiDa = int.Parse(row[2].ToString());
                temp.SoNgayMuonToiDa = int.Parse(row[3].ToString());
                kq.Add(temp);
            }
            return kq;
        }

        public void chinhSuaLoaiDG(DTO_LoaiDG ldg)
        {
            dal.chinhSuaLoaiDG(ldg);
        }
    }
}
