﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DAL;
using DTO;
using System.Data;
using System.Security.Cryptography;

namespace BLL
{
    public class BLL_TaiKhoan
    {
        DAL_TaiKhoan dal = new DAL_TaiKhoan();
        string salt = "_q1u2y0n6h@";

        public bool KiemTraTK(string id)
        {
            DataTable dt = new DataTable();
            dt = dal.TimKiemTK(id);
            if (dt.Rows.Count != 0)
            {
                if (dt.Rows[0][0].ToString() == id)
                    return true;
            }
            return false;
        }

        public bool KiemTraMK(string id, string pass)
        {
            HashAlgorithm md5 = new MD5CryptoServiceProvider();
            pass = pass + salt;
            byte[] EncryptPass = md5.ComputeHash(Encoding.Default.GetBytes(pass));

            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < EncryptPass.Length; i++)
            {
                sb.Append(EncryptPass[i].ToString("x2"));
            }
            string EncryptPass_string = sb.ToString();

            DataTable dt = new DataTable();
            dt = dal.TimKiemMK(id, EncryptPass_string);
            if (dt.Rows.Count != 0)
            {
                if (dt.Rows[0][0].ToString() == EncryptPass_string)
                    return true;
            }
            return false;
        }

        public void LuuTK(string hoten, string id, string pass)
        {
            HashAlgorithm md5 = new MD5CryptoServiceProvider();
            pass = pass + salt;

            byte[] EncryptPass = md5.ComputeHash(Encoding.Default.GetBytes(pass));
            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < EncryptPass.Length; i++)
            {
                sb.Append(EncryptPass[i].ToString("x2"));
            }
            string EncryptPass_string = sb.ToString();

            dal.LuuTK(hoten, id, EncryptPass_string);
        }

        public DTO_TaiKhoan xemTK(string id)
        {
            DataTable dt = dal.xemTK(id);
            DTO_TaiKhoan tk = new DTO_TaiKhoan();
            foreach (DataRow row in dt.Rows)
            {
                tk.TenTK = row[0].ToString();
                tk.MatKhau = row[1].ToString();
                tk.HoTen = row[2].ToString();
                tk.LoaiTK = row[3].ToString();
            }
            return tk;
        }

        public void capNhatMK(string id, string pass)
        {
            HashAlgorithm md5 = new MD5CryptoServiceProvider();
            pass = pass + salt;
            byte[] EncryptPass = md5.ComputeHash(Encoding.Default.GetBytes(pass));

            StringBuilder sb = new StringBuilder();
            for (int i = 0; i < EncryptPass.Length; i++)
            {
                sb.Append(EncryptPass[i].ToString("x2"));
            }
            string EncryptPass_string = sb.ToString();

            dal.capNhatMK(id, EncryptPass_string);
        }

        public void capNhatHoTen(string id, string hoten)
        {
            dal.capNhatHoTen(id, hoten);
        }
    }
}
