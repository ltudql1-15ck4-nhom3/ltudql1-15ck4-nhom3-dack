﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DTO;
using DAL;
using System.Data;
using System.Globalization;

namespace BLL
{
    public class BLL_DocGia
    {
        DAL_DocGia dal = new DAL_DocGia();

        public void ThemDG(DTO_DocGia dg)
        {
            dal.ThemDG(dg);
        }

        public DTO_DocGia timDGTheoCMND(string cmnd)
        {
            DTO_DocGia dg = new DTO_DocGia();
            DataTable dt = dal.timDGTheoCMND(cmnd);
            foreach (DataRow dr in dt.Rows)
            {
                dg.MaDG = dr[1].ToString();
                dg.CMND = dr[2].ToString();
                dg.HoTen = dr[3].ToString();
                dg.SDT = dr[4].ToString();
                dg.Email = dr[5].ToString();
                dg.NgaySinh = dr[6].ToString();
                dg.DiaChi = dr[7].ToString();
                dg.Loai = dr[8].ToString();
                dg.MaSo = dr[9].ToString();
            }
            return dg;
        }

        public List<DTO_DocGia> xemTatCaDG()
        {
            DataTable dt = new DataTable();
            dt = dal.xemTatCaDG();
            List<DTO_DocGia> dg = new List<DTO_DocGia>();
            foreach (DataRow row in dt.Rows)
            {
                DTO_DocGia temp = new DTO_DocGia();
                temp.MaDG = row[0].ToString();
                temp.HoTen = row[1].ToString();
                temp.CMND = row[2].ToString();
                temp.Loai = row[3].ToString();
                temp.SoSachDangMuon = dal.soSachMuon(temp.MaDG);
                temp.SoSachQuaHan = dal.soSachQuaHan(temp.MaDG);
                dg.Add(temp);
            }
            return dg;
        }

        public DTO_DocGia timDGTheoMaDG(string maDG)
        {
            DTO_DocGia dg = new DTO_DocGia();
            DataTable dt = dal.timDGTheoMaDG(maDG);
            foreach(DataRow row in dt.Rows)
            {
                dg.MaDG = row[1].ToString();
                dg.CMND = row[2].ToString();
                dg.HoTen = row[3].ToString();
                dg.SDT = row[4].ToString();
                dg.Email = row[5].ToString();

                DateTime eDate = new DateTime();
                eDate = DateTime.Parse(row[6].ToString());
                string dayOfBirth = eDate.ToString("dd/MM/yyyy");
                dg.NgaySinh = dayOfBirth;

                dg.DiaChi = row[7].ToString();
                dg.Loai = row[8].ToString();
                dg.MaSo = row[9].ToString();
                dg.SoSachDangMuon = dal.soSachMuon(dg.MaDG);
                dg.SoSachQuaHan = dal.soSachQuaHan(dg.MaDG);
            }
            return dg;
        }

        public DTO_DocGia timDGTheoMaSo(string maSo)
        {
            DTO_DocGia dg = new DTO_DocGia();
            DataTable dt = dal.timDGTheoMaSo(maSo);
            foreach (DataRow row in dt.Rows)
            {
                dg.MaDG = row[0].ToString();
                dg.HoTen = row[1].ToString();
                dg.CMND = row[2].ToString();
                dg.Loai = row[3].ToString();
                dg.SoSachDangMuon = dal.soSachMuon(dg.MaDG);
                dg.SoSachQuaHan = dal.soSachQuaHan(dg.MaDG);
            }
            return dg;
        }

        public List<DTO_DocGia> timDGTheoHoTen(string hoTen)
        {
            List<DTO_DocGia> dg = new List<DTO_DocGia>();
            DataTable dt = dal.timDGTheoHoTen(hoTen);
            foreach (DataRow row in dt.Rows)
            {
                DTO_DocGia temp = new DTO_DocGia();
                temp.MaDG = row[0].ToString();
                temp.HoTen = row[1].ToString();
                temp.CMND = row[2].ToString();
                temp.Loai = row[3].ToString();
                temp.SoSachDangMuon = dal.soSachMuon(temp.MaDG);
                temp.SoSachQuaHan = dal.soSachQuaHan(temp.MaDG);
                dg.Add(temp);
            }
            return dg;
        }

        public void chinhSuaDG(DTO_DocGia dg)
        {
            DateTime dateOfBirth = DateTime.ParseExact(dg.NgaySinh, "dd/MM/yyyy", CultureInfo.InvariantCulture);
            dg.NgaySinh = dateOfBirth.ToString("MM-dd-yyyy");
            dal.chinhSuaDG(dg);
        }

        public void xoaDG(string maDG)
        {
            dal.xoaDG(maDG);
        }

        public int SoSachMuon(string madg)
        {
            return dal.soSachMuon(madg);
        }

        public void themPhieuCanhCao(string madg)
        {
            dal.themPhieuCanhCao(madg);
        }

        public bool kiemTraPhieuPhat(string madg)
        {
            DataTable dt = dal.kiemTraPhieuPhat(madg);
            if (dt.Rows.Count != 0)
                return true;
            return false;
        }

        public DataTable xemDGDangKyTheoThang(string thang, string nam)
        {
            return dal.xemDGDangKyTheoThang(thang, nam);
        }

        public List<DTO_DocGia> xemDGBiPhat()
        {
            DataTable dt = new DataTable();
            dt = dal.xemDGBiPhat();
            List<DTO_DocGia> dg = new List<DTO_DocGia>();
            foreach (DataRow row in dt.Rows)
            {
                DTO_DocGia temp = new DTO_DocGia();
                temp.MaDG = row[0].ToString();
                temp.HoTen = row[1].ToString();
                temp.CMND = row[2].ToString();
                temp.Loai = row[3].ToString();
                temp.SoSachDangMuon = dal.soSachMuon(temp.MaDG);
                temp.SoSachQuaHan = dal.soSachQuaHan(temp.MaDG);
                dg.Add(temp);
            }
            return dg;
        }
    }
}
