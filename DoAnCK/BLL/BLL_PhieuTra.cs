﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DTO;
using DAL;
using System.Data;
using System.Globalization;

namespace BLL
{
    public class BLL_PhieuTra
    {
        DAL_PhieuTra dal = new DAL_PhieuTra();

        public string LapPT()
        {
            return dal.LapPT();
        }

        public string MaPTMoiNhat()
        {
            string mapt = "";
            DataTable dt = dal.MaPTMoiNhat();
            foreach (DataRow row in dt.Rows)
            {
                mapt = row[0].ToString();
            }
            return mapt;
        }

        public void LapCTPT(string mapt, string mapm, string matl)
        {
            dal.LapCTPT(mapt, mapm, matl);
        }

        public List<DTO_PhieuTra> xemTatCaPT()
        {
            DataTable dt = new DataTable();
            dt = dal.xemTatCaPT();
            List<DTO_PhieuTra> pt = new List<DTO_PhieuTra>();
            foreach (DataRow row in dt.Rows)
            {
                DTO_PhieuTra temp = new DTO_PhieuTra();
                temp.MaPT = row[0].ToString();
                pt.Add(temp);
            }
            return pt;
        }

        public DTO_PhieuTra timPTTheoMaPT(string maPT)
        {
            DTO_PhieuTra pt = new DTO_PhieuTra();
            DataTable dt = dal.timPTTheoMaPT(maPT);
            foreach (DataRow row in dt.Rows)
            {
                pt.MaPT = row[0].ToString();
                pt.MaPM = row[1].ToString();
            }
            return pt;
        }

        public DTO_PhieuTra timPTTheoMaPM(string maPM)
        {
            DTO_PhieuTra pt = new DTO_PhieuTra();
            DataTable dt = dal.timPTTheoMaPM(maPM);
            foreach (DataRow row in dt.Rows)
            {
                pt.MaPT = row[0].ToString();
                pt.MaPM = row[1].ToString();
            }
            return pt;
        }

        public List<DTO_PhieuTra> timPTTheoMaDG(string maDG)
        {
            List<DTO_PhieuTra> pt = new List<DTO_PhieuTra>();
            DataTable dt = dal.timPTTheoMaDG(maDG);
            foreach (DataRow row in dt.Rows)
            {
                DTO_PhieuTra temp = new DTO_PhieuTra();
                temp.MaPT = row[0].ToString();
                temp.MaPM = row[1].ToString();
                pt.Add(temp);
            }
            return pt;
        }

        public List<DTO_PhieuTra> timCTPTTheoMaPT(string maPT)
        {
            List<DTO_PhieuTra> pt = new List<DTO_PhieuTra>();
            DataTable dt = dal.timCTPTTheoMaPT(maPT);
            foreach (DataRow row in dt.Rows)
            {
                DTO_PhieuTra temp = new DTO_PhieuTra();
                temp.MaPT = row[0].ToString();

                DateTime eDate = new DateTime();
                eDate = DateTime.Parse(row[1].ToString());
                string date = eDate.ToString("dd/MM/yyyy");
                temp.NgayLapPT = date;

                temp.MaPM = row[2].ToString();
                temp.MaTL = row[3].ToString();

                pt.Add(temp);
            }
            return pt;
        }

        public void chinhSuaPT(DTO_PhieuTra pt)
        {
            DateTime date = DateTime.ParseExact(pt.NgayLapPT, "dd/MM/yyyy", CultureInfo.InvariantCulture);
            pt.NgayLapPT = date.ToString("MM-dd-yyyy");
            dal.chinhSuaPT(pt);
        }

        public void xoaPT(string maPT)
        {
            dal.xoaPT(maPT);
        }
    }
}
