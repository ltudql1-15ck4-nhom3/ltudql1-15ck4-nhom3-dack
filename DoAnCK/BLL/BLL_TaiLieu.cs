﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DAL;
using DTO;
using System.Data;

namespace BLL
{
    public class BLL_TaiLieu
    {
        DAL_TaiLieu dal = new DAL_TaiLieu();

        public void chinhSuaTL(DTO_TaiLieu tl)
        {
            dal.chinhSuaTL(tl);
        }

        public DTO_TaiLieu timTLTheoMaTL(string maTL)
        {
            DTO_TaiLieu tl = new DTO_TaiLieu();
            DataTable dt = dal.timTLTheoMaTL(maTL);
            foreach (DataRow row in dt.Rows)
            {
                tl.MaNguonTL = row[1].ToString();
                tl.MaTL = row[2].ToString();
                tl.TenTL = row[3].ToString();
                tl.LoaiTL = row[4].ToString();
                tl.LinhVuc = row[5].ToString();
                tl.TrangThai = row[6].ToString();
            }
            return tl;
        }

        public void xoaTL(string maTL)
        {
            dal.xoaTL(maTL);
        }

        public List<DTO_TaiLieu> timTLTheoTen(string ten)
        {
            List<DTO_TaiLieu> tl = new List<DTO_TaiLieu>();
            DataTable dt = dal.timTLTheoTen(ten);
            foreach (DataRow row in dt.Rows)
            {
                DTO_TaiLieu temp = new DTO_TaiLieu();
                temp.MaTL = row[0].ToString();
                temp.TenTL = row[1].ToString();
                temp.LoaiTL = row[2].ToString();
                tl.Add(temp);
            }
            return tl;
        }

        public List<DTO_TaiLieu> xemTatCaTL()
        {
            DataTable dt = new DataTable();
            dt = dal.xemTatCaTL();
            List<DTO_TaiLieu> tl = new List<DTO_TaiLieu>();
            foreach (DataRow row in dt.Rows)
            {
                DTO_TaiLieu temp = new DTO_TaiLieu();
                temp.MaTL = row[0].ToString();
                temp.TenTL = row[1].ToString();
                temp.LoaiTL = row[2].ToString();
                tl.Add(temp);
            }
            return tl;
        }

        public List<DTO_TaiLieu> xemTLCoTheMuon()
        {
            DataTable dt = new DataTable();
            dt = dal.xemTLCoTheMuon();
            List<DTO_TaiLieu> tl = new List<DTO_TaiLieu>();
            foreach (DataRow row in dt.Rows)
            {
                DTO_TaiLieu temp = new DTO_TaiLieu();
                temp.MaTL = row[0].ToString();
                temp.TenTL = row[1].ToString();
                temp.LoaiTL = row[2].ToString();
                temp.TrangThai = row[3].ToString();
                tl.Add(temp);
            }
            return tl;
        }

        public List<DTO_TaiLieu> xemTLChuaTra(string mapm)
        {
            DataTable dt = new DataTable();
            dt = dal.xemTLChuaTra(mapm);
            List<DTO_TaiLieu> tl = new List<DTO_TaiLieu>();
            foreach (DataRow row in dt.Rows)
            {
                DTO_TaiLieu temp = new DTO_TaiLieu();
                temp.MaTL = row[0].ToString();
                temp.TenTL = row[1].ToString();
                temp.LoaiTL = row[2].ToString();
                temp.TrangThai = row[3].ToString();
                tl.Add(temp);
            }
            return tl;
        }

        public void capNhatTrangThaiTL(string matl)
        {
            dal.capNhatTrangThaiTL(matl);
        }

        public void capNhatTrangThaiTLCoTheMuon(string matl)
        {
            dal.capNhatTrangThaiTLCoTheMuon(matl);
        }

        public void themYeuCauTL(string tentl, string linhvuc)
        {
            dal.themYeuCauTL(tentl, linhvuc);
        }

        public List<DTO_TaiLieu> timTLTheoLoai(string loaiTL)
        {
            List<DTO_TaiLieu> tl = new List<DTO_TaiLieu>();
            DataTable dt = dal.timTLTheoLoai(loaiTL);
            foreach (DataRow row in dt.Rows)
            {
                DTO_TaiLieu temp = new DTO_TaiLieu();
                temp.MaTL = row[0].ToString();
                temp.TenTL = row[1].ToString();
                temp.LoaiTL = row[2].ToString();
                tl.Add(temp);
            }
            return tl;
        }

        public List<DTO_TaiLieu> timTLTheoLinhVuc(string linhVuc)
        {
            List<DTO_TaiLieu> tl = new List<DTO_TaiLieu>();
            DataTable dt = dal.timTLTheoLinhVuc(linhVuc);
            foreach (DataRow row in dt.Rows)
            {
                DTO_TaiLieu temp = new DTO_TaiLieu();
                temp.MaTL = row[0].ToString();
                temp.TenTL = row[1].ToString();
                temp.LoaiTL = row[2].ToString();
                tl.Add(temp);
            }
            return tl;
        }

        public List<string> xemLinhVuc()
        {
            List<string> linhVuc = new List<string>();
            DataTable dt = dal.xemLinhVuc();
            foreach (DataRow row in dt.Rows)
            {
                if(row[0].ToString() != "")
                    linhVuc.Add(row[0].ToString());
            }
            return linhVuc;
        }

        public DataTable xemTLDaMuonTheoThang(string thang, string nam)
        {
            return dal.xemTLDaMuonTheoThang(thang, nam);
        }

        public DataTable xemTLDocGiaDaMuon(string tendg)
        {
            return dal.xemTLDocGiaDaMuon(tendg);
        }

        public List<DTO_TaiLieu> xemTatCaTLThem()
        {
            DataTable dt = new DataTable();
            dt = dal.xemTatCaTLThem();
            List<DTO_TaiLieu> tl = new List<DTO_TaiLieu>();
            foreach (DataRow row in dt.Rows)
            {
                DTO_TaiLieu temp = new DTO_TaiLieu();
                temp.TenTL = row[0].ToString();
                temp.LinhVuc = row[1].ToString();
                tl.Add(temp);
            }
            return tl;
        }

        public List<string> xemLoaiTL()
        {
            List<string> loai = new List<string>();
            DataTable dt = dal.xemLoaiTL();
            foreach (DataRow row in dt.Rows)
            {
                if (row[0].ToString() != "")
                    loai.Add(row[0].ToString());
            }
            return loai;
        }

        public List<string> xemMaNguonTL()
        {
            List<string> ma = new List<string>();
            DataTable dt = dal.xemMaNguonTL();
            foreach (DataRow row in dt.Rows)
            {
                if (row[0].ToString() != "")
                    ma.Add(row[0].ToString());
            }
            return ma;
        }

        public List<string> xemTenTLThem()
        {
            List<string> ten = new List<string>();
            DataTable dt = dal.xemTenTLThem();
            foreach (DataRow row in dt.Rows)
            {
                if (row[0].ToString() != "")
                    ten.Add(row[0].ToString());
            }
            return ten;
        }

        public void ThemTLYeuCau(DTO_TaiLieu tl)
        {
            dal.ThemTLYeuCau(tl);
        }
    }
}
