﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using DAL;
using DTO;
using System.Data;
using System.Globalization;

namespace BLL
{
    public class BLL_PhieuMuon
    {
        DAL_PhieuMuon dal = new DAL_PhieuMuon();

        public string LapPM(string madg)
        {
            return dal.LapPM(madg);
        }

        public void LapTaiLieuPM(string mapm, string matl, int dl)
        {
            dal.LapTaiLieuPM(mapm, matl, dl);
        }

        public string MaPMMoiNhat()
        {
            string mapm = "";
            DataTable dt = dal.MaPMMoiNhat();
            foreach (DataRow row in dt.Rows)
            {
                mapm = row[0].ToString();
            }
            return mapm;
        }

        public DTO_PhieuMuon timPMTheoMaPM(string maPM)
        {
            DTO_PhieuMuon pm = new DTO_PhieuMuon();
            DataTable dt = dal.timPMTheoMaPM(maPM);
            foreach (DataRow row in dt.Rows)
            {
                pm.MaPM = row[1].ToString();
                pm.MaDG = row[2].ToString();
            }
            return pm;
        }

        public List<DTO_PhieuMuon> timPMTheoMaDG(string maDG)
        {
            List<DTO_PhieuMuon> pm = new List<DTO_PhieuMuon>();
            DataTable dt = dal.timPMTheoMaDG(maDG);
            foreach (DataRow row in dt.Rows)
            {
                DTO_PhieuMuon temp = new DTO_PhieuMuon();
                temp.MaPM = row[1].ToString();
                temp.MaDG = row[2].ToString();
                pm.Add(temp);
            }
            return pm;
        }

        public List<DTO_PhieuMuon> xemTatCaPM()
        {
            DataTable dt = new DataTable();
            dt = dal.xemTatCaPM();
            List<DTO_PhieuMuon> pm = new List<DTO_PhieuMuon>();
            foreach (DataRow row in dt.Rows)
            {
                DTO_PhieuMuon temp = new DTO_PhieuMuon();
                temp.MaPM = row[0].ToString();
                temp.MaDG = row[1].ToString();
                pm.Add(temp);
            }
            return pm;
        }

        public List<DTO_PhieuMuon> xemTatCaPMDaTra()
        {
            DataTable dt = new DataTable();
            dt = dal.xemTatCaPMDaTra();
            List<DTO_PhieuMuon> pm = new List<DTO_PhieuMuon>();
            foreach (DataRow row in dt.Rows)
            {
                DTO_PhieuMuon temp = new DTO_PhieuMuon();
                temp.MaPM = row[0].ToString();
                temp.MaDG = row[1].ToString();
                pm.Add(temp);
            }
            return pm;
        }

        public List<DTO_PhieuMuon> xemTatCaPMQH()
        {
            DataTable dt = new DataTable();
            dt = dal.xemTatCaPMQH();
            List<DTO_PhieuMuon> pm = new List<DTO_PhieuMuon>();
            foreach (DataRow row in dt.Rows)
            {
                DTO_PhieuMuon temp = new DTO_PhieuMuon();
                temp.MaPM = row[0].ToString();
                temp.MaDG = row[1].ToString();
                pm.Add(temp);
            }
            return pm;
        }

        public List<DTO_PhieuMuon> timCTPMTheoMaPM(string maPM)
        {
            List<DTO_PhieuMuon> pm = new List<DTO_PhieuMuon>();
            DataTable dt = dal.timCTPMTheoMaPM(maPM);
            foreach (DataRow row in dt.Rows)
            {
                DTO_PhieuMuon temp = new DTO_PhieuMuon();
                temp.MaPM = row[0].ToString();

                DateTime eDate = new DateTime();
                eDate = DateTime.Parse(row[1].ToString());
                string ngayLap = eDate.ToString("dd/MM/yyyy");
                temp.NgayLap = ngayLap;

                eDate = DateTime.Parse(row[2].ToString());
                string ngayTra = eDate.ToString("dd/MM/yyyy");
                temp.HanChotTra = ngayTra;

                temp.MaTL = row[3].ToString();
                temp.TrangThai = row[4].ToString();

                pm.Add(temp);
            }
            return pm;
        }

        public void chinhSuaPM(DTO_PhieuMuon pm)
        {
            DateTime date = DateTime.ParseExact(pm.NgayLap, "dd/MM/yyyy", CultureInfo.InvariantCulture);
            pm.NgayLap = date.ToString("MM-dd-yyyy");
            date = DateTime.ParseExact(pm.HanChotTra, "dd/MM/yyyy", CultureInfo.InvariantCulture);
            pm.HanChotTra = date.ToString("MM-dd-yyyy");
            dal.chinhSuaPM(pm);
        }

        public void xoaPM(string maPM)
        {
            dal.xoaPM(maPM);
        }

        public void capNhatTrangThaiPM(string mapm, string matl)
        {
            dal.capNhatTrangThaiPM(mapm, matl);
        }
    }

}
