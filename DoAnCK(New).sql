---Tạo CSDL
CREATE DATABASE QLThuVien;
GO
USE QLThuVien;
GO
------------------
CREATE TABLE DocGia
(
  stt		 INT IDENTITY(1, 1),
  MaDocGia	 AS 'DG' + RIGHT('0000' + CAST(stt AS VARCHAR(6)), 4)PERSISTED,
  CMND		 VARCHAR(12),
  HoTen		 NVARCHAR(50),
  Sdt		 VARCHAR(15),
  Email		 VARCHAR(50),
  NgaySinh	 DATE,
  DiaChi	 NVARCHAR(50),
  Loai		 NVARCHAR(50),
  MaSo		 VARCHAR(10),
  NgayDangKi DATE,
  CONSTRAINT PK_DocGia
	PRIMARY KEY (MaDocGia)
);

CREATE TABLE LoaiDG
(
  Loai			  NVARCHAR(50),
  PhiThuongNien	  INT,
  SoSachMuonToiDa INT,
  SoNgayMuonToiDa INT,
  CONSTRAINT PK_LoaiDG
	PRIMARY KEY (Loai)
);


CREATE TABLE PhieuMuon
(
  stt	   INT IDENTITY(1, 1),
  MaPM	   AS 'PM' + RIGHT('0000' + CAST(stt AS VARCHAR(6)), 4)PERSISTED,
  MaDocGia VARCHAR(6),
  CONSTRAINT PK_PhieuMuon
	PRIMARY KEY (MaPM),
);

CREATE TABLE PhieuTra
(
  stt  INT IDENTITY(1, 1),
  MaPT AS 'PT' + RIGHT('0000' + CAST(stt AS VARCHAR(6)), 4)PERSISTED,
  CONSTRAINT PK_PhieuTra
	PRIMARY KEY (MaPT)
);
CREATE TABLE CTPhieuMuon
(
  MaPM		 VARCHAR(6),
  NgayLapPM	 DATE,
  HanChotTra DATE,
  MaTaiLieu	 VARCHAR(6),
  TrangThai	 NVARCHAR(50),
  CONSTRAINT PK_CTPhieuMuon
	PRIMARY KEY
	(
	  MaPM,
	  MaTaiLieu
	),
	CHECK (TrangThai = N'Đã trả'
		 OR TrangThai = N'Chưa trả'
	)
);


CREATE TABLE CTPhieuTra
(
  MaPT		VARCHAR(6),
  NgayLapPT DATETIME,
  MaPM		VARCHAR(6),
  MaTaiLieu VARCHAR(6),
  CONSTRAINT PK_CTPhieuTra
	PRIMARY KEY
	(
	  MaPT,
	  MaPM,
	  MaTaiLieu
	)
);

CREATE TABLE NguonTaiLieu
(
  MaNguonTL VARCHAR(5),
  SoLuong	INT,
  CONSTRAINT PK_NguonTaiLieu
	PRIMARY KEY (MaNguonTL)
);

CREATE TABLE TaiLieu
(
  stt		  INT IDENTITY(1, 1),
  MaNguonTL	  VARCHAR(5),
  MaTaiLieu	  AS 'TL' + RIGHT('0000' + CAST(stt AS VARCHAR(6)), 4)PERSISTED,
  TenTaiLieu  NVARCHAR(100),
  LoaiTaiLieu NVARCHAR(50),
  LinhVuc	  NVARCHAR(50),
  TrangThai	  NVARCHAR(50),
  CONSTRAINT PK_TaiLieu
	PRIMARY KEY (MaTaiLieu),
  CHECK (TrangThai = N'Đã mượn'
		 OR TrangThai = N'Có thể mượn'
		)
);

CREATE TABLE TaiKhoan
(
  TenTK	  VARCHAR(50),
  MatKhau VARCHAR(64),
  HoTen	  NVARCHAR(100),
  LoaiTK  NVARCHAR(50),
  CONSTRAINT TenTK
	PRIMARY KEY (TenTK),
  CHECK (LoaiTK = N'Nhân Viên'
		 OR LoaiTK = N'Quản trị'
		)
);

CREATE TABLE TLYeuCau
(
  TenTL	  NVARCHAR(100),
  LinhVuc NVARCHAR(50)
);

CREATE TABLE PhieuCanhCao
(
	stt		  INT IDENTITY(1, 1),
	MaPhieuCC AS 'CC' + RIGHT('0000' + CAST(stt AS VARCHAR(6)), 4)PERSISTED,
	MaDocGia VARCHAR(6),
  CONSTRAINT PK_PhieuCC
	PRIMARY KEY (MaPhieuCC)
);

CREATE TABLE PhieuPhat
(
	stt		  INT IDENTITY(1, 1),
	MaPhieuPhat AS 'PP' + RIGHT('0000' + CAST(stt AS VARCHAR(6)), 4)PERSISTED,
	MaDocGia VARCHAR(6),
  CONSTRAINT PK_PhieuPhat
	PRIMARY KEY (MaPhieuPhat)
);


ALTER TABLE DocGia
ADD CONSTRAINT FK_DG_LDG
	FOREIGN KEY (Loai)
	REFERENCES LoaiDG (Loai);

ALTER TABLE PhieuMuon
ADD CONSTRAINT FK_PM_DG
	FOREIGN KEY (MaDocGia)
	REFERENCES DocGia (MaDocGia);

ALTER TABLE CTPhieuMuon
ADD CONSTRAINT FK_CTPM_PM
	FOREIGN KEY (MaPM)
	REFERENCES PhieuMuon (MaPM);
ALTER TABLE CTPhieuMuon
ADD CONSTRAINT FK_CTPM_TL
	FOREIGN KEY (MaTaiLieu)
	REFERENCES TaiLieu (MaTaiLieu);

ALTER TABLE CTPhieuTra
ADD CONSTRAINT FK_CTPT_CTPM
	FOREIGN KEY
	(
	  MaPM,
	  MaTaiLieu
	)
	REFERENCES CTPhieuMuon
	(
	  MaPM,
	  MaTaiLieu
	);
ALTER TABLE CTPhieuTra
ADD CONSTRAINT FK_CTPT_PT
	FOREIGN KEY (MaPT)
	REFERENCES PhieuTra (MaPT);

ALTER TABLE TaiLieu
ADD CONSTRAINT FK_TL_NTL
	FOREIGN KEY (MaNguonTL)
	REFERENCES NguonTaiLieu (MaNguonTL);
	
ALTER TABLE PhieuCanhCao
ADD CONSTRAINT FK_PCC_DG
	FOREIGN KEY (MaDocGia)
	REFERENCES DocGia (MaDocGia);
	
ALTER TABLE PhieuPhat
ADD CONSTRAINT FK_PP_DG
	FOREIGN KEY (MaDocGia)
	REFERENCES DocGia (MaDocGia);


INSERT INTO LoaiDG
VALUES (N'Sinh viên', 80000, 5, 10),
	   (N'Giảng viên', 100000, 6, 15),
	   (N'Khách vãng lai', 120000, 3, 6);
GO


SET IDENTITY_INSERT [dbo].[DocGia] ON 
INSERT [dbo].[DocGia] ([stt], [CMND], [HoTen], [Sdt], [Email], [NgaySinh], [DiaChi], [Loai], [MaSo], [NgayDangKi]) VALUES (1, N'361704852', N'Doãn Khải Hy', N'01205673889', N'futcrtk_uhzoaeww@earthdome.com', CAST(N'1996-05-01' AS Date), N'371 North Rocky Clarendon Road', N'Sinh viên', N'1359154', CAST(N'2017-05-22' AS Date))
INSERT [dbo].[DocGia] ([stt], [CMND], [HoTen], [Sdt], [Email], [NgaySinh], [DiaChi], [Loai], [MaSo], [NgayDangKi]) VALUES (2, N'999925910', N'Giàng Thu Túc', N'0920354228', N'pnygxza_mcfwln@mail2fashion.com', CAST(N'1980-06-15' AS Date), N'208 Second Road', N'Khách vãng lai', null, CAST(N'2016-05-13' AS Date))
INSERT [dbo].[DocGia] ([stt], [CMND], [HoTen], [Sdt], [Email], [NgaySinh], [DiaChi], [Loai], [MaSo], [NgayDangKi]) VALUES (3, N'741604870', N'Tri Công Sở', N'0978831136', N'myrpc_itvqso@mail2mypc.com', CAST(N'1979-12-01' AS Date), N'700 Green Oak Blvd.', N'Khách vãng lai', null, CAST(N'2014-06-05' AS Date))
INSERT [dbo].[DocGia] ([stt], [CMND], [HoTen], [Sdt], [Email], [NgaySinh], [DiaChi], [Loai], [MaSo], [NgayDangKi]) VALUES (4, N'463461449', N'Mạch Xuân Sinh', N'01244168870', N'qtgtgowd_umfoknbhoj@mail2neal.com', CAST(N'2006-05-07' AS Date), N'12 White Fabien Boulevard', N'Khách vãng lai', NULL, CAST(N'2017-02-13' AS Date))
INSERT [dbo].[DocGia] ([stt], [CMND], [HoTen], [Sdt], [Email], [NgaySinh], [DiaChi], [Loai], [MaSo], [NgayDangKi]) VALUES (5, N'202717541', N'Mang Hải Nội', N'01214784664', N'rnujxe00@mail-center.com', CAST(N'1983-09-04' AS Date), N'76 South Oak Boulevard', N'Giảng viên', N'1547752', CAST(N'2014-10-27' AS Date))
INSERT [dbo].[DocGia] ([stt], [CMND], [HoTen], [Sdt], [Email], [NgaySinh], [DiaChi], [Loai], [MaSo], [NgayDangKi]) VALUES (6, N'641698557', N'Ưng Ái Nhuệ', N'01295041273', N'odetmcqu3@earthcam.net', CAST(N'1982-12-22' AS Date), N'376 Second Parkway', N'Khách vãng lai', null, CAST(N'2013-03-16' AS Date))
INSERT [dbo].[DocGia] ([stt], [CMND], [HoTen], [Sdt], [Email], [NgaySinh], [DiaChi], [Loai], [MaSo], [NgayDangKi]) VALUES (7, N'515212553', N'Dã Đình Hiền', N'01251200886', N'tiholao079@mail2marshallislands.com', CAST(N'1964-10-18' AS Date), N'56 White Old Street', N'Khách vãng lai', null, CAST(N'2013-01-13' AS Date))
INSERT [dbo].[DocGia] ([stt], [CMND], [HoTen], [Sdt], [Email], [NgaySinh], [DiaChi], [Loai], [MaSo], [NgayDangKi]) VALUES (8, N'152897145', N'Bạc Khải Mậu', N'01241061309', N'syfzt_zhihulsdpc@cybercafemaui.com', CAST(N'2002-08-10' AS Date), N'264 South Green New Freeway', N'Giảng viên', N'1777686', CAST(N'2017-07-18' AS Date))
INSERT [dbo].[DocGia] ([stt], [CMND], [HoTen], [Sdt], [Email], [NgaySinh], [DiaChi], [Loai], [MaSo], [NgayDangKi]) VALUES (9, N'846439575', N'Phường Thiện Hải', N'01259810936', N'zevwvp@acceso.or.cr', CAST(N'1958-01-16' AS Date), N'722 White Old Avenue', N'Sinh viên', N'1599269', CAST(N'2015-12-07' AS Date))
INSERT [dbo].[DocGia] ([stt], [CMND], [HoTen], [Sdt], [Email], [NgaySinh], [DiaChi], [Loai], [MaSo], [NgayDangKi]) VALUES (10, N'697371405', N'Liêu Mai Tràng', N'01204462926', N'blvply_ooiht@agoodmail.com', CAST(N'1990-11-12' AS Date), N'61 Old St.', N'Giảng viên', N'1104216', CAST(N'2013-10-26' AS Date))
INSERT [dbo].[DocGia] ([stt], [CMND], [HoTen], [Sdt], [Email], [NgaySinh], [DiaChi], [Loai], [MaSo], [NgayDangKi]) VALUES (11, N'201040237', N'Hình Thiện Bố', N'01279610744', N'hqow_zokxmwc@givepeaceachance.com', CAST(N'1982-07-03' AS Date), N'24 Fabien Way', N'Sinh viên', N'1035508', CAST(N'2015-06-02' AS Date))
INSERT [dbo].[DocGia] ([stt], [CMND], [HoTen], [Sdt], [Email], [NgaySinh], [DiaChi], [Loai], [MaSo], [NgayDangKi]) VALUES (12, N'609470408', N'Kha Anh Phiệt', N'0917406620', N'sycmgwm727@lex.bg', CAST(N'1956-01-06' AS Date), N'49 Old Blvd.', N'Giảng viên', N'1331566', CAST(N'2013-11-21' AS Date))
INSERT [dbo].[DocGia] ([stt], [CMND], [HoTen], [Sdt], [Email], [NgaySinh], [DiaChi], [Loai], [MaSo], [NgayDangKi]) VALUES (13, N'789551880', N'Phạm Quỳnh Ngung', N'0941015064', N'frkux_ldca@archaeologist.com', CAST(N'1989-02-21' AS Date), N'30 Green Second Road', N'Khách vãng lai', null, CAST(N'2015-01-14' AS Date))
INSERT [dbo].[DocGia] ([stt], [CMND], [HoTen], [Sdt], [Email], [NgaySinh], [DiaChi], [Loai], [MaSo], [NgayDangKi]) VALUES (14, N'218698350', N'Lỗ Lan Mại', N'0903128526', N'krzc_evkfhucv@home-email.com', CAST(N'1957-01-23' AS Date), N'76 Hague Avenue', N'Sinh viên', N'1497973', CAST(N'2013-10-10' AS Date))
INSERT [dbo].[DocGia] ([stt], [CMND], [HoTen], [Sdt], [Email], [NgaySinh], [DiaChi], [Loai], [MaSo], [NgayDangKi]) VALUES (15, N'719405926', N'Mang Yến Ðốc', N'0949374294', N'cqrlxf018@grabmail.com', CAST(N'1992-11-03' AS Date), N'735 South Fabien Way', N'Giảng viên', N'1414520', CAST(N'2015-08-16' AS Date))
INSERT [dbo].[DocGia] ([stt], [CMND], [HoTen], [Sdt], [Email], [NgaySinh], [DiaChi], [Loai], [MaSo], [NgayDangKi]) VALUES (16, N'641404055', N'Phan Như Nhã', N'01272671595', N'uzlfzbn5@gh2000.com', CAST(N'1996-10-17' AS Date), N'74 Old Street', N'Khách vãng lai', NULL, CAST(N'2014-06-22' AS Date))
INSERT [dbo].[DocGia] ([stt], [CMND], [HoTen], [Sdt], [Email], [NgaySinh], [DiaChi], [Loai], [MaSo], [NgayDangKi]) VALUES (17, N'132985134', N'Lô Vi Quyến', N'01210027762', N'vvmdaus_bliq@163.com', CAST(N'1968-05-24' AS Date), N'38 Oak Way', N'Sinh viên', N'1713779', CAST(N'2017-10-22' AS Date))
INSERT [dbo].[DocGia] ([stt], [CMND], [HoTen], [Sdt], [Email], [NgaySinh], [DiaChi], [Loai], [MaSo], [NgayDangKi]) VALUES (18, N'789545855', N'Cam Thu Hậu', N'0946986583', NULL, CAST(N'1979-01-26' AS Date), N'134 Rocky Old Drive', N'Sinh viên', N'1019223', CAST(N'2017-10-08' AS Date))
INSERT [dbo].[DocGia] ([stt], [CMND], [HoTen], [Sdt], [Email], [NgaySinh], [DiaChi], [Loai], [MaSo], [NgayDangKi]) VALUES (19, N'262318946', N'Trần Thu Yến', N'01202484773', N'myaooupp@mail2airbag.com', CAST(N'1968-05-21' AS Date), N'31 Cowley Boulevard', N'Khách vãng lai', null, CAST(N'2014-12-10' AS Date))
INSERT [dbo].[DocGia] ([stt], [CMND], [HoTen], [Sdt], [Email], [NgaySinh], [DiaChi], [Loai], [MaSo], [NgayDangKi]) VALUES (20, N'780803905', N'Trang Khải Lời', N'0966001391', N'txnkdhcc9@argentina.com', CAST(N'1987-11-03' AS Date), N'127 Rocky Oak Parkway', N'Sinh viên', N'1004131', CAST(N'2013-11-29' AS Date))
INSERT [dbo].[DocGia] ([stt], [CMND], [HoTen], [Sdt], [Email], [NgaySinh], [DiaChi], [Loai], [MaSo], [NgayDangKi]) VALUES (21, N'468797459', N'Ninh Yên Hiệp', N'01257171038', N'cwjtoc_xbcr@mail2portugal.com', CAST(N'1993-04-25' AS Date), N'68 Rocky Milton Freeway', N'Sinh viên', N'1936367', CAST(N'2014-04-28' AS Date))
INSERT [dbo].[DocGia] ([stt], [CMND], [HoTen], [Sdt], [Email], [NgaySinh], [DiaChi], [Loai], [MaSo], [NgayDangKi]) VALUES (22, N'649595187', N'Tống Thủy Nông', N'0976939851', N'ogfh69@mail2brazil.com', CAST(N'1995-05-01' AS Date), N'676 South Hague Drive', N'Giảng viên', N'1292044', CAST(N'2016-10-03' AS Date))
INSERT [dbo].[DocGia] ([stt], [CMND], [HoTen], [Sdt], [Email], [NgaySinh], [DiaChi], [Loai], [MaSo], [NgayDangKi]) VALUES (23, N'577908815', N'Nguyễn Thu Sáng', N'01292195758', N'qibnyut@catcha.com', CAST(N'1960-01-20' AS Date), N'679 South New Boulevard', N'Sinh viên', N'1067744', CAST(N'2017-01-08' AS Date))
INSERT [dbo].[DocGia] ([stt], [CMND], [HoTen], [Sdt], [Email], [NgaySinh], [DiaChi], [Loai], [MaSo], [NgayDangKi]) VALUES (24, N'670056744', N'Phạm Thanh Tri', N'01257386207', N'yfwqta@mail2malaysia.com', CAST(N'1985-05-26' AS Date), N'358 New Blvd.', N'Khách vãng lai', null, CAST(N'2017-12-07' AS Date))
INSERT [dbo].[DocGia] ([stt], [CMND], [HoTen], [Sdt], [Email], [NgaySinh], [DiaChi], [Loai], [MaSo], [NgayDangKi]) VALUES (25, N'335183848', N'Bạc Thành Khiêm', N'01249345976', N'tdmnve6@mail2fat.com', CAST(N'1953-03-15' AS Date), N'77 Oak Drive', N'Giảng viên', N'1926543', CAST(N'2014-12-24' AS Date))
INSERT [dbo].[DocGia] ([stt], [CMND], [HoTen], [Sdt], [Email], [NgaySinh], [DiaChi], [Loai], [MaSo], [NgayDangKi]) VALUES (26, N'103596996', N'Thiều Đức Ðiệp', NULL, N'fzcgbbbc783@mail2brook.com', CAST(N'1987-05-01' AS Date), N'57 Green New Way', N'Sinh viên', N'1283091', CAST(N'2013-12-26' AS Date))
INSERT [dbo].[DocGia] ([stt], [CMND], [HoTen], [Sdt], [Email], [NgaySinh], [DiaChi], [Loai], [MaSo], [NgayDangKi]) VALUES (27, N'930789490', N'Đôn Tuấn Nghĩa', N'0977298362', N'irplodtz_ozcphkiki@mail2eli.com', CAST(N'2002-01-07' AS Date), N'40 Milton Street', N'Khách vãng lai', NULL, CAST(N'2014-08-27' AS Date))
INSERT [dbo].[DocGia] ([stt], [CMND], [HoTen], [Sdt], [Email], [NgaySinh], [DiaChi], [Loai], [MaSo], [NgayDangKi]) VALUES (28, N'874362160', N'Quàng Bảo Thao', N'0954160920', N'tjrybto318@mail2judy.com', CAST(N'1972-12-06' AS Date), N'40 Hague Way', N'Sinh viên', N'1022304', CAST(N'2017-03-16' AS Date))
INSERT [dbo].[DocGia] ([stt], [CMND], [HoTen], [Sdt], [Email], [NgaySinh], [DiaChi], [Loai], [MaSo], [NgayDangKi]) VALUES (29, N'933005350', N'Cổ Vân Linh', N'01278545096', N'tafe02@bangkok.com', CAST(N'2001-11-09' AS Date), N'910 North Green Old Road', N'Sinh viên', N'1423246', CAST(N'2017-12-19' AS Date))
INSERT [dbo].[DocGia] ([stt], [CMND], [HoTen], [Sdt], [Email], [NgaySinh], [DiaChi], [Loai], [MaSo], [NgayDangKi]) VALUES (30, N'687340272', N'Quàng Hiếu Niệm', N'0909745743', N'bzmark975@fbi-agent.com', CAST(N'1968-12-07' AS Date), N'133 Hague Freeway', N'Sinh viên', N'1847263', CAST(N'2015-07-23' AS Date))
INSERT [dbo].[DocGia] ([stt], [CMND], [HoTen], [Sdt], [Email], [NgaySinh], [DiaChi], [Loai], [MaSo], [NgayDangKi]) VALUES (31, N'894464698', N'Ngụy Như Lục', N'0968438986', N'mmjf_wlcg@mail2glen.com', CAST(N'1979-04-21' AS Date), N'462 South Rocky Milton Blvd.', N'Giảng viên', N'1608271', CAST(N'2017-10-10' AS Date))
INSERT [dbo].[DocGia] ([stt], [CMND], [HoTen], [Sdt], [Email], [NgaySinh], [DiaChi], [Loai], [MaSo], [NgayDangKi]) VALUES (32, N'438916907', N'Bì Nhật Quân', N'0977945827', N'kcfigf_ewchorpwc@mail2pickup.com', CAST(N'1980-01-01' AS Date), N'87 Nobel Way', N'Sinh viên', N'1920011', CAST(N'2015-09-22' AS Date))
INSERT [dbo].[DocGia] ([stt], [CMND], [HoTen], [Sdt], [Email], [NgaySinh], [DiaChi], [Loai], [MaSo], [NgayDangKi]) VALUES (33, N'598269918', N'Cự Vỹ Cử', N'0930963577', N'hmcssqgj097@mail2eu.com', CAST(N'1967-04-16' AS Date), N'60 Oak Avenue', N'Sinh viên', N'1590999', CAST(N'2017-05-05' AS Date))
INSERT [dbo].[DocGia] ([stt], [CMND], [HoTen], [Sdt], [Email], [NgaySinh], [DiaChi], [Loai], [MaSo], [NgayDangKi]) VALUES (34, N'943090983', N'Ngọc Xuân Tước', N'0958030931', N'ghfsro763@beep.ru', CAST(N'1979-08-07' AS Date), N'168 North Oak Road', N'Khách vãng lai', NULL, CAST(N'2015-12-03' AS Date))
INSERT [dbo].[DocGia] ([stt], [CMND], [HoTen], [Sdt], [Email], [NgaySinh], [DiaChi], [Loai], [MaSo], [NgayDangKi]) VALUES (35, N'947333982', N'Trình Hiếu Hoành', N'01222178202', N'zgzid02@gramszu.net', CAST(N'2000-01-22' AS Date), N'943 White Old Way', N'Giảng viên', N'1697145', CAST(N'2014-04-06' AS Date))
INSERT [dbo].[DocGia] ([stt], [CMND], [HoTen], [Sdt], [Email], [NgaySinh], [DiaChi], [Loai], [MaSo], [NgayDangKi]) VALUES (36, N'020949017', N'Thôi Mai Lực', N'01256919588', N'savgbc_ockirkin@elsitio.com', CAST(N'1995-11-10' AS Date), N'863 South White Hague Road', N'Khách vãng lai', null, CAST(N'2017-10-06' AS Date))
INSERT [dbo].[DocGia] ([stt], [CMND], [HoTen], [Sdt], [Email], [NgaySinh], [DiaChi], [Loai], [MaSo], [NgayDangKi]) VALUES (37, N'441180979', N'Ti Mai Tần', N'01210510865', N'jcjoqk785@dogsnob.net', CAST(N'1978-04-17' AS Date), N'795 South White Nobel Drive', N'Sinh viên', N'1702866', CAST(N'2016-11-12' AS Date))
INSERT [dbo].[DocGia] ([stt], [CMND], [HoTen], [Sdt], [Email], [NgaySinh], [DiaChi], [Loai], [MaSo], [NgayDangKi]) VALUES (38, N'717131259', N'Tào Hoàng Trình', N'0940186182', N'zxsaqs_rszo@lycosmail.com', CAST(N'1978-04-17' AS Date), N'49 Green Oak St.', N'Giảng viên', N'1394728', CAST(N'2017-07-12' AS Date))
INSERT [dbo].[DocGia] ([stt], [CMND], [HoTen], [Sdt], [Email], [NgaySinh], [DiaChi], [Loai], [MaSo], [NgayDangKi]) VALUES (39, N'909057346', N'Viên Khánh Huỳnh', N'01254847988', N'yiyvrf_iddj@home.se', CAST(N'1962-11-17' AS Date), N'557 White Oak Drive', N'Sinh viên', N'1930305', CAST(N'2016-11-24' AS Date))
INSERT [dbo].[DocGia] ([stt], [CMND], [HoTen], [Sdt], [Email], [NgaySinh], [DiaChi], [Loai], [MaSo], [NgayDangKi]) VALUES (40, N'070504355', N'Mạc Thành Tường', N'0905244160', N'vibwhrlk_rvmmrkjz@ato.check.com', CAST(N'1958-03-15' AS Date), N'879 Rocky Old Street', N'Sinh viên', N'1656965', CAST(N'2015-06-26' AS Date))
INSERT [dbo].[DocGia] ([stt], [CMND], [HoTen], [Sdt], [Email], [NgaySinh], [DiaChi], [Loai], [MaSo], [NgayDangKi]) VALUES (41, N'418119485', N'Cấn Minh Phấn', N'01255932451', N'cyva_uapowhgpdi@mail2brunei.com', CAST(N'1965-05-17' AS Date), N'74 East Green Fabien Drive', N'Khách vãng lai', null, CAST(N'2015-02-13' AS Date))
INSERT [dbo].[DocGia] ([stt], [CMND], [HoTen], [Sdt], [Email], [NgaySinh], [DiaChi], [Loai], [MaSo], [NgayDangKi]) VALUES (42, N'742484505', N'Bồ Xuân Thy', N'01266086621', N'gwdojodw_gyuehpbxnb@1mail.net', CAST(N'1972-05-24' AS Date), N'373 North Rocky First Road', N'Giảng viên', N'1072618', CAST(N'2015-05-22' AS Date))
INSERT [dbo].[DocGia] ([stt], [CMND], [HoTen], [Sdt], [Email], [NgaySinh], [DiaChi], [Loai], [MaSo], [NgayDangKi]) VALUES (43, N'741099010', N'Hi Thanh Thâu', N'0998084986', N'wkvkoko_jtftfsoo@emailaccount.com', CAST(N'1973-02-19' AS Date), N'947 First Boulevard', N'Khách vãng lai', NULL, CAST(N'2015-12-26' AS Date))
INSERT [dbo].[DocGia] ([stt], [CMND], [HoTen], [Sdt], [Email], [NgaySinh], [DiaChi], [Loai], [MaSo], [NgayDangKi]) VALUES (44, N'760687178', N'Sử Khải Quyết', N'0958584054', N'zgru_rzrog@mail2jew.com', CAST(N'1969-12-15' AS Date), N'245 White Hague Street', N'Sinh viên', N'1087042', CAST(N'2013-10-29' AS Date))
INSERT [dbo].[DocGia] ([stt], [CMND], [HoTen], [Sdt], [Email], [NgaySinh], [DiaChi], [Loai], [MaSo], [NgayDangKi]) VALUES (45, N'249315567', N'Bồ Khải Ích', N'0937368453', N'dyzssz@fnmail.com', CAST(N'1992-03-05' AS Date), N'44 Oak St.', N'Sinh viên', N'1417569', CAST(N'2017-10-17' AS Date))
INSERT [dbo].[DocGia] ([stt], [CMND], [HoTen], [Sdt], [Email], [NgaySinh], [DiaChi], [Loai], [MaSo], [NgayDangKi]) VALUES (46, N'929450081', N'Nhâm Gia Khuyết', N'01294540740', N'txeo843@dygo.com', CAST(N'1999-09-27' AS Date), N'23 West Green Milton Blvd.', N'Giảng viên', N'1576646', CAST(N'2013-10-05' AS Date))
INSERT [dbo].[DocGia] ([stt], [CMND], [HoTen], [Sdt], [Email], [NgaySinh], [DiaChi], [Loai], [MaSo], [NgayDangKi]) VALUES (47, N'938750456', N'Kha Yến Du', N'01258702801', N'aafqepwk_dnecufaw@mail2ballerina.com', CAST(N'1987-08-10' AS Date), N'332 East Clarendon Freeway', N'Khách vãng lai', null, CAST(N'2017-10-17' AS Date))
INSERT [dbo].[DocGia] ([stt], [CMND], [HoTen], [Sdt], [Email], [NgaySinh], [DiaChi], [Loai], [MaSo], [NgayDangKi]) VALUES (48, N'282982588', N'Hoàng Anh Lãng', N'0917907254', N'crkxwmf@mail2junk.com', CAST(N'1961-05-07' AS Date), N'125 New Parkway', N'Giảng viên', N'1710321', CAST(N'2015-07-26' AS Date))
INSERT [dbo].[DocGia] ([stt], [CMND], [HoTen], [Sdt], [Email], [NgaySinh], [DiaChi], [Loai], [MaSo], [NgayDangKi]) VALUES (49, N'343495650', N'Triệu Nhật Minh', N'0935966120', N'yokcyuco345@mail2ca.com', CAST(N'1987-12-04' AS Date), N'90 Cowley Road', N'Giảng viên', N'1765928', CAST(N'2016-06-27' AS Date))
INSERT [dbo].[DocGia] ([stt], [CMND], [HoTen], [Sdt], [Email], [NgaySinh], [DiaChi], [Loai], [MaSo], [NgayDangKi]) VALUES (50, N'635679617', N'Nhâm Như Bái', N'01265597592', N'amqxwqs_jqhtrkpnah@coolkiwi.com', CAST(N'1997-07-14' AS Date), N'65 South Green Oak Way', N'Giảng viên', N'1660974', CAST(N'2014-01-02' AS Date))
INSERT [dbo].[DocGia] ([stt], [CMND], [HoTen], [Sdt], [Email], [NgaySinh], [DiaChi], [Loai], [MaSo], [NgayDangKi]) VALUES (51, N'199730005', N'Đức Khải Ca', N'0933806144', N'tzwk_gctfwhnm@mail2fly.com', CAST(N'1991-05-07' AS Date), N'74 White Old Street', N'Sinh viên', N'1700490', CAST(N'2014-02-17' AS Date))
INSERT [dbo].[DocGia] ([stt], [CMND], [HoTen], [Sdt], [Email], [NgaySinh], [DiaChi], [Loai], [MaSo], [NgayDangKi]) VALUES (52, N'420785947', N'Diệp Hải Hựu', N'01220099081', N'zzkfp@f1fans.net', CAST(N'1955-05-04' AS Date), N'50 Oak Parkway', N'Sinh viên', N'1135418', CAST(N'2015-07-16' AS Date))
INSERT [dbo].[DocGia] ([stt], [CMND], [HoTen], [Sdt], [Email], [NgaySinh], [DiaChi], [Loai], [MaSo], [NgayDangKi]) VALUES (53, N'484839252', N'Đức Anh Ðam', N'0950370620', N'iwzovk32@jokes.com', CAST(N'1978-02-28' AS Date), N'73 Rocky New Drive', N'Sinh viên', N'1934565', CAST(N'2014-04-21' AS Date))
INSERT [dbo].[DocGia] ([stt], [CMND], [HoTen], [Sdt], [Email], [NgaySinh], [DiaChi], [Loai], [MaSo], [NgayDangKi]) VALUES (54, N'530724460', N'Lục Đức Châu', N'0932996968', N'nyjlw702@cyclefanz.com', CAST(N'1973-11-21' AS Date), N'14 Milton Drive', N'Sinh viên', N'1979857', CAST(N'2014-06-08' AS Date))
INSERT [dbo].[DocGia] ([stt], [CMND], [HoTen], [Sdt], [Email], [NgaySinh], [DiaChi], [Loai], [MaSo], [NgayDangKi]) VALUES (55, N'460734378', N'Tán Thảo Kiều', N'0967304645', N'yobwz25@mail2mail4.com', CAST(N'2004-11-12' AS Date), N'755 South Green Cowley Drive', N'Giảng viên', N'1644889', CAST(N'2016-10-16' AS Date))
INSERT [dbo].[DocGia] ([stt], [CMND], [HoTen], [Sdt], [Email], [NgaySinh], [DiaChi], [Loai], [MaSo], [NgayDangKi]) VALUES (56, N'023762403', N'Bàng Thành Tưởng', N'01277834301', N'lhnxup_fgwmbbtsaq@kellychen.com', CAST(N'1978-08-31' AS Date), N'729 Nobel Boulevard', N'Giảng viên', N'1556007', CAST(N'2014-05-04' AS Date))
INSERT [dbo].[DocGia] ([stt], [CMND], [HoTen], [Sdt], [Email], [NgaySinh], [DiaChi], [Loai], [MaSo], [NgayDangKi]) VALUES (57, N'848681225', N'Liêu Xuân Biên', N'0983154000', N'cqiqhj08@everyone.net', CAST(N'1954-01-01' AS Date), N'829 Green New Parkway', N'Sinh viên', N'1636420', CAST(N'2013-08-20' AS Date))
INSERT [dbo].[DocGia] ([stt], [CMND], [HoTen], [Sdt], [Email], [NgaySinh], [DiaChi], [Loai], [MaSo], [NgayDangKi]) VALUES (58, N'023957875', N'Văn Thảo Giao', N'0939094763', N'qixejkie030@athenachu.net', CAST(N'1973-05-28' AS Date), N'756 North White Oak Road', N'Giảng viên', N'1815707', CAST(N'2017-05-06' AS Date))
INSERT [dbo].[DocGia] ([stt], [CMND], [HoTen], [Sdt], [Email], [NgaySinh], [DiaChi], [Loai], [MaSo], [NgayDangKi]) VALUES (59, N'657082920', N'Tào Quỳnh Chình', N'0916054535', N'vfrkobjn_xqlf@fromnewhampshire.com', CAST(N'2002-07-08' AS Date), N'93 Rocky Milton Parkway', N'Khách vãng lai', null, CAST(N'2014-06-30' AS Date))
INSERT [dbo].[DocGia] ([stt], [CMND], [HoTen], [Sdt], [Email], [NgaySinh], [DiaChi], [Loai], [MaSo], [NgayDangKi]) VALUES (60, N'943600839', N'Chúng Thu Khê', N'01294805257', N'iyrwsdpj_ijzqyofj@lover-boy.com', CAST(N'2006-02-22' AS Date), N'39 Fabien Parkway', N'Khách vãng lai', null, CAST(N'2015-02-21' AS Date))
INSERT [dbo].[DocGia] ([stt], [CMND], [HoTen], [Sdt], [Email], [NgaySinh], [DiaChi], [Loai], [MaSo], [NgayDangKi]) VALUES (61, N'202418942', N'Đỗ Vỹ Tiếp', N'01253203769', N'ilsdhvd78@mail2psycho.com', CAST(N'2004-05-06' AS Date), N'435 First Blvd.', N'Sinh viên', N'1421297', CAST(N'2013-11-01' AS Date))
INSERT [dbo].[DocGia] ([stt], [CMND], [HoTen], [Sdt], [Email], [NgaySinh], [DiaChi], [Loai], [MaSo], [NgayDangKi]) VALUES (62, N'365462788', N'Sử Đức Xuân', N'01276868235', N'zrlk76@freenet.kg', CAST(N'1988-12-15' AS Date), N'96 Clarendon Boulevard', N'Sinh viên', N'1619145', CAST(N'2016-07-20' AS Date))
INSERT [dbo].[DocGia] ([stt], [CMND], [HoTen], [Sdt], [Email], [NgaySinh], [DiaChi], [Loai], [MaSo], [NgayDangKi]) VALUES (63, N'102446869', N'Nhâm Khánh Chế', N'0975500910', N'gtzn340@mail2clinic.com', CAST(N'1999-11-13' AS Date), N'978 Nobel Freeway', N'Giảng viên', N'1736529', CAST(N'2013-10-30' AS Date))
INSERT [dbo].[DocGia] ([stt], [CMND], [HoTen], [Sdt], [Email], [NgaySinh], [DiaChi], [Loai], [MaSo], [NgayDangKi]) VALUES (64, N'958376238', N'An Xuân Bến', N'0918899740', N'uxgoowaj_lioay@jump.com', CAST(N'1953-10-01' AS Date), N'55 Milton Blvd.', N'Sinh viên', N'1474699', CAST(N'2017-07-30' AS Date))
INSERT [dbo].[DocGia] ([stt], [CMND], [HoTen], [Sdt], [Email], [NgaySinh], [DiaChi], [Loai], [MaSo], [NgayDangKi]) VALUES (65, N'359162899', N'Dương Khánh Tùng', N'0984585262', N'zbvqaplk92@bettergolf.net', CAST(N'1997-02-08' AS Date), N'411 West Cowley Road', N'Khách vãng lai', null, CAST(N'2013-05-18' AS Date))
INSERT [dbo].[DocGia] ([stt], [CMND], [HoTen], [Sdt], [Email], [NgaySinh], [DiaChi], [Loai], [MaSo], [NgayDangKi]) VALUES (66, N'294302879', N'Bình Thủy Phát', N'0990666981', NULL, CAST(N'1964-07-25' AS Date), N'46 Fabien Drive', N'Giảng viên', N'1783827', CAST(N'2014-01-16' AS Date))
INSERT [dbo].[DocGia] ([stt], [CMND], [HoTen], [Sdt], [Email], [NgaySinh], [DiaChi], [Loai], [MaSo], [NgayDangKi]) VALUES (67, N'924898827', N'Ấu Gia Ðẳng', N'0966584889', N'uisftew_sgkzvkln@cuemail.com', CAST(N'1955-07-03' AS Date), N'61 Green Fabien St.', N'Khách vãng lai', NULL, CAST(N'2017-09-11' AS Date))
INSERT [dbo].[DocGia] ([stt], [CMND], [HoTen], [Sdt], [Email], [NgaySinh], [DiaChi], [Loai], [MaSo], [NgayDangKi]) VALUES (68, N'049816008', N'Mâu Như Thuật', N'01272350296', N'fmavmu@centrum.cz', CAST(N'1996-09-21' AS Date), N'71 First Way', N'Giảng viên', N'1673403', CAST(N'2014-07-19' AS Date))
INSERT [dbo].[DocGia] ([stt], [CMND], [HoTen], [Sdt], [Email], [NgaySinh], [DiaChi], [Loai], [MaSo], [NgayDangKi]) VALUES (69, N'796229042', N'Hoàng Yến Khuyến', N'01229891121', N'oaeunqa_eihiq@bumerang.ro', CAST(N'1962-07-18' AS Date), N'90 North Green Hague Freeway', N'Sinh viên', N'1719466', CAST(N'2017-02-11' AS Date))
INSERT [dbo].[DocGia] ([stt], [CMND], [HoTen], [Sdt], [Email], [NgaySinh], [DiaChi], [Loai], [MaSo], [NgayDangKi]) VALUES (70, N'853300012', N'Thào Đình Huy', N'0992089287', N'pqqzvut_wumg@hollywoodkids.com', CAST(N'1965-01-02' AS Date), N'36 Rocky Oak Way', N'Giảng viên', N'1605258', CAST(N'2017-07-07' AS Date))
INSERT [dbo].[DocGia] ([stt], [CMND], [HoTen], [Sdt], [Email], [NgaySinh], [DiaChi], [Loai], [MaSo], [NgayDangKi]) VALUES (71, N'462978062', N'Đinh Quỳnh Nhâm', N'0947415661', N'ksjwrc_ekqsafx@casino.com', CAST(N'2004-12-24' AS Date), N'30 New Blvd.', N'Khách vãng lai', null, CAST(N'2015-11-10' AS Date))
INSERT [dbo].[DocGia] ([stt], [CMND], [HoTen], [Sdt], [Email], [NgaySinh], [DiaChi], [Loai], [MaSo], [NgayDangKi]) VALUES (72, N'179870625', N'Thi Trâm Quý', N'01292621775', NULL, CAST(N'1971-07-03' AS Date), N'78 Fabien Road', N'Giảng viên', N'1726800', CAST(N'2015-08-10' AS Date))
INSERT [dbo].[DocGia] ([stt], [CMND], [HoTen], [Sdt], [Email], [NgaySinh], [DiaChi], [Loai], [MaSo], [NgayDangKi]) VALUES (73, N'730224167', N'Đỗ Ái Tài', N'01266671525', N'sberdem_mxrvnojsx@mail.salu.net', CAST(N'1958-07-09' AS Date), N'35 South Green Second Road', N'Khách vãng lai', NULL, CAST(N'2013-04-12' AS Date))
INSERT [dbo].[DocGia] ([stt], [CMND], [HoTen], [Sdt], [Email], [NgaySinh], [DiaChi], [Loai], [MaSo], [NgayDangKi]) VALUES (74, N'598071675', N'Lạc Đức Thông', N'01242283547', N'ulhz51@bigfoot.de', CAST(N'1973-11-13' AS Date), N'64 Fabien Avenue', N'Sinh viên', N'1900669', CAST(N'2013-11-15' AS Date))
INSERT [dbo].[DocGia] ([stt], [CMND], [HoTen], [Sdt], [Email], [NgaySinh], [DiaChi], [Loai], [MaSo], [NgayDangKi]) VALUES (75, N'932730645', N'Tôn Duy Ấn', N'0940699667', N'nzslu725@mail2mali.com', CAST(N'1956-01-06' AS Date), N'537 Rocky Second Street', N'Giảng viên', N'1123421', CAST(N'2017-06-17' AS Date))
INSERT [dbo].[DocGia] ([stt], [CMND], [HoTen], [Sdt], [Email], [NgaySinh], [DiaChi], [Loai], [MaSo], [NgayDangKi]) VALUES (76, N'485200606', N'An Thiện Giản', N'01242110716', N'kqsviwbi31@mail2007.com', CAST(N'1968-06-12' AS Date), N'677 East White Fabien Blvd.', N'Khách vãng lai', NULL, CAST(N'2016-10-17' AS Date))
INSERT [dbo].[DocGia] ([stt], [CMND], [HoTen], [Sdt], [Email], [NgaySinh], [DiaChi], [Loai], [MaSo], [NgayDangKi]) VALUES (77, N'649191880', N'Hình Thành Túc', N'01241569798', N'uforohr_yyks@golfemail.com', CAST(N'1973-03-06' AS Date), N'221 Green Clarendon Drive', N'Sinh viên', N'1514933', CAST(N'2016-12-08' AS Date))
INSERT [dbo].[DocGia] ([stt], [CMND], [HoTen], [Sdt], [Email], [NgaySinh], [DiaChi], [Loai], [MaSo], [NgayDangKi]) VALUES (78, N'497103128', N'Lều Quỳnh Huỳnh', N'01239118597', N'yracsbrg717@iamawoman.com', CAST(N'1978-05-17' AS Date), N'38 Milton Street', N'Giảng viên', N'1462883', CAST(N'2013-10-09' AS Date))
INSERT [dbo].[DocGia] ([stt], [CMND], [HoTen], [Sdt], [Email], [NgaySinh], [DiaChi], [Loai], [MaSo], [NgayDangKi]) VALUES (79, N'860767369', N'Cổ Thành Phái', N'01285843101', N'qnwokaf_plwr@mail2heal.com', CAST(N'1975-11-16' AS Date), N'66 Hague Street', N'Sinh viên', N'1075143', CAST(N'2016-03-13' AS Date))
INSERT [dbo].[DocGia] ([stt], [CMND], [HoTen], [Sdt], [Email], [NgaySinh], [DiaChi], [Loai], [MaSo], [NgayDangKi]) VALUES (80, N'460274418', N'Bạc An Bông', N'01247830249', N'xerf3@azimiweb.com', CAST(N'1993-08-29' AS Date), N'98 West Nobel Way', N'Khách vãng lai', NULL, CAST(N'2013-02-20' AS Date))
INSERT [dbo].[DocGia] ([stt], [CMND], [HoTen], [Sdt], [Email], [NgaySinh], [DiaChi], [Loai], [MaSo], [NgayDangKi]) VALUES (81, N'173854141', N'Lã Gia Kháng', N'01278708510', N'eczg_ewcth@hello.hu', CAST(N'1973-08-06' AS Date), N'424 Hague Boulevard', N'Sinh viên', N'1035207', CAST(N'2014-03-22' AS Date))
INSERT [dbo].[DocGia] ([stt], [CMND], [HoTen], [Sdt], [Email], [NgaySinh], [DiaChi], [Loai], [MaSo], [NgayDangKi]) VALUES (82, N'831449314', N'Sử Vỹ Diệm', N'0973841931', N'dzdscb_famkgolpc@mail2biker.com', CAST(N'1978-12-25' AS Date), N'936 Milton Drive', N'Khách vãng lai', NULL, CAST(N'2016-06-03' AS Date))
INSERT [dbo].[DocGia] ([stt], [CMND], [HoTen], [Sdt], [Email], [NgaySinh], [DiaChi], [Loai], [MaSo], [NgayDangKi]) VALUES (83, N'729317843', N'Khúc Đức Oai', N'0978042518', N'lzkhbzb57@mail2myplane.com', CAST(N'1967-02-05' AS Date), N'75 Fabien Parkway', N'Sinh viên', N'1761809', CAST(N'2016-03-01' AS Date))
INSERT [dbo].[DocGia] ([stt], [CMND], [HoTen], [Sdt], [Email], [NgaySinh], [DiaChi], [Loai], [MaSo], [NgayDangKi]) VALUES (84, N'904600212', N'Đan Anh Dược', N'0910767078', N'miarbq_oqycmwhi@mail2edgar.com', CAST(N'1983-05-25' AS Date), N'96 Rocky Oak Street', N'Khách vãng lai', NULL, CAST(N'2016-03-15' AS Date))
INSERT [dbo].[DocGia] ([stt], [CMND], [HoTen], [Sdt], [Email], [NgaySinh], [DiaChi], [Loai], [MaSo], [NgayDangKi]) VALUES (85, N'694381458', N'Ti Hiếu Kết', N'01200827083', N'zcazpts@abdulnour.com', CAST(N'1963-07-06' AS Date), N'81 Cowley Avenue', N'Sinh viên', N'1881036', CAST(N'2016-11-02' AS Date))
INSERT [dbo].[DocGia] ([stt], [CMND], [HoTen], [Sdt], [Email], [NgaySinh], [DiaChi], [Loai], [MaSo], [NgayDangKi]) VALUES (86, N'037639807', N'Khúc Vỹ Nhung', N'0992604873', N'cslc@mail2megan.com', CAST(N'1966-08-07' AS Date), N'399 Hague Blvd.', N'Giảng viên', N'1921951', CAST(N'2013-01-08' AS Date))
INSERT [dbo].[DocGia] ([stt], [CMND], [HoTen], [Sdt], [Email], [NgaySinh], [DiaChi], [Loai], [MaSo], [NgayDangKi]) VALUES (87, N'660578944', N'Thập Thanh Phủ', N'01232973752', N'unlcaw25@bimamail.com', CAST(N'1954-02-21' AS Date), N'96 Green Hague Parkway', N'Khách vãng lai', null, CAST(N'2016-06-22' AS Date))
INSERT [dbo].[DocGia] ([stt], [CMND], [HoTen], [Sdt], [Email], [NgaySinh], [DiaChi], [Loai], [MaSo], [NgayDangKi]) VALUES (88, N'628257754', N'Châu Công Thông', N'01214648797', N'vrgtv72@mail2peggy.com', CAST(N'1959-02-11' AS Date), N'341 Rocky New Boulevard', N'Giảng viên', N'1492218', CAST(N'2014-04-16' AS Date))
INSERT [dbo].[DocGia] ([stt], [CMND], [HoTen], [Sdt], [Email], [NgaySinh], [DiaChi], [Loai], [MaSo], [NgayDangKi]) VALUES (89, N'236655382', N'Khúc An Xuyên', N'01278795836', N'xmjmxmam_uyunaacg@end-war.com', CAST(N'1970-06-03' AS Date), N'88 Hague Way', N'Giảng viên', N'1863421', CAST(N'2013-07-21' AS Date))
INSERT [dbo].[DocGia] ([stt], [CMND], [HoTen], [Sdt], [Email], [NgaySinh], [DiaChi], [Loai], [MaSo], [NgayDangKi]) VALUES (90, N'124459985', N'Diêm Khải Nghiệp', N'0934035701', N'mqgckl_oldfvmopq@gotomy.com', CAST(N'1953-12-07' AS Date), N'964 Cowley Drive', N'Khách vãng lai', null, CAST(N'2014-08-01' AS Date))
INSERT [dbo].[DocGia] ([stt], [CMND], [HoTen], [Sdt], [Email], [NgaySinh], [DiaChi], [Loai], [MaSo], [NgayDangKi]) VALUES (91, N'101318440', N'Bành Trâm Ðệ', N'01245509462', N'zfcfka_gvlknam@laposte.net', CAST(N'1999-01-21' AS Date), N'849 Green Oak Road', N'Giảng viên', N'1448787', CAST(N'2013-08-18' AS Date))
INSERT [dbo].[DocGia] ([stt], [CMND], [HoTen], [Sdt], [Email], [NgaySinh], [DiaChi], [Loai], [MaSo], [NgayDangKi]) VALUES (92, N'125642040', N'Nông Minh Nham', N'0999665549', N'ggbmutx503@mail2patricia.com', CAST(N'1975-06-16' AS Date), N'399 West White Clarendon Boulevard', N'Sinh viên', N'1850541', CAST(N'2013-06-03' AS Date))
INSERT [dbo].[DocGia] ([stt], [CMND], [HoTen], [Sdt], [Email], [NgaySinh], [DiaChi], [Loai], [MaSo], [NgayDangKi]) VALUES (93, N'501324746', N'Hồ Ngọc Thảo', N'0948097742', N'ksuhhjl_qallcbqfjz@freeyellow.com', CAST(N'1983-10-11' AS Date), N'942 North White Old Freeway', N'Sinh viên', N'1267902', CAST(N'2015-05-17' AS Date))
INSERT [dbo].[DocGia] ([stt], [CMND], [HoTen], [Sdt], [Email], [NgaySinh], [DiaChi], [Loai], [MaSo], [NgayDangKi]) VALUES (94, N'880492124', N'Tòng Hoàng Ðổng', N'01268866126', N'jizemft61@emailgroups.net', CAST(N'1964-09-25' AS Date), N'78 North Cowley Boulevard', N'Khách vãng lai', null, CAST(N'2015-05-23' AS Date))
INSERT [dbo].[DocGia] ([stt], [CMND], [HoTen], [Sdt], [Email], [NgaySinh], [DiaChi], [Loai], [MaSo], [NgayDangKi]) VALUES (95, N'088699789', N'Nông Quỳnh Trác', N'0921449373', N'bsssed_ekmihukq@geecities.com', CAST(N'1961-11-09' AS Date), N'96 Oak Avenue', N'Sinh viên', N'1865099', CAST(N'2016-01-24' AS Date))
INSERT [dbo].[DocGia] ([stt], [CMND], [HoTen], [Sdt], [Email], [NgaySinh], [DiaChi], [Loai], [MaSo], [NgayDangKi]) VALUES (96, N'958441881', N'Phường Hoàng Nhàn', N'01220946546', N'jqaaj_vessrzweab@fromwashingtondc.com', CAST(N'1984-06-02' AS Date), N'28 White Old St.', N'Khách vãng lai', null, CAST(N'2016-12-06' AS Date))
INSERT [dbo].[DocGia] ([stt], [CMND], [HoTen], [Sdt], [Email], [NgaySinh], [DiaChi], [Loai], [MaSo], [NgayDangKi]) VALUES (97, N'008372207', N'Huỳnh Tuấn Tuần', N'0903320010', N'mevo_ymvekinby@mail2harry.com', CAST(N'1980-07-11' AS Date), N'38 First Blvd.', N'Sinh viên', N'1948511', CAST(N'2014-04-16' AS Date))
INSERT [dbo].[DocGia] ([stt], [CMND], [HoTen], [Sdt], [Email], [NgaySinh], [DiaChi], [Loai], [MaSo], [NgayDangKi]) VALUES (98, N'179307253', N'Ấu Trâm Thủ', N'01228912246', N'kaixp@atlink.com', CAST(N'1972-05-22' AS Date), N'52 Clarendon St.', N'Sinh viên', N'1077332', CAST(N'2015-10-24' AS Date))
INSERT [dbo].[DocGia] ([stt], [CMND], [HoTen], [Sdt], [Email], [NgaySinh], [DiaChi], [Loai], [MaSo], [NgayDangKi]) VALUES (99, N'794372069', N'Kha Yến Khoá', N'01290497593', N'nlpdg_sxoncbwkih@mail2beverly.com', CAST(N'1980-11-06' AS Date), N'848 Green Hague St.', N'Khách vãng lai', NULL, CAST(N'2015-08-11' AS Date))
GO
INSERT [dbo].[DocGia] ([stt], [CMND], [HoTen], [Sdt], [Email], [NgaySinh], [DiaChi], [Loai], [MaSo], [NgayDangKi]) VALUES (100, N'431860948', N'Nhâm Vi Uyên', N'0966480872', N'jfpbc_baaxuw@iname.com', CAST(N'1980-08-12' AS Date), N'55 Second St.', N'Sinh viên', N'1649771', CAST(N'2017-10-13' AS Date))
SET IDENTITY_INSERT [dbo].[DocGia] OFF

INSERT [dbo].[NguonTaiLieu] ([MaNguonTL], [SoLuong]) VALUES (N'CNTT', 3)
INSERT [dbo].[NguonTaiLieu] ([MaNguonTL], [SoLuong]) VALUES (N'HH', 3)
INSERT [dbo].[NguonTaiLieu] ([MaNguonTL], [SoLuong]) VALUES (N'VL', 2)

SET IDENTITY_INSERT [dbo].[TaiLieu] ON 

INSERT [dbo].[TaiLieu] ([stt], [MaNguonTL], [TenTaiLieu], [LoaiTaiLieu], [LinhVuc], [TrangThai]) VALUES (1, N'VL', N'SQL Server Hardware', N'Thường', N'Kỹ thuật', N'Có thể mượn')
INSERT [dbo].[TaiLieu] ([stt], [MaNguonTL], [TenTaiLieu], [LoaiTaiLieu], [LinhVuc], [TrangThai]) VALUES (2, N'HH', N'Inside the SQL Server Query Optimizer', N'Đặc biệt', N'Văn học', N'Có thể mượn')
INSERT [dbo].[TaiLieu] ([stt], [MaNguonTL], [TenTaiLieu], [LoaiTaiLieu], [LinhVuc], [TrangThai]) VALUES (3, N'HH', N'SQL Server Statistics', N'Hiếm', N'Văn học', N'Có thể mượn')
INSERT [dbo].[TaiLieu] ([stt], [MaNguonTL], [TenTaiLieu], [LoaiTaiLieu], [LinhVuc], [TrangThai]) VALUES (4, N'HH', N'The Red Gate Guide to SQL Server Team-based Development', N'Hiếm', N'Nghệ thuật', N'Có thể mượn')
INSERT [dbo].[TaiLieu] ([stt], [MaNguonTL], [TenTaiLieu], [LoaiTaiLieu], [LinhVuc], [TrangThai]) VALUES (5, N'CNTT', N'SQL Server Hardware', N'Thường', N'Kỹ thuật', N'Đã mượn')
INSERT [dbo].[TaiLieu] ([stt], [MaNguonTL], [TenTaiLieu], [LoaiTaiLieu], [LinhVuc], [TrangThai]) VALUES (6, N'HH', N'Inside the SQL Server Query Optimizer', N'Thường', N'Khoa học', N'Có thể mượn')
INSERT [dbo].[TaiLieu] ([stt], [MaNguonTL], [TenTaiLieu], [LoaiTaiLieu], [LinhVuc], [TrangThai]) VALUES (7, N'HH', N'SQL Server Statistics', N'Hiếm', N'Khoa học', N'Có thể mượn')
INSERT [dbo].[TaiLieu] ([stt], [MaNguonTL], [TenTaiLieu], [LoaiTaiLieu], [LinhVuc], [TrangThai]) VALUES (8, N'CNTT', N'The Red Gate Guide to SQL Server Team-based Development', N'Hiếm', N'Đời sống', N'Có thể mượn')
INSERT [dbo].[TaiLieu] ([stt], [MaNguonTL], [TenTaiLieu], [LoaiTaiLieu], [LinhVuc], [TrangThai]) VALUES (9, N'VL', N'SQL Server Hardware', N'Thường', N'Nghệ thuật', N'Có thể mượn')
INSERT [dbo].[TaiLieu] ([stt], [MaNguonTL], [TenTaiLieu], [LoaiTaiLieu], [LinhVuc], [TrangThai]) VALUES (10, N'CNTT', N'Inside the SQL Server Query Optimizer', N'Thường', N'Kỹ thuật', N'Có thể mượn')
INSERT [dbo].[TaiLieu] ([stt], [MaNguonTL], [TenTaiLieu], [LoaiTaiLieu], [LinhVuc], [TrangThai]) VALUES (11, N'VL', N'SQL Server Statistics', N'Hiếm', N'Kỹ thuật', N'Có thể mượn')
INSERT [dbo].[TaiLieu] ([stt], [MaNguonTL], [TenTaiLieu], [LoaiTaiLieu], [LinhVuc], [TrangThai]) VALUES (12, N'CNTT', N'The Red Gate Guide to SQL Server Team-based Development', N'Đặc biệt', N'Đời sống', N'Đã mượn')
INSERT [dbo].[TaiLieu] ([stt], [MaNguonTL], [TenTaiLieu], [LoaiTaiLieu], [LinhVuc], [TrangThai]) VALUES (13, N'HH', N'SQL Server Hardware', N'Thường', N'Nghệ thuật', N'Có thể mượn')
INSERT [dbo].[TaiLieu] ([stt], [MaNguonTL], [TenTaiLieu], [LoaiTaiLieu], [LinhVuc], [TrangThai]) VALUES (14, N'VL', N'Inside the SQL Server Query Optimizer', N'Hiếm', N'Kỹ thuật', N'Có thể mượn')
INSERT [dbo].[TaiLieu] ([stt], [MaNguonTL], [TenTaiLieu], [LoaiTaiLieu], [LinhVuc], [TrangThai]) VALUES (15, N'CNTT', N'SQL Server Statistics', N'Thường', N'Nghệ thuật', N'Đã mượn')
INSERT [dbo].[TaiLieu] ([stt], [MaNguonTL], [TenTaiLieu], [LoaiTaiLieu], [LinhVuc], [TrangThai]) VALUES (16, N'HH', N'The Red Gate Guide to SQL Server Team-based Development', N'Hiếm', N'Văn học', N'Có thể mượn')
INSERT [dbo].[TaiLieu] ([stt], [MaNguonTL], [TenTaiLieu], [LoaiTaiLieu], [LinhVuc], [TrangThai]) VALUES (17, N'VL', N'SQL Server Hardware', N'Hiếm', N'Khoa học', N'Có thể mượn')
INSERT [dbo].[TaiLieu] ([stt], [MaNguonTL], [TenTaiLieu], [LoaiTaiLieu], [LinhVuc], [TrangThai]) VALUES (18, N'VL', N'Inside the SQL Server Query Optimizer', N'Hiếm', N'Kỹ thuật', N'Có thể mượn')
INSERT [dbo].[TaiLieu] ([stt], [MaNguonTL], [TenTaiLieu], [LoaiTaiLieu], [LinhVuc], [TrangThai]) VALUES (19, N'HH', N'SQL Server Statistics', N'Đặc biệt', N'Kỹ thuật', N'Có thể mượn')
INSERT [dbo].[TaiLieu] ([stt], [MaNguonTL], [TenTaiLieu], [LoaiTaiLieu], [LinhVuc], [TrangThai]) VALUES (20, N'VL', N'The Red Gate Guide to SQL Server Team-based Development', N'Đặc biệt', N'Nghệ thuật', N'Có thể mượn')
INSERT [dbo].[TaiLieu] ([stt], [MaNguonTL], [TenTaiLieu], [LoaiTaiLieu], [LinhVuc], [TrangThai]) VALUES (21, N'VL', N'SQL Server Hardware', N'Hiếm', N'Nghệ thuật', N'Có thể mượn')
INSERT [dbo].[TaiLieu] ([stt], [MaNguonTL], [TenTaiLieu], [LoaiTaiLieu], [LinhVuc], [TrangThai]) VALUES (22, N'CNTT', N'Inside the SQL Server Query Optimizer', N'Đặc biệt', N'Văn học', N'Có thể mượn')
INSERT [dbo].[TaiLieu] ([stt], [MaNguonTL], [TenTaiLieu], [LoaiTaiLieu], [LinhVuc], [TrangThai]) VALUES (23, N'VL', N'SQL Server Statistics', N'Thường', N'Khoa học', N'Có thể mượn')
INSERT [dbo].[TaiLieu] ([stt], [MaNguonTL], [TenTaiLieu], [LoaiTaiLieu], [LinhVuc], [TrangThai]) VALUES (24, N'HH', N'The Red Gate Guide to SQL Server Team-based Development', N'Hiếm', N'Đời sống', N'Có thể mượn')
INSERT [dbo].[TaiLieu] ([stt], [MaNguonTL], [TenTaiLieu], [LoaiTaiLieu], [LinhVuc], [TrangThai]) VALUES (25, N'CNTT', N'SQL Server Hardware', N'Hiếm', N'Nghệ thuật', N'Có thể mượn')
INSERT [dbo].[TaiLieu] ([stt], [MaNguonTL], [TenTaiLieu], [LoaiTaiLieu], [LinhVuc], [TrangThai]) VALUES (26, N'VL', N'Inside the SQL Server Query Optimizer', N'Đặc biệt', N'Khoa học', N'Có thể mượn')
INSERT [dbo].[TaiLieu] ([stt], [MaNguonTL], [TenTaiLieu], [LoaiTaiLieu], [LinhVuc], [TrangThai]) VALUES (27, N'HH', N'SQL Server Statistics', N'Hiếm', N'Kỹ thuật', N'Có thể mượn')
INSERT [dbo].[TaiLieu] ([stt], [MaNguonTL], [TenTaiLieu], [LoaiTaiLieu], [LinhVuc], [TrangThai]) VALUES (28, N'VL', N'The Red Gate Guide to SQL Server Team-based Development', N'Đặc biệt', N'Nghệ thuật', N'Có thể mượn')
INSERT [dbo].[TaiLieu] ([stt], [MaNguonTL], [TenTaiLieu], [LoaiTaiLieu], [LinhVuc], [TrangThai]) VALUES (29, N'VL', N'SQL Server Hardware', N'Thường', N'Đời sống', N'Có thể mượn')
INSERT [dbo].[TaiLieu] ([stt], [MaNguonTL], [TenTaiLieu], [LoaiTaiLieu], [LinhVuc], [TrangThai]) VALUES (30, N'VL', N'Inside the SQL Server Query Optimizer', N'Thường', N'Nghệ thuật', N'Đã mượn')
INSERT [dbo].[TaiLieu] ([stt], [MaNguonTL], [TenTaiLieu], [LoaiTaiLieu], [LinhVuc], [TrangThai]) VALUES (31, N'CNTT', N'SQL Server Statistics', N'Thường', N'Nghệ thuật', N'Có thể mượn')
INSERT [dbo].[TaiLieu] ([stt], [MaNguonTL], [TenTaiLieu], [LoaiTaiLieu], [LinhVuc], [TrangThai]) VALUES (32, N'VL', N'The Red Gate Guide to SQL Server Team-based Development', N'Thường', N'Đời sống', N'Đã mượn')
INSERT [dbo].[TaiLieu] ([stt], [MaNguonTL], [TenTaiLieu], [LoaiTaiLieu], [LinhVuc], [TrangThai]) VALUES (33, N'VL', N'SQL Server Hardware', N'Hiếm', N'Khoa học', N'Có thể mượn')
INSERT [dbo].[TaiLieu] ([stt], [MaNguonTL], [TenTaiLieu], [LoaiTaiLieu], [LinhVuc], [TrangThai]) VALUES (34, N'HH', N'Inside the SQL Server Query Optimizer', N'Đặc biệt', N'Kỹ thuật', N'Có thể mượn')
INSERT [dbo].[TaiLieu] ([stt], [MaNguonTL], [TenTaiLieu], [LoaiTaiLieu], [LinhVuc], [TrangThai]) VALUES (35, N'CNTT', N'SQL Server Statistics', N'Thường', N'Kỹ thuật', N'Có thể mượn')
INSERT [dbo].[TaiLieu] ([stt], [MaNguonTL], [TenTaiLieu], [LoaiTaiLieu], [LinhVuc], [TrangThai]) VALUES (36, N'HH', N'The Red Gate Guide to SQL Server Team-based Development', N'Hiếm', N'Nghệ thuật', N'Có thể mượn')
INSERT [dbo].[TaiLieu] ([stt], [MaNguonTL], [TenTaiLieu], [LoaiTaiLieu], [LinhVuc], [TrangThai]) VALUES (37, N'VL', N'SQL Server Hardware', N'Đặc biệt', N'Khoa học', N'Có thể mượn')
INSERT [dbo].[TaiLieu] ([stt], [MaNguonTL], [TenTaiLieu], [LoaiTaiLieu], [LinhVuc], [TrangThai]) VALUES (38, N'CNTT', N'Inside the SQL Server Query Optimizer', N'Thường', N'Nghệ thuật', N'Có thể mượn')
INSERT [dbo].[TaiLieu] ([stt], [MaNguonTL], [TenTaiLieu], [LoaiTaiLieu], [LinhVuc], [TrangThai]) VALUES (39, N'VL', N'SQL Server Statistics', N'Hiếm', N'Nghệ thuật', N'Có thể mượn')
INSERT [dbo].[TaiLieu] ([stt], [MaNguonTL], [TenTaiLieu], [LoaiTaiLieu], [LinhVuc], [TrangThai]) VALUES (40, N'VL', N'The Red Gate Guide to SQL Server Team-based Development', N'Thường', N'Văn học', N'Có thể mượn')
INSERT [dbo].[TaiLieu] ([stt], [MaNguonTL], [TenTaiLieu], [LoaiTaiLieu], [LinhVuc], [TrangThai]) VALUES (41, N'HH', N'SQL Server Hardware', N'Thường', N'Kỹ thuật', N'Có thể mượn')
INSERT [dbo].[TaiLieu] ([stt], [MaNguonTL], [TenTaiLieu], [LoaiTaiLieu], [LinhVuc], [TrangThai]) VALUES (42, N'CNTT', N'Inside the SQL Server Query Optimizer', N'Thường', N'Văn học', N'Có thể mượn')
INSERT [dbo].[TaiLieu] ([stt], [MaNguonTL], [TenTaiLieu], [LoaiTaiLieu], [LinhVuc], [TrangThai]) VALUES (43, N'HH', N'SQL Server Statistics', N'Hiếm', N'Kỹ thuật', N'Có thể mượn')
INSERT [dbo].[TaiLieu] ([stt], [MaNguonTL], [TenTaiLieu], [LoaiTaiLieu], [LinhVuc], [TrangThai]) VALUES (44, N'VL', N'The Red Gate Guide to SQL Server Team-based Development', N'Thường', N'Văn học', N'Có thể mượn')
INSERT [dbo].[TaiLieu] ([stt], [MaNguonTL], [TenTaiLieu], [LoaiTaiLieu], [LinhVuc], [TrangThai]) VALUES (45, N'VL', N'SQL Server Hardware', N'Đặc biệt', N'Khoa học', N'Có thể mượn')
INSERT [dbo].[TaiLieu] ([stt], [MaNguonTL], [TenTaiLieu], [LoaiTaiLieu], [LinhVuc], [TrangThai]) VALUES (46, N'CNTT', N'Inside the SQL Server Query Optimizer', N'Thường', N'Kỹ thuật', N'Có thể mượn')
INSERT [dbo].[TaiLieu] ([stt], [MaNguonTL], [TenTaiLieu], [LoaiTaiLieu], [LinhVuc], [TrangThai]) VALUES (47, N'HH', N'SQL Server Statistics', N'Đặc biệt', N'Văn học', N'Có thể mượn')
INSERT [dbo].[TaiLieu] ([stt], [MaNguonTL], [TenTaiLieu], [LoaiTaiLieu], [LinhVuc], [TrangThai]) VALUES (48, N'CNTT', N'The Red Gate Guide to SQL Server Team-based Development', N'Hiếm', N'Nghệ thuật', N'Có thể mượn')
INSERT [dbo].[TaiLieu] ([stt], [MaNguonTL], [TenTaiLieu], [LoaiTaiLieu], [LinhVuc], [TrangThai]) VALUES (49, N'CNTT', N'SQL Server Hardware', N'Hiếm', N'Nghệ thuật', N'Có thể mượn')
INSERT [dbo].[TaiLieu] ([stt], [MaNguonTL], [TenTaiLieu], [LoaiTaiLieu], [LinhVuc], [TrangThai]) VALUES (50, N'CNTT', N'Inside the SQL Server Query Optimizer', N'Thường', N'Kỹ thuật', N'Có thể mượn')
INSERT [dbo].[TaiLieu] ([stt], [MaNguonTL], [TenTaiLieu], [LoaiTaiLieu], [LinhVuc], [TrangThai]) VALUES (51, N'VL', N'SQL Server Statistics', N'Đặc biệt', N'Đời sống', N'Có thể mượn')
INSERT [dbo].[TaiLieu] ([stt], [MaNguonTL], [TenTaiLieu], [LoaiTaiLieu], [LinhVuc], [TrangThai]) VALUES (52, N'VL', N'The Red Gate Guide to SQL Server Team-based Development', N'Thường', N'Văn học', N'Có thể mượn')
INSERT [dbo].[TaiLieu] ([stt], [MaNguonTL], [TenTaiLieu], [LoaiTaiLieu], [LinhVuc], [TrangThai]) VALUES (53, N'VL', N'SQL Server Hardware', N'Đặc biệt', N'Văn học', N'Có thể mượn')
INSERT [dbo].[TaiLieu] ([stt], [MaNguonTL], [TenTaiLieu], [LoaiTaiLieu], [LinhVuc], [TrangThai]) VALUES (54, N'VL', N'Inside the SQL Server Query Optimizer', N'Thường', N'Khoa học', N'Có thể mượn')
INSERT [dbo].[TaiLieu] ([stt], [MaNguonTL], [TenTaiLieu], [LoaiTaiLieu], [LinhVuc], [TrangThai]) VALUES (55, N'CNTT', N'SQL Server Statistics', N'Thường', N'Đời sống', N'Có thể mượn')
INSERT [dbo].[TaiLieu] ([stt], [MaNguonTL], [TenTaiLieu], [LoaiTaiLieu], [LinhVuc], [TrangThai]) VALUES (56, N'CNTT', N'The Red Gate Guide to SQL Server Team-based Development', N'Thường', N'Đời sống', N'Có thể mượn')
INSERT [dbo].[TaiLieu] ([stt], [MaNguonTL], [TenTaiLieu], [LoaiTaiLieu], [LinhVuc], [TrangThai]) VALUES (57, N'VL', N'SQL Server Hardware', N'Đặc biệt', N'Nghệ thuật', N'Có thể mượn')
INSERT [dbo].[TaiLieu] ([stt], [MaNguonTL], [TenTaiLieu], [LoaiTaiLieu], [LinhVuc], [TrangThai]) VALUES (58, N'CNTT', N'Inside the SQL Server Query Optimizer', N'Thường', N'Kỹ thuật', N'Có thể mượn')
INSERT [dbo].[TaiLieu] ([stt], [MaNguonTL], [TenTaiLieu], [LoaiTaiLieu], [LinhVuc], [TrangThai]) VALUES (59, N'HH', N'SQL Server Statistics', N'Thường', N'Kỹ thuật', N'Có thể mượn')
INSERT [dbo].[TaiLieu] ([stt], [MaNguonTL], [TenTaiLieu], [LoaiTaiLieu], [LinhVuc], [TrangThai]) VALUES (60, N'HH', N'The Red Gate Guide to SQL Server Team-based Development', N'Hiếm', N'Nghệ thuật', N'Có thể mượn')
INSERT [dbo].[TaiLieu] ([stt], [MaNguonTL], [TenTaiLieu], [LoaiTaiLieu], [LinhVuc], [TrangThai]) VALUES (61, NULL, N'SQL Server Hardware', N'Đặc biệt', N'Kỹ thuật', N'Có thể mượn')
INSERT [dbo].[TaiLieu] ([stt], [MaNguonTL], [TenTaiLieu], [LoaiTaiLieu], [LinhVuc], [TrangThai]) VALUES (62, N'VL', N'Inside the SQL Server Query Optimizer', N'Đặc biệt', N'Kỹ thuật', N'Có thể mượn')
INSERT [dbo].[TaiLieu] ([stt], [MaNguonTL], [TenTaiLieu], [LoaiTaiLieu], [LinhVuc], [TrangThai]) VALUES (63, N'CNTT', N'SQL Server Statistics', N'Đặc biệt', N'Khoa học', N'Có thể mượn')
INSERT [dbo].[TaiLieu] ([stt], [MaNguonTL], [TenTaiLieu], [LoaiTaiLieu], [LinhVuc], [TrangThai]) VALUES (64, N'VL', N'The Red Gate Guide to SQL Server Team-based Development', N'Hiếm', N'Nghệ thuật', N'Có thể mượn')
INSERT [dbo].[TaiLieu] ([stt], [MaNguonTL], [TenTaiLieu], [LoaiTaiLieu], [LinhVuc], [TrangThai]) VALUES (65, N'HH', N'SQL Server Hardware', N'Đặc biệt', N'Khoa học', N'Có thể mượn')
INSERT [dbo].[TaiLieu] ([stt], [MaNguonTL], [TenTaiLieu], [LoaiTaiLieu], [LinhVuc], [TrangThai]) VALUES (66, N'CNTT', N'Inside the SQL Server Query Optimizer', N'Hiếm', N'Kỹ thuật', N'Có thể mượn')
INSERT [dbo].[TaiLieu] ([stt], [MaNguonTL], [TenTaiLieu], [LoaiTaiLieu], [LinhVuc], [TrangThai]) VALUES (67, N'HH', N'SQL Server Statistics', N'Hiếm', N'Khoa học', N'Có thể mượn')
INSERT [dbo].[TaiLieu] ([stt], [MaNguonTL], [TenTaiLieu], [LoaiTaiLieu], [LinhVuc], [TrangThai]) VALUES (68, N'CNTT', N'The Red Gate Guide to SQL Server Team-based Development', N'Hiếm', N'Khoa học', N'Có thể mượn')
INSERT [dbo].[TaiLieu] ([stt], [MaNguonTL], [TenTaiLieu], [LoaiTaiLieu], [LinhVuc], [TrangThai]) VALUES (69, N'VL', N'SQL Server Hardware', N'Hiếm', N'Kỹ thuật', N'Có thể mượn')
INSERT [dbo].[TaiLieu] ([stt], [MaNguonTL], [TenTaiLieu], [LoaiTaiLieu], [LinhVuc], [TrangThai]) VALUES (70, N'CNTT', N'Inside the SQL Server Query Optimizer', N'Đặc biệt', NULL, N'Có thể mượn')
INSERT [dbo].[TaiLieu] ([stt], [MaNguonTL], [TenTaiLieu], [LoaiTaiLieu], [LinhVuc], [TrangThai]) VALUES (71, N'HH', N'SQL Server Statistics', N'Đặc biệt', N'Nghệ thuật', N'Có thể mượn')
INSERT [dbo].[TaiLieu] ([stt], [MaNguonTL], [TenTaiLieu], [LoaiTaiLieu], [LinhVuc], [TrangThai]) VALUES (72, N'CNTT', N'The Red Gate Guide to SQL Server Team-based Development', N'Đặc biệt', N'Nghệ thuật', N'Có thể mượn')
INSERT [dbo].[TaiLieu] ([stt], [MaNguonTL], [TenTaiLieu], [LoaiTaiLieu], [LinhVuc], [TrangThai]) VALUES (73, N'HH', N'SQL Server Hardware', N'Thường', N'Kỹ thuật', N'Có thể mượn')
INSERT [dbo].[TaiLieu] ([stt], [MaNguonTL], [TenTaiLieu], [LoaiTaiLieu], [LinhVuc], [TrangThai]) VALUES (74, N'VL', N'Inside the SQL Server Query Optimizer', N'Thường', N'Khoa học', N'Có thể mượn')
INSERT [dbo].[TaiLieu] ([stt], [MaNguonTL], [TenTaiLieu], [LoaiTaiLieu], [LinhVuc], [TrangThai]) VALUES (75, N'CNTT', N'SQL Server Statistics', N'Hiếm', N'Kỹ thuật', N'Có thể mượn')
INSERT [dbo].[TaiLieu] ([stt], [MaNguonTL], [TenTaiLieu], [LoaiTaiLieu], [LinhVuc], [TrangThai]) VALUES (76, N'HH', N'The Red Gate Guide to SQL Server Team-based Development', N'Thường', N'Nghệ thuật', N'Có thể mượn')
INSERT [dbo].[TaiLieu] ([stt], [MaNguonTL], [TenTaiLieu], [LoaiTaiLieu], [LinhVuc], [TrangThai]) VALUES (77, N'VL', N'SQL Server Hardware', N'Thường', N'Đời sống', N'Có thể mượn')
INSERT [dbo].[TaiLieu] ([stt], [MaNguonTL], [TenTaiLieu], [LoaiTaiLieu], [LinhVuc], [TrangThai]) VALUES (78, N'CNTT', N'Inside the SQL Server Query Optimizer', N'Thường', N'Khoa học', N'Có thể mượn')
INSERT [dbo].[TaiLieu] ([stt], [MaNguonTL], [TenTaiLieu], [LoaiTaiLieu], [LinhVuc], [TrangThai]) VALUES (79, N'VL', N'SQL Server Statistics', N'Thường', N'Khoa học', N'Có thể mượn')
INSERT [dbo].[TaiLieu] ([stt], [MaNguonTL], [TenTaiLieu], [LoaiTaiLieu], [LinhVuc], [TrangThai]) VALUES (80, N'HH', N'The Red Gate Guide to SQL Server Team-based Development', N'Thường', N'Đời sống', N'Có thể mượn')
INSERT [dbo].[TaiLieu] ([stt], [MaNguonTL], [TenTaiLieu], [LoaiTaiLieu], [LinhVuc], [TrangThai]) VALUES (81, N'VL', N'SQL Server Hardware', N'Thường', N'Khoa học', N'Có thể mượn')
INSERT [dbo].[TaiLieu] ([stt], [MaNguonTL], [TenTaiLieu], [LoaiTaiLieu], [LinhVuc], [TrangThai]) VALUES (82, N'HH', N'Inside the SQL Server Query Optimizer', N'Hiếm', N'Kỹ thuật', N'Có thể mượn')
INSERT [dbo].[TaiLieu] ([stt], [MaNguonTL], [TenTaiLieu], [LoaiTaiLieu], [LinhVuc], [TrangThai]) VALUES (83, N'VL', N'SQL Server Statistics', N'Hiếm', N'Đời sống', N'Có thể mượn')
INSERT [dbo].[TaiLieu] ([stt], [MaNguonTL], [TenTaiLieu], [LoaiTaiLieu], [LinhVuc], [TrangThai]) VALUES (84, N'HH', N'The Red Gate Guide to SQL Server Team-based Development', N'Thường', N'Văn học', N'Có thể mượn')
INSERT [dbo].[TaiLieu] ([stt], [MaNguonTL], [TenTaiLieu], [LoaiTaiLieu], [LinhVuc], [TrangThai]) VALUES (85, N'VL', N'SQL Server Hardware', N'Hiếm', N'Khoa học', N'Có thể mượn')
INSERT [dbo].[TaiLieu] ([stt], [MaNguonTL], [TenTaiLieu], [LoaiTaiLieu], [LinhVuc], [TrangThai]) VALUES (86, N'CNTT', N'Inside the SQL Server Query Optimizer', N'Đặc biệt', N'Nghệ thuật', N'Có thể mượn')
INSERT [dbo].[TaiLieu] ([stt], [MaNguonTL], [TenTaiLieu], [LoaiTaiLieu], [LinhVuc], [TrangThai]) VALUES (87, N'HH', N'SQL Server Statistics', N'Thường', N'Khoa học', N'Có thể mượn')
INSERT [dbo].[TaiLieu] ([stt], [MaNguonTL], [TenTaiLieu], [LoaiTaiLieu], [LinhVuc], [TrangThai]) VALUES (88, N'CNTT', N'The Red Gate Guide to SQL Server Team-based Development', N'Đặc biệt', N'Đời sống', N'Có thể mượn')
INSERT [dbo].[TaiLieu] ([stt], [MaNguonTL], [TenTaiLieu], [LoaiTaiLieu], [LinhVuc], [TrangThai]) VALUES (89, N'CNTT', N'SQL Server Hardware', N'Thường', N'Đời sống', N'Có thể mượn')
INSERT [dbo].[TaiLieu] ([stt], [MaNguonTL], [TenTaiLieu], [LoaiTaiLieu], [LinhVuc], [TrangThai]) VALUES (90, N'HH', N'Inside the SQL Server Query Optimizer', N'Hiếm', N'Nghệ thuật', N'Có thể mượn')
INSERT [dbo].[TaiLieu] ([stt], [MaNguonTL], [TenTaiLieu], [LoaiTaiLieu], [LinhVuc], [TrangThai]) VALUES (91, N'CNTT', N'SQL Server Statistics', N'Thường', N'Khoa học', N'Có thể mượn')
INSERT [dbo].[TaiLieu] ([stt], [MaNguonTL], [TenTaiLieu], [LoaiTaiLieu], [LinhVuc], [TrangThai]) VALUES (92, N'VL', N'The Red Gate Guide to SQL Server Team-based Development', N'Đặc biệt', N'Văn học', N'Có thể mượn')
INSERT [dbo].[TaiLieu] ([stt], [MaNguonTL], [TenTaiLieu], [LoaiTaiLieu], [LinhVuc], [TrangThai]) VALUES (93, N'VL', N'SQL Server Hardware', N'Đặc biệt', N'Kỹ thuật', N'Có thể mượn')
INSERT [dbo].[TaiLieu] ([stt], [MaNguonTL], [TenTaiLieu], [LoaiTaiLieu], [LinhVuc], [TrangThai]) VALUES (94, N'HH', N'Inside the SQL Server Query Optimizer', N'Thường', N'Văn học', N'Có thể mượn')
INSERT [dbo].[TaiLieu] ([stt], [MaNguonTL], [TenTaiLieu], [LoaiTaiLieu], [LinhVuc], [TrangThai]) VALUES (95, N'VL', N'SQL Server Statistics', N'Đặc biệt', N'Nghệ thuật', N'Có thể mượn')
INSERT [dbo].[TaiLieu] ([stt], [MaNguonTL], [TenTaiLieu], [LoaiTaiLieu], [LinhVuc], [TrangThai]) VALUES (96, N'HH', N'The Red Gate Guide to SQL Server Team-based Development', N'Thường', N'Đời sống', N'Có thể mượn')
INSERT [dbo].[TaiLieu] ([stt], [MaNguonTL], [TenTaiLieu], [LoaiTaiLieu], [LinhVuc], [TrangThai]) VALUES (97, N'VL', N'SQL Server Hardware', N'Thường', N'Đời sống', N'Có thể mượn')
INSERT [dbo].[TaiLieu] ([stt], [MaNguonTL], [TenTaiLieu], [LoaiTaiLieu], [LinhVuc], [TrangThai]) VALUES (98, N'VL', N'Inside the SQL Server Query Optimizer', N'Đặc biệt', N'Đời sống', N'Có thể mượn')
INSERT [dbo].[TaiLieu] ([stt], [MaNguonTL], [TenTaiLieu], [LoaiTaiLieu], [LinhVuc], [TrangThai]) VALUES (99, N'HH', N'SQL Server Statistics', N'Thường', N'Nghệ thuật', N'Có thể mượn')
GO
INSERT [dbo].[TaiLieu] ([stt], [MaNguonTL], [TenTaiLieu], [LoaiTaiLieu], [LinhVuc], [TrangThai]) VALUES (100, N'VL', N'The Red Gate Guide to SQL Server Team-based Development', N'Thường', N'Đời sống', N'Có thể mượn')
SET IDENTITY_INSERT [dbo].[TaiLieu] OFF



INSERT INTO TaiKhoan
VALUES ('Admin', '1a2d5a5793a51b81a0aa8964a15785b4', 'Admin', N'Quản trị')
GO

CREATE PROC XemTatCaDG
AS
  BEGIN
	SELECT	MaDocGia, HoTen, CMND, Loai
	FROM	DocGia;
  END;
GO

CREATE PROC SoSachMuon
  @madg VARCHAR(6), @kq INT OUTPUT
AS
  BEGIN
	SELECT	@kq = COUNT (*)
	FROM	DocGia dg, PhieuMuon pm, CTPhieuMuon ct
	WHERE	dg.MaDocGia = @madg
			AND dg.MaDocGia = pm.MaDocGia
			AND ct.MaPM = pm.MaPM
			AND ct.TrangThai = N'Chưa trả';
  END;
GO

CREATE PROC SoSachQuaHan
  @madg VARCHAR(6), @kq INT OUTPUT
AS
  BEGIN
	SELECT	@kq = COUNT (*)
	FROM	DocGia dg, PhieuMuon pm, CTPhieuMuon ct
	WHERE	dg.MaDocGia = @madg
			AND dg.MaDocGia = pm.MaDocGia
			AND ct.MaPM = pm.MaPM
			AND ct.TrangThai = N'Chưa trả'
			AND ct.HanChotTra < GETDATE ();
  END;
GO

CREATE TRIGGER ThemPhieuPhat
ON PhieuCanhCao
FOR INSERT
AS
BEGIN
	DECLARE cs_CC CURSOR FOR SELECT MaDocGia
							 FROM INSERTED
	OPEN cs_CC
	DECLARE @madg VARCHAR(6)
	DECLARE @temp INT, @flag INT
	SET @temp = 0
	FETCH NEXT FROM cs_CC INTO @madg
	WHILE(@@FETCH_STATUS = 0)
	BEGIN
		IF((SELECT COUNT(*) FROM PhieuCanhCao WHERE MaDocGia = @madg) = 3)
		BEGIN
			SET @flag = 1
			SELECT @temp = MIN(stt) FROM PhieuCanhCao WHERE MaDocGia = @madg
			
			INSERT INTO PhieuPhat
			VALUES(@madg)
			
			DELETE PhieuCanhCao
			WHERE MaDocGia = @madg
		END
		FETCH NEXT FROM cs_CC INTO @madg
	END
	CLOSE cs_CC
	DEALLOCATE cs_CC
	
	IF(@flag = 1)
	BEGIN
		SET @temp = @temp - 1
		DBCC CHECKIDENT('PhieuCanhCao', RESEED, @temp);
	END
END
GO


CREATE TRIGGER XoaDG
ON DocGia
INSTEAD OF DELETE
AS
  BEGIN
	IF EXISTS (
				SELECT	*
				FROM	Deleted d, PhieuMuon pm
				WHERE	d.MaDocGia = pm.MaDocGia
			  )
	  BEGIN
		DELETE	PhieuMuon
		WHERE	MaDocGia = (
							 SELECT d.MaDocGia FROM Deleted d
						   );
	  END;
	IF EXISTS (
				SELECT	*
				FROM	Deleted d, PhieuPhat pp
				WHERE	d.MaDocGia = pp.MaDocGia
			  )
	  Begin
		DELETE	PhieuPhat
		WHERE	MaDocGia = (
							 SELECT d.MaDocGia FROM Deleted d
						   );
	  End;
	  IF EXISTS (
				SELECT	*
				FROM	Deleted d, PhieuCanhCao pcc
				WHERE	d.MaDocGia = pcc.MaDocGia
			  )
	  Begin
		Delete PhieuCanhCao
		Where MaDocGia = (
							 SELECT d.MaDocGia FROM Deleted d
					     );
	  End;

	DELETE	DocGia
	WHERE	MaDocGia = (
						 SELECT d.MaDocGia FROM Deleted d
					   );
  END;
GO

CREATE TRIGGER XoaTL
ON TaiLieu
INSTEAD OF DELETE
AS
  BEGIN
	IF EXISTS (
				SELECT	*
				FROM	Deleted d, CTPhieuTra ct
				WHERE	d.MaTaiLieu = ct.MaTaiLieu
			  )
	  BEGIN
		DELETE	CTPhieuTra
		WHERE	MaTaiLieu = (
							 SELECT d.MaTaiLieu FROM Deleted d
						   );
	  END;
	  IF EXISTS (
				SELECT	*
				FROM	Deleted d, CTPhieuMuon ct
				WHERE	d.MaTaiLieu = ct.MaTaiLieu
			  )
	  BEGIN
		DELETE	CTPhieuMuon
		WHERE	MaTaiLieu = (
							 SELECT d.MaTaiLieu FROM Deleted d
						   );
	  END;

	DELETE	TaiLieu
	WHERE	MaTaiLieu = (
						 SELECT d.MaTaiLieu FROM Deleted d
					   );
  END;
GO


CREATE PROC XemTatCaTL
AS
  BEGIN
	SELECT	MaTaiLieu, TenTaiLieu, LoaiTaiLieu
	FROM	TaiLieu;
  END;
GO

CREATE PROC XemTatCaPM
AS
  BEGIN
	SELECT DISTINCT
			pm.MaPM, pm.MaDocGia
	FROM	PhieuMuon pm, CTPhieuMuon ct
	WHERE	ct.MaPM = pm.MaPM
			AND ct.TrangThai = N'Chưa trả';
  END;
GO

CREATE PROC XemTatCaPMDaTra
AS
  BEGIN
	SELECT DISTINCT
			pm.MaPM, pm.MaDocGia
	FROM	PhieuMuon pm, CTPhieuMuon ct
	WHERE	ct.MaPM = pm.MaPM
			AND ct.TrangThai = N'Đã trả';
  END;
GO

CREATE PROC XemTatCaPT
AS
  BEGIN
	SELECT DISTINCT
			MaPT
	FROM	PhieuTra
  END;
GO

CREATE PROC XemTatCaPMQH
AS
  BEGIN
	SELECT DISTINCT
			pm.MaPM, pm.MaDocGia
	FROM	PhieuMuon pm, CTPhieuMuon ct
	WHERE	ct.MaPM = pm.MaPM
			AND ct.TrangThai = N'Chưa trả'
			AND ct.HanChotTra < GETDATE ();
  END;
GO

CREATE PROC ChinhSuaDG
  @madg	  VARCHAR(6), @cmnd VARCHAR(10), @hoten NVARCHAR(50), @sdt VARCHAR(10), @email VARCHAR(50), @ngaysinh DATE,
  @diachi NVARCHAR(50), @loai NVARCHAR(50), @maso VARCHAR(10)
AS
  BEGIN
	UPDATE	DocGia
	SET		CMND = @cmnd, HoTen = @hoten, Sdt = @sdt, Email = @email, NgaySinh = @ngaysinh, DiaChi = @diachi, Loai = @loai,
			MaSo = @maso
	WHERE	MaDocGia = @madg;
  END;
GO

CREATE PROC ChinhSuaTL
  @matl VARCHAR(10), @tentl NVARCHAR(50), @loaitl NVARCHAR(50), @trangthai NVARCHAR(50)
AS
  BEGIN
	UPDATE	TaiLieu
	SET		TenTaiLieu = @tentl, LoaiTaiLieu = @loaitl, TrangThai = @trangthai
	WHERE	MaTaiLieu = @matl;
  END;
GO

CREATE PROC ChinhSuaPM
  @mapm VARCHAR(10), @ngaylap DATE, @hanchot DATE, @matl VARCHAR(12), @trangthai NVARCHAR(50)
AS
  BEGIN
	UPDATE	CTPhieuMuon
	SET		NgayLapPM = @ngaylap, HanChotTra = @hanchot, TrangThai = @trangthai
	WHERE	MaPM = @mapm
			AND MaTaiLieu = @matl;
  END;
GO

Create trigger SuaPM
On CTPhieuMuon
for Update
as
	Begin
		if(exists (Select* from Inserted Where TrangThai = N'Chưa trả'))
		Begin
			Update TaiLieu
			Set TrangThai = N'Đã mượn'
			Where MaTaiLieu = (Select MaTaiLieu From Inserted)
		End
		else
		Begin
			Update TaiLieu
			Set TrangThai = N'Có thể mượn'
			Where MaTaiLieu = (Select MaTaiLieu From Inserted)
		End
	End
go


CREATE TRIGGER XoaCTPM
ON PhieuMuon
INSTEAD OF DELETE
AS
  BEGIN
	DECLARE cs_i CURSOR FOR
	  SELECT	d.MaPM
	  FROM		Deleted d;
	OPEN cs_i;
	DECLARE cs_j CURSOR FOR
	  SELECT	MaPT, MaPM
	  FROM		CTPhieuTra;
	OPEN cs_j;
	DECLARE @mapm VARCHAR(10), @mapt VARCHAR(10), @ctpt_mapm VARCHAR(10);
	FETCH NEXT FROM cs_i
	INTO @mapm;
	WHILE (@@FETCH_STATUS = 0)
	  BEGIN

		FETCH NEXT FROM cs_j
		INTO @mapt, @ctpt_mapm;
		WHILE (@@FETCH_STATUS = 0)
		  BEGIN
			IF (@mapm = @ctpt_mapm)
			  BEGIN
				DELETE	PhieuTra
				WHERE	MaPT = @mapt;
			  END;
			FETCH NEXT FROM cs_j
			INTO @mapt, @ctpt_mapm;
		  END;
		FETCH NEXT FROM cs_i
		INTO @mapm;
	  END;
	CLOSE cs_i;
	CLOSE cs_j;
	DEALLOCATE cs_i;
	DEALLOCATE cs_j;


	DECLARE cs_k CURSOR FOR
	  SELECT	d.MaPM
	  FROM		Deleted d;
	OPEN cs_k;
	DECLARE @ctmapm VARCHAR(10);
	FETCH NEXT FROM cs_k
	INTO @ctmapm;
	WHILE (@@FETCH_STATUS = 0)
	  BEGIN
		DECLARE cs_l CURSOR FOR
		  SELECT	MaTaiLieu
		  FROM		CTPhieuMuon
		  WHERE		MaPM = @ctmapm;
		OPEN cs_l;
		DECLARE @matl VARCHAR(12);
		FETCH NEXT FROM cs_l
		INTO @matl;
		WHILE (@@FETCH_STATUS = 0)
		  BEGIN
			UPDATE	TaiLieu
			SET		TrangThai = N'Có thể mượn'
			WHERE	MaTaiLieu = @matl;
			FETCH NEXT FROM cs_l
			INTO @matl;
		  END;
		CLOSE cs_l;
		DEALLOCATE cs_l;

		DELETE	CTPhieuMuon
		WHERE	MaPM = @ctmapm;

		DELETE	PhieuMuon
		WHERE	MaPM = @ctmapm;
		FETCH NEXT FROM cs_k
		INTO @ctmapm;
	  END;
	CLOSE cs_k;
	DEALLOCATE cs_k;
  END;
GO


CREATE PROC ChinhSuaPT
  @mapt VARCHAR(10), @mapm VARCHAR(10), @matl VARCHAR(12), @ngaylap DATE
AS
  BEGIN
	UPDATE	CTPhieuTra
	SET		NgayLapPT = @ngaylap
	WHERE	MaPT = @mapt
			AND MaTaiLieu = @matl
			AND MaPM = @mapm;
  END;
GO

CREATE TRIGGER XoaCTPT
ON PhieuTra
INSTEAD OF DELETE
AS
  BEGIN
	DECLARE cs_m CURSOR FOR
	  SELECT	d.MaPT
	  FROM		Deleted d;
	OPEN cs_m;
	DECLARE @mapt VARCHAR(10);
	FETCH NEXT FROM cs_m
	INTO @mapt;
	WHILE (@@FETCH_STATUS = 0)
	  BEGIN
		DELETE	CTPhieuTra
		WHERE	MaPT = @mapt;

		DELETE	PhieuTra
		WHERE	MaPT = @mapt;

		FETCH NEXT FROM cs_m
		INTO @mapt;
	  END;
	CLOSE cs_m;
	DEALLOCATE cs_m;
  END;
GO

CREATE TRIGGER reseedSTT_DG
ON DocGia
INSTEAD OF INSERT
AS
  BEGIN
	DECLARE @temp INT, @seed INT, @flag INT;
	SET @temp = 1;
	SET @flag = 0;
	WHILE (@temp <= (SELECT MAX (stt) FROM DocGia))
	  BEGIN
		SET @flag = 1;
		IF EXISTS (SELECT * FROM DocGia WHERE stt = @temp)
		  SET @temp = @temp + 1;
		ELSE
		  BEGIN
			SET @seed = @temp - 1;
			DBCC CHECKIDENT('DocGia', RESEED, @seed);
			BREAK;
		  END;
	  END;
	IF (@flag = 0)
	  DBCC CHECKIDENT('DocGia', RESEED, 0);
	INSERT INTO DocGia (CMND, HoTen, Sdt, Email, NgaySinh, DiaChi, Loai, MaSo, NgayDangKi)
	SELECT	CMND, HoTen, Sdt, Email, NgaySinh, DiaChi, Loai, MaSo, NgayDangKi
	FROM	inserted;

	SET IDENTITY_INSERT DocGia ON;

	SELECT	@seed = MAX (stt)
	FROM	DocGia;
	DBCC CHECKIDENT('DocGia', RESEED, @seed);
  END;
GO

CREATE TRIGGER reseedSTT_TL
ON TaiLieu
INSTEAD OF INSERT
AS
  BEGIN
	DECLARE @temp INT, @seed INT, @flag INT;
	SET @temp = 1;
	SET @flag = 0;
	WHILE (@temp <= (SELECT MAX (stt) FROM TaiLieu))
	  BEGIN
		SET @flag = 1;
		IF EXISTS (SELECT * FROM TaiLieu WHERE stt = @temp)
		  SET @temp = @temp + 1;
		ELSE
		  BEGIN
			SET @seed = @temp - 1;
			DBCC CHECKIDENT('TaiLieu', RESEED, @seed);
			BREAK;
		  END;
	  END;
	IF (@flag = 0)
	  DBCC CHECKIDENT('TaiLieu', RESEED, 0);
	INSERT INTO TaiLieu (MaNguonTL, TenTaiLieu, LoaiTaiLieu, LinhVuc, TrangThai)
	SELECT	MaNguonTL, TenTaiLieu, LoaiTaiLieu, LinhVuc, TrangThai
	FROM	inserted;

	SET IDENTITY_INSERT TaiLieu ON;

	SELECT	@seed = MAX (stt)
	FROM	TaiLieu;
	DBCC CHECKIDENT('TaiLieu', RESEED, @seed);
  END;
GO

CREATE TRIGGER reseedSTT_PM
ON PhieuMuon
INSTEAD OF INSERT
AS
  BEGIN
	DECLARE @temp INT, @seed INT, @flag INT;
	SET @temp = 1;
	SET @flag = 0;
	WHILE (@temp <= (SELECT MAX (stt) FROM PhieuMuon))
	  BEGIN
		SET @flag = 1;
		IF EXISTS (SELECT * FROM PhieuMuon WHERE stt = @temp)
		  SET @temp = @temp + 1;
		ELSE
		  BEGIN
			SET @seed = @temp - 1;
			DBCC CHECKIDENT('PhieuMuon', RESEED, @seed);
			BREAK;
		  END;
	  END;
	IF (@flag = 0)
	  DBCC CHECKIDENT('PhieuMuon', RESEED, 0);
	INSERT INTO PhieuMuon (MaDocGia)
	SELECT	MaDocGia
	FROM	inserted;

	SET IDENTITY_INSERT PhieuMuon ON;

	SELECT	@seed = MAX (stt)
	FROM	PhieuMuon;
	DBCC CHECKIDENT('PhieuMuon', RESEED, @seed);
  END;
GO

CREATE TRIGGER reseedSTT_PT
ON PhieuTra
INSTEAD OF INSERT
AS
  BEGIN
	DECLARE @temp INT, @seed INT, @flag INT;
	SET @temp = 1;
	SET @flag = 0;
	WHILE (@temp <= (SELECT MAX (stt) FROM PhieuTra))
	  BEGIN
		SET @flag = 1;
		IF EXISTS (SELECT * FROM PhieuTra WHERE stt = @temp)
		  SET @temp = @temp + 1;
		ELSE
		  BEGIN
			SET @seed = @temp - 1;
			DBCC CHECKIDENT('PhieuTra', RESEED, @seed);
			BREAK;
		  END;
	  END;
	IF (@flag = 0)
	  DBCC CHECKIDENT('PhieuTra', RESEED, 0);
	INSERT INTO PhieuTra
	DEFAULT VALUES;

	SET IDENTITY_INSERT PhieuTra ON;

	SELECT	@seed = MAX (stt)
	FROM	PhieuTra;
	DBCC CHECKIDENT('PhieuTra', RESEED, @seed);
  END;
GO

Create trigger ThemTLYeuCau
On TaiLieu
For Insert
as
Begin
	if(exists (Select* from TLYeuCau tl, Inserted i Where tl.TenTL = i.TenTaiLieu))
	Begin
		Delete TLYeuCau
		Where TenTL = (Select TenTaiLieu from Inserted)
	End
End
go

--Insert PhieuMuon

INSERT dbo.PhieuMuon(MaDocGia) VALUES ('DG0001')
INSERT CTPhieuMuon VALUES('PM0000', '2017-11-30', '2017-12-05', 'TL0015', N'Chưa trả')

INSERT dbo.PhieuMuon(MaDocGia) VALUES ('DG0002')
INSERT CTPhieuMuon VALUES('PM0001', '2017-11-29', '2017-12-04', 'TL0005', N'Chưa trả')

INSERT dbo.PhieuMuon(MaDocGia) VALUES ('DG0005')
INSERT CTPhieuMuon VALUES('PM0002', '2017-12-05', '2017-12-10', 'TL0012', N'Chưa trả')

INSERT dbo.PhieuMuon(MaDocGia) VALUES ('DG0014')
INSERT CTPhieuMuon VALUES('PM0003', '2017-11-30', '2017-12-05', 'TL0032', N'Chưa trả')

INSERT dbo.PhieuMuon(MaDocGia) VALUES ('DG0024')
INSERT CTPhieuMuon VALUES('PM0004', '2017-11-03', '2017-11-08', 'TL0030', N'Chưa trả')

--------------------